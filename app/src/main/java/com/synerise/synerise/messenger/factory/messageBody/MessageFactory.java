package com.synerise.synerise.messenger.factory.messageBody;

import android.content.Context;
import android.util.Log;

import com.synerise.synerise.messenger.model.MessengerMessage;
import com.synerise.synerise.messenger.model.messageBody.MessageBody;

import org.json.JSONArray;
import org.json.JSONException;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class MessageFactory {

    private Context context;
    private MessageBodyFactory messageBodyFactory;

    SimpleDateFormat fullFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm");
    SimpleDateFormat shortFormat = new SimpleDateFormat("hh:mm a");

    public MessageFactory(Context context) {
        this.context = context;
        messageBodyFactory = new AllInOneMessageBodyFactory(context);
    }


    public ArrayList<MessengerMessage> parseMessages(JSONArray jsonArray) {
        ArrayList<MessengerMessage> messages = new ArrayList<>();
        try {
            for (int i = 0; i < jsonArray.length(); i++) {
                MessengerMessage message = new MessengerMessage(jsonArray.getJSONObject(i));
                MessageBody messageBody = messageBodyFactory.getMessageBody(message);
                message.setMessageBody(messageBody);
                message.setAvatarPrintable(isClientAvatarPrinted(i, messages));
                message.setFormattedTime(formatDate(message.getMessageTime()));
                messages.add(message);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return messages;
    }

    private boolean isClientAvatarPrinted(int position, ArrayList<MessengerMessage> messages) {
        if (position == 0) {
            Log.v("Message return ", "true" );
            return true;
        }

        if (position > 0 && position <= messages.size()
                && messages.get(position - 1).getAuthor().getType().compareToIgnoreCase("CLIENT") != 0) {
            return  true;
        }
        return false;
    }

    private String formatDate(Date date){
        Calendar c = Calendar.getInstance();
        c.setTime(new Date());
        c.add(Calendar.DATE, -1);
        SimpleDateFormat f = fullFormat;
        if (date != null) {
            if (date.after(c.getTime())) {
                f = shortFormat;
            }
        }

        return f.format(date);
    }
}

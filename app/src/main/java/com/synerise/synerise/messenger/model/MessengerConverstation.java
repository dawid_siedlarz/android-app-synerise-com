package com.synerise.synerise.messenger.model;

import java.util.ArrayList;

public class MessengerConverstation {
    MessengerClient client;
    ArrayList<MessengerMessage> messages;

    public MessengerClient getClient() {
        return client;
    }

    public void setClient(MessengerClient client) {
        this.client = client;
    }

    public ArrayList<MessengerMessage> getMessages() {
        return messages;
    }

    public void setMessages(ArrayList<MessengerMessage> messages) {
        this.messages = messages;
    }
}

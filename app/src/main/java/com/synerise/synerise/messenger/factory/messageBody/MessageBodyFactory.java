package com.synerise.synerise.messenger.factory.messageBody;

import com.synerise.synerise.messenger.model.MessengerMessage;
import com.synerise.synerise.messenger.model.messageBody.MessageBody;

public interface MessageBodyFactory {
    MessageBody getMessageBody(MessengerMessage messengerMessage);
}

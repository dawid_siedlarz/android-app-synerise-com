package com.synerise.synerise.app.fragment;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.text.style.TypefaceSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.Switch;

import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.synerise.sdk.tracking.Tracker;
import com.synerise.sdk.tracking.TrackerParams;
import com.synerise.synerise.MainActivity;
import com.synerise.synerise.R;
import com.synerise.synerise.SyneriseOnFragmentInteractionListener;
import com.synerise.synerise.app.ActionBarConfigurableFragment;
import com.synerise.synerise.app.adapter.EventRecyclerViewAdapter;
import com.synerise.synerise.app.factory.EventFactory;
import com.synerise.synerise.app.model.Event;
import com.synerise.synerise.app.syneutil.SyneHttpCacheHandler;
import com.synerise.synerise.app.syneutil.SyneValueFormatter;
import com.synerise.synerise.app.syneview.SyneChartTitle;

import org.apache.http.client.methods.HttpGet;
import org.json.JSONArray;
import org.json.JSONException;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;


public class LiveStreamFragment extends ActionBarConfigurableFragment {

    private SyneriseOnFragmentInteractionListener mListener;
    private EventRecyclerViewAdapter adapter;

    PieChart usersLivePieChart;
    PieChart recognizedContactsLivePieChart;
    PieChart recognizedContactsLast24PieChart;

    SyneChartTitle usersLiveTitle;
    SyneChartTitle recognizedContactsLiveTitle;
    SyneChartTitle recognizedContactsLast24Title;


    PieChartSelectedValueListener usersLivePieChartListener;
    PieChartSelectedValueListener recognizedContactsLivePieChartListener;
    PieChartSelectedValueListener recognizedContactsLast24PieChartListener;

    private boolean isUserLiveInitialized = false;
    private boolean isRecognizedContectsLiveInitialized = false;
    private boolean isRecognizedContactsLast24Initialized = false;
    private boolean isGetEventsInitialized = false;

    boolean isScreenActive = false;

    Typeface tf;

    private ArrayList<Event> events;
    private EventFactory eventFactory;


    String transparency = "CC";

    private void initEventsList() {
        View rootView = getView();
        if (rootView == null) {
            return;
        }
        eventFactory = new EventFactory(getContext());
        RecyclerView eventsList = rootView.findViewById(R.id.eventListView);
        events = new ArrayList<>();
        adapter = new EventRecyclerViewAdapter(getActivity(), events);
        eventsList.setNestedScrollingEnabled(false);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        eventsList.setLayoutManager(linearLayoutManager);
        eventsList.setAdapter(adapter);
    }


    public LiveStreamFragment() {
    }

    public static LiveStreamFragment newInstance() {
        return new LiveStreamFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_live_stream, container, false);
    }

    @Override
    public void onResume() {
        super.onResume();
        isScreenActive = true;
        getCharts();
        getEvents();
    }

    @Override
    public void onPause() {
        super.onPause();
        isScreenActive = false;
    }

    @Override
    public void onStop() {
        super.onStop();
        isScreenActive = false;
    }

    @Override
    public void setUpActionBar() {
        ActionBar actionBar = (ActionBar) ((AppCompatActivity) getActivity()).getSupportActionBar();
        if (actionBar != null) {
            actionBar.show();
            actionBar.setTitle(MainActivity.businessProfileName != null ? MainActivity.businessProfileName : "Synerise");
            actionBar.setSubtitle("Live Stream");
            actionBar.setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.colorPrimary)));
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                Window window = getActivity().getWindow();
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                window.setStatusBarColor(getResources().getColor(R.color.colorPrimary));
            }
            getActivity().setTheme(R.style.AppThemeDashboard);
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Tracker.trackScreen("Live stream", new TrackerParams[]{});
        initEventsList();

        View rootView = getView();
        if (rootView == null) {
            return;
        }

        final ImageButton button = (ImageButton) rootView.findViewById(R.id.refreshTrafficButton);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new GetEventList().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                /*Animation animation = AnimationUtils.loadAnimation(getContext(), R.anim.rotate);  animation.setRepeatCount(-1);;               animation.setDuration(1000);                 button.startAnimation(animation); */
            }
        });

        Switch autoload = (Switch) rootView.findViewById(R.id.autoloadTrafficSwitch);
        autoload.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    new GetEventList().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                }
            }
        });

        Activity activity = getActivity();

        tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/Lato-Thin.ttf");

        usersLiveTitle = (SyneChartTitle) rootView.findViewById(R.id.usersLiveTitle);
        recognizedContactsLiveTitle = (SyneChartTitle) rootView.findViewById(R.id.recognizedContactsLiveTitle);
        recognizedContactsLast24Title = (SyneChartTitle) rootView.findViewById(R.id.recognizedContactsLast24Title);

        usersLivePieChart = (PieChart) rootView.findViewById(R.id.allUsersLive);
        recognizedContactsLivePieChart = (PieChart) rootView.findViewById(R.id.recognizedContactsLive);
        recognizedContactsLast24PieChart = (PieChart) rootView.findViewById(R.id.recognizedContactsLast24);
    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof SyneriseOnFragmentInteractionListener) {
            mListener = (SyneriseOnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    private void getCharts() {

        new GetUsersLive().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        new GetRecognizedContactsLive().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        new GetRecognizedContactsLast24().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

    }

    private void getEvents() {
        new GetEventList().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

    }


    private class GetUsersLive extends AsyncTask<Void, Void, String> {
        protected String doInBackground(Void... params) {
            try {

                HttpGet httpget = new HttpGet("https://api.synerise.com/rtom/stats/backend/users-channel?period=300");

                SyneHttpCacheHandler handler = new SyneHttpCacheHandler();
                boolean cacheUsed = false;
                if (!isUserLiveInitialized) {
                    cacheUsed = MainActivity.cacheString.contains(SyneHttpCacheHandler.getCacheKey(httpget));
                }
                final String body = handler.executeWithCache(httpget, !isUserLiveInitialized, true);

                if (body == null) {
                    return "not ok";
                }
                JSONArray jsonArray = null;

                try {
                    jsonArray = new JSONArray(body);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                final JSONArray finalJsonArray = jsonArray;
                final Activity activity = getActivity();
                if (activity != null) {
                    List<PieEntry> entries = new ArrayList<>();
                    int counter = 0;
                    int nonZeroValues = 0;
                    int mobileApp = 0;
                    int desktop = 0;
                    int mobileSite = 0;
                    int[] colors = new int[3];

                    if (!isUserLiveInitialized) {
                        try {
                            mobileApp = finalJsonArray.getJSONObject(1).getInt("value");
                            desktop = finalJsonArray.getJSONObject(2).getInt("value");
                            mobileSite = finalJsonArray.getJSONObject(3).getInt("value");
                            if (mobileApp > 0 || true) {
                                entries.add(new PieEntry(mobileApp, "Mobile App", 0));
                                colors[nonZeroValues] = Color.parseColor("#" + transparency + "FE3867");
                                nonZeroValues++;
                            }
                            if (desktop > 0 || true) {
                                entries.add(new PieEntry(desktop, "Desktop", 1));
                                colors[nonZeroValues] = Color.parseColor("#" + transparency + "3DD292");
                                nonZeroValues++;
                            }
                            if (mobileSite > 0 || true) {
                                entries.add(new PieEntry(mobileSite, "Mobile Site", 2));

                                colors[nonZeroValues] = Color.parseColor("#" + transparency + "02B1F1");
                                nonZeroValues++;
                            }

                            counter = mobileApp + desktop + mobileSite;

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        PieDataSet set = new PieDataSet(entries, "");

                        int maxIndex = 0;
                        float maxValue = 0;
                        for (int i = 0; i < set.getEntryCount(); i++) {
                            if (maxValue < set.getEntryForIndex(i).getValue()) {
                                maxValue = set.getEntryForIndex(i).getValue();
                                maxIndex = i;
                            }
                        }
                        set.setColors(colors);

                        set.setValueLinePart1OffsetPercentage(100.0f);
                        set.setValueLinePart1Length(0.15f);
                        set.setValueLinePart2Length(0.0f);

                        set.setValueLineColor(Color.parseColor("#00000000"));

                        set.setYValuePosition(PieDataSet.ValuePosition.OUTSIDE_SLICE);

                        final PieData data = new PieData(set);


                        data.setValueFormatter(new SyneValueFormatter(0));
                        data.setValueTextSize(14f);
                        data.setValueTextColor(Color.BLACK);


                        data.setValueTypeface(tf);

                        final int finalCounter = counter;
                        final int finalMaxIndex = maxIndex;
                        final boolean finalCacheUsed = cacheUsed;
                        activity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {

                                usersLiveTitle.setIntCounter(finalCounter);
                                usersLivePieChart.setData(data);

                                usersLivePieChartListener = new PieChartSelectedValueListener(usersLivePieChart);
                                usersLivePieChart.setOnChartValueSelectedListener(usersLivePieChartListener);

                                setUpChart(usersLivePieChart, finalCacheUsed);

                                usersLivePieChart.invalidate(); // refresh
                                Highlight h = new Highlight(finalMaxIndex, 0, 0);
                                usersLivePieChart.highlightValue(h);
                                drawCenter(usersLivePieChart, h);
                                isUserLiveInitialized = true;

                                if (finalCacheUsed) {
                                    usersLivePieChart.setAlpha(0.4f);
                                }

                                View rootView = getView();
                                if (rootView == null) {
                                    return;
                                }

                                View mobileAppLegend = rootView.findViewById(R.id.allUsersLiveMobileAppLegend);
                                mobileAppLegend.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        Highlight h = new Highlight(0, 0, 0);
                                        usersLivePieChart.highlightValue(h);
                                        drawCenter(usersLivePieChart, h);
                                    }
                                });

                                View destkopLegend = rootView.findViewById(R.id.allUsersLiveDesktopLegend);
                                destkopLegend.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        Highlight h = new Highlight(1, 0, 0);
                                        usersLivePieChart.highlightValue(h);
                                        drawCenter(usersLivePieChart, h);
                                    }
                                });

                                View mobileSiteLegend = rootView.findViewById(R.id.allUsersLiveMobileSiteLegend);
                                mobileSiteLegend.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        Highlight h = new Highlight(2, 0, 0);
                                        usersLivePieChart.highlightValue(h);
                                        drawCenter(usersLivePieChart, h);
                                    }
                                });

                            }
                        });
                    } else {
                        try {
                            mobileApp = finalJsonArray.getJSONObject(1).getInt("value");
                            desktop = finalJsonArray.getJSONObject(2).getInt("value");
                            mobileSite = finalJsonArray.getJSONObject(3).getInt("value");
                            final int finalDesktop = desktop;
                            final int finalMobileApp = mobileApp;
                            final int finalMobileSite = mobileSite;
                            counter = mobileApp + desktop + mobileSite;


                            final int finalCounter1 = counter;
                            activity.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    usersLiveTitle.setIntCounter(finalCounter1);

                                    usersLivePieChart.getData().getDataSet().getEntryForIndex(0).setY(finalMobileApp);
                                    usersLivePieChart.getData().getDataSet().getEntryForIndex(1).setY(finalDesktop);
                                    usersLivePieChart.getData().getDataSet().getEntryForIndex(2).setY(finalMobileSite);
                                    usersLivePieChart.setAlpha(1f);
                                    usersLivePieChart.notifyDataSetChanged();

                                    Highlight[] highlights = usersLivePieChart.getHighlighted();
                                    if (highlights != null && highlights.length > 0) {
                                        drawCenter(usersLivePieChart, highlights[0]);
                                    }

                                }
                            });
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                }
            } catch (NullPointerException e) {

            } catch (IllegalStateException e) {

            }

            return "ok";
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            Handler myHandler = new Handler();
            myHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (couldDownloadData()) {
                        new GetUsersLive().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                    }
                }
            }, 3000);

        }
    }


    private class GetRecognizedContactsLive extends AsyncTask<Void, Void, String> {
        protected String doInBackground(Void... params) {
            try {

                HttpGet httpget = new HttpGet("https://api.synerise.com/rtom/stats/backend/users-channel-monitored?period=300");

                SyneHttpCacheHandler handler = new SyneHttpCacheHandler();
                boolean cacheUsed = false;
                if (!isRecognizedContectsLiveInitialized) {
                    cacheUsed = MainActivity.cacheString.contains(SyneHttpCacheHandler.getCacheKey(httpget));
                }
                final String body = handler.executeWithCache(httpget, !isRecognizedContectsLiveInitialized, true);

                if (body == null) {
                    return "not ok";
                }
                JSONArray jsonArray = null;

                try {
                    jsonArray = new JSONArray(body);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                final JSONArray finalJsonArray = jsonArray;
                final Activity activity = getActivity();
                if (activity != null) {
                    List<PieEntry> entries = new ArrayList<>();
                    int counter = 0;
                    int nonZeroValues = 0;
                    int mobileApp = 0;
                    int desktop = 0;
                    int mobileSite = 0;
                    int[] colors = new int[3];

                    if (!isRecognizedContectsLiveInitialized) {
                        try {
                            mobileApp = finalJsonArray.getJSONObject(1).getInt("value");
                            desktop = finalJsonArray.getJSONObject(2).getInt("value");
                            mobileSite = finalJsonArray.getJSONObject(3).getInt("value");
                            if (mobileApp > 0 || true) {
                                entries.add(new PieEntry(mobileApp, "Mobile App", 0));
                                colors[nonZeroValues] = Color.parseColor("#" + transparency + "FE3867");
                                nonZeroValues++;
                            }
                            if (desktop > 0 || true) {
                                entries.add(new PieEntry(desktop, "Desktop", 1));
                                colors[nonZeroValues] = Color.parseColor("#" + transparency + "3DD292");
                                nonZeroValues++;
                            }
                            if (mobileSite > 0 || true) {
                                entries.add(new PieEntry(mobileSite, "Mobile Site", 2));

                                colors[nonZeroValues] = Color.parseColor("#" + transparency + "02B1F1");
                                nonZeroValues++;
                            }

                            counter = mobileApp + desktop + mobileSite;

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        PieDataSet set = new PieDataSet(entries, "");

                        set.setColors(colors);

                        set.setValueLinePart1OffsetPercentage(100.0f);
                        set.setValueLinePart1Length(0.15f);
                        set.setValueLinePart2Length(0.0f);

                        int maxIndex = 0;
                        float maxValue = 0;
                        for (int i = 0; i < set.getEntryCount(); i++) {
                            if (maxValue < set.getEntryForIndex(i).getValue()) {
                                maxValue = set.getEntryForIndex(i).getValue();
                                maxIndex = i;
                            }
                        }

                        set.setValueLineColor(Color.parseColor("#00000000"));

                        set.setYValuePosition(PieDataSet.ValuePosition.OUTSIDE_SLICE);

                        final PieData data = new PieData(set);


                        data.setValueFormatter(new SyneValueFormatter(0));
                        data.setValueTextSize(14f);
                        data.setValueTextColor(Color.BLACK);

                        data.setValueTypeface(tf);

                        final int finalCounter = counter;
                        final int finalMaxIndex = maxIndex;
                        final boolean finalCacheUsed = cacheUsed;
                        activity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {

                                recognizedContactsLiveTitle.setIntCounter(finalCounter);
                                recognizedContactsLivePieChart.setData(data);

                                recognizedContactsLivePieChartListener = new PieChartSelectedValueListener(recognizedContactsLivePieChart);
                                recognizedContactsLivePieChart.setOnChartValueSelectedListener(recognizedContactsLivePieChartListener);

                                setUpChart(recognizedContactsLivePieChart, finalCacheUsed);

                                recognizedContactsLivePieChart.invalidate(); // refresh

                                Highlight h = new Highlight(finalMaxIndex, 0, 0);
                                recognizedContactsLivePieChart.highlightValue(h);
                                drawCenter(recognizedContactsLivePieChart, h);

                                isRecognizedContectsLiveInitialized = true;
                                View rootView = getView();
                                if (rootView == null) {
                                    return;
                                }
                                View mobileAppLegend = rootView.findViewById(R.id.recognizedContactsLiveMobileSiteLegend);
                                mobileAppLegend.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        Highlight h = new Highlight(0, 0, 0);
                                        recognizedContactsLivePieChart.highlightValue(h);
                                        drawCenter(recognizedContactsLivePieChart, h);
                                    }
                                });

                                View destkopLegend = rootView.findViewById(R.id.recognizedContactsLiveDesktopLegend);
                                destkopLegend.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        Highlight h = new Highlight(1, 0, 0);
                                        recognizedContactsLivePieChart.highlightValue(h);
                                        drawCenter(recognizedContactsLivePieChart, h);
                                    }
                                });

                                View mobileSiteLegend = rootView.findViewById(R.id.recognizedContactsLiveMobileSiteLegend);
                                mobileSiteLegend.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        Highlight h = new Highlight(2, 0, 0);
                                        recognizedContactsLivePieChart.highlightValue(h);
                                        drawCenter(recognizedContactsLivePieChart, h);
                                    }
                                });

                            }
                        });
                    } else {
                        try {
                            mobileApp = finalJsonArray.getJSONObject(1).getInt("value");
                            desktop = finalJsonArray.getJSONObject(2).getInt("value");
                            mobileSite = finalJsonArray.getJSONObject(3).getInt("value");
                            final int finalDesktop = desktop;
                            final int finalMobileApp = mobileApp;
                            final int finalMobileSite = mobileSite;
                            counter = mobileApp + desktop + mobileSite;

                            final int finalCounter1 = counter;
                            activity.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {

                                    recognizedContactsLiveTitle.setIntCounter(finalCounter1);

                                    recognizedContactsLivePieChart.getData().getDataSet().getEntryForIndex(0).setY(finalMobileApp);
                                    recognizedContactsLivePieChart.getData().getDataSet().getEntryForIndex(1).setY(finalDesktop);
                                    recognizedContactsLivePieChart.getData().getDataSet().getEntryForIndex(2).setY(finalMobileSite);
                                    recognizedContactsLivePieChart.notifyDataSetChanged();

                                    recognizedContactsLivePieChart.setAlpha(1f);

                                    Highlight[] highlights = recognizedContactsLivePieChart.getHighlighted();
                                    if (highlights != null && highlights.length > 0) {
                                        drawCenter(recognizedContactsLivePieChart, highlights[0]);
                                    }

                                }
                            });
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                }
            } catch (NullPointerException e) {

            } catch (IllegalStateException e) {

            }

            return "ok";
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            Handler myHandler = new Handler();
            myHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (couldDownloadData()) {
                        new GetRecognizedContactsLive().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                    }
                }
            }, 3000);

        }
    }

    private void setUpChart(PieChart chart, boolean cacheUsed) {
        chart.getDescription().setEnabled(false);

        chart.setCenterText("");
        chart.setDrawEntryLabels(false);
        chart.setExtraOffsets(15f, 15f, 15f, 15f);
        chart.setTransparentCircleRadius(90);
        chart.setHoleRadius(90);
        chart.setTransparentCircleAlpha(100);

        chart.disableScroll();
        chart.getLegend().setEnabled(false);
        chart.getDescription().setEnabled(false);

        if (cacheUsed) {
            chart.setAlpha(0.4f);
        }

    }

    private class GetRecognizedContactsLast24 extends AsyncTask<Void, Void, String> {
        protected String doInBackground(Void... params) {
            try {

                HttpGet httpget = new HttpGet("https://api.synerise.com/rtom/stats/backend/users-channel-monitored?period=86400");

                SyneHttpCacheHandler handler = new SyneHttpCacheHandler();
                boolean cacheUsed = false;
                if (!isRecognizedContactsLast24Initialized) {
                    cacheUsed = MainActivity.cacheString.contains(SyneHttpCacheHandler.getCacheKey(httpget));
                }
                final String body = handler.executeWithCache(httpget, !isRecognizedContactsLast24Initialized, true);

                if (body == null) {
                    return "not ok";
                }

                JSONArray jsonArray = null;

                try {
                    jsonArray = new JSONArray(body);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                final JSONArray finalJsonArray = jsonArray;
                final Activity activity = getActivity();
                if (activity != null) {
                    List<PieEntry> entries = new ArrayList<>();
                    int counter = 0;
                    int nonZeroValues = 0;
                    int mobileApp = 0;
                    int desktop = 0;
                    int mobileSite = 0;
                    int[] colors = new int[3];

                    if (!isRecognizedContactsLast24Initialized) {
                        try {
                            mobileApp = finalJsonArray.getJSONObject(1).getInt("value");
                            desktop = finalJsonArray.getJSONObject(2).getInt("value");
                            mobileSite = finalJsonArray.getJSONObject(3).getInt("value");
                            if (mobileApp > 0 || true) {
                                entries.add(new PieEntry(mobileApp, "Mobile App", 0));
                                colors[nonZeroValues] = Color.parseColor("#" + transparency + "FE3867");
                                nonZeroValues++;
                            }
                            if (desktop > 0 || true) {
                                entries.add(new PieEntry(desktop, "Desktop", 1));
                                colors[nonZeroValues] = Color.parseColor("#" + transparency + "3DD292");
                                nonZeroValues++;
                            }
                            if (mobileSite > 0 || true) {
                                entries.add(new PieEntry(mobileSite, "Mobile Site", 2));

                                colors[nonZeroValues] = Color.parseColor("#" + transparency + "02B1F1");
                                nonZeroValues++;
                            }

                            counter = mobileApp + desktop + mobileSite;

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        PieDataSet set = new PieDataSet(entries, "");

                        set.setColors(colors);

                        set.setValueLinePart1OffsetPercentage(100.0f);
                        set.setValueLinePart1Length(0.15f);
                        set.setValueLinePart2Length(0.0f);

                        set.setValueLineColor(Color.parseColor("#00000000"));

                        set.setYValuePosition(PieDataSet.ValuePosition.OUTSIDE_SLICE);

                        int maxIndex = 0;
                        float maxValue = 0;
                        for (int i = 0; i < set.getEntryCount(); i++) {
                            if (maxValue < set.getEntryForIndex(i).getValue()) {
                                maxValue = set.getEntryForIndex(i).getValue();
                                maxIndex = i;
                            }
                        }

                        final PieData data = new PieData(set);


                        data.setValueFormatter(new SyneValueFormatter(0));
                        data.setValueTextSize(14f);
                        data.setValueTextColor(Color.BLACK);


                        Typeface tf = Typeface.createFromAsset(getContext().getAssets(),
                                "fonts/Lato-Thin.ttf");
                        data.setValueTypeface(tf);

                        final int finalCounter = counter;
                        final int finalMaxIndex = maxIndex;
                        final boolean finalCacheUsed = cacheUsed;
                        activity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                recognizedContactsLast24Title.setIntCounter(finalCounter);
                                recognizedContactsLast24PieChart.setData(data);

                                recognizedContactsLast24PieChartListener = new PieChartSelectedValueListener(recognizedContactsLast24PieChart);
                                recognizedContactsLast24PieChart.setOnChartValueSelectedListener(recognizedContactsLast24PieChartListener);

                                setUpChart(recognizedContactsLast24PieChart, finalCacheUsed);

                                recognizedContactsLast24PieChart.invalidate(); // refresh

                                Highlight h = new Highlight(finalMaxIndex, 0, 0);
                                recognizedContactsLast24PieChart.highlightValue(h);
                                drawCenter(recognizedContactsLast24PieChart, h);

                                isRecognizedContactsLast24Initialized = true;

                                View rootView = getView();
                                if (rootView == null) {
                                    return;
                                }
                                View mobileAppLegend = rootView.findViewById(R.id.recognizedContactsLast24MobileSiteLegend);
                                mobileAppLegend.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        Highlight h = new Highlight(0, 0, 0);
                                        recognizedContactsLast24PieChart.highlightValue(h);
                                        drawCenter(recognizedContactsLast24PieChart, h);
                                    }
                                });

                                View destkopLegend = rootView.findViewById(R.id.recognizedContactsLast24DesktopLegend);
                                destkopLegend.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        Highlight h = new Highlight(1, 0, 0);
                                        recognizedContactsLast24PieChart.highlightValue(h);
                                        drawCenter(recognizedContactsLast24PieChart, h);
                                    }
                                });

                                View mobileSiteLegend = rootView.findViewById(R.id.recognizedContactsLast24MobileSiteLegend);
                                mobileSiteLegend.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        Highlight h = new Highlight(2, 0, 0);
                                        recognizedContactsLast24PieChart.highlightValue(h);
                                        drawCenter(recognizedContactsLast24PieChart, h);
                                    }
                                });
                            }
                        });
                    } else {
                        try {
                            mobileApp = finalJsonArray.getJSONObject(1).getInt("value");
                            desktop = finalJsonArray.getJSONObject(2).getInt("value");
                            mobileSite = finalJsonArray.getJSONObject(3).getInt("value");
                            final int finalDesktop = desktop;
                            final int finalMobileApp = mobileApp;
                            final int finalMobileSite = mobileSite;
                            counter = mobileApp + desktop + mobileSite;

                            final int finalCounter1 = counter;
                            activity.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {

                                    recognizedContactsLast24Title.setIntCounter(finalCounter1);

                                    recognizedContactsLast24PieChart.setAlpha(1f);

                                    recognizedContactsLast24PieChart.getData().getDataSet().getEntryForIndex(0).setY(finalMobileApp);
                                    recognizedContactsLast24PieChart.getData().getDataSet().getEntryForIndex(1).setY(finalDesktop);
                                    recognizedContactsLast24PieChart.getData().getDataSet().getEntryForIndex(2).setY(finalMobileSite);
                                    recognizedContactsLast24PieChart.notifyDataSetChanged();

                                    Highlight[] highlights = recognizedContactsLast24PieChart.getHighlighted();
                                    if (highlights != null && highlights.length > 0) {
                                        drawCenter(recognizedContactsLast24PieChart, highlights[0]);
                                    }

                                }
                            });
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                }
            } catch (NullPointerException e) {

            } catch (IllegalStateException e) {

            }

            return "ok";
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            Handler myHandler = new Handler();
            myHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (couldDownloadData()) {
                        new GetRecognizedContactsLast24().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                    }
                }
            }, 3000);

        }
    }

    private boolean couldDownloadData() {
        Activity activity = getActivity();

        if (activity == null) {
            return false;
        }

        Fragment fragment = ((MainActivity) getActivity()).getSupportFragmentManager().findFragmentById(R.id.mainContainer);


        return isScreenActive && fragment instanceof LiveStreamFragment;
    }

    private class GetEventList extends AsyncTask<Void, Void, String> {
        protected String doInBackground(Void... params) {
            try {


                HttpGet httpget = new HttpGet("https://app.synerise.com/activity-histories/stream-for-business-profile/?listing%5BPaginator%5D");


                SyneHttpCacheHandler handler = new SyneHttpCacheHandler();
                final String body = handler.executeWithCache(httpget, !isGetEventsInitialized, true);

                if (body == null) {
                    return "not ok";
                }

                JSONArray jsonArray = null;

                try {
                    jsonArray = new JSONArray(body);
                } catch (JSONException e) {
                    e.printStackTrace();
                }


                assert jsonArray != null;

                if (jsonArray.length() == 0) {
                    return "not ok";
                }

                eventFactory.createEventsArray(events, jsonArray);

                Activity activity = getActivity();
                if (activity != null) {
                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            adapter.notifyDataSetChanged();
                            if (!isGetEventsInitialized) {
                                new GetEventList().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                            }
                            isGetEventsInitialized = true;
                        }
                    });
                }
            } catch (NullPointerException e) {

            } catch (IllegalStateException e) {

            } catch (JSONException e) {
                e.printStackTrace();
            }

            return "ok";

        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            Handler myHandler = new Handler();
            myHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    View rootView = getView();
                    if (rootView == null) {
                        return;
                    }
                    if (couldDownloadData() && ((Switch) rootView.findViewById(R.id.autoloadTrafficSwitch)).isChecked()) {
                        new GetEventList().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                    }
                }
            }, 5000);
        }
    }


    private void drawCenter(PieChart chart, Highlight h) {
        DecimalFormat df = new DecimalFormat("#.##");
        Entry e = chart.getData().getEntryForHighlight(h);
        PieEntry entry = (PieEntry) e;
        String label = entry.getLabel() + "\n";
        String value = (int) entry.getValue() + " ";
        String percent = df.format((double) ((entry.getValue() / chart.getData().getYValueSum()) * 100)) + "%";

        SpannableString s = new SpannableString(label + value + percent);
        s.setSpan(new TypefaceSpan("fonts/Lato-Thin.ttf"), 0, s.length(), 0);
        s.setSpan(new RelativeSizeSpan(1.0f), 0, label.length(), 0);
        s.setSpan(new RelativeSizeSpan(2.0f), label.length(), label.length() + value.length(), 0);
        s.setSpan(new ForegroundColorSpan(Color.BLACK), label.length(), label.length() + value.length(), 0);
        s.setSpan(new RelativeSizeSpan(0.7f), s.length() - percent.length(), s.length(), 0);

        chart.setCenterText(s);

        chart.setCenterTextSize(24);

        chart.setCenterTextColor(Color.parseColor("#647f97"));
    }

    private class PieChartSelectedValueListener implements OnChartValueSelectedListener {

        PieChart chart = null;


        public void setChart(PieChart chart) {
            this.chart = chart;
        }

        public PieChartSelectedValueListener(PieChart chart) {
            this.chart = chart;
        }

        @Override
        public void onValueSelected(Entry e, Highlight h) {
            drawCenter(chart, h);
        }

        @Override
        public void onNothingSelected() {
            chart.setCenterText("");
        }
    }
}

package com.synerise.synerise.app.syneutil;

import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;

/**
 * Created by dsiedlarz on 12.02.2017.
 */

public class SyneLabelFormatter implements IAxisValueFormatter {
    private final String[] mLabels;

    public SyneLabelFormatter(String[] labels) {
        mLabels = labels;
    }

    @Override
    public String getFormattedValue(float value, AxisBase axis) {
        return mLabels[(int) value];
    }
}
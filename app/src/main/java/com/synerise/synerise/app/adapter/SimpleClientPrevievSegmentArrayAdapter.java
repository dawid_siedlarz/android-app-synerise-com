package com.synerise.synerise.app.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.synerise.synerise.R;
import com.synerise.synerise.app.model.ClientPreviewSegment;

import java.util.ArrayList;

/**
 * Created by dsiedlarz on 15.10.2016.
 */
public class SimpleClientPrevievSegmentArrayAdapter extends ArrayAdapter<ClientPreviewSegment.Segment> {
    private final Context context;
    private final ArrayList<ClientPreviewSegment.Segment> values;
    private TextView mAvatar;
    private ClientPreviewSegment.Segment segment;



    public SimpleClientPrevievSegmentArrayAdapter(Context context, ArrayList<ClientPreviewSegment.Segment> values) {
        super(context, -1, values);
        this.context = context;
        this.values = values;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        segment = ClientPreviewSegment.ITEMS.get(position);
        View rowView = inflater.inflate(R.layout.list_element_client_preview_segment, parent, false);
        TextView mTagName = (TextView) rowView.findViewById(R.id.tagName);
        mTagName.setText("#"+segment.name);
        return rowView;
    }

}

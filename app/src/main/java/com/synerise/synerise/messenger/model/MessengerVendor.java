package com.synerise.synerise.messenger.model;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by dsiedlarz on 28.12.2016.
 */
public class MessengerVendor extends MessengerUser {
    private String externalId;
    private Date lastActive;
    private DateFormat dateFormat;

    public MessengerVendor(JSONObject object) {
        super(object);
        parseJsonObject(object);
    }

    public void parseJsonObject(JSONObject object) {
        super.parseJsonObject(object);
        if (getUserType() == null) {
            setUserType(USER_TYPE_VENDORS);
        }
        DateFormat df1 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
        try {
            if (object.has("attributes")) {
                JSONObject attributes = object.getJSONObject("attributes");
                if (attributes.has("external_id")) {
                    externalId = attributes.getString("external_id");
                }
                if (attributes.has("last_active")) {
                    lastActive = df1.parse(attributes.getString("last_active"));
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    public String getExternalId() {
        return externalId;
    }

    public void setExternalId(String externalId) {
        this.externalId = externalId;
    }

    public Date getLastActive() {
        return lastActive;
    }

    public void setLastActive(Date lastActive) {
        this.lastActive = lastActive;
    }

    public DateFormat getDateFormat() {
        return dateFormat;
    }

    public void setDateFormat(DateFormat dateFormat) {
        this.dateFormat = dateFormat;
    }
}

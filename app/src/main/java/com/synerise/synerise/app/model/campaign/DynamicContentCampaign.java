package com.synerise.synerise.app.model.campaign;

import android.os.Parcel;

import org.json.JSONException;
import org.json.JSONObject;

public class DynamicContentCampaign extends Campaign {

    private String type;
    private String viewsExcludePageUrl;
    private String viewsPageUrl;
    private String recipientsType;
    private String selector;

    public DynamicContentCampaign(JSONObject jsonObject) {
        super(jsonObject);

        try {
            type = jsonObject.getString("type");
            if (!jsonObject.isNull("viewsExcludePageUrl")) {
                viewsExcludePageUrl = jsonObject.getString("viewsExcludePageUrl");
            }
            if (!jsonObject.isNull("viewsPageUrl")) {
                viewsPageUrl = jsonObject.getString("viewsPageUrl");
            }
            if (!jsonObject.isNull("recipientsType")) {
                recipientsType = jsonObject.getString("recipientsType");
            }

            if (!jsonObject.isNull("selector")) {
                selector = jsonObject.getString("selector");
            }

            if (jsonObject.getBoolean("status")) {
                status = "Active";
            } else {
                status = "Inactive";
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public DynamicContentCampaign(Parcel in) {
        super(in);

        type = in.readString();
        viewsExcludePageUrl = in.readString();
        viewsPageUrl = in.readString();
        recipientsType = in.readString();
        selector = in.readString();
    }


    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);

        dest.writeString(type);
        dest.writeString(viewsExcludePageUrl);
        dest.writeString(viewsPageUrl);
        dest.writeString(recipientsType);
        dest.writeString(selector);
    }

    @Override
    public int describeContents() {
        return super.describeContents();
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getViewsExcludePageUrl() {
        return viewsExcludePageUrl;
    }

    public void setViewsExcludePageUrl(String viewsExcludePageUrl) {
        this.viewsExcludePageUrl = viewsExcludePageUrl;
    }

    public String getViewsPageUrl() {
        return viewsPageUrl;
    }

    public void setViewsPageUrl(String viewsPageUrl) {
        this.viewsPageUrl = viewsPageUrl;
    }

    public String getRecipientsType() {
        return recipientsType;
    }

    public void setRecipientsType(String recipientsType) {
        this.recipientsType = recipientsType;
    }

    public String getSelector() {
        return selector;
    }

    public void setSelector(String selector) {
        this.selector = selector;
    }
}

package com.synerise.synerise.app.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.synerise.synerise.R;
import com.synerise.synerise.app.model.Smartlist;

import java.util.ArrayList;

/**
 * Created by dsiedlarz on 15.10.2016.
 */
public class SimpleSmartlistsArrayAdapter extends ArrayAdapter<Smartlist.SmartlistModel> {
        private final Context context;
    private final ArrayList<Smartlist.SmartlistModel> values;
    private TextView mAvatar;
    private Smartlist.SmartlistModel smartlistModel;



    public SimpleSmartlistsArrayAdapter(Context context, ArrayList<Smartlist.SmartlistModel> values) {
        super(context, -1, values);
        this.context = context;
        this.values = values;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        smartlistModel = Smartlist.ITEMS.get(position);
        View rowView = inflater.inflate(R.layout.list_element_tag, parent, false);
        TextView mTagName = (TextView) rowView.findViewById(R.id.tagName);
        TextView mTagCount = (TextView) rowView.findViewById(R.id.tagCount);

        mTagName.setText(smartlistModel.name);
        return rowView;
    }
}

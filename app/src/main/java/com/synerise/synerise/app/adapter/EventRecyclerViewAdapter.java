package com.synerise.synerise.app.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.synerise.synerise.EventPreview;
import com.synerise.synerise.MainActivity;
import com.synerise.synerise.R;
import com.synerise.synerise.app.model.Event;

import java.util.ArrayList;

public class EventRecyclerViewAdapter extends RecyclerView.Adapter<EventRecyclerViewAdapter.EventViewHolder> {
    private final Context context;
    private final Activity activity;
    private final ArrayList<Event> values;
    private Typeface font;

    public EventRecyclerViewAdapter(Activity activity, ArrayList<Event> values) {
        this.activity = activity;
        this.context = activity.getApplicationContext();
        this.values = values;
        this.font = Typeface.createFromAsset(context.getAssets(), "fonts/action-icons-up.ttf");
    }

    @Override
    public int getItemCount() {
        return values.size();
    }

    @Override
    public EventViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_element_event, parent, false);
        EventViewHolder viewHolder = new EventViewHolder(view);
        return viewHolder;
    }



    @Override
    public void onBindViewHolder(final EventViewHolder holder, final int position) {

        final Event event = values.get(position);
        holder.mEventUser.setText(event.nameContent);

        holder.mEventName.setText(event.titleContent);
        holder.mEventTime.setText(event.timeContent);

        if(event.hasModal) {
            holder.mEventMore.setVisibility(View.VISIBLE);
            holder.mEventMore.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {

                    Fragment newFragment = new EventPreview();
                    Bundle args = new Bundle();
                    args.putString("body", event.modalContent);
                    args.putString("header", event.titleHtml);
                    newFragment.setArguments(args);

                    MainActivity.addFragment(newFragment);
                }


            });

        }else{
            holder.mEventMore.setVisibility(View.INVISIBLE);
        }
        holder.mEventIcon.setImageDrawable(event.icon);
    }


    class EventViewHolder extends RecyclerView.ViewHolder {
        private TextView mEventUser;
        private TextView mEventName;
        private TextView mEventTime;
        private ImageView mEventIcon;
        private ImageView mEventMore;
        public View mView;

        public EventViewHolder(View view) {
            super(view);
            this.mView = view;
            this.mEventUser = (TextView) view.findViewById(R.id.eventUser);
            this.mEventName = (TextView) view.findViewById(R.id.eventName);
            this.mEventIcon = (ImageView) view.findViewById(R.id.eventIcon);
            this.mEventMore = (ImageView) view.findViewById(R.id.eventMore);
            this.mEventTime = (TextView) view.findViewById(R.id.eventTime);

        }
    }
}



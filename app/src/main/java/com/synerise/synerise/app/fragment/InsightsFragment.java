package com.synerise.synerise.app.fragment;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;

import com.synerise.sdk.tracking.Tracker;
import com.synerise.sdk.tracking.TrackerParams;
import com.synerise.synerise.MainActivity;
import com.synerise.synerise.R;
import com.synerise.synerise.SyneriseOnFragmentInteractionListener;
import com.synerise.synerise.app.ActionBarConfigurableFragment;
import com.synerise.synerise.app.adapter.InsightsRecyclerViewAdapter;
import com.synerise.synerise.app.model.Insight;
import com.synerise.synerise.app.syneutil.SyneHttpCacheHandler;

import org.apache.http.client.methods.HttpGet;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


public class InsightsFragment extends ActionBarConfigurableFragment {
    private ArrayList<Insight> insights;
    private SyneriseOnFragmentInteractionListener mListener;
    private RecyclerView insightsList;
    private InsightsRecyclerViewAdapter adapter;
    private LinearLayoutManager linearLayoutManager;

    private void initInsightsList() {
        View rootView = getView();
        if (rootView == null) {
            return;
        }
        insights = new ArrayList<>();
        insightsList = (RecyclerView) rootView.findViewById(R.id.insightsRecyclerView);
        adapter = new InsightsRecyclerViewAdapter((MainActivity) getActivity(), insights);
        insightsList.setNestedScrollingEnabled(false);
        linearLayoutManager = new LinearLayoutManager(getContext());
        insightsList.setLayoutManager(linearLayoutManager);
        insightsList.setAdapter(adapter);
    }

    boolean isScreenActive = false;
    boolean isinsightsInitialized = false;

    @Override
    public void onResume() {
        super.onResume();
        isScreenActive = true;
        System.out.println("onResume");
        new GetIsightsList().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    @Override
    public void onPause() {
        super.onPause();
        isScreenActive = false;
    }

    @Override
    public void onStop() {
        super.onStop();
        isScreenActive = false;
    }

    public InsightsFragment() {
        // Required empty public constructor
    }


    public static InsightsFragment newInstance(String param1, String param2) {
        InsightsFragment fragment = new InsightsFragment();
        return fragment;
    }

    @Override
    public void setUpActionBar() {
        ActionBar actionBar = (ActionBar) ((AppCompatActivity) getActivity()).getSupportActionBar();
        if (actionBar != null) {
            actionBar.show();
            actionBar.setTitle(MainActivity.businessProfileName != null ? MainActivity.businessProfileName : "Synerise");
            actionBar.setSubtitle("Insights");
            actionBar.setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.colorPrimaryInsights)));
            getActivity().setTheme(R.style.AppThemeInsights);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                Window window = getActivity().getWindow();
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                window.setStatusBarColor(getResources().getColor(R.color.colorPrimaryInsights));
            }
            actionBar.setElevation(0);
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_insights, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Tracker.trackScreen("Insights", new TrackerParams[]{});

        initInsightsList();
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    private class GetIsightsList extends AsyncTask<Void, Void, String> {
        protected String doInBackground(Void... params) {
            try {

                HttpGet httpget = new HttpGet("http://synerise.com/insight/?v1&category=all");

                SyneHttpCacheHandler handler = new SyneHttpCacheHandler();
                boolean cacheUsed = false;


                final String body = handler.executeWithCache(httpget, false, true);

                JSONObject jsonObject = null;

                try {
                    jsonObject = new JSONObject(body);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if (jsonObject == null) {
                    return null;
                }
                if (!jsonObject.has("aaData")) {
                    return "not ok";
                }
                JSONArray jsonArray = jsonObject.getJSONArray("aaData");
                insights.clear();
                for (int i = 0; i < jsonArray.length(); i++) {
                    insights.add(new Insight(jsonArray.getJSONObject(i)));
                }

                Activity activity = getActivity();

                if (activity != null) {
                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            adapter.notifyDataSetChanged();
                        }
                    });
                }

            } catch (JSONException e) {
                e.printStackTrace();
            } catch (NullPointerException e) {

            } catch (IllegalStateException e) {

            }
            return "ok";
        }
    }
}

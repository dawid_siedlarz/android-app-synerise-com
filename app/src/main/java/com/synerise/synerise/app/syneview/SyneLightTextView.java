package com.synerise.synerise.app.syneview;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Build;
import android.util.AttributeSet;
import android.widget.TextView;

import com.synerise.synerise.R;

/**
 * Created by dsiedlarz on 17.11.2016.
 */
public class SyneLightTextView extends TextView {
    private Context context;

    public SyneLightTextView(Context context) {
        super(context);
        this.context = context;
        initFont();
    }

    public SyneLightTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        initFont();
    }

    public SyneLightTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.context = context;
        initFont();
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public SyneLightTextView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        this.context = context;
        initFont();
    }


    private void initFont() {
        Typeface tf = Typeface.createFromAsset(context.getAssets(),
                "fonts/Lato-Thin.ttf");
        setTypeface(tf);
        setTextColor(getResources().getColor(R.color.textGray));
    }
}

package com.synerise.synerise.app.fragment.campaign.statistic;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.github.mikephil.charting.charts.HorizontalBarChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.synerise.synerise.MainActivity;
import com.synerise.synerise.R;
import com.synerise.synerise.app.ActionBarConfigurableFragment;
import com.synerise.synerise.app.model.campaign.EmailCampaign;
import com.synerise.synerise.app.syneutil.SyneHttpCacheHandler;
import com.synerise.synerise.app.syneview.SyneComplexLineChart;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;


public class EmailCampaignStatisticsFragment extends ActionBarConfigurableFragment {

    public static final String ARG_CAMPAIGN = "campaign";

    private EmailCampaign emailCampaign;

    private HorizontalBarChart deliveryStatusBarChart;
    private SyneComplexLineChart emailCampaignStatsLineChart;

    private boolean isCampaignAnalyticsInitialized = false;

    private TextView emailCampaignStatisticTitle;
    private TextView ctrPercentValueTextView;
    private TextView clickUniqueValueTextView;
    private TextView orPercentValueTextView;
    private TextView openUniqueValueTextView;

    private TextView sendTextView;
    private TextView deliverdTextView;
    private TextView deliveredPercentTextView;
    private TextView softBounceTextView;
    private TextView softBouncePercentTextView;
    private TextView hardBounceTextView;
    private TextView hardBouncePercentTextView;
    private TextView totalTransactionsTextView;
    private TextView amountTextView;
    private TextView amountPerSendTextView;
    private TextView averateTransactionValueTextView;

    public EmailCampaignStatisticsFragment() {
        // Required empty public constructor
    }

    @Override
    public void setUpActionBar() {
        ActionBar actionBar = (ActionBar) ((AppCompatActivity) getActivity()).getSupportActionBar();
        if (actionBar != null) {
            actionBar.show();
            actionBar.setTitle(MainActivity.businessProfileName != null ? MainActivity.businessProfileName : "Synerise");
            actionBar.setSubtitle("Email campaign");
            actionBar.setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.colorPrimaryCampaigns)));
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                Window window = getActivity().getWindow();
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                window.setStatusBarColor(getResources().getColor(R.color.colorPrimaryDarkCampaigns));
            }
            actionBar.setElevation(0);
        }
    }

    public static EmailCampaignStatisticsFragment newInstance(EmailCampaign emailCampaign) {
        EmailCampaignStatisticsFragment fragment = new EmailCampaignStatisticsFragment();
        Bundle args = new Bundle();
        args.putParcelable(ARG_CAMPAIGN, emailCampaign);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            emailCampaign = (EmailCampaign) getArguments().getParcelable(ARG_CAMPAIGN);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_email_campaign_statistics, container, false);
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        findHolders();
        initDeliveryStatusBarChart();

        fillHolders();

        new GetReport().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        View rootView = getView();
        if (rootView == null) {
            return;
        }
        emailCampaignStatsLineChart = rootView.findViewById(R.id.emailCampaignStatsLineChart);
        emailCampaignStatsLineChart.getChart().getAxisLeft().setAxisMinimum(-100f);
        new GetEmailAnalytics().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

        ActionBar actionBar = (ActionBar) ((AppCompatActivity) getActivity()).getSupportActionBar();
        if (actionBar != null) {
            actionBar.show();
            actionBar.setSubtitle("Email campaign");
        }
    }

    private void fillHolders() {
        emailCampaignStatisticTitle.setText(emailCampaign.getName());
        long delivered = emailCampaign.getSent();
        long softBounce = emailCampaign.getSoftbounce();
        long hardBounce = emailCampaign.getHardbounce();
        long allSent = delivered + softBounce + hardBounce;

        long uniqueClick = emailCampaign.getUniqueClicked();
        long uniqueOpen = emailCampaign.getUniqueOpened();


        float ctr = delivered > 0 ? uniqueClick / (float) delivered : 0;
        float or = delivered > 0 ? uniqueOpen / (float) delivered : 0;

        float deliveredPercent = allSent > 0 ? delivered / (float) allSent : 0;
        float softBouncePercent = allSent > 0 ? softBounce / (float) allSent : 0;
        float hardBouncePercent = allSent > 0 ? hardBounce / (float) allSent : 0;

        DecimalFormat df = new DecimalFormat("###,###,###.##");

        ctrPercentValueTextView.setText(String.format("%.2f%%", ctr * 100));
        clickUniqueValueTextView.setText(df.format(uniqueClick));
        orPercentValueTextView.setText(String.format("%.2f%%", or * 100));
        openUniqueValueTextView.setText(df.format(uniqueOpen));

        sendTextView.setText(df.format(allSent));
        deliverdTextView.setText(df.format(delivered));
        deliveredPercentTextView.setText(String.format("(%.2f%%)", deliveredPercent * 100));

        softBounceTextView.setText(df.format(softBounce));
        softBouncePercentTextView.setText(String.format("(%.2f%%)", softBouncePercent * 100));

        hardBounceTextView.setText(df.format(hardBounce));
        hardBouncePercentTextView.setText(String.format("(%.2f%%)", hardBouncePercent * 100));


        long totalTransactions = emailCampaign.getNumberOrders();
        double amount = emailCampaign.getTotalAmountOrder();

        double amountPerSend = delivered > 0 ? totalTransactions / (double) delivered : 0;
        double averageTransactionValue = totalTransactions > 0 ? amount / (double) totalTransactions : 0;

        totalTransactionsTextView.setText(df.format(totalTransactions));
        amountTextView.setText(df.format(amount));
        amountPerSendTextView.setText(String.format("%.8f", amountPerSend));
        averateTransactionValueTextView.setText(df.format(averageTransactionValue));
    }


    private void findHolders() {
        View rootView = getView();
        if (rootView == null) {
            return;
        }
        emailCampaignStatisticTitle = rootView.findViewById(R.id.emailCampaignStatisticTitle);
        ctrPercentValueTextView = rootView.findViewById(R.id.campaignStatisticCtrPercent);
        clickUniqueValueTextView = rootView.findViewById(R.id.campaignStatisticCtrUnique);
        orPercentValueTextView = rootView.findViewById(R.id.campaignStatisticOrPercent);
        openUniqueValueTextView = rootView.findViewById(R.id.campaignStatisticOrUnique);
        sendTextView = rootView.findViewById(R.id.emailStatisticSend);
        deliverdTextView = rootView.findViewById(R.id.emailStatisticDelivered);
        deliveredPercentTextView = rootView.findViewById(R.id.emailStatisticDeliveredPercent);
        softBounceTextView = rootView.findViewById(R.id.emailStatisticSoftBounce);
        softBouncePercentTextView = rootView.findViewById(R.id.emailStatisticSoftBouncePercent);
        hardBounceTextView = rootView.findViewById(R.id.emailStatisticHardBounce);
        hardBouncePercentTextView = rootView.findViewById(R.id.emailStatisticHardBouncePercent);
        totalTransactionsTextView = rootView.findViewById(R.id.emailStatisticTotalTransactions);
        amountTextView = rootView.findViewById(R.id.emailStatisticAmount);
        amountPerSendTextView = rootView.findViewById(R.id.emailStatisticAmountPerSend);
        averateTransactionValueTextView = rootView.findViewById(R.id.emailStatisticAverageTransactionValue);
    }


    private void initDeliveryStatusBarChart() {
        View rootView = getView();
        if (rootView == null) {
            return;
        }
        deliveryStatusBarChart = rootView.findViewById(R.id.deliveryStatusChart);

        final List<String> labels = new ArrayList<String>();
        ArrayList<BarEntry> values = new ArrayList<BarEntry>();


        float[] deliveryStatusValues = new float[3];

        labels.add("sent");
        labels.add("softBounce");
        labels.add("hardBounce");
        deliveryStatusValues[0] = emailCampaign.getSent();
        deliveryStatusValues[1] = emailCampaign.getSoftbounce();
        deliveryStatusValues[2] = emailCampaign.getHardbounce();
        values.add(new BarEntry(1F, deliveryStatusValues));


        int[] colors = new int[]{
                getResources().getColor(R.color.legendDelivered),
                getResources().getColor(R.color.legendSoftBounce),
                getResources().getColor(R.color.legendHardBounce)
        };


        BarDataSet set1 = new BarDataSet(values, "Delivery Status");
        set1.setColors(colors);
        set1.setDrawValues(false);
        set1.setStackLabels(labels.toArray(new String[labels.size()]));

        ArrayList<IBarDataSet> dataSets = new ArrayList<IBarDataSet>();
        dataSets.add(set1);

        final BarData barData = new BarData(dataSets);
        barData.setValueTextColor(Color.WHITE);

        initBarChart(deliveryStatusBarChart);
        update(deliveryStatusBarChart, barData, 0d, labels, new String[]{"1", "2", "3"}, true, false);
    }

    public void initBarChart(HorizontalBarChart chart) {
        chart.getDescription().setEnabled(false);
        chart.getLegend().setEnabled(false);

        chart.setDrawGridBackground(false);
        chart.setDrawGridBackground(false);

        chart.getAxisRight().setDrawAxisLine(false);
        chart.getAxisRight().setEnabled(false);
        chart.getAxisRight().setDrawGridLines(false);

        chart.getAxisLeft().setDrawAxisLine(false);
        chart.getAxisLeft().setEnabled(false);
        chart.getAxisLeft().setDrawGridLines(false);

        chart.getXAxis().setDrawGridLines(false);
        chart.getXAxis().setEnabled(false);

        chart.setDrawBorders(false);
        chart.setViewPortOffsets(0f, 20f, 0f, 40f);
        chart.getAxisRight().setEnabled(false);
        chart.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM);
        chart.setBackgroundColor(Color.parseColor("#FFFFFF"));
        chart.setDrawGridBackground(false);
    }

    public void update(HorizontalBarChart chart, BarData data, Double counter, List<String> xLabels, String[] lineLabels, boolean isChartInitialized, boolean isCacheUsed) {

        chart.setData(data);
        chart.getXAxis().setValueFormatter(new IndexAxisValueFormatter(xLabels));

        if (!isChartInitialized) {
            if (isCacheUsed) {
                chart.setAlpha(0.5f);
            } else {
                chart.animateY(1000);
            }
            chart.invalidate();
        } else {
            chart.setAlpha(1f);
            chart.notifyDataSetChanged();
        }
    }


    private class GetReport extends AsyncTask<Integer, Void, String> {
        protected String doInBackground(Integer... params) {
            try {
                HttpGet httpget = new HttpGet("https://app.synerise.com/em/rest/campaigns/" + emailCampaign.getId() + "/report");

                HttpResponse eventList = MainActivity.httpClient.execute(httpget);
                if (eventList == null) {
                    return "not OK";
                }

                HttpEntity entity = eventList.getEntity();

                final String body = EntityUtils.toString(entity);


                JSONObject jsonObject = null;

                try {
                    jsonObject = new JSONObject(body);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if (jsonObject == null) {
                    return null;
                }


                emailCampaign.updateFromReport(jsonObject);

                Activity activity = getActivity();
                if (activity != null) {

                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            fillHolders();
                        }
                    });
                }
            } catch (IOException e) {
                e.printStackTrace();
            } catch (NullPointerException | IllegalStateException e) {

            }
            return "ok";
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            if (s != null && s.compareToIgnoreCase("ok") == 0) {
                Handler myHandler = new Handler();
                myHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Fragment fragment = getActivity().getSupportFragmentManager().findFragmentById(R.id.mainContainer);
                        if (getActivity() != null && fragment instanceof EmailCampaignStatisticsFragment) {
                            new GetReport().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                        }
                    }
                }, 10000);

            }
        }
    }

    private float getMax(float[] set) {

        float max = Float.MIN_VALUE;

        for (float number :
                set) {
            if (number > max) {
                max = number;
            }
        }
        return max;
    }


    private class GetEmailAnalytics extends AsyncTask<Void, Void, String> {
        protected String doInBackground(Void... params) {
            try {
                String url = "https://app.synerise.com/em/rest/campaigns/" + emailCampaign.getId() + "/stats?date%5BdateFormat%5D=hour&date%5BminPeriod%5D=hh&client=all&campaignId=" + emailCampaign.getHash();

                HttpGet httpget = new HttpGet(url);

                SyneHttpCacheHandler handler = new SyneHttpCacheHandler();
                boolean cacheUsed = false;
                if (!isCampaignAnalyticsInitialized) {
                    cacheUsed = MainActivity.cacheString.contains(SyneHttpCacheHandler.getCacheKeyFromUrl(createCacheKey(url)));
                }
                final String body = handler.executeWithCacheKey(httpget, createCacheKey(url), !isCampaignAnalyticsInitialized, true);

                if (body == null) {
                    return "not ok";
                }

                JSONArray jsonArray = null;

                try {
                    jsonArray = new JSONArray(body);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                final JSONArray finalJSONArray = jsonArray;

                final ArrayList<String> labels = new ArrayList<String>();
                ArrayList<Entry> newsletterOpen = new ArrayList<Entry>();
                ArrayList<Entry> newsletterClick = new ArrayList<Entry>();
                ArrayList<Entry> newsletterUnsubscribe = new ArrayList<Entry>();
                ArrayList<Entry> newsletterHardbounce = new ArrayList<Entry>();
                ArrayList<Entry> newsletterSoftbounce = new ArrayList<Entry>();
                Double all = 0.0;
                float maxValue = 0;
                try {
                    int j = 0;
                    for (int i = 0; i < finalJSONArray.length(); i++) {
                        JSONObject jsonObject = finalJSONArray.getJSONObject(i);

                        float newsletterOpenIdValue = (float) jsonObject.getDouble("newsletterOpenId");
                        float newsletterClickIdValue = (float) jsonObject.getDouble("newsletterClickId");
                        float newsletterUnsubscribeIdValue = (float) jsonObject.getDouble("newsletterUnsubscribeId");
                        float newsletterHardbounceIdValue = (float) jsonObject.getDouble("newletterHardbounceId");
                        float newsletterSoftbounceValue = (float) jsonObject.getDouble("newletterSoftbounce");

                        maxValue = getMax(new float[]{maxValue, newsletterOpenIdValue, newsletterClickIdValue, newsletterUnsubscribeIdValue, newsletterHardbounceIdValue, newsletterSoftbounceValue});

                        newsletterOpen.add(new Entry(j, newsletterOpenIdValue));
                        newsletterUnsubscribe.add(new Entry(j, newsletterUnsubscribeIdValue));
                        newsletterHardbounce.add(new Entry(j, newsletterHardbounceIdValue));
                        newsletterSoftbounce.add(new Entry(j, newsletterSoftbounceValue));
                        newsletterClick.add(new Entry(j, newsletterClickIdValue));

                        labels.add(jsonObject.getString("humanDate"));
                        all += newsletterOpenIdValue
                                + newsletterUnsubscribeIdValue
                                + newsletterHardbounceIdValue
                                + newsletterSoftbounceValue
                                + newsletterClickIdValue;
                        j++;
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                final String[] lineLabels = {"Open", "Click", "Unsubscribe", "Hardbounce", "Softbounce"};
                LineDataSet newsletterOpenSet = new LineDataSet(newsletterOpen, "Open");
                LineDataSet newsletterUnsubscribeSet = new LineDataSet(newsletterUnsubscribe, "Unsubscribe");
                LineDataSet newsletterHardbounceSet = new LineDataSet(newsletterHardbounce, "Hardbounce");
                LineDataSet newsletterSoftbounceSet = new LineDataSet(newsletterSoftbounce, "Softbounce");
                LineDataSet newsletterClickSet = new LineDataSet(newsletterSoftbounce, "Click");

                Resources resources;
                try {
                    resources = getResources();
                } catch (RuntimeException e) {
                    //ignore
                }

                if (getResources() == null) {
                    return "not OK";
                }
                newsletterOpenSet.setColor(getResources().getColor(R.color.legendNewsletterOpen));
                newsletterClickSet.setColor(getResources().getColor(R.color.legendNewsletterClick));
                newsletterUnsubscribeSet.setColor(getResources().getColor(R.color.legendNewsletterUnsubscribe));
                newsletterHardbounceSet.setColor(getResources().getColor(R.color.legendNewsletterHardbounce));
                newsletterSoftbounceSet.setColor(getResources().getColor(R.color.legendNewsletterSoftbounce));

                ArrayList<LineDataSet> sets = new ArrayList<LineDataSet>();

                sets.add(newsletterOpenSet);
                sets.add(newsletterClickSet);
                sets.add(newsletterUnsubscribeSet);
                sets.add(newsletterSoftbounceSet);
                sets.add(newsletterHardbounceSet);

                for (LineDataSet set : sets) {
                    set.setDrawCircles(false);
                    set.setDrawFilled(true);
                    set.setFillAlpha(0);
                    set.setMode(LineDataSet.Mode.HORIZONTAL_BEZIER);
                    set.setDrawHighlightIndicators(true);
                    set.setDrawHorizontalHighlightIndicator(false);
                    set.setDrawVerticalHighlightIndicator(false);
                }


                ArrayList<ILineDataSet> dataSets = new ArrayList<ILineDataSet>();

                dataSets.add(newsletterOpenSet);
                dataSets.add(newsletterClickSet);
                dataSets.add(newsletterUnsubscribeSet);
                dataSets.add(newsletterHardbounceSet);
                dataSets.add(newsletterSoftbounceSet);

                final LineData data = new LineData(dataSets);
                data.setDrawValues(false);


                final Activity activity = getActivity();
                if (activity != null) {
                    final Double finalAll = all;
                    final boolean finalCacheUsed = cacheUsed;
                    final float finalMaxValue = maxValue;
                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            emailCampaignStatsLineChart.update(data, finalAll, labels, lineLabels, isCampaignAnalyticsInitialized, finalCacheUsed);
                            emailCampaignStatsLineChart.getChart().getAxisLeft().setAxisMinimum(-0.1f * finalMaxValue);
                            emailCampaignStatsLineChart.getChart().invalidate();
                            isCampaignAnalyticsInitialized = true;
                            Log.v("minimumvalue", (-0.05f * finalMaxValue) + "");
                        }
                    });
                }
            } catch (IOException | NullPointerException | IllegalStateException e) {
                e.printStackTrace();
            }
            return "ok";
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            Handler myHandler = new Handler();
            myHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    Fragment fragment = getActivity().getSupportFragmentManager().findFragmentById(R.id.mainContainer);
                    if (getActivity() != null && fragment instanceof EmailCampaignStatisticsFragment) {
                        new EmailCampaignStatisticsFragment.GetEmailAnalytics().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                    }
                }
            }, 3000);

        }

        private String createCacheKey(String key) {
            return key;
        }
    }
}
package com.synerise.synerise.app.model.campaign;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;

public class EmailCampaign extends Campaign implements Parcelable {

    private Date created;
    private int hardbounce;
    private String hash;
    private int opened;
    private int sent;
    private int softbounce;
    private int uniqueClicked;
    private int uniqueOpened;
    private int numberOrders;
    private int numberProductsOrder;
    private double totalAmountOrder;

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public int getHardbounce() {
        return hardbounce;
    }

    public void setHardbounce(int hardbounce) {
        this.hardbounce = hardbounce;
    }

    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    public int getOpened() {
        return opened;
    }

    public void setOpened(int opened) {
        this.opened = opened;
    }


    public int getSent() {
        return sent;
    }

    public void setSent(int sent) {
        this.sent = sent;
    }

    public int getSoftbounce() {
        return softbounce;
    }

    public void setSoftbounce(int softbounce) {
        this.softbounce = softbounce;
    }

    public int getUniqueClicked() {
        return uniqueClicked;
    }

    public void setUniqueClicked(int uniqueClicked) {
        this.uniqueClicked = uniqueClicked;
    }

    public int getUniqueOpened() {
        return uniqueOpened;
    }

    public void setUniqueOpened(int uniqueOpened) {
        this.uniqueOpened = uniqueOpened;
    }

    public int getNumberOrders() {
        return numberOrders;
    }

    public void setNumberOrders(int numberOrders) {
        this.numberOrders = numberOrders;
    }

    public int getNumberProductsOrder() {
        return numberProductsOrder;
    }

    public void setNumberProductsOrder(int numberProductsOrder) {
        this.numberProductsOrder = numberProductsOrder;
    }

    public double getTotalAmountOrder() {
        return totalAmountOrder;
    }

    public void setTotalAmountOrder(double totalAmountOrder) {
        this.totalAmountOrder = totalAmountOrder;
    }

    public EmailCampaign(JSONObject jsonObject) {
        super(jsonObject);
        try {
            created = new Date(jsonObject.getLong("created") * 1000);
            hardbounce = jsonObject.getInt("hardbounce");
            hash = jsonObject.getString("hash");
            opened = jsonObject.getInt("opened");
            sent = jsonObject.getInt("sent");
            softbounce = jsonObject.getInt("softbounce");
            uniqueClicked = jsonObject.getInt("uniqueClicked");
            uniqueOpened = jsonObject.getInt("uniqueOpened");

            numberOrders = jsonObject.getInt("numberOrders");
            if (jsonObject.has("numberProductsOrder") && !jsonObject.isNull("numberProductsOrder")) {
                numberProductsOrder = jsonObject.getInt("numberProductsOrder");
            }
            if (jsonObject.has("totalAmountOrder") && !jsonObject.isNull("totalAmountOrder")) {
                totalAmountOrder = jsonObject.getInt("totalAmountOrder");
            }
        } catch (JSONException | NullPointerException e) {
//            e.printStackTrace();
        }
    }


    public void updateFromReport(JSONObject jsonObject) {
        try {

            Log.v("updateFromReport", jsonObject.toString());

            JSONObject statistics = jsonObject.getJSONObject("statistics");

            numberOrders = statistics.getInt("numberOrders");
            numberProductsOrder = statistics.getInt("numberProductsOrder");
            totalAmountOrder = statistics.getDouble("totalAmountOrder");

            dateSend = new Date(jsonObject.getLong("sentDate"));
            recipients = new CampaignRecipients(jsonObject.getJSONObject("recipients"));
            status = jsonObject.getString("status");

            hardbounce = statistics.getInt("hardbounced");
            softbounce = statistics.getInt("softbounced");
            opened = statistics.getInt("opened");
            sent = statistics.getInt("sent");
            if (statistics.has("title")) {
                title = statistics.getString("title");
            }
            uniqueClicked = statistics.getInt("uniqueClick");
            uniqueOpened = statistics.getInt("uniqueOpened");

        } catch (JSONException e) {
            e.printStackTrace();
        } catch (NullPointerException e) {
            //ignore
        }
    }

    protected EmailCampaign(Parcel in) {
        super(in);

        created = (Date)in.readSerializable();
        hardbounce = in.readInt();
        hash = in.readString();
        opened = in.readInt();
        sent = in.readInt();
        softbounce = in.readInt();
        uniqueClicked = in.readInt();
        uniqueOpened = in.readInt();
        numberOrders = in.readInt();
        numberProductsOrder = in.readInt();
        totalAmountOrder = in.readDouble();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);

        dest.writeSerializable(created);
        dest.writeInt(hardbounce);
        dest.writeString(hash);
        dest.writeInt(opened);
        dest.writeInt(sent);
        dest.writeInt(softbounce);
        dest.writeInt(uniqueClicked);
        dest.writeInt(uniqueOpened);
        dest.writeInt(numberOrders);
        dest.writeInt(numberProductsOrder);
        dest.writeDouble(totalAmountOrder);
    }

    @Override
    public int describeContents() {
        return super.describeContents();
    }

}

package com.synerise.synerise.app.syneview;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter;
import com.synerise.synerise.R;
import com.synerise.synerise.app.syneutil.SyneFillFormatter;

import java.util.List;

/**
 * Created by dsiedlarz on 22.02.2017.
 */

public class SyneComplexLineChart extends RelativeLayout {
    View view;

    SyneChartTitle title;
    LineChart chart;

    private static Typeface tf;
    static int fancyGray = Color.parseColor("#C7C7C7");

    public SyneChartTitle getTitle() {
        return title;
    }

    public LineChart getChart() {
        return chart;
    }

    public SyneComplexLineChart(Context context, AttributeSet attrs) {
        super(context, attrs);
        initViews();
        TypedArray a = context.getTheme().obtainStyledAttributes(
                attrs,
                R.styleable.SyneComplexLineChart,
                0, 0);

        try {
            String titleText = a.getString(R.styleable.SyneComplexLineChart_titleText);
            String unitText = a.getString(R.styleable.SyneComplexLineChart_unitText);

            title.setTitle(titleText);
            title.setUnit(unitText);

        } finally {
            a.recycle();
        }
        tf = Typeface.createFromAsset(getContext().getAssets(),
                "fonts/Lato-Thin.ttf");

        initLineChart(chart);
    }


    private void initViews() {
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view = inflater.inflate(R.layout.syne_complex_line_chart, this, true);

        chart = (LineChart) view.findViewById(R.id.complexChart);
        title = (SyneChartTitle) view.findViewById(R.id.complexTitle);

    }

    public static void initLineChart(LineChart chart) {
        chart.setDescription(new Description());
        chart.getDescription().setEnabled(false);
        chart.setDrawGridBackground(false);
        chart.setDrawGridBackground(false);
        chart.getXAxis().setTypeface(tf);
        chart.getXAxis().setTextColor(fancyGray);
        chart.getAxisLeft().setDrawGridLines(true);
        chart.getAxisLeft().setGridColor(fancyGray);
        chart.getAxisLeft().setDrawAxisLine(false);
        chart.getAxisLeft().setTypeface(tf);
        chart.getAxisLeft().setTextColor(fancyGray);
        chart.getAxisLeft().setDrawGridLines(false);
        chart.getXAxis().setDrawGridLines(false);
        chart.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM);
        chart.getXAxis().setLabelCount(3);
        chart.getAxisRight().setDrawAxisLine(false);
        chart.getAxisRight().setEnabled(false);
        chart.getAxisRight().setDrawGridLines(false);
        chart.setDrawBorders(false);
        chart.getAxisRight().setEnabled(false);


        XAxis xAxis = chart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        chart.setBackgroundColor(Color.parseColor("#FFFFFF"));
        chart.setDrawGridBackground(false);

        chart.setViewPortOffsets(0f, 40f, 0f, 40f);

        chart.getAxisLeft().setStartAtZero(false);
        chart.getAxisLeft().setAxisMinimum(-10);
        chart.getAxisLeft().setPosition(YAxis.YAxisLabelPosition.INSIDE_CHART);
        chart.getAxisLeft().setYOffset(-10);

        Legend l = chart.getLegend();
        l.setEnabled(false);


    }

    public void update(LineData data, Double counter, List<String> xLabels, String[] lineLabels, boolean isChartInitialized, boolean isCacheUsed) {

        title.setDoubleCounter(counter);
        LineChartMarkerView mv = new LineChartMarkerView(getContext(), R.layout.custom_marker_view);
        mv.setChartView(chart); // For bounds control
        mv.setDates(xLabels);
        mv.setLabels(lineLabels);
        chart.setMarker(mv);
        chart.setData(data);

        chart.getXAxis().setValueFormatter(new IndexAxisValueFormatter(xLabels));


        if (!isChartInitialized) {
            chart.invalidate();

            if (isCacheUsed) {
                chart.setAlpha(0.4f);
            }
        } else {
            chart.setAlpha(1f);

            chart.notifyDataSetChanged();
        }
    }

    public static void initLineDataSet(LineDataSet set) {
        set.setDrawCircles(false);
        set.setFillFormatter(new SyneFillFormatter());
        set.setDrawFilled(true);
        set.setFillAlpha(10);
        set.setMode(LineDataSet.Mode.CUBIC_BEZIER);
        set.setDrawHighlightIndicators(true);
        set.setDrawHorizontalHighlightIndicator(false);
        set.setDrawVerticalHighlightIndicator(false);
    }
}

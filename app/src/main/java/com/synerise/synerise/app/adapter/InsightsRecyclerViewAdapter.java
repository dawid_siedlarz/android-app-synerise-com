package com.synerise.synerise.app.adapter;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.synerise.synerise.InsightPreview;
import com.synerise.synerise.MainActivity;
import com.synerise.synerise.R;
import com.synerise.synerise.app.model.FilterAttributes;
import com.synerise.synerise.app.model.Insight;

import java.util.ArrayList;


public class InsightsRecyclerViewAdapter extends RecyclerView.Adapter<InsightsRecyclerViewAdapter.ClientViewHolder> {
    private final Context context;
    private final Activity activity;
    private final ArrayList<Insight> values;

    private FilterAttributes.FilterAttribute filterAttribute;


    public InsightsRecyclerViewAdapter(Activity activity, ArrayList<Insight> values) {
        this.activity = activity;
        this.context = activity.getApplicationContext();
        this.values = values;
    }

    @Override
    public ClientViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_element_insight, null);
        ClientViewHolder viewHolder = new ClientViewHolder(view);
        return viewHolder;
    }



    @Override
    public void onBindViewHolder(ClientViewHolder holder, final int position) {

        final Insight insihgtModel = values.get(position);
        holder.mInsight = insihgtModel;
        holder.mCategory.setText(insihgtModel.insightcategory);
        holder.mTitle.setText(insihgtModel.title);
        holder.mDesc.setText(insihgtModel.desc );
        holder.mImage.setImageResource(insihgtModel.insightcategory.compareToIgnoreCase("customer") == 0 ? R.drawable.customer : R.drawable.time);

        holder.mView.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Fragment newFragment = new InsightPreview();
                Bundle bundle = new Bundle();
                bundle.putParcelable(InsightPreview.ARG_INSIGHT, insihgtModel);
                newFragment.setArguments(bundle);

                MainActivity.addFragment(newFragment);
            }
        });
    }

    @Override
    public int getItemCount() {
        return values.size();
    }

//    @Override
//    public View getView(int position, View convertView, ViewGroup parent) {
//        LayoutInflater inflater = (LayoutInflater) context
//                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//
//        filterAttribute = FilterAttributes.ITEMS.get(position);
//        View rowView = inflater.inflate(R.layout.list_element_filter, parent, false);
//        TextView mTagName = (TextView) rowView.findViewById(R.id.filterName);
//
//        mTagName.setText(filterAttribute.name);
//        return rowView;
//    }

    class ClientViewHolder extends RecyclerView.ViewHolder {
        private Insight mInsight;
        private TextView mCategory;
        private TextView mTitle;
        private TextView mDesc;
        private ImageView mImage;
        private View mView;


        public ClientViewHolder(View view) {
            super(view);
            this.mView = view;

            this.mCategory = (TextView) view.findViewById(R.id.insightCategory);
            this.mTitle = (TextView) view.findViewById(R.id.insightTitle);
            this.mDesc = (TextView) view.findViewById(R.id.insightDesc);
            this.mImage = (ImageView) view.findViewById(R.id.insightImage);
        }
    }


//    private class LoadImageInBackground extends AsyncTask<InsightsRecyclerViewAdapter.ClientViewHolder, Void, Void> {
//
//        @Override
//        protected Void doInBackground(ClientViewHolder... params) {
//
//            final ClientViewHolder holder = params[0];
//            Bitmap bmp = null;
//
//       String url = "";
//
//            Client.ClientModel clientModel = holder.mItem;
//                try {
//                    if(clientModel.externalAvatarUrl!= null && clientModel.externalAvatarUrl.compareToIgnoreCase("null") != 0){
//                        url = clientModel.externalAvatarUrl;
//                    } else {
//                        url = clientModel.avatarUrl;
//                    }
//                    bmp = BitmapFactory.decodeStream(new URL(url).openConnection().getInputStream());
//
//// bmp = Bitmap.createScaledBitmap(bmp, 150, 150, false);
//                } catch (MalformedURLException e) {
//                    Log.v("SYNERISE", "Malformed URL " + url);
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//
//            final Bitmap finalBmp = bmp;
//            clientModel.avatarBmp = finalBmp;
//
//            if (activity != null) {
//
//                activity.runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
//                        try{
//                            holder.mAvatarImage.setImageBitmap(finalBmp);
//                        }
//                        catch (IndexOutOfBoundsException e){
//                            //ignore
//                        }
//                    }
//                });
//
//            }
//            return null;
//        }
//
//    }
//
//
//    private void loadAvatarImage(ClientViewHolder holder){
//        if (holder.mItem.avatarBmp != null){
//            holder.mAvatarImage.setImageBitmap(holder.mItem.avatarBmp);
//        }
//        else {
//            new LoadImageInBackground().execute(holder);
//        }
//    }



}

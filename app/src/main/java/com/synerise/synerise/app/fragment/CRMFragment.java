package com.synerise.synerise.app.fragment;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;

import com.github.clans.fab.FloatingActionButton;
import com.synerise.sdk.tracking.Tracker;
import com.synerise.sdk.tracking.TrackerParams;
import com.synerise.synerise.MainActivity;
import com.synerise.synerise.R;
import com.synerise.synerise.SyneriseOnFragmentInteractionListener;
import com.synerise.synerise.app.ActionBarConfigurableFragment;
import com.synerise.synerise.app.adapter.ClientRecyclerViewAdapter;
import com.synerise.synerise.app.adapter.SimpleSegmentsArrayAdapter;
import com.synerise.synerise.app.adapter.SimpleSelectedFiltersArrayAdapter;
import com.synerise.synerise.app.adapter.SimpleSelectedSegmentsArrayAdapter;
import com.synerise.synerise.app.adapter.SimpleSelectedSmartlistsArrayAdapter;
import com.synerise.synerise.app.adapter.SimpleSelectedTagsArrayAdapter;
import com.synerise.synerise.app.adapter.SimpleSmartlistsArrayAdapter;
import com.synerise.synerise.app.adapter.SimpleTagsArrayAdapter;
import com.synerise.synerise.app.crmFilters.CreatedDate;
import com.synerise.synerise.app.crmFilters.DateOfLastTransaction;
import com.synerise.synerise.app.crmFilters.Filters;
import com.synerise.synerise.app.crmFilters.LastActivityDate;
import com.synerise.synerise.app.crmFilters.SegmentsActivity;
import com.synerise.synerise.app.crmFilters.TagsActivity;
import com.synerise.synerise.app.model.Client;
import com.synerise.synerise.app.model.FilterAttributes;
import com.synerise.synerise.app.model.Segment;
import com.synerise.synerise.app.model.SegmentSelected;
import com.synerise.synerise.app.model.Smartlist;
import com.synerise.synerise.app.model.SmartlistSelected;
import com.synerise.synerise.app.model.Tag;
import com.synerise.synerise.app.model.TagSelected;
import com.synerise.synerise.app.syneutil.EndlessRecyclerViewScrollListener;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

public class CRMFragment extends ActionBarConfigurableFragment {


    //event list
    public static ClientRecyclerViewAdapter adapter;
    private RecyclerView listView;
    private LinearLayoutManager linearLayoutManager;
    private RecyclerView.OnScrollListener scrollListener;

    private View view;

    private ArrayList<Client> clients;

    private String listing_Order_orderColumn = "u.last_activity_date";
    private String listing_Order_orderType = "desc";
    public static String listing_SimpleFilter_simpleFilterValue = null;
    public static ArrayList<String> listingChooseTagchooseColumnValue = new ArrayList<String>();
    public static ArrayList<String> listingChooseSmartSegmentchooseColumnValue = new ArrayList<String>();
    private String listing_ChooseAnonymousTraffic_chooseColumnValue = null;
    private String listingSmartlist = null;
    private String listing_MultipleColumnChoose_chooseColumnValue = null;
    private String listing_Paginator_offset = "0";
    private String listing_Paginator_limit = "50";
    public static String listing_anonymous_type = "IDENTIFIED";
    //Date of last transaction
    public static String listing_last_transaction_dateOption = null;
    public static String listing_last_transaction_relativeDateValue = null;
    public static String listing_last_transaction_absoluteDateValue = null;
    public static String listing_last_transaction_typeOption = null;
    //last activity date
    public static String listing_activity_option = null;
    public static String listing_activity_relativeValue = null;
    public static String listing_activity_absoluteValue = null;

    public static ArrayList<FilterAttributes.FilterAttribute> attributes = new ArrayList<>();


    private ListView selectedTagsList;
    private SimpleSelectedTagsArrayAdapter selectedTagsArrayAdapter;
    private Button chooseTagButton;
    private SimpleTagsArrayAdapter tagsAdapter;
    private Dialog tagsDialog;
    private CardView tagBar = null;

    private ListView selectedSegmentsList;
    private SimpleSelectedSegmentsArrayAdapter selectedSegmentsArrayAdapter;
    private Button chooseSegmentButton;
    private SimpleSegmentsArrayAdapter segmentsAdapter;
    private Dialog segmentsDialog;
    private CardView segmentBar = null;


    private ListView selectedSmartlistsList;
    private SimpleSelectedSmartlistsArrayAdapter selectedSmartlistsArrayAdapter;
    private Button chooseSmartlistButton;
    private SimpleSmartlistsArrayAdapter smartlistsAdapter;
    private Dialog smartlistsDialog;
    private CardView smartlistBar = null;

    private ListView selectedFiltersList;
    private SimpleSelectedFiltersArrayAdapter selectedFiltersArrayAdapter;


    private int counter = 0;
    private int barHeight = 0;
    private DecimalFormat formatter = new DecimalFormat("###,###,###");


    private SyneriseOnFragmentInteractionListener mListener;

    public CRMFragment() {
    }

    public static CRMFragment newInstance(String param1, String param2) {
        CRMFragment fragment = new CRMFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_crm_with_fab, container, false);

        return view;
    }


    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof SyneriseOnFragmentInteractionListener) {
            mListener = (SyneriseOnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void setUpActionBar() {
        ActionBar actionBar = (ActionBar) ((AppCompatActivity) getActivity()).getSupportActionBar();
        if (actionBar != null) {
            actionBar.show();
            actionBar.setTitle(MainActivity.businessProfileName != null ? MainActivity.businessProfileName : "Synerise");
            actionBar.setSubtitle("CRM");
            actionBar.setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.colorPrimaryCRM)));
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                Window window = getActivity().getWindow();
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                window.setStatusBarColor(getResources().getColor(R.color.colorPrimaryCRM));
            }
            getActivity().setTheme(R.style.AppThemeCRM);
            actionBar.setElevation(0);

            if (MainActivity.searchItem != null) {
                MainActivity.searchItem.setVisible(true);
            }
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Tracker.trackScreen("CRM", new TrackerParams[]{});
        resetListing();

        initList();
        new GetClients().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

        barHeight = Float.valueOf(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 60, getResources().getDisplayMetrics())).intValue();

//        new GetSmartlists().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

        View rootView = getView();
        if (rootView == null) {
            return;
        }
        tagBar = (CardView) rootView.findViewById(R.id.crmTagBar);
        selectedTagsList = (ListView) rootView.findViewById(R.id.selectedTagsListView);
        selectedTagsArrayAdapter = new SimpleSelectedTagsArrayAdapter(getContext(), TagSelected.ITEMS);
        chooseTagButton = (Button) rootView.findViewById(R.id.chooseTagButton);
        tagsAdapter = new SimpleTagsArrayAdapter(getContext(), Tag.ITEMS);
        selectedTagsList.setAdapter(selectedTagsArrayAdapter);
        tagsDialog = new AlertDialog.Builder(getContext())
                .setTitle("Tags")
                .setAdapter(tagsAdapter, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int position) {

                        listingChooseTagchooseColumnValue.add(Tag.ITEMS.get(position).name);
                        new GetClients().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

                        dialog.dismiss();
                    }
                }).create();

        chooseTagButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tagsDialog.show();
            }
        });

        selectedTagsList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                listingChooseTagchooseColumnValue.remove(position);
                new GetClients().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            }
        });


        segmentBar = (CardView) rootView.findViewById(R.id.crmSegmentBar);
        selectedSegmentsList = (ListView) rootView.findViewById(R.id.selectedSegmentsListView);
        selectedSegmentsArrayAdapter = new SimpleSelectedSegmentsArrayAdapter(getContext(), SegmentSelected.ITEMS);
        chooseSegmentButton = (Button) rootView.findViewById(R.id.chooseSegmentButton);
        segmentsAdapter = new SimpleSegmentsArrayAdapter(getContext(), Segment.ITEMS);
        selectedSegmentsList.setAdapter(selectedSegmentsArrayAdapter);
        segmentsDialog = new AlertDialog.Builder(getContext())
                .setTitle("Smart segments")
                .setAdapter(segmentsAdapter, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int position) {

                        listingChooseSmartSegmentchooseColumnValue.add(Segment.ITEMS.get(position).name);
                        new GetClients().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

                        dialog.dismiss();
                    }
                }).create();

        chooseSegmentButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                segmentsDialog.show();
            }
        });

        selectedSegmentsList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                listingChooseSmartSegmentchooseColumnValue.remove(SegmentSelected.ITEMS.get(position).name);
                new GetClients().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            }
        });


        smartlistBar = (CardView) rootView.findViewById(R.id.crmSmartlistBar);
        selectedSmartlistsList = (ListView) rootView.findViewById(R.id.selectedSmartlistsListView);
        selectedSmartlistsArrayAdapter = new SimpleSelectedSmartlistsArrayAdapter(getContext(), SmartlistSelected.ITEMS);
        chooseSmartlistButton = (Button) rootView.findViewById(R.id.chooseSmartlistButton);
        smartlistsAdapter = new SimpleSmartlistsArrayAdapter(getContext(), Smartlist.ITEMS);
        selectedSmartlistsList.setAdapter(selectedSmartlistsArrayAdapter);
        smartlistsDialog = new AlertDialog.Builder(getContext())
                .setTitle("Smart smartlists")
                .setAdapter(smartlistsAdapter, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int position) {

                        listingSmartlist = Smartlist.ITEMS.get(position).id;
                        new GetClients().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

                        dialog.dismiss();
                    }
                }).create();

        chooseSmartlistButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                smartlistsDialog.show();
            }
        });


        selectedFiltersList = (ListView) rootView.findViewById(R.id.selectedFiltersListView);
        selectedFiltersArrayAdapter = new SimpleSelectedFiltersArrayAdapter(getContext(), CRMFragment.attributes);
        selectedFiltersList.setAdapter(selectedFiltersArrayAdapter);

        selectedFiltersList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                attributes.remove(position);
                selectedFiltersArrayAdapter.notifyDataSetChanged();
                new GetClients().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            }
        });

        ((FloatingActionButton) rootView.findViewById(R.id.crmMenuTags)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), TagsActivity.class);
                getActivity().startActivity(intent);
            }
        });

        ((FloatingActionButton) rootView.findViewById(R.id.crmMenuSegments)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), SegmentsActivity.class);
                getActivity().startActivity(intent);
            }
        });

        ((FloatingActionButton) rootView.findViewById(R.id.crmMenuSmartlists)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                if (smartlistBar.getLayoutParams().height == 0) {
//                    smartlistBar.getLayoutParams().height = barHeight;
//                } else {
//                    smartlistBar.getLayoutParams().height = 0;
//                }
//                smartlistBar.requestLayout();
            }
        });

        ((FloatingActionButton) rootView.findViewById(R.id.crmMenuDateOfLastTransaction)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), DateOfLastTransaction.class);
                startActivity(intent);
            }
        });
        ((FloatingActionButton) rootView.findViewById(R.id.crmMenuLastActivityDate)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), LastActivityDate.class);
                startActivity(intent);
            }
        });
        ((FloatingActionButton) rootView.findViewById(R.id.crmMenuCreatedDate)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), CreatedDate.class);
                startActivity(intent);
            }
        });
        ((FloatingActionButton) rootView.findViewById(R.id.crmMenuFilters)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), Filters.class);
                startActivity(intent);
            }
        });

    }

    private void initList() {

//        builder = new AlertDialog.Builder(getActivity());
//        wv = new WebView(getContext());
//        dialog = builder.create();
//        builder.setView(wv);

        clients = new ArrayList<>();
        adapter = new ClientRecyclerViewAdapter(getActivity(), clients);
        listView = view.findViewById(R.id.crmListView);
        linearLayoutManager = new LinearLayoutManager(getActivity());
        listView.setLayoutManager(linearLayoutManager);
        listView.setAdapter(adapter);
        scrollListener = new EndlessRecyclerViewScrollListener(linearLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                // Triggered only when new data needs to be appended to the list
                // Add whatever code is needed to append new items to the bottom of the list
                Log.v("GetMoreClients", "GetMoreClients");
                new GetMoreClients().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            }
        };
        listView.addOnScrollListener(scrollListener);

//        listView.setFocusable(false);


    }

    public void clearClientsArray() {
        this.clients.clear();
    }

    public void getClients() {
        new GetClients().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    private class GetClients extends AsyncTask<Void, Void, String> {
        protected String doInBackground(Void... params) {
            try {

                listing_Paginator_offset = "0";
                counter = 0;

                HttpPost httpost = new HttpPost("https://app.synerise.com/rest/crm/list");
                httpost.setEntity(new UrlEncodedFormEntity(getPostParams(), HTTP.UTF_8));

                HttpResponse eventList = MainActivity.httpClient.execute(httpost);
                if (eventList == null) {
                    return "not OK";
                }

                HttpEntity eventListEntity = eventList.getEntity();

                final String body = EntityUtils.toString(eventListEntity);


                Log.v("CRM_GetClients", body);

                JSONObject jsonObject = null;


                try {
                    jsonObject = new JSONObject(body);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if (jsonObject == null) {
                    return null;
                }

                JSONArray data = jsonObject.getJSONArray("data");

                ArrayList<Client> newClients = new ArrayList<>();

                for (int i = 0; i < data.length(); i++) {
                    newClients.add(new Client(data.getJSONObject(i)));
                }

                clients.clear();
                clients.addAll(newClients);

                counter = jsonObject.has("recordsFiltered") ? jsonObject.getInt("recordsFiltered") : 0;
                reloadLists();


            } catch (IOException | NullPointerException | IllegalStateException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                reloadLists();
                e.printStackTrace();
            }
            return "ok";
        }
    }


    private void reloadLists() {
        Activity activity = getActivity();
        if (activity != null) {

            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    ((ActionBar) ((AppCompatActivity) getActivity()).getSupportActionBar()).setSubtitle(formatter.format(counter));

                    Log.v("__clients__", clients.toString());
                    adapter.notifyDataSetChanged();
                }
            });
        }
    }

    private class GetMoreClients extends AsyncTask<Void, Void, String> {
        protected String doInBackground(Void... params) {
            try {
                listing_Paginator_offset = (Integer.valueOf(listing_Paginator_offset) + 50) + "";

                HttpPost httpost = new HttpPost("https://app.synerise.com/rest/crm/list");
                httpost.setEntity(new UrlEncodedFormEntity(getPostParams(), HTTP.UTF_8));


                HttpResponse eventList = MainActivity.httpClient.execute(httpost);

                Log.v("GetMoreClients", eventList.toString());

                if (eventList == null) {
                    return "not OK";
                }

                HttpEntity eventListEntity = eventList.getEntity();

                final String body = EntityUtils.toString(eventListEntity);

                Log.v("GetMoreClients body", body);

                JSONObject jsonObject = null;

                try {
                    jsonObject = new JSONObject(body);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if (jsonObject == null) {
                    return null;
                }

                JSONArray data = jsonObject.getJSONArray("data");

                ArrayList<Client> newClients = new ArrayList<>();

                for (int i = 0; i < data.length(); i++) {
                    newClients.add(new Client(data.getJSONObject(i)));
                }

                clients.addAll(newClients);

                Activity activity = getActivity();
                if (activity != null) {
                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            adapter.notifyDataSetChanged();
                        }
                    });
                }

            } catch (IOException | JSONException e) {
                e.printStackTrace();
            } catch (NullPointerException | IllegalStateException e) {

            }
            return "ok";
        }
    }

    private List<NameValuePair> getPostParams() {
        List<NameValuePair> nvps = new ArrayList<NameValuePair>();
        nvps.add(new BasicNameValuePair("start", listing_Paginator_offset));
        nvps.add(new BasicNameValuePair("length", listing_Paginator_limit));
        nvps.add(new BasicNameValuePair("search[value]", listing_SimpleFilter_simpleFilterValue));
        nvps.add(new BasicNameValuePair("search[regex]", "false"));
        nvps.add(new BasicNameValuePair("order[0][column]", "7"));
        nvps.add(new BasicNameValuePair("order[0][dir]", "desc"));

        return nvps;
    }

    private void resetListing() {
        Log.v("CRM", "resetListing");
        listing_SimpleFilter_simpleFilterValue = "";
        listing_Paginator_offset = "0";
    }
}
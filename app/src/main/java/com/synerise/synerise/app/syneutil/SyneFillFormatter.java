package com.synerise.synerise.app.syneutil;

import com.github.mikephil.charting.formatter.IFillFormatter;
import com.github.mikephil.charting.interfaces.dataprovider.LineDataProvider;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;

/**
 * Created by dsiedlarz on 26.02.2017.
 */

public class SyneFillFormatter implements IFillFormatter {
    @Override
    public float getFillLinePosition(ILineDataSet dataSet, LineDataProvider dataProvider) {
        return -10000000f;
    }
}

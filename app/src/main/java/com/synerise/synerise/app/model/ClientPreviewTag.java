package com.synerise.synerise.app.model;

import java.util.ArrayList;

public class ClientPreviewTag {

    public static final ArrayList<Tag> ITEMS = new ArrayList<Tag>();

    public static void add(String id, String name) {
        ITEMS.add(new Tag(id, name));
    }

    public static void clear() {
        ITEMS.clear();

    }

    public static class Tag {
        public String id;
        public String name;

        public Tag( String id, String name) {
            this.id = id;
            this.name = name;
        }

        @Override
        public String toString() {
            return "#" + name;
        }
    }

}

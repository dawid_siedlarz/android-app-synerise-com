package com.synerise.synerise.app.model;

import java.util.ArrayList;

public class SmartlistSelected {

    public static final ArrayList<Smartlist.SmartlistModel> ITEMS = new ArrayList<Smartlist.SmartlistModel>();

    public static void add(String name, String id) {
        ITEMS.add(new Smartlist.SmartlistModel(name, id));
    }

    public static void clear() {
        ITEMS.clear();
    }


}

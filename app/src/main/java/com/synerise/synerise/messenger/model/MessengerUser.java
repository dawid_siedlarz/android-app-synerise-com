package com.synerise.synerise.messenger.model;

import android.graphics.Bitmap;
import android.util.Log;
import android.widget.RelativeLayout;

import com.synerise.synerise.R;
import com.synerise.synerise.messenger.fragment.MessengerFragment;
import com.synerise.synerise.app.syneutil.SyneHttpCacheHandler;
import com.synerise.synerise.app.syneview.SyneTextViewWhite;

import org.apache.http.client.methods.HttpGet;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by dsiedlarz on 28.12.2016.
 */

public class MessengerUser {
    static final String USER_TYPE_VENDORS = "vendors";
    static final String USER_TYPE_CLIENTS = "clients";

    private String userType;
    private int id;
    private String name;
    private String email;
    private String avatar;
    private String type;
    private Bitmap avatarBitmap;

    public Bitmap getAvatarBitmap() {
        return avatarBitmap;
    }

    public void setAvatarBitmap(Bitmap avatarBitmap) {
        this.avatarBitmap = avatarBitmap;
    }

    public String getInitials() {
        return initials;
    }

    public void setInitials(String initials) {
        this.initials = initials;
    }

    public boolean isDataDownloaded() {
        return isDataDownloaded;
    }

    public void setDataDownloaded(boolean dataDownloaded) {
        isDataDownloaded = dataDownloaded;
    }

    private String initials;

    private boolean isDataDownloaded = false;

    MessengerUser(JSONObject object) {
        parseJsonObject(object);
    }


    public void parseJsonObject(JSONObject object) {
        System.out.println(object.toString());
        try {
            if (object.has("id")) {
                id = object.getInt("id");
            }
            if (object.has("attributes")) {
                JSONObject attributes = object.getJSONObject("attributes");
                if (attributes.has("name")) {
                    name = attributes.getString("name");
                }
                if (attributes.has("email")) {
                    email = attributes.getString("email");
                }
                if (attributes.has("avatar")) {
                    avatar = attributes.getString("avatar");
                }
                if (attributes.has("type")) {
                    type = attributes.getString("type");
                }
                if (attributes.has("initials")) {
                    initials = attributes.getString("initials");
                }
            }
            if (object.has("data")) {
                JSONObject data = object.getJSONObject("data");
                if (data.has("type")) {
                    type = data.getString("type");
                }
                if (data.has("id")) {
                    id = data.getInt("id");
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public String getUserType() {
        return userType;
    }

    void setUserType(String userType) {
        this.userType = userType;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getNameFromApi() {
        String name = "";
//        Log.v("OwnerType", "o: " + type);
        String urlBase = "";
        switch (type) {
            case "client":
                urlBase = MessengerFragment.API_ENDPOINT + "/clients/";
                break;
            case "vendor":
                urlBase = MessengerFragment.API_ENDPOINT + "/vendors/";
                break;
            default:
                return "N/A";
        }


        HttpGet httpget = new HttpGet(urlBase + id);

        SyneHttpCacheHandler handler = new SyneHttpCacheHandler();
//        Log.v("body", urlBase + id);
        final String body = handler.executeWithCache(httpget, true, true);
        if (body == null) {
            return "N/A";
        }
//        Log.v("body", body);

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject = new JSONObject(body).getJSONObject("data");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        MessengerClient client = new MessengerClient(jsonObject);

        return client.getName();

    }


    public void setUserAvatar(RelativeLayout layout) {
        Log.v("setUserAvatar", avatar);
        if (avatar != null && avatar.compareToIgnoreCase("null") != 0) {

        } else {
            Log.v("setUserAvatar", "setBackground");

            layout.setBackgroundResource(R.drawable.avatar_messenger);
            SyneTextViewWhite text = new SyneTextViewWhite(layout.getContext());
            text.setText(initials);

            RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) layout.getLayoutParams();
            params.addRule(RelativeLayout.CENTER_HORIZONTAL);
            params.addRule(RelativeLayout.CENTER_VERTICAL);
            text.setLayoutParams(params);
            layout.removeAllViewsInLayout();
            layout.addView(text);
        }
    }


}

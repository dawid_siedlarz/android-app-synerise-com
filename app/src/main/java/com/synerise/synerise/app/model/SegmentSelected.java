package com.synerise.synerise.app.model;

import java.util.ArrayList;

public class SegmentSelected {

    public static final ArrayList<Segment.SegmentModel> ITEMS = new ArrayList<Segment.SegmentModel>();

    public static void add(String name, String count) {
        ITEMS.add(new Segment.SegmentModel(name, count));
    }

    public static void clear() {
        ITEMS.clear();
    }


}

package com.synerise.synerise;

import android.net.Uri;

/**
 * Created by dsiedlarz on 15.10.2016.
 */

public interface SyneriseOnFragmentInteractionListener {
    // TODO: Update argument type and name
    void onFragmentInteraction(Uri uri);
}
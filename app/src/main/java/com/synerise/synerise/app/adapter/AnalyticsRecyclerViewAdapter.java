package com.synerise.synerise.app.adapter;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.synerise.synerise.MainActivity;
import com.synerise.synerise.R;
import com.synerise.synerise.app.fragment.analytics.AnalyticsPreviewFragment;
import com.synerise.synerise.app.model.AnalyticsQuery;

import java.text.SimpleDateFormat;
import java.util.List;


public class AnalyticsRecyclerViewAdapter extends RecyclerView.Adapter<AnalyticsRecyclerViewAdapter.ViewHolder> {

    private final List<AnalyticsQuery> mValues;
    private final Fragment fragment;

    private Drawable icFunnels;
    private Drawable icMetrics;
    private Drawable icSegmentations;
    private Drawable icSqlSquery;
    private Drawable icTrends;

    private SimpleDateFormat dateFormat;

    public AnalyticsRecyclerViewAdapter(List<AnalyticsQuery> items, Fragment fragment) {
        mValues = items;
        this.fragment = fragment;

        icFunnels = ResourcesCompat.getDrawable(fragment.getResources(), R.drawable.ic_analytics_funnels, null);
        icMetrics = ResourcesCompat.getDrawable(fragment.getResources(), R.drawable.ic_analytics_metrics, null);
        icSegmentations = ResourcesCompat.getDrawable(fragment.getResources(), R.drawable.ic_analytics_segments, null);
        icSqlSquery = ResourcesCompat.getDrawable(fragment.getResources(), R.drawable.ic_analytics_sqlquery, null);
        icTrends = ResourcesCompat.getDrawable(fragment.getResources(), R.drawable.ic_analytics_trends, null);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_element_analytics, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final AnalyticsQuery analyticsQuery = mValues.get(position);

        holder.mItem = analyticsQuery;
        setType(holder);
        holder.analyticsTitle.setText(analyticsQuery.getQuery_name());
        holder.analyticsAuthor.setText(analyticsQuery.getAuthor_name());

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment newFragment = new AnalyticsPreviewFragment();

                Bundle bundle = new Bundle();
                bundle.putParcelable(AnalyticsPreviewFragment.ARG_QUERY, analyticsQuery);
                newFragment.setArguments(bundle);

                MainActivity.addFragment(newFragment);
            }
        });
    }

    private void setType(ViewHolder holder) {
        AnalyticsQuery analyticsQuery = holder.mItem;
        Drawable drawable;
        String name;
        switch (analyticsQuery.getQuery_type()) {
            case 0:
            drawable=icSegmentations;
                name = "Segmentation";
                break;
            case 1:
                drawable=icFunnels;
                name = "Funnel";
                break;
            case 2:
                drawable=icTrends;
                name = "Trend";
                break;
            case 3:
                drawable=icSqlSquery;
                name = "SQL query";
                break;
            case 4:
                drawable=icMetrics;
                name = "Metric";
                break;
            default:
                return;
        }
        holder.analyticsTypeIcon.setImageDrawable(drawable);
        holder.analyticsTypeName.setText(name);

    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final ImageView analyticsTypeIcon;
        public final TextView analyticsTypeName;
        public final TextView analyticsTitle;
        public final TextView analyticsAuthor;


        public AnalyticsQuery mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            analyticsTypeIcon = view.findViewById(R.id.analyticsTypeIcon);
            analyticsTypeName = view.findViewById(R.id.analyticsTypeName);
            analyticsTitle = view.findViewById(R.id.analyticsTitle);
            analyticsAuthor = view.findViewById(R.id.analyticsAuthor);
        }
    }
}


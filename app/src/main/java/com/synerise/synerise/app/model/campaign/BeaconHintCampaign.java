package com.synerise.synerise.app.model.campaign;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONException;
import org.json.JSONObject;

public class BeaconHintCampaign extends Campaign implements Parcelable {

    private int read;
    private int delivered;
    private int sent;

    public BeaconHintCampaign(JSONObject jsonObject) {
        super(jsonObject);

        try {
            read = jsonObject.getInt("read");
            delivered = jsonObject.getInt("delivered");
            sent = jsonObject.getInt("sent");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public int getRead() {
        return read;
    }

    public void setRead(int read) {
        this.read = read;
    }

    public int getDelivered() {
        return delivered;
    }

    public void setDelivered(int delivered) {
        this.delivered = delivered;
    }

    public int getSent() {
        return sent;
    }

    public void setSent(int sent) {
        this.sent = sent;
    }

    protected BeaconHintCampaign(Parcel in) {
        super(in);
        read = in.readInt();
        delivered = in.readInt();
        sent = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);

        dest.writeInt(read);
        dest.writeInt(delivered);
        dest.writeInt(sent);
    }

    @Override
    public int describeContents() {
        return super.describeContents();
    }
}

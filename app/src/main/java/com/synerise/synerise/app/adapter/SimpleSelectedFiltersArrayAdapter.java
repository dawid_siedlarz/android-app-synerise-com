package com.synerise.synerise.app.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.synerise.synerise.app.fragment.CRMFragment;
import com.synerise.synerise.R;
import com.synerise.synerise.app.model.FilterAttributes;

import java.util.ArrayList;

/**
 * Created by dsiedlarz on 15.10.2016.
 */
public class SimpleSelectedFiltersArrayAdapter extends ArrayAdapter<FilterAttributes.FilterAttribute> {
    private final Context context;
    private final ArrayList<FilterAttributes.FilterAttribute> values;
    private TextView mAvatar;
    private FilterAttributes.FilterAttribute filterAttribute;



    public SimpleSelectedFiltersArrayAdapter(Context context, ArrayList<FilterAttributes.FilterAttribute> values) {
        super(context, -1, values);
        this.context = context;
        this.values = values;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        filterAttribute = CRMFragment.attributes.get(position);
        View rowView = inflater.inflate(R.layout.list_element_filter_selected, parent, false);
        TextView mTagName = (TextView) rowView.findViewById(R.id.selectedFilterName);

        mTagName.setText(filterAttribute.name + " " + filterAttribute.name + " " + filterAttribute.value);
        return rowView;
    }

}

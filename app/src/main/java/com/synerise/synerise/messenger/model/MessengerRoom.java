package com.synerise.synerise.messenger.model;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;

public class MessengerRoom implements Parcelable {


    private String id;
    private MessengerMessage lastMessage;
    private Date lastMessageTime;
    private String lastMessageAuthorType;
    private int lastMessageAuthorId;
    //        private String ownerType;
//        private int ownerId;
    private MessengerUser owner;

    public MessengerRoom(JSONObject object) {
        try {
            id = object.getString("id");
            JSONObject attributes = object.getJSONObject("attributes");
            lastMessage = new MessengerMessage();
            lastMessage.setMessageText(attributes.getString("last-message"));

            lastMessageTime = new Date(attributes.getLong("time"));

            JSONObject relationships = object.getJSONObject("relationships");
            JSONObject lastMessageAuthor = relationships.getJSONObject("last-message-author").getJSONObject("data");

            lastMessageAuthorType = lastMessageAuthor.getString("type");
            lastMessageAuthorId = lastMessageAuthor.getInt("id");

            owner = new MessengerClient(relationships.getJSONObject("owner"));
            lastMessage.setAuthor(owner);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    protected MessengerRoom(Parcel in) {
        id = in.readString();
        lastMessageAuthorType = in.readString();
        lastMessageAuthorId = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(lastMessageAuthorType);
        dest.writeInt(lastMessageAuthorId);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<MessengerRoom> CREATOR = new Creator<MessengerRoom>() {
        @Override
        public MessengerRoom createFromParcel(Parcel in) {
            return new MessengerRoom(in);
        }

        @Override
        public MessengerRoom[] newArray(int size) {
            return new MessengerRoom[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public MessengerMessage getLastMessage() {
        return lastMessage;
    }

    public void setLastMessage(MessengerMessage lastMessage) {
        this.lastMessage = lastMessage;
    }

    public Date getLastMessageTime() {
        return lastMessageTime;
    }

    public void setLastMessageTime(Date lastMessageTime) {
        this.lastMessageTime = lastMessageTime;
    }

    public String getLastMessageAuthorType() {
        return lastMessageAuthorType;
    }

    public void setLastMessageAuthorType(String lastMessageAuthorType) {
        this.lastMessageAuthorType = lastMessageAuthorType;
    }

    public int getLastMessageAuthorId() {
        return lastMessageAuthorId;
    }

    public void setLastMessageAuthorId(int lastMessageAuthorId) {
        this.lastMessageAuthorId = lastMessageAuthorId;
    }

    public MessengerUser getOwner() {
        return owner;
    }

    public void setOwner(MessengerUser owner) {
        this.owner = owner;
    }

    //        public String getOwnerType() {
//            return ownerType;
//        }
//
//        public void setOwnerType(String ownerType) {
//            this.ownerType = ownerType;
//        }
//
//        public int getOwnerId() {
//            return ownerId;
//        }
//
//        public void setOwnerId(int ownerId) {
//            this.ownerId = ownerId;
//        }
}

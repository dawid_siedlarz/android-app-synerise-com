package com.synerise.synerise.app.factory;

import android.content.Context;

import com.synerise.synerise.app.model.Event;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

public class EventFactory {

    private Context context;

    public EventFactory(Context context) {
        this.context = context;
    }

    public void createEventsArray(ArrayList<Event> events, JSONArray jsonArray) throws JSONException {
        ArrayList<Event> newEvents = new ArrayList<>();

        for (int i = 0; i < jsonArray.length(); i++) {
            newEvents.add(new Event(jsonArray.getJSONObject(i), context));
        }

        events.clear();
        events.addAll(newEvents);
    }
}

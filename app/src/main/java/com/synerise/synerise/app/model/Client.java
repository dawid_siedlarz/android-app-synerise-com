package com.synerise.synerise.app.model;

import android.graphics.Bitmap;
import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONException;
import org.json.JSONObject;

public class Client implements Parcelable {
    public String anonymousType = "";
    public String avatarUrl = "";
    public String created = "";
    public String email = "";
    public String externalAvatarUrl = "";
    public String firstname = "";
    public String geoLocCity = "";
    public long id = 0;
    public String lastColumn = "";
    public String lastname = "";
    public String phone = "";
    public int points = 0;
    public Bitmap avatarBmp = null;
    private String displayName = "";

    public Client() {

    }

    public Client(JSONObject jsonObject) {
        try {
            this.anonymousType = jsonObject.getString("anonymous_type");
            this.avatarUrl = jsonObject.getString("avatarUrl");
            this.created = jsonObject.getString("created");
            this.email = jsonObject.getString("email");
            if (jsonObject.has("externalAvatarUrl")) {
                this.externalAvatarUrl = jsonObject.getString("externalAvatarUrl");
            }
            this.firstname = jsonObject.getString("firstname");
            this.geoLocCity = jsonObject.getString("city");
            this.id = jsonObject.getLong("id");
            this.lastname = jsonObject.getString("lastname");
            this.phone = jsonObject.getString("phone");

            if (avatarUrl.startsWith("//")) {
                avatarUrl = "https:" + avatarUrl;
            }

            displayName = buildDisplayName(this);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    protected Client(Parcel in) {
        anonymousType = in.readString();
        avatarUrl = in.readString();
        created = in.readString();
        email = in.readString();
        externalAvatarUrl = in.readString();
        firstname = in.readString();
        geoLocCity = in.readString();
        id = in.readLong();
        lastColumn = in.readString();
        lastname = in.readString();
        phone = in.readString();
        points = in.readInt();
        avatarBmp = in.readParcelable(Bitmap.class.getClassLoader());
        displayName = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(anonymousType);
        dest.writeString(avatarUrl);
        dest.writeString(created);
        dest.writeString(email);
        dest.writeString(externalAvatarUrl);
        dest.writeString(firstname);
        dest.writeString(geoLocCity);
        dest.writeLong(id);
        dest.writeString(lastColumn);
        dest.writeString(lastname);
        dest.writeString(phone);
        dest.writeInt(points);
        dest.writeParcelable(avatarBmp, flags);
        dest.writeString(displayName);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Client> CREATOR = new Creator<Client>() {
        @Override
        public Client createFromParcel(Parcel in) {
            return new Client(in);
        }

        @Override
        public Client[] newArray(int size) {
            return new Client[size];
        }
    };

    public String getAnonymousType() {
        return anonymousType;
    }

    public void setAnonymousType(String anonymousType) {
        this.anonymousType = anonymousType;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getExternalAvatarUrl() {
        return externalAvatarUrl;
    }

    public void setExternalAvatarUrl(String externalAvatarUrl) {
        this.externalAvatarUrl = externalAvatarUrl;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getGeoLocCity() {
        return geoLocCity;
    }

    public void setGeoLocCity(String geoLocCity) {
        this.geoLocCity = geoLocCity;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getLastColumn() {
        return lastColumn;
    }

    public void setLastColumn(String lastColumn) {
        this.lastColumn = lastColumn;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    public Bitmap getAvatarBmp() {
        return avatarBmp;
    }

    public void setAvatarBmp(Bitmap avatarBmp) {
        this.avatarBmp = avatarBmp;
    }

    private String buildDisplayName(Client client) {
        String firstname = client.getFirstname().compareTo("null") == 0 ? "" :client.getFirstname();
        String lastname = client.getLastname().compareTo("null") == 0 ? "" :client.getLastname();

        return firstname + " " + lastname;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }
}

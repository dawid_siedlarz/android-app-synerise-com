package com.synerise.synerise.app.syneutil;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;

import com.vincentbrison.openlibraries.android.dualcache.CacheSerializer;
import com.vincentbrison.openlibraries.android.dualcache.SizeOf;

import java.io.ByteArrayOutputStream;

/**
 * Created by dsiedlarz on 21.02.2017.
 */

public class SyneBitmapCacheUtil implements SizeOf<Bitmap>, CacheSerializer<Bitmap> {
    @Override
    public Bitmap fromString(String data) {
        try {
            byte[] encodeByte = Base64.decode(data, Base64.DEFAULT);
            Bitmap bitmap = BitmapFactory.decodeByteArray(encodeByte, 0,
                    encodeByte.length);
            return bitmap;
        } catch (Exception e) {
            e.getMessage();
            return null;
        }
    }

    @Override
    public String toString(Bitmap object) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        object.compress(Bitmap.CompressFormat.PNG, 100, baos);
        byte[] b = baos.toByteArray();
        String temp = Base64.encodeToString(b, Base64.DEFAULT);
        return temp;
    }

    @Override
    public int sizeOf(Bitmap object) {
        return object.getByteCount();
    }
}

package com.synerise.synerise.messenger.utils;

import android.util.Log;

import com.synerise.synerise.app.model.Client;
import com.synerise.synerise.messenger.model.MessengerClient;

public class ClientConverter {

    public Client fromMessengerToBasic(MessengerClient messengerClient) {
        Client client = new Client();
        Log.v("converter", " " + messengerClient.getId());
        client.setId(messengerClient.getId());
        client.setEmail(messengerClient.getEmail());
        client.setAvatarUrl(messengerClient.getAvatar());
        client.setCreated(messengerClient.getCreatedTime().toLocaleString());
        client.setFirstname(messengerClient.getName());

        return client;
    }
}

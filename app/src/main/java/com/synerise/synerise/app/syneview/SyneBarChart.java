package com.synerise.synerise.app.syneview;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.util.AttributeSet;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;

/**
 * Created by dsiedlarz on 24.02.2017.
 */

public class SyneBarChart extends BarChart {
    private static Typeface tf;
    static int fancyGray = Color.parseColor("#C7C7C7");

    public SyneBarChart(Context context, AttributeSet attrs) {
        super(context, attrs);
        tf = Typeface.createFromAsset(getContext().getAssets(),
                "fonts/Lato-Thin.ttf");

        initLineChart(this);
    }

    public static void initLineChart(BarChart chart) {
        chart.getDescription().setEnabled(false);
        chart.setDrawGridBackground(false);
        chart.setDrawGridBackground(false);
        chart.getXAxis().setTypeface(tf);
        chart.getXAxis().setTextColor(fancyGray);
        chart.getAxisLeft().setDrawGridLines(true);
        chart.getAxisLeft().setGridColor(fancyGray);
        chart.getAxisLeft().setDrawAxisLine(true);
        chart.getAxisLeft().setTypeface(tf);
        chart.getAxisLeft().setTextColor(fancyGray);
        chart.getAxisLeft().setDrawGridLines(false);
        chart.getXAxis().setDrawGridLines(false);
        chart.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM);
        chart.getXAxis().setLabelCount(3);
        chart.getAxisRight().setDrawAxisLine(false);
        chart.getAxisRight().setEnabled(false);
        chart.getAxisRight().setDrawGridLines(false);
        chart.setDrawBorders(false);
        chart.getAxisRight().setEnabled(false);

        XAxis xAxis = chart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        chart.setBackgroundColor(Color.parseColor("#FFFFFF"));
        chart.setDrawGridBackground(false);

        chart.setViewPortOffsets(0f, 40f, 0f, 40f);


        chart.getAxisLeft().setPosition(YAxis.YAxisLabelPosition.INSIDE_CHART);
        chart.getAxisLeft().setYOffset(-10);

        Legend l = chart.getLegend();
        l.setEnabled(false);

    }
}

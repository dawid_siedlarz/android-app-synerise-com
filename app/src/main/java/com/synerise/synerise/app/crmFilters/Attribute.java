package com.synerise.synerise.app.crmFilters;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;

import com.synerise.synerise.R;
import com.synerise.synerise.app.fragment.CRMFragment;
import com.synerise.synerise.app.model.FilterAttributes;

public class Attribute extends AppCompatActivity {
    private RadioButton attributeIs;
    private RadioButton attributeIsNot;
    private RadioButton attributeStartsWith;
    private RadioButton attributeEndsWith;
    private RadioButton attributeContains;
    private RadioButton attributeDoesntContains;
    private RadioButton attributeUnknown;
    private RadioButton attributeAnyValue;
    private EditText attributeValue;

    private RadioButton clientsAll;
    private RadioButton clientsIdentified;
    private RadioButton clientsAnonymousWeb;
    private RadioButton clientsAnonymousMobile;
    private RadioButton clientsAnonymousOnlyPhone;
    private RadioButton clientsAnonymousExternalId;

    private RadioButton clientsScoreMoreThan;
    private RadioButton clientsScoreExactly;
    private RadioButton clientsScoreLessThan;

    private FilterAttributes.FilterAttribute filterAttribute;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle extras = getIntent().getExtras();
        int attributeId = extras.getInt("attributeId");
        filterAttribute = FilterAttributes.ITEMS.get(attributeId);
        switch (filterAttribute.type) {
            case "TEXT":
                setContentView(R.layout.activity_attribute);
                TextView attributeName = (TextView) findViewById(R.id.attributeName);
                attributeName.setText(filterAttribute.name);

                attributeIs = (RadioButton) findViewById(R.id.attributeIs);
                attributeIsNot = (RadioButton) findViewById(R.id.attributeIsNot);
                attributeStartsWith = (RadioButton) findViewById(R.id.attributeStartsWith);
                attributeEndsWith = (RadioButton) findViewById(R.id.attributeEndsWith);
                attributeContains = (RadioButton) findViewById(R.id.attributeContains);
                attributeDoesntContains = (RadioButton) findViewById(R.id.attributeDoesntContains);
                attributeUnknown = (RadioButton) findViewById(R.id.attributeUnknown);
                attributeAnyValue = (RadioButton) findViewById(R.id.attributeAnyValue);
                attributeValue = (EditText) findViewById(R.id.attributeValue);
                break;
            case "ANONYMITY":
                setContentView(R.layout.activity_attribute_anonymous);

                clientsAll = (RadioButton) findViewById(R.id.clientsAll);
                clientsIdentified = (RadioButton) findViewById(R.id.clientsIdentified);
                clientsAnonymousWeb = (RadioButton) findViewById(R.id.clientsAnonymousWeb);
                clientsAnonymousMobile = (RadioButton) findViewById(R.id.clientsAnonymousMobile);
                clientsAnonymousOnlyPhone = (RadioButton) findViewById(R.id.clientsAnonymousOnlyPhone);
                clientsAnonymousExternalId = (RadioButton) findViewById(R.id.clientsAnonymousExternalId);

                break;
            case "SCORE":
                setContentView(R.layout.activity_attribute_score);
                clientsScoreMoreThan = (RadioButton) findViewById(R.id.clientsScoreMoreThan);
                clientsScoreExactly = (RadioButton) findViewById(R.id.clientsScoreExactly);
                clientsScoreLessThan = (RadioButton) findViewById(R.id.clientsScoreLessThan);
                attributeValue = (EditText) findViewById(R.id.attributeValue);

                break;
            default:
                finish();
        }

    }

    public void attributeSearchAction(View v) {
        String option = null;

        if (attributeIs.isChecked()) {
            option = "is";
        }

        if (attributeIsNot.isChecked()) {
            option = "is_not";
        }

        if (attributeStartsWith.isChecked()) {
            option = "starts_with";
        }

        if (attributeEndsWith.isChecked()) {
            option = "ends_with";
        }

        if (attributeContains.isChecked()) {
            option = "contains";
        }

        if (attributeDoesntContains.isChecked()) {
            option = "doesnt_contain";
        }

        if (attributeUnknown.isChecked()) {
            option = "unknown";
        }

        if (attributeAnyValue.isChecked()) {
            option = "any";
        }
        String value = attributeValue.getText().toString();

        if (option != null && !value.isEmpty()) {
            filterAttribute.option = option;
            filterAttribute.value = value;

            CRMFragment.attributes.add(filterAttribute);
        }

        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.mainContainer);

        ((CRMFragment) fragment).getClients();

        setResult(RESULT_OK, null);
        finish();

    }

    public void attributeAnonymitySearchAction(View v) {

        String anonymousType = null;
        if (clientsAll.isChecked()) {
            anonymousType = "ALL";
        }
        if (clientsIdentified.isChecked()) {
            anonymousType = "IDENTIFIED";
        }
        if (clientsAnonymousWeb.isChecked()) {
            anonymousType = "UUID";
        }
        if (clientsAnonymousMobile.isChecked()) {
            anonymousType = "DEVICE_ID";
        }
        if (clientsAnonymousOnlyPhone.isChecked()) {
            anonymousType = "PHONE";
        }
        if (clientsAnonymousExternalId.isChecked()) {
            anonymousType = "CUSTOM_ID";
        }

        CRMFragment.listing_anonymous_type = anonymousType;
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.mainContainer);
        ((CRMFragment) fragment).getClients();

        setResult(RESULT_OK, null);
        finish();

    }

    public void attributeScoreSearchAction(View v) {
        String option = null;

        if (clientsScoreMoreThan.isChecked()) {
            option = "more_than";
        }
        if (clientsScoreExactly.isChecked()) {
            option = "esactly";
        }
        if (clientsScoreLessThan.isChecked()) {
            option = "less_than";
        }

        String value = attributeValue.getText().toString();

        if (option != null && !value.isEmpty()) {
            filterAttribute.option = option;
            filterAttribute.value = value;

            CRMFragment.attributes.add(filterAttribute);
        }
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.mainContainer);
        ((CRMFragment) fragment).getClients();

        setResult(RESULT_OK, null);
        finish();
    }
}

package com.synerise.synerise.app.model.campaign;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONException;
import org.json.JSONObject;

public class SmsCampaign extends Campaign implements Parcelable {

    private int sent;

    public SmsCampaign(JSONObject jsonObject) {
        super(jsonObject);

        try {
            sent = jsonObject.getInt("sent");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    protected SmsCampaign(Parcel in) {
        super(in);
        sent = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);

        dest.writeInt(sent);
    }

    @Override
    public int describeContents() {
        return super.describeContents();
    }

    public int getSent() {
        return sent;
    }

    public void setSent(int sent) {
        this.sent = sent;
    }
}

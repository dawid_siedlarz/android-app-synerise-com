package com.synerise.synerise.app.fragment;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;

import com.astuetz.PagerSlidingTabStrip;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.IFillFormatter;
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.interfaces.dataprovider.LineDataProvider;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.synerise.sdk.tracking.Tracker;
import com.synerise.sdk.tracking.TrackerParams;
import com.synerise.synerise.MainActivity;
import com.synerise.synerise.R;
import com.synerise.synerise.SyneriseOnFragmentInteractionListener;
import com.synerise.synerise.app.ActionBarConfigurableFragment;
import com.synerise.synerise.app.adapter.EventRecyclerViewAdapter;
import com.synerise.synerise.app.factory.EventFactory;
import com.synerise.synerise.app.model.Event;
import com.synerise.synerise.app.syneutil.SampleFragmentPagerAdapter;
import com.synerise.synerise.app.syneutil.SyneHttpCacheHandler;
import com.synerise.synerise.app.syneview.LineChartMarkerView;
import com.synerise.synerise.app.syneview.SyneChartTitle;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.List;


public class CommerceFragment extends ActionBarConfigurableFragment {
    private static Calendar dateFrom;
    private static Calendar dateTo;
    private String peridod;
    private static String groupDate = "yyyy-MM-dd";
    private static String minPeriod = "DD";
    private static String dateFateFormat = "yyyy-MM-dd";
    private static String dateMinPeriod = "DD";
    private static String dateGroup = "day";
    private static String client = "all";
    public static DecimalFormat formatter = new DecimalFormat("###,###,###");

    LineData abandonedProductsData;
    LineData addProductsData;
    LineData buyProductsData;
    LineData valueAbandonedProductsData;
    LineData valueAddToCardData;
    LineData valueBuyProductsData;


    private TextView hour;
    private TextView day;
    private TextView week;
    private TextView month;
    private TextView year;

    private View hourLine;
    private View dayLine;
    private View weekLine;
    private View monthLine;
    private View yearLine;

    static int fancyGray = Color.parseColor("#C7C7C7");

    private RecyclerView eventsList;
    private EventRecyclerViewAdapter adapter;
    private LinearLayoutManager linearLayoutManager;

    private static Typeface tf;

    boolean isScreenActive = false;

    private boolean isRevenueInitialized = false;
    private boolean isNoOfTransactionsInitialized = false;
    private boolean isUniquePurchasesInitialized = false;
    private boolean isAvarageOrderValueInitialized = false;
    private boolean isBasketsInitialized = false;
    private boolean isReturnedInitialized = false;

    private boolean isGetEventsInitialized = false;

    private ArrayList<Event> events;
    private EventFactory eventFactory;

    @Override
    public void onResume() {
        super.onResume();
        isScreenActive = true;
        loadCharts();
    }

    @Override
    public void onPause() {
        super.onPause();
        isScreenActive = false;
    }

    @Override
    public void onStop() {
        super.onStop();
        isScreenActive = false;
    }


    private void initEventsList() {
        View rootView = getView();
        if (rootView == null) {
            return;
        }
        eventsList = (RecyclerView) rootView.findViewById(R.id.commerceEventsListView);
        events = new ArrayList<>();
        adapter = new EventRecyclerViewAdapter((MainActivity) getActivity(), events);
        eventsList.setNestedScrollingEnabled(false);
        linearLayoutManager = new LinearLayoutManager(getContext());
        eventsList.setLayoutManager(linearLayoutManager);
        eventsList.setAdapter(adapter);
    }


    private SyneriseOnFragmentInteractionListener mListener;

    public CommerceFragment() {
        // Required empty public constructor
    }


    public static CommerceFragment newInstance(String param1, String param2) {
        CommerceFragment fragment = new CommerceFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        dateTo = Calendar.getInstance();
        dateFrom = Calendar.getInstance();
        dateFrom.add(Calendar.MONTH, -1);
        tf = Typeface.createFromAsset(getContext().getAssets(),
                "fonts/Lato-Thin.ttf");

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
//        container.removeAllViewsInLayout();
        return inflater.inflate(R.layout.fragment_commerce, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void setUpActionBar() {
        ActionBar actionBar = (ActionBar) ((AppCompatActivity) getActivity()).getSupportActionBar();
        if (actionBar != null) {
            actionBar.show();
            actionBar.setTitle(MainActivity.businessProfileName != null ? MainActivity.businessProfileName : "Synerise");
            actionBar.setSubtitle("Commerce");
            actionBar.setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.colorPrimary)));
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                Window window = getActivity().getWindow();
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                window.setStatusBarColor(getResources().getColor(R.color.colorPrimary));
            }
            getActivity().setTheme(R.style.AppThemeDashboard);
            actionBar.setElevation(0);
        }
    }


    private class GetRevenue extends AsyncTask<Void, Void, String> {
        @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
        protected String doInBackground(Void... params) {
            try {

                String url = "https://api.synerise.com/rtom/stats/transaction/turnover-in-location";
                HttpGet httpget = new HttpGet(url + CommerceFragment.getParams());

                SyneHttpCacheHandler handler = new SyneHttpCacheHandler();
                boolean cacheUsed = false;
                if (!isRevenueInitialized) {
                    cacheUsed = MainActivity.cacheString.contains(SyneHttpCacheHandler.getCacheKeyFromUrl(createCacheKey(url)));
                }
                final String body = handler.executeWithCacheKey(httpget, createCacheKey(url), !isRevenueInitialized, true);

                if (body == null) {
                    return "not ok";
                }

                JSONArray jsonArray = null;

                try {
                    jsonArray = new JSONArray(body);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                final JSONArray finalJSONArray = jsonArray;

                final ArrayList<String> labels = new ArrayList<String>();
                ArrayList<Entry> valueMobileApp = new ArrayList<Entry>();
                ArrayList<Entry> valueMobileSite = new ArrayList<Entry>();
                ArrayList<Entry> valueOffline = new ArrayList<Entry>();
                ArrayList<Entry> valueOnline = new ArrayList<Entry>();
                ArrayList<Entry> valueWeb = new ArrayList<Entry>();
                ArrayList<Entry> temp = new ArrayList<Entry>();
                Double all = 0.0;
                try {
                    int j = 0;
                    for (int i = finalJSONArray.length() - 1; i >= 0; i--) {
                        JSONObject jsonObject = finalJSONArray.getJSONObject(i);
                        if (jsonObject.getString("currency").compareToIgnoreCase("PLN") == 0) {
                            all += jsonObject.getDouble("valueAll");
                            valueMobileApp.add(new Entry(j, (float) jsonObject.getDouble("valueMobileApp")));
                            valueMobileSite.add(new Entry(j, (float) jsonObject.getDouble("valueMobileSite")));
                            valueOffline.add(new Entry(j, (float) jsonObject.getDouble("valueOffline")));
                            valueOnline.add(new Entry(j, (float) jsonObject.getDouble("valueOnline")));
                            valueWeb.add(new Entry(j, (float) jsonObject.getDouble("valueWeb")));
                            temp.add(new Entry(j, (float) jsonObject.getDouble("temp")));

                            labels.add(jsonObject.getString("eventDate").replace(" 00:00:00", ""));
                            j++;
                        }
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }

                final String[] lineLabels = {"Mobile app", "Mobile site", "Offline", "Web"};
                LineDataSet valueMobileAppSet = new LineDataSet(valueMobileApp, "Moblie app");
                LineDataSet valueMobileSiteSet = new LineDataSet(valueMobileSite, "Mobile site");
                LineDataSet valueOfflineSet = new LineDataSet(valueOffline, "Offline");
                LineDataSet valueWebSet = new LineDataSet(valueWeb, "Web");
                LineDataSet tempSet = new LineDataSet(temp, "Temp");

                Resources resources;
                try {
                    resources = getResources();
                } catch (RuntimeException e) {
                    //ignore
                }

                if (getResources() == null) {
                    return "not OK";
                }
                valueMobileAppSet.setColor(getResources().getColor(R.color.legendMobileApp));
                valueMobileSiteSet.setColor(getResources().getColor(R.color.legendMobileSite));
                valueOfflineSet.setColor(getResources().getColor(R.color.legendOffline));
                valueWebSet.setColor(getResources().getColor(R.color.legendWeb));

                valueMobileAppSet.setFillColor(getResources().getColor(R.color.legendMobileApp));
                valueMobileSiteSet.setFillColor(getResources().getColor(R.color.legendMobileSite));
                valueOfflineSet.setFillColor(getResources().getColor(R.color.legendOffline));
                valueWebSet.setFillColor(getResources().getColor(R.color.legendWeb));
//                tempSet.setColor(getResources().getColor(R.color.fancyMaterialYellow));

                ArrayList<LineDataSet> sets = new ArrayList<LineDataSet>();

                sets.add(valueMobileAppSet);
                sets.add(valueMobileSiteSet);
                sets.add(valueOfflineSet);
                sets.add(valueWebSet);
//                sets.add(tempSet);

                for (LineDataSet set : sets) {
                    set.setDrawCircles(false);
                    set.setDrawFilled(true);
                    set.setFillAlpha(10);
                    set.setMode(LineDataSet.Mode.CUBIC_BEZIER);
                    set.setDrawHighlightIndicators(true);
                    set.setDrawHorizontalHighlightIndicator(false);
                    set.setDrawVerticalHighlightIndicator(false);
                }


                ArrayList<ILineDataSet> dataSets = new ArrayList<ILineDataSet>();

                dataSets.add(valueWebSet);
                dataSets.add(valueMobileSiteSet);
                dataSets.add(valueMobileAppSet);
                dataSets.add(valueOfflineSet);

//                dataSets.add(tempSet);

                final LineData data = new LineData(dataSets);
                data.setDrawValues(false);


                final Activity activity = getActivity();
                if (activity != null) {
                    final Double finalAll = all;
                    final boolean finalCacheUsed = cacheUsed;
                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            View rootView = getView();
                            if (rootView == null) {
                                return;
                            }
                            SyneChartTitle title = ((SyneChartTitle) rootView.findViewById(R.id.revenueTitle));
                            LineChart lineChart = ((LineChart) rootView.findViewById(R.id.commerceChart));

                            if (title == null || lineChart == null) {
                                return;
                            }
                            title.setDoubleCounter(finalAll);
                            LineChartMarkerView mv = new LineChartMarkerView(getContext(), R.layout.custom_marker_view);
                            mv.setChartView(lineChart); // For bounds control
                            mv.setDates(labels);
                            mv.setLabels(lineLabels);
                            lineChart.setMarker(mv);

                            if (!isRevenueInitialized) {
                                CommerceFragment.initLineChart(lineChart, labels);
//                                CommerceFragment.LineChartSelectedValueListener listener = new CommerceFragment.LineChartSelectedValueListener(lineChart, lineLabels, labels, (TextView)rootView.findViewById(R.id.revenueChartSelected));
//                                lineChart.setOnChartValueSelectedListener(listener);


                                lineChart.getAxisLeft().setAxisMinimum(0f);


                                lineChart.setData(data);
                                lineChart.invalidate(); // refresh

                                if (finalCacheUsed) {
                                    lineChart.setAlpha(0.4f);
                                }
//                                lineChart.animateY(1500);
                                isRevenueInitialized = true;
                            } else {
                                lineChart.setAlpha(1f);


                                lineChart.setData(data);
                                lineChart.notifyDataSetChanged();
                            }

                        }
                    });
                }
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (NullPointerException e) {

            } catch (IllegalStateException e) {

            }
            return "ok";
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            Handler myHandler = new Handler();
            myHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    Fragment fragment = getActivity().getSupportFragmentManager().findFragmentById(R.id.mainContainer);

                    if (isScreenActive && getActivity() != null && fragment instanceof CommerceFragment) {
                        new GetRevenue().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                    }
                }
            }, 3000);

        }
    }


    private class GetNoOfTransaction extends AsyncTask<Void, Void, String> {
        protected String doInBackground(Void... params) {
            try {

                String url = "https://api.synerise.com/rtom/stats/transaction/count-transaction-in-location";

                HttpGet httpget = new HttpGet(url + CommerceFragment.getParams());

                SyneHttpCacheHandler handler = new SyneHttpCacheHandler();
                boolean cacheUsed = false;
                if (!isNoOfTransactionsInitialized) {
                    cacheUsed = MainActivity.cacheString.contains(SyneHttpCacheHandler.getCacheKeyFromUrl(createCacheKey(url)));
                }
                final String body = handler.executeWithCacheKey(httpget, createCacheKey(url), !isNoOfTransactionsInitialized, true);

                if (body == null) {
                    return "not ok";
                }

                JSONArray jsonArray = null;

                try {
                    jsonArray = new JSONArray(body);
                } catch (JSONException e) {
//                    e.printStackTrace();
                    return "not ok";
                }

                final JSONArray finalJSONArray = jsonArray;

                final ArrayList<String> labels = new ArrayList<String>();
                ArrayList<Entry> valueMobileApp = new ArrayList<Entry>();
                ArrayList<Entry> valueMobileSite = new ArrayList<Entry>();
                ArrayList<Entry> valueOffline = new ArrayList<Entry>();
                ArrayList<Entry> valueOnline = new ArrayList<Entry>();
                ArrayList<Entry> valueWeb = new ArrayList<Entry>();
                Double all = 0.0;
                try {
                    int j = 0;
                    for (int i = finalJSONArray.length() - 1; i >= 0; i--) {
                        JSONObject jsonObject = finalJSONArray.getJSONObject(i);
                        if (jsonObject.getString("currency").compareToIgnoreCase("PLN") == 0) {
                            all += jsonObject.getDouble("valueAll");
                            valueMobileApp.add(new Entry(j, (float) jsonObject.getDouble("valueMobileApp")));
                            valueMobileSite.add(new Entry(j, (float) jsonObject.getDouble("valueMobileSite")));
                            valueOffline.add(new Entry(j, (float) jsonObject.getDouble("valueOffline")));
                            valueOnline.add(new Entry(j, (float) jsonObject.getDouble("valueOnline")));
                            valueWeb.add(new Entry(j, (float) jsonObject.getDouble("valueWeb")));

                            labels.add(jsonObject.getString("eventDate"));
                            j++;
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                final String[] lineLabels = {"Mobile app", "Mobile site", "Offline", "Web"};
                LineDataSet valueMobileAppSet = new LineDataSet(valueMobileApp, "Mobile App");
                LineDataSet valueMobileSiteSet = new LineDataSet(valueMobileSite, "Mobile Site");
                LineDataSet valueOfflineSet = new LineDataSet(valueOffline, "Offline");
                LineDataSet valueWebSet = new LineDataSet(valueWeb, "Web");

                valueMobileAppSet.setColor(getResources().getColor(R.color.legendMobileApp));
                valueMobileSiteSet.setColor(getResources().getColor(R.color.legendMobileSite));
                valueOfflineSet.setColor(getResources().getColor(R.color.legendOffline));
                valueWebSet.setColor(getResources().getColor(R.color.legendWeb));

                valueMobileAppSet.setFillColor(getResources().getColor(R.color.legendMobileApp));
                valueMobileSiteSet.setFillColor(getResources().getColor(R.color.legendMobileSite));
                valueOfflineSet.setFillColor(getResources().getColor(R.color.legendOffline));
                valueWebSet.setFillColor(getResources().getColor(R.color.legendWeb));

                ArrayList<LineDataSet> sets = new ArrayList<LineDataSet>();

                sets.add(valueMobileAppSet);
                sets.add(valueMobileSiteSet);
                sets.add(valueOfflineSet);
                sets.add(valueWebSet);

                for (LineDataSet set : sets) {
                    set.setDrawCircles(false);
                    set.setDrawFilled(true);
                    set.setFillAlpha(10);
                    set.setMode(LineDataSet.Mode.CUBIC_BEZIER);
                    set.setDrawHighlightIndicators(true);
                    set.setDrawHorizontalHighlightIndicator(false);
                    set.setDrawVerticalHighlightIndicator(false);
                }


                final LineData data = new LineData(valueMobileAppSet);
                data.addDataSet(valueMobileSiteSet);
                data.addDataSet(valueOfflineSet);
                data.addDataSet(valueWebSet);

                data.setDrawValues(false);


                final Activity activity = getActivity();
                if (activity != null) {
                    final Double finalAll = all;
                    final boolean finalCacheUsed = cacheUsed;
                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            View rootView = getView();
                            if (rootView == null) {
                                return;
                            }
                            SyneChartTitle title = ((SyneChartTitle) rootView.findViewById(R.id.noOfTransactionTitle));
                            LineChart lineChart = ((LineChart) rootView.findViewById(R.id.noOfTransactionChart));


                            if (title == null || lineChart == null) {
                                return;
                            }

                            title.setDoubleCounter(finalAll);

                            if (!isNoOfTransactionsInitialized) {
                                CommerceFragment.initLineChart(lineChart, labels);
//                                CommerceFragment.LineChartSelectedValueListener listener = new CommerceFragment.LineChartSelectedValueListener(lineChart, lineLabels, labels, (TextView)rootView.findViewById(R.id.revenueChartSelected));
//                                lineChart.setOnChartValueSelectedListener(listener);

                                LineChartMarkerView mv = new LineChartMarkerView(getContext(), R.layout.custom_marker_view);
                                mv.setChartView(lineChart); // For bounds control
                                mv.setDates(labels);
                                mv.setLabels(lineLabels);
                                lineChart.setMarker(mv);
                                lineChart.getAxisLeft().setAxisMinimum(0f);

                                lineChart.setData(data);
                                lineChart.invalidate(); // refresh
//                                lineChart.animateY(1500);
                                if (finalCacheUsed) {
                                    lineChart.setAlpha(0.4f);
                                }
                                isNoOfTransactionsInitialized = true;
                            } else {
                                lineChart.setAlpha(1f);

                                LineChartMarkerView mv = new LineChartMarkerView(getContext(), R.layout.custom_marker_view);
                                mv.setChartView(lineChart); // For bounds control
                                mv.setDates(labels);
                                mv.setLabels(lineLabels);
                                lineChart.setMarker(mv);


                                lineChart.setData(data);
                                lineChart.notifyDataSetChanged();
                            }

                        }
                    });
                }
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (NullPointerException e) {

            } catch (IllegalStateException e) {

            }
            return "ok";
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            Handler myHandler = new Handler();
            myHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    Fragment fragment = getActivity().getSupportFragmentManager().findFragmentById(R.id.mainContainer);
                    if (isScreenActive && getActivity() != null && fragment instanceof CommerceFragment) {
                        new GetNoOfTransaction().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                    }
                }
            }, 3000);

        }
    }


    private class GetUniquePurchases extends AsyncTask<Void, Void, String> {
        protected String doInBackground(Void... params) {
            try {

                String url = "https://api.synerise.com/rtom/stats/transaction/quantity-in-location";

                HttpGet httpget = new HttpGet(url + CommerceFragment.getParams());

                SyneHttpCacheHandler handler = new SyneHttpCacheHandler();
                boolean cacheUsed = false;
                if (!isUniquePurchasesInitialized) {
                    cacheUsed = MainActivity.cacheString.contains(SyneHttpCacheHandler.getCacheKeyFromUrl(createCacheKey(url)));
                }
                final String body = handler.executeWithCacheKey(httpget, createCacheKey(url), !isUniquePurchasesInitialized, true);

                if (body == null) {
                    return "not ok";
                }


                JSONArray jsonArray = null;

                try {
                    jsonArray = new JSONArray(body);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                final JSONArray finalJSONArray = jsonArray;


                final ArrayList<String> labels = new ArrayList<String>();
                ArrayList<Entry> valueMobileApp = new ArrayList<Entry>();
                ArrayList<Entry> valueMobileSite = new ArrayList<Entry>();
                ArrayList<Entry> valueOffline = new ArrayList<Entry>();
                ArrayList<Entry> valueOnline = new ArrayList<Entry>();
                ArrayList<Entry> valueWeb = new ArrayList<Entry>();
                Double all = 0.0;
                try {
                    int j = 0;
                    for (int i = finalJSONArray.length() - 1; i >= 0; i--) {
                        JSONObject jsonObject = finalJSONArray.getJSONObject(i);
                        if (jsonObject.getString("currency").compareToIgnoreCase("PLN") == 0) {
                            all += jsonObject.getDouble("valueAll");
                            valueMobileApp.add(new Entry(j, (float) jsonObject.getDouble("valueMobileApp")));
                            valueMobileSite.add(new Entry(j, (float) jsonObject.getDouble("valueMobileSite")));
                            valueOffline.add(new Entry(j, (float) jsonObject.getDouble("valueOffline")));
                            valueOnline.add(new Entry(j, (float) jsonObject.getDouble("valueOnline")));
                            valueWeb.add(new Entry(j, (float) jsonObject.getDouble("valueWeb")));
                            j++;
                            labels.add(jsonObject.getString("eventDate"));

                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                final String[] dataSets = new String[]{"Mobile App", "Mobile Site", "Offline", "Web"};

                LineDataSet valueMobileAppSet = new LineDataSet(valueMobileApp, dataSets[0]);
                LineDataSet valueMobileSiteSet = new LineDataSet(valueMobileSite, dataSets[1]);
                LineDataSet valueOfflineSet = new LineDataSet(valueOffline, dataSets[2]);
                LineDataSet valueWebSet = new LineDataSet(valueWeb, dataSets[3]);

                valueMobileAppSet.setColor(getResources().getColor(R.color.legendMobileApp));
                valueMobileSiteSet.setColor(getResources().getColor(R.color.legendMobileSite));
                valueOfflineSet.setColor(getResources().getColor(R.color.legendOffline));
                valueWebSet.setColor(getResources().getColor(R.color.legendWeb));

                valueMobileAppSet.setFillColor(getResources().getColor(R.color.legendMobileApp));
                valueMobileSiteSet.setFillColor(getResources().getColor(R.color.legendMobileSite));
                valueOfflineSet.setFillColor(getResources().getColor(R.color.legendOffline));
                valueWebSet.setFillColor(getResources().getColor(R.color.legendWeb));


                ArrayList<LineDataSet> sets = new ArrayList<LineDataSet>();

                sets.add(valueMobileAppSet);
                sets.add(valueMobileSiteSet);
                sets.add(valueOfflineSet);
                sets.add(valueWebSet);

                for (LineDataSet set : sets) {
                    set.setDrawCircles(false);
                    set.setFillFormatter(new YourFillFormatter());
                    set.setDrawFilled(true);
                    set.setFillAlpha(10);
                    set.setMode(LineDataSet.Mode.CUBIC_BEZIER);
                    set.setDrawHighlightIndicators(true);
                    set.setDrawHorizontalHighlightIndicator(false);
                    set.setDrawVerticalHighlightIndicator(false);
                }


                final LineData data = new LineData(valueMobileAppSet);
                data.addDataSet(valueMobileSiteSet);
                data.addDataSet(valueOfflineSet);
                data.addDataSet(valueWebSet);

                data.setDrawValues(false);


                final Activity activity = getActivity();
                if (activity != null) {
                    final Double finalAll = all;
                    final boolean finalCacheUsed = cacheUsed;
                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            View rootView = getView();
                            if (rootView == null) {
                                return;
                            }
                            LineChart lineChart = ((LineChart) rootView.findViewById(R.id.uniquePurchasesChart));
                            SyneChartTitle title = ((SyneChartTitle) rootView.findViewById(R.id.uniquePurchasesTitle));

                            if (title == null || lineChart == null) {
                                return;
                            }

                            title.setDoubleCounter(finalAll);
                            lineChart.setData(data);


                            LineChartMarkerView mv = new LineChartMarkerView(getContext(), R.layout.custom_marker_view);
                            mv.setChartView(lineChart); // For bounds control
                            mv.setDates(labels);
                            mv.setLabels(dataSets);
                            lineChart.setMarker(mv);

                            if (!isUniquePurchasesInitialized) {
                                initLineChart(lineChart, labels);
                                lineChart.invalidate(); // refresh
                                if (finalCacheUsed) {
                                    lineChart.setAlpha(0.4f);
                                }
                                isUniquePurchasesInitialized = true;
                            } else {
                                lineChart.setAlpha(1f);
                                lineChart.getXAxis().setValueFormatter(new IndexAxisValueFormatter(labels));
                                lineChart.setData(data);
                                lineChart.notifyDataSetChanged();
                            }
                        }
                    });
                }
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (NullPointerException e) {

            } catch (IllegalStateException e) {

            }
            return "ok";
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            Handler myHandler = new Handler();
            myHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    Fragment fragment = getActivity().getSupportFragmentManager().findFragmentById(R.id.mainContainer);
                    if (isScreenActive && getActivity() != null && fragment instanceof CommerceFragment) {
                        new GetUniquePurchases().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                    }
                }
            }, 3000);

        }
    }


    private class GetAvarageOrderValue extends AsyncTask<Void, Void, String> {
        protected String doInBackground(Void... params) {
            try {

                String url = "https://api.synerise.com/rtom/stats/transaction/average-transaction-in-location";

                HttpGet httpget = new HttpGet(url + CommerceFragment.getParams());

                SyneHttpCacheHandler handler = new SyneHttpCacheHandler();
                boolean cacheUsed = false;
                if (!isAvarageOrderValueInitialized) {
                    cacheUsed = MainActivity.cacheString.contains(SyneHttpCacheHandler.getCacheKeyFromUrl(createCacheKey(url)));
                }
                final String body = handler.executeWithCacheKey(httpget, createCacheKey(url), !isAvarageOrderValueInitialized, true);

                if (body == null) {
                    return "not ok";
                }


                JSONArray jsonArray = null;

                try {
                    jsonArray = new JSONArray(body);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                final JSONArray finalJSONArray = jsonArray;


                final ArrayList<String> labels = new ArrayList<String>();
                ArrayList<Entry> valueMobileApp = new ArrayList<Entry>();
                ArrayList<Entry> valueMobileSite = new ArrayList<Entry>();
                ArrayList<Entry> valueOffline = new ArrayList<Entry>();
                ArrayList<Entry> valueOnline = new ArrayList<Entry>();
                ArrayList<Entry> valueWeb = new ArrayList<Entry>();
                Double all = 0.0;
                int count = 0;
                try {
                    int j = 0;
                    for (int i = finalJSONArray.length() - 1; i >= 0; i--) {
                        JSONObject jsonObject = finalJSONArray.getJSONObject(i);
                        if (jsonObject.getString("currency").compareToIgnoreCase("PLN") == 0) {
                            all += jsonObject.getDouble("valueAll");
                            valueMobileApp.add(new Entry(j, (float) jsonObject.getDouble("valueMobileApp")));
                            valueMobileSite.add(new Entry(j, (float) jsonObject.getDouble("valueMobileSite")));
                            valueOffline.add(new Entry(j, (float) jsonObject.getDouble("valueOffline")));
                            valueOnline.add(new Entry(j, (float) jsonObject.getDouble("valueOnline")));
                            valueWeb.add(new Entry(j, (float) jsonObject.getDouble("valueWeb")));

                            labels.add(jsonObject.getString("eventDate"));
                            j++;
                            count++;
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                final String[] dataSets = new String[]{"Mobile App", "Mobile Site", "Offline", "Web"};

                LineDataSet valueMobileAppSet = new LineDataSet(valueMobileApp, dataSets[0]);
                LineDataSet valueMobileSiteSet = new LineDataSet(valueMobileSite, dataSets[1]);
                LineDataSet valueOfflineSet = new LineDataSet(valueOffline, dataSets[2]);
                LineDataSet valueWebSet = new LineDataSet(valueWeb, dataSets[3]);

                valueMobileAppSet.setColor(getResources().getColor(R.color.legendMobileApp));
                valueMobileSiteSet.setColor(getResources().getColor(R.color.legendMobileSite));
                valueOfflineSet.setColor(getResources().getColor(R.color.legendOffline));
                valueWebSet.setColor(getResources().getColor(R.color.legendWeb));

                valueMobileAppSet.setFillColor(getResources().getColor(R.color.legendMobileApp));
                valueMobileSiteSet.setFillColor(getResources().getColor(R.color.legendMobileSite));
                valueOfflineSet.setFillColor(getResources().getColor(R.color.legendOffline));
                valueWebSet.setFillColor(getResources().getColor(R.color.legendWeb));

                ArrayList<LineDataSet> sets = new ArrayList<LineDataSet>();

                sets.add(valueMobileAppSet);
                sets.add(valueMobileSiteSet);
                sets.add(valueOfflineSet);
                sets.add(valueWebSet);

                for (LineDataSet set : sets) {
                    set.setDrawCircles(false);
                    set.setFillFormatter(new YourFillFormatter());
                    set.setFillAlpha(10);
                    set.setDrawFilled(true);
                    set.setMode(LineDataSet.Mode.CUBIC_BEZIER);
                    set.setDrawHighlightIndicators(true);
                    set.setDrawHorizontalHighlightIndicator(false);
                    set.setDrawVerticalHighlightIndicator(false);
                }


                final LineData data = new LineData(valueMobileAppSet);
                data.addDataSet(valueMobileSiteSet);
                data.addDataSet(valueOfflineSet);
                data.addDataSet(valueWebSet);

                data.setDrawValues(false);


                final Double avarage = all / count;

                final Activity activity = getActivity();
                if (activity != null) {
                    final Double finalAll = all;

                    final boolean finalCacheUsed = cacheUsed;
                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            View rootView = getView();
                            if (rootView == null) {
                                return;
                            }
                            LineChart lineChart = ((LineChart) rootView.findViewById(R.id.AvaregeOrderValueChart));
                            SyneChartTitle title = ((SyneChartTitle) rootView.findViewById(R.id.averageOrderValueTitle));

                            if (title == null || lineChart == null) {
                                return;
                            }

                            title.setDoubleCounter(avarage);

                            LineChartMarkerView mv = new LineChartMarkerView(getContext(), R.layout.custom_marker_view);
                            mv.setChartView(lineChart); // For bounds control
                            mv.setDates(labels);
                            mv.setLabels(dataSets);
                            lineChart.setMarker(mv);

                            if (!isAvarageOrderValueInitialized) {
                                CommerceFragment.initLineChart(lineChart, labels);
                                lineChart.invalidate(); // refresh
                                if (finalCacheUsed) {
                                    lineChart.setAlpha(0.4f);
                                }
                                isAvarageOrderValueInitialized = true;
                            } else {
                                lineChart.setAlpha(1f);
                                lineChart.getXAxis().setValueFormatter(new IndexAxisValueFormatter(labels));
                                lineChart.setData(data);
                                lineChart.notifyDataSetChanged();
                            }

                        }
                    });
                }
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (NullPointerException e) {

            } catch (IllegalStateException e) {

            }
            return "ok";
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            Handler myHandler = new Handler();
            myHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    Fragment fragment = getActivity().getSupportFragmentManager().findFragmentById(R.id.mainContainer);
                    if (isScreenActive && getActivity() != null && fragment instanceof CommerceFragment) {
                        new GetAvarageOrderValue().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                    }
                }
            }, 3000);

        }
    }


    private class GetBasketCharts extends AsyncTask<Void, Void, String> {
        protected String doInBackground(Void... params) {
            try {

                String url = "https://api.synerise.com/rtom/stats/transaction/abandoned-shopping-baskets";

                HttpGet httpget = new HttpGet(url + CommerceFragment.getParams());

                SyneHttpCacheHandler handler = new SyneHttpCacheHandler();
                boolean cacheUsed = false;
                if (!isBasketsInitialized) {
                    cacheUsed = MainActivity.cacheString.contains(SyneHttpCacheHandler.getCacheKeyFromUrl(createCacheKey(url)));
                }
                final String body = handler.executeWithCacheKey(httpget, createCacheKey(url), !isBasketsInitialized, true);

                if (body == null) {
                    return "not ok";
                }


                JSONArray jsonArray = null;

                try {
                    jsonArray = new JSONArray(body);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                final JSONArray finalJSONArray = jsonArray;


                final ArrayList<String> labels = new ArrayList<String>();
                final ArrayList<Entry> abandonedProducts = new ArrayList<Entry>();
                final ArrayList<Entry> addProducts = new ArrayList<Entry>();
                final ArrayList<Entry> buyProducts = new ArrayList<Entry>();
                final ArrayList<Entry> valueAbandonedProducts = new ArrayList<Entry>();
                final ArrayList<Entry> valueAddToCard = new ArrayList<Entry>();
                final ArrayList<Entry> valueBuyProducts = new ArrayList<Entry>();

                float allAbandonedProducts = 0.0f;
                float allAddProducts = 0.0f;
                float allBuyProducts = 0.0f;
                float allValueAbandonedProducts = 0.0f;
                float allValueAddToCard = 0.0f;
                float allValueBuyProducts = 0.0f;

                float allAbandonedProductsValue = 0.0f;
                float allAddProductsValue = 0.0f;
                float allBuyProductsValue = 0.0f;
                float allValueAbandonedProductsValue = 0.0f;
                float allValueAddToCardValue = 0.0f;
                float allValueBuyProductsValue = 0.0f;


                try {
                    int j = 0;
                    for (int i = finalJSONArray.length() - 1; i >= 0; i--) {
//                    for (int i = 0; i < finalJSONArray.length(); i++) {
                        JSONObject jsonObject = finalJSONArray.getJSONObject(i);
                        if (jsonObject.getString("currency").compareToIgnoreCase("PLN") == 0) {

                            allAbandonedProductsValue = (float) jsonObject.getDouble("abondedProducts");
                            allAddProductsValue = (float) jsonObject.getDouble("addProducts");
                            allBuyProductsValue = (float) jsonObject.getDouble("buyProducts");
                            allValueAbandonedProductsValue = (float) jsonObject.getDouble("valueAbondedProducts");
                            allValueAddToCardValue = (float) jsonObject.getDouble("valueAddToCard");
                            allValueBuyProductsValue = (float) jsonObject.getDouble("valueBuyProducts");

                            allAbandonedProducts += allAbandonedProductsValue;
                            allAddProducts += allAddProductsValue;
                            allBuyProducts += allBuyProductsValue;
                            allValueAbandonedProducts += allValueAbandonedProductsValue;
                            allValueAddToCard += allValueAddToCardValue;
                            allValueBuyProducts += allValueBuyProductsValue;

                            abandonedProducts.add(new Entry(j, allAbandonedProductsValue));
                            addProducts.add(new Entry(j, allAddProductsValue));
                            buyProducts.add(new Entry(j, allBuyProductsValue));
                            valueAbandonedProducts.add(new Entry(j, allValueAbandonedProductsValue));
                            valueAddToCard.add(new Entry(j, allValueAddToCardValue));
                            valueBuyProducts.add(new Entry(j, allValueBuyProductsValue));

                            j++;
                            labels.add(jsonObject.getString("createDate"));

                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                LineDataSet abandonedProductsSet = new LineDataSet(abandonedProducts, "abandonedProducts");
                LineDataSet addProductsSet = new LineDataSet(addProducts, "addProducts");
                LineDataSet buyProductsSet = new LineDataSet(buyProducts, "buyProducts");
                LineDataSet valueAbandonedProductsSet = new LineDataSet(valueAbandonedProducts, "valueAbandonedProducts");
                LineDataSet valueAddToCardSet = new LineDataSet(valueAddToCard, "valueAddToCard");
                LineDataSet valueBuyProductsSet = new LineDataSet(valueBuyProducts, "valueBuyProducts");


                int blue = getResources().getColor(R.color.fancyBlue);
                int blueAlpha = getResources().getColor(R.color.fancyBlueAlpha);

                ArrayList<LineDataSet> sets = new ArrayList<LineDataSet>();

                sets.add(abandonedProductsSet);
                sets.add(addProductsSet);
                sets.add(buyProductsSet);
                sets.add(valueAbandonedProductsSet);
                sets.add(valueAddToCardSet);
                sets.add(valueBuyProductsSet);


                for (LineDataSet set : sets) {
                    set.setColor(blue);
                    set.setFillColor(blue);
                    set.setFillFormatter(new YourFillFormatter());
                    set.setFillColor(blue);
                    set.setDrawCircles(false);
                    set.setDrawFilled(true);
                    set.setFillAlpha(10);
                    set.setFillFormatter(new YourFillFormatter());
                    set.setMode(LineDataSet.Mode.CUBIC_BEZIER);
                    set.setDrawHighlightIndicators(true);
                    set.setDrawHorizontalHighlightIndicator(false);
                    set.setDrawVerticalHighlightIndicator(false);
                }


                abandonedProductsData = new LineData(abandonedProductsSet);
                addProductsData = new LineData(addProductsSet);
                buyProductsData = new LineData(buyProductsSet);
                valueAbandonedProductsData = new LineData(valueAbandonedProductsSet);
                valueAddToCardData = new LineData(valueAddToCardSet);
                valueBuyProductsData = new LineData(valueBuyProductsSet);

                abandonedProductsData.setDrawValues(false);
                addProductsData.setDrawValues(false);
                buyProductsData.setDrawValues(false);
                valueAbandonedProductsData.setDrawValues(false);
                valueAddToCardData.setDrawValues(false);
                valueBuyProductsData.setDrawValues(false);


                final Activity activity = getActivity();
                if (activity != null) {

                    final float abandonedProductsAll = allAbandonedProducts;
                    final float addProductsAll = allAddProducts;
                    final float buyProductsAll = allBuyProducts;
                    final float valueAbandonedProductsAll = allValueAbandonedProducts;
                    final float valueAddToCardAll = allValueAddToCard;
                    final float valueBuyProductsAll = allValueBuyProducts;
                    final boolean finalCacheUsed = cacheUsed;
                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            View rootView = getView();
                            if (rootView == null) {
                                return;
                            }
                            SyneChartTitle abandonedBasketsTitle = ((SyneChartTitle) rootView.findViewById(R.id.abandonedBasketsTitle));
                            SyneChartTitle purchasedProductsValueTitle = ((SyneChartTitle) rootView.findViewById(R.id.purchasedProductsValueTitle));
                            SyneChartTitle purchasedProductsTitle = ((SyneChartTitle) rootView.findViewById(R.id.purchasedProductsTitle));
                            SyneChartTitle abanodnedBasketsValueTitle = ((SyneChartTitle) rootView.findViewById(R.id.abanodnedBasketsValueTitle));
                            SyneChartTitle productsAddedToCartValueTitle = ((SyneChartTitle) rootView.findViewById(R.id.productsAddedToCartValueTitle));
                            SyneChartTitle productsAddedToCartTitle = ((SyneChartTitle) rootView.findViewById(R.id.productsAddedToCartTitle));

                            LineChart abandonedBasketsChart = ((LineChart) rootView.findViewById(R.id.AbandonedBasketsChart));
                            LineChart productsAddedToCartChart = ((LineChart) rootView.findViewById(R.id.ProductsAddedToCartChart));
                            LineChart purchasedProductsChart = ((LineChart) rootView.findViewById(R.id.purchasedProductsChart));
                            LineChart abanodnedBasketsValueChart = ((LineChart) rootView.findViewById(R.id.AbanodnedBasketsValueChart));
                            LineChart productsAddedToCartValueChart = ((LineChart) rootView.findViewById(R.id.ProductsAddedToCartValueChart));
                            LineChart purchasedProductsValueChart = ((LineChart) rootView.findViewById(R.id.PurchasedProductsValueChart));

                            if (abandonedBasketsTitle == null ||
                                    productsAddedToCartTitle == null ||
                                    purchasedProductsTitle == null ||
                                    abanodnedBasketsValueTitle == null ||
                                    productsAddedToCartValueTitle == null ||
                                    purchasedProductsValueTitle == null ||
                                    abandonedBasketsChart == null ||
                                    productsAddedToCartChart == null ||
                                    purchasedProductsChart == null ||
                                    abanodnedBasketsValueChart == null ||
                                    productsAddedToCartValueChart == null ||
                                    purchasedProductsValueChart == null
                                    ) {
                                return;
                            }


                            abandonedBasketsTitle.setFloatCounter(abandonedProductsAll);
                            purchasedProductsValueTitle.setFloatCounter(valueBuyProductsAll);
                            purchasedProductsTitle.setFloatCounter(buyProductsAll);
                            abanodnedBasketsValueTitle.setFloatCounter(valueAbandonedProductsAll);
                            productsAddedToCartValueTitle.setFloatCounter(valueAddToCardAll);
                            productsAddedToCartTitle.setFloatCounter(addProductsAll);

                            abandonedBasketsChart.setData(abandonedProductsData);
                            productsAddedToCartChart.setData(addProductsData);
                            purchasedProductsChart.setData(buyProductsData);
                            abanodnedBasketsValueChart.setData(valueAbandonedProductsData);
                            productsAddedToCartValueChart.setData(valueAddToCardData);
                            purchasedProductsValueChart.setData(valueBuyProductsData);


                            LineChartMarkerView mv = new LineChartMarkerView(getContext(), R.layout.custom_marker_view);
                            mv.setChartView(abandonedBasketsChart); // For bounds control
                            mv.setDates(labels);
                            mv.setLabels(new String[]{"Units"});
                            abandonedBasketsChart.setMarker(mv);


                            abandonedBasketsChart.getAxisLeft().setDrawGridLines(false);
                            abandonedBasketsChart.getAxisLeft().setDrawZeroLine(false);

//                            LimitLine limitLine = new LimitLine(0);
//                            limitLine.setLineColor(getResources().getColor(R.color.fancyGray));
//                            limitLine.enableDashedLine(2, 1, 0);
//
//
//                            abandonedBasketsChart.getAxisLeft().addLimitLine(limitLine);
//                            productsAddedToCartChart.getAxisLeft().addLimitLine(limitLine);
//                            purchasedProductsChart.getAxisLeft().addLimitLine(limitLine);
//                            abanodnedBasketsValueChart.getAxisLeft().addLimitLine(limitLine);
//                            productsAddedToCartValueChart.getAxisLeft().addLimitLine(limitLine);
//                            purchasedProductsValueChart.getAxisLeft().addLimitLine(limitLine);


                            LineChartMarkerView mv1 = new LineChartMarkerView(getContext(), R.layout.custom_marker_view);
                            mv1.setChartView(productsAddedToCartChart); // For bounds control
                            mv1.setDates(labels);
                            mv1.setLabels(new String[]{"Units"});
                            productsAddedToCartChart.setMarker(mv1);

                            LineChartMarkerView mv2 = new LineChartMarkerView(getContext(), R.layout.custom_marker_view);
                            mv2.setChartView(purchasedProductsChart); // For bounds control
                            mv2.setDates(labels);
                            mv2.setLabels(new String[]{"Units"});
                            purchasedProductsChart.setMarker(mv2);

                            LineChartMarkerView mv3 = new LineChartMarkerView(getContext(), R.layout.custom_marker_view);
                            mv3.setChartView(abanodnedBasketsValueChart); // For bounds control
                            mv3.setDates(labels);
                            mv3.setLabels(new String[]{"Value"});
                            abanodnedBasketsValueChart.setMarker(mv3);

                            LineChartMarkerView mv4 = new LineChartMarkerView(getContext(), R.layout.custom_marker_view);
                            mv4.setChartView(productsAddedToCartValueChart); // For bounds control
                            mv4.setDates(labels);
                            mv4.setLabels(new String[]{"Value"});
                            productsAddedToCartValueChart.setMarker(mv4);

                            LineChartMarkerView mv5 = new LineChartMarkerView(getContext(), R.layout.custom_marker_view);
                            mv5.setChartView(purchasedProductsValueChart); // For bounds control
                            mv5.setDates(labels);
                            mv5.setLabels(new String[]{"Value"});
                            purchasedProductsValueChart.setMarker(mv5);

                            if (!isBasketsInitialized) {
                                CommerceFragment.initLineChart(abandonedBasketsChart, labels);
                                CommerceFragment.initLineChart(productsAddedToCartChart, labels);
                                CommerceFragment.initLineChart(purchasedProductsChart, labels);
                                CommerceFragment.initLineChart(abanodnedBasketsValueChart, labels);
                                CommerceFragment.initLineChart(productsAddedToCartValueChart, labels);
                                CommerceFragment.initLineChart(purchasedProductsValueChart, labels);

                                abandonedBasketsChart.invalidate();
                                productsAddedToCartChart.invalidate();
                                purchasedProductsChart.invalidate();
                                abanodnedBasketsValueChart.invalidate();
                                productsAddedToCartValueChart.invalidate();
                                purchasedProductsValueChart.invalidate();

                                if (finalCacheUsed) {
                                    abandonedBasketsChart.setAlpha(0.4f);
                                    productsAddedToCartChart.setAlpha(0.4f);
                                    purchasedProductsChart.setAlpha(0.4f);
                                    abanodnedBasketsValueChart.setAlpha(0.4f);
                                    productsAddedToCartValueChart.setAlpha(0.4f);
                                    purchasedProductsValueChart.setAlpha(0.4f);
                                }
                            } else {
                                abandonedBasketsChart.setAlpha(1f);
                                productsAddedToCartChart.setAlpha(1f);
                                purchasedProductsChart.setAlpha(1f);
                                abanodnedBasketsValueChart.setAlpha(1f);
                                productsAddedToCartValueChart.setAlpha(1f);
                                purchasedProductsValueChart.setAlpha(1f);

                                abandonedBasketsChart.notifyDataSetChanged();
                                productsAddedToCartChart.notifyDataSetChanged();
                                purchasedProductsChart.notifyDataSetChanged();
                                abanodnedBasketsValueChart.notifyDataSetChanged();
                                productsAddedToCartValueChart.notifyDataSetChanged();
                                purchasedProductsValueChart.notifyDataSetChanged();
                            }
                        }
                    });
                }
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (NullPointerException e) {

            } catch (IllegalStateException e) {

            }
            return "ok";
        }


        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            Handler myHandler = new Handler();
            myHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    Fragment fragment = getActivity().getSupportFragmentManager().findFragmentById(R.id.mainContainer);

                    if (isScreenActive && getActivity() != null && fragment instanceof CommerceFragment) {
                        new GetBasketCharts().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                    }
                }
            }, 3000);

        }
    }


    private class GetReturned extends AsyncTask<Void, Void, String> {
        protected String doInBackground(Void... params) {
            try {

                String url = "https://api.synerise.com/rtom/stats/transaction/return-transaction";

                HttpGet httpget = new HttpGet(url + CommerceFragment.getParams());

                SyneHttpCacheHandler handler = new SyneHttpCacheHandler();
                boolean cacheUsed = false;
                if (!isReturnedInitialized) {
                    cacheUsed = MainActivity.cacheString.contains(SyneHttpCacheHandler.getCacheKeyFromUrl(createCacheKey(url)));
                }
                final String body = handler.executeWithCacheKey(httpget, createCacheKey(url), !isReturnedInitialized, true);

                if (body == null) {
                    return "not ok";
                }

                JSONArray jsonArray = null;

                try {
                    jsonArray = new JSONArray(body);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                final JSONArray finalJSONArray = jsonArray;


                final ArrayList<String> labels = new ArrayList<String>();
                final ArrayList<Entry> returnedProductsValue = new ArrayList<Entry>();
                final ArrayList<Entry> returnedProducts = new ArrayList<Entry>();


                float allReturnProductsValue = 0.0f;
                float allReturnProducts = 0.0f;

                float products;
                float productsValue;


                try {
                    int j = 0;
                    for (int i = finalJSONArray.length() - 1; i >= 0; i--) {
//                    for (int i = 0; i < finalJSONArray.length(); i++) {
                        JSONObject jsonObject = finalJSONArray.getJSONObject(i);
                        if (jsonObject.getString("currency").compareToIgnoreCase("PLN") == 0) {

                            products = (float) jsonObject.getDouble("productsCount");
                            productsValue = (float) jsonObject.getDouble("transactionValue");

                            allReturnProducts += products;
                            allReturnProductsValue += productsValue;

                            returnedProducts.add(new Entry(j, products));
                            returnedProductsValue.add(new Entry(j, productsValue));

                            j++;
                            labels.add(jsonObject.getString("transactionDate"));

                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                LineDataSet allReturnProductsValueSet = new LineDataSet(returnedProductsValue, "returned products value");
                LineDataSet allReturnProductsSet = new LineDataSet(returnedProducts, "returned products");

                int blue = getResources().getColor(R.color.fancyGreen);

                allReturnProductsValueSet.setColor(blue);
                allReturnProductsSet.setColor(blue);
                allReturnProductsValueSet.setFillColor(blue);
                allReturnProductsSet.setFillColor(blue);

                ArrayList<LineDataSet> sets = new ArrayList<LineDataSet>();

                sets.add(allReturnProductsValueSet);
                sets.add(allReturnProductsSet);

                for (LineDataSet set : sets) {
                    set.setDrawCircles(false);
                    set.setFillFormatter(new YourFillFormatter());
                    set.setDrawFilled(true);
                    set.setFillAlpha(10);
                    set.setMode(LineDataSet.Mode.CUBIC_BEZIER);
                    set.setDrawHighlightIndicators(true);
                    set.setDrawHorizontalHighlightIndicator(false);
                    set.setDrawVerticalHighlightIndicator(false);
                }


                final LineData allReturnProductsValueData = new LineData(allReturnProductsValueSet);
                final LineData allReturnProductsData = new LineData(allReturnProductsSet);

                allReturnProductsValueData.setDrawValues(false);
                allReturnProductsData.setDrawValues(false);


                final Activity activity = getActivity();
                if (activity != null) {

                    final float finalAllReturnProductsValue = allReturnProductsValue;
                    final float finalAllReturnProducts = allReturnProducts;
                    final boolean finalCacheUsed = cacheUsed;
                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {


                            View rootView = getView();
                            if (rootView == null) {
                                return;
                            }
                            SyneChartTitle returnedTransactionValueTitle = ((SyneChartTitle) rootView.findViewById(R.id.returnedTransactionValueTitle));
                            SyneChartTitle returnedProductsTitle = ((SyneChartTitle) rootView.findViewById(R.id.returnedProductsTitle));


                            LineChart allReturnProductsChart = ((LineChart) rootView.findViewById(R.id.ReturnedProductsChart));
                            LineChart allReturnProductsValueChart = ((LineChart) rootView.findViewById(R.id.ReturnedTransactionValueChart));

                            if (returnedProductsTitle == null ||
                                    returnedTransactionValueTitle == null ||
                                    allReturnProductsChart == null ||
                                    allReturnProductsValueChart == null
                                    ) {
                                return;
                            }

                            returnedProductsTitle.setFloatCounter(finalAllReturnProducts);
                            returnedTransactionValueTitle.setFloatCounter(finalAllReturnProductsValue);

                            allReturnProductsChart.setData(allReturnProductsData);
                            allReturnProductsValueChart.setData(allReturnProductsValueData);

                            LineChartMarkerView mv = new LineChartMarkerView(getContext(), R.layout.custom_marker_view);
                            mv.setChartView(allReturnProductsChart); // For bounds control
                            mv.setDates(labels);
                            mv.setLabels(new String[]{"Value"});
                            allReturnProductsChart.setMarker(mv);

                            LineChartMarkerView mv2 = new LineChartMarkerView(getContext(), R.layout.custom_marker_view);
                            mv2.setChartView(allReturnProductsValueChart); // For bounds control
                            mv2.setDates(labels);
                            mv2.setLabels(new String[]{"Units"});
                            allReturnProductsValueChart.setMarker(mv2);

                            allReturnProductsChart.getAxisLeft().setDrawZeroLine(true);
                            allReturnProductsValueChart.getAxisLeft().setDrawZeroLine(true);


                            if (!isReturnedInitialized) {
                                CommerceFragment.initLineChart(allReturnProductsChart, labels);
                                CommerceFragment.initLineChart(allReturnProductsValueChart, labels);

                                allReturnProductsChart.invalidate();
                                allReturnProductsValueChart.invalidate();

                                if (finalCacheUsed) {
                                    allReturnProductsChart.setAlpha(0.4f);
                                    allReturnProductsValueChart.setAlpha(0.4f);
                                }
                            } else {
                                allReturnProductsChart.notifyDataSetChanged();
                                allReturnProductsValueChart.notifyDataSetChanged();

                                allReturnProductsChart.setAlpha(1f);
                                allReturnProductsValueChart.setAlpha(1f);
                            }


                        }
                    });
                }
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (NullPointerException e) {

            } catch (IllegalStateException e) {

            }
            return "ok";
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            Handler myHandler = new Handler();
            myHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    Fragment fragment = getActivity().getSupportFragmentManager().findFragmentById(R.id.mainContainer);

                    if (isScreenActive && getActivity() != null && fragment instanceof CommerceFragment) {
                        new GetReturned().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                    }
                }
            }, 3000);

        }
    }

    public class YourFillFormatter implements IFillFormatter {
        @Override
        public float getFillLinePosition(ILineDataSet dataSet, LineDataProvider dataProvider) {
            return -10000000f;
        }
    }

    private class GetEventList extends AsyncTask<Void, Void, String> {
        protected String doInBackground(Void... params) {
            try {

                HttpGet httpget = new HttpGet("https://app.synerise.com/activity-histories/stream-for-business-profile/?action=transaction.charge");

                SyneHttpCacheHandler handler = new SyneHttpCacheHandler();
                final String body = handler.executeWithCache(httpget, !isGetEventsInitialized, true);

                if (body == null) {
                    return "not ok";
                }

                JSONArray jsonArray = null;

                try {
                    jsonArray = new JSONArray(body);
                } catch (JSONException e) {
                    e.printStackTrace();
                }


                assert jsonArray != null;

                if (jsonArray.length() == 0) {
                    return "not ok";
                }

                eventFactory.createEventsArray(events, jsonArray);

                Activity activity = getActivity();
                if (activity != null) {
                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            adapter.notifyDataSetChanged();
                            isGetEventsInitialized = true;
                        }
                    });
                }
            } catch (NullPointerException e) {

            } catch (IllegalStateException e) {

            } catch (JSONException e) {
                e.printStackTrace();
            }
            return "ok";
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            Handler myHandler = new Handler();
            myHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    View rootView = getView();
                    if (rootView == null) {
                        return;
                    }
                    Fragment fragment = getActivity().getSupportFragmentManager().findFragmentById(R.id.mainContainer);

                    if (isScreenActive && ((Switch) rootView.findViewById(R.id.autoloadTrafficSwitch)).isChecked() && fragment instanceof CommerceFragment) {
                        new GetEventList().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                    }
                }
            }, 5000);

        }


    }

    private void loadCharts() {
        new GetRevenue().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        new GetNoOfTransaction().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        new GetUniquePurchases().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        new GetAvarageOrderValue().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        new GetBasketCharts().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        new GetReturned().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        View rootView = getView();
        if (rootView == null) {
            return;
        }
        // Get the ViewPager and set it's PagerAdapter so that it can display items
        ViewPager viewPager = (ViewPager) rootView.findViewById(R.id.viewpager);
        viewPager.setOffscreenPageLimit(4);
        viewPager.setAdapter(new SampleFragmentPagerAdapter(getActivity().getSupportFragmentManager()));

        // Give the PagerSlidingTabStrip the ViewPager
        PagerSlidingTabStrip tabsStrip = (PagerSlidingTabStrip) rootView.findViewById(R.id.tabs);
        // Attach the view pager to the tab strip
        tabsStrip.setViewPager(viewPager);
        tabsStrip.setTextSize(24);
        tabsStrip.setTextColor(Color.parseColor("#FFFFFF"));
        tabsStrip.setIndicatorColor(getResources().getColor(R.color.fancyBlue));
        tabsStrip.setIndicatorHeight(8);

        Tracker.trackScreen("Commerce", new TrackerParams[]{});

        loadCharts();

        initEventsList();
        eventFactory = new EventFactory(getContext());
        new GetEventList().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

        final ImageButton button = (ImageButton) rootView.findViewById(R.id.refreshTrafficButton);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new GetEventList().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                /*Animation animation = AnimationUtils.loadAnimation(getContext(), R.anim.rotate);  animation.setRepeatCount(-1);;               animation.setDuration(1000);                 button.startAnimation(animation); */
            }
        });

        Switch autoload = (Switch) rootView.findViewById(R.id.autoloadTrafficSwitch);
        autoload.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    new GetEventList().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                }
            }
        });


        final SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");

        final TextView dateFromTextView = (TextView) rootView.findViewById(R.id.dateFrom);

        dateFromTextView.setText(format.format(dateFrom.getTime()));

        final DatePickerDialog dateFromPickerDialog = new DatePickerDialog(getContext(), new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                dateFrom.set(year, monthOfYear, dayOfMonth);
                dateFromTextView.setText(format.format(dateFrom.getTime()));
                loadCharts();
            }

        }, dateFrom.get(Calendar.YEAR), dateFrom.get(Calendar.MONTH), dateFrom.get(Calendar.DAY_OF_MONTH));


        dateFromTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                dateFromPickerDialog.show();
            }
        });

        TextView dateToTextView = (TextView) rootView.findViewById(R.id.dateTo);

        final DatePickerDialog dateToPickerDialog = new DatePickerDialog(getContext(), new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                dateFrom.set(year, monthOfYear, dayOfMonth);
                dateFromTextView.setText(format.format(dateFrom.getTime()));
                loadCharts();
            }

        }, dateFrom.get(Calendar.YEAR), dateFrom.get(Calendar.MONTH), dateFrom.get(Calendar.DAY_OF_MONTH));

        dateToTextView.setText(format.format(dateTo.getTime()));

        dateToTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                dateToPickerDialog.show();
            }
        });


        hour = (TextView) rootView.findViewById(R.id.datePickerHour);
        day = (TextView) rootView.findViewById(R.id.datePickerDay);
        week = (TextView) rootView.findViewById(R.id.datePickerWeek);
        month = (TextView) rootView.findViewById(R.id.datePickerMonth);
        year = (TextView) rootView.findViewById(R.id.datePickerYear);

        int gray = getResources().getColor(R.color.textDarkGray);

        hour.setTextColor(gray);
        day.setTextColor(gray);
        week.setTextColor(gray);
        month.setTextColor(gray);
        year.setTextColor(gray);


        ((RelativeLayout) rootView.findViewById(R.id.datePickerWeekBox)).setVisibility(View.GONE);

//        week.setVisibility(View.INVISIBLE);

        hourLine = rootView.findViewById(R.id.datePickerHourLine);
        dayLine = rootView.findViewById(R.id.datePickerDayLine);
        weekLine = rootView.findViewById(R.id.datePickerWeekLine);
        monthLine = rootView.findViewById(R.id.datePickerMonthLine);
        yearLine = rootView.findViewById(R.id.datePickerYearLine);


        setChoosed("day");


        hour.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                groupDate = "yyyy-MM-dd+HH";
                minPeriod = "hh";

                loadCharts();

                setChoosed("hour");
                return true;
            }
        });


        day.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                groupDate = "yyyy-MM-dd";
                minPeriod = "DD";

                loadCharts();

                setChoosed("day");
                return true;
            }
        });

        week.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                //// TODO: 19.02.2017 ogarnąć
                groupDate = "yyyy-MM";
                minPeriod = "DD";

                loadCharts();

                setChoosed("week");

                return true;
            }
        });


        month.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                groupDate = "yyyy-MM";
                minPeriod = "DD";

                loadCharts();

                setChoosed("month");

                return true;
            }
        });


        year.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                groupDate = "yyyy";
                minPeriod = "MM";

                loadCharts();

                setChoosed("year");
                return true;
            }
        });

    }

    void releaseButtons() {
        hour.setPressed(false);
        day.setPressed(false);
        month.setPressed(false);
        year.setPressed(false);

    }

    public static String getParams() {
        return "?groupDate=" + groupDate + "&dateTo=" + dateTo.getTimeInMillis() + "&dateFrom=" + dateFrom.getTimeInMillis() + "&minPeriod=" +
                minPeriod + "&date[dateFormat]=" + dateFateFormat + "&date[dateTo]=" + dateTo.getTimeInMillis() + "&date[dateFrom]=" + dateFrom.getTimeInMillis() +
                "&date[minPeriod]=" + dateMinPeriod + "&client=" + client + "&date[group]=" + dateGroup;
    }


    public static void initLineChart(LineChart chart, Collection<String> labels) {
        chart.setDescription(new Description());
        chart.getDescription().setEnabled(false);
        chart.setDrawGridBackground(false);
        chart.setDrawGridBackground(false);
        chart.getXAxis().setValueFormatter(new IndexAxisValueFormatter(labels));
        chart.getXAxis().setTypeface(tf);
        chart.getXAxis().setTextColor(fancyGray);
        chart.getAxisLeft().setDrawGridLines(true);
        chart.getAxisLeft().setGridColor(fancyGray);
        chart.getAxisLeft().setDrawAxisLine(false);
        chart.getAxisLeft().setTypeface(tf);
        chart.getAxisLeft().setTextColor(fancyGray);
        chart.getAxisLeft().setDrawGridLines(false);
        chart.getXAxis().setDrawGridLines(false);
        chart.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM);
        chart.getXAxis().setLabelCount(3);
        chart.getAxisRight().setDrawAxisLine(false);
        chart.getAxisRight().setEnabled(false);
        chart.getAxisRight().setDrawGridLines(false);
        chart.setDrawBorders(false);
        chart.getAxisRight().setEnabled(false);


        XAxis xAxis = chart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        chart.setBackgroundColor(Color.parseColor("#FFFFFF"));
        chart.setDrawGridBackground(false);

        chart.setViewPortOffsets(0f, 40f, 0f, 40f);


        chart.getAxisLeft().setPosition(YAxis.YAxisLabelPosition.INSIDE_CHART);
        chart.getAxisLeft().setYOffset(-10);

        Legend l = chart.getLegend();
        l.setEnabled(false);


    }

    class LineChartSelectedValueListener implements OnChartValueSelectedListener {

        LineChart chart = null;
        String[] labels;
        List<String> dates;
        TextView textView;


        public void setChart(LineChart chart) {
            this.chart = chart;
        }

        public LineChartSelectedValueListener(LineChart chart, String[] labels, List<String> dates, TextView textView) {
            this.chart = chart;
            this.labels = labels;
            this.dates = dates;
            this.textView = textView;
        }

        @Override
        public void onValueSelected(Entry e, Highlight h) {

            String x = "";
            String y = "";
            String date;

            DecimalFormat df = new DecimalFormat("###,###,###.##");

            y = df.format((double) e.getY());
            x = labels[h.getDataSetIndex()];


            date = dates.get((int) e.getX());

            textView.setText(x + " on " + date + " : " + y);
        }

        @Override
        public void onNothingSelected() {
        }
    }

    private void setChoosed(String type) {
        int gray = getResources().getColor(R.color.fancyGray);
        int blue = getResources().getColor(R.color.fancyBlue);
        hourLine.setBackgroundColor(gray);
        dayLine.setBackgroundColor(gray);
        weekLine.setBackgroundColor(gray);
        monthLine.setBackgroundColor(gray);
        yearLine.setBackgroundColor(gray);

        hour.setTextColor(gray);
        day.setTextColor(gray);
        week.setTextColor(gray);
        month.setTextColor(gray);
        year.setTextColor(gray);

        switch (type) {
            case "hour":
                hour.setTextColor(blue);
                hourLine.setBackgroundColor(blue);
                break;
            case "day":
                day.setTextColor(blue);
                dayLine.setBackgroundColor(blue);
                break;
            case "week":
                week.setTextColor(blue);
                weekLine.setBackgroundColor(blue);
                break;
            case "month":
                month.setTextColor(blue);
                monthLine.setBackgroundColor(blue);
                break;
            case "year":
                year.setTextColor(blue);
                yearLine.setBackgroundColor(blue);
                break;
        }
    }

    private String createCacheKey(String key) {

        return key + "?groupDate=" + groupDate + "&dateTo=" + "&minPeriod=" +
                minPeriod + "&date[dateFormat]=" + dateFateFormat + "&date[dateTo]=" + dateTo.getTimeInMillis() + "&date[dateFrom]=" + dateFrom.getTimeInMillis() +
                "&date[minPeriod]=" + dateMinPeriod + "&client=" + client + "&date[group]=" + dateGroup;
    }
}

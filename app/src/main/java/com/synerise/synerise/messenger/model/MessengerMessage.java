package com.synerise.synerise.messenger.model;

import com.synerise.synerise.messenger.model.messageBody.MessageBody;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;

/**
 * Created by dsiedlarz on 11.03.2017.
 */
public class MessengerMessage {

    private String type;
    private String id;
    private String messageText;
    private String messageSource;
    private String messagePlatformInfo;
    private String messageType;
    private Date messageTime;
    private String messageId;
    private String roomId;
    private MessengerUser author;
    private MessageBody messageBody;
    private String formattedTime;

    private Boolean avatarPrintable = null;




    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMessageSource() {
        return messageSource;
    }

    public void setMessageSource(String messageSource) {
        this.messageSource = messageSource;
    }

    public String getMessagePlatformInfo() {
        return messagePlatformInfo;
    }

    public void setMessagePlatformInfo(String messagePlatformInfo) {
        this.messagePlatformInfo = messagePlatformInfo;
    }

    public String getMessageType() {
        return messageType;
    }

    public void setMessageType(String messageType) {
        this.messageType = messageType;
    }

    public Date getMessageTime() {
        return messageTime;
    }

    public void setMessageTime(Date messageTime) {
        this.messageTime = messageTime;
    }

    public String getMessageId() {
        return messageId;
    }

    public void setMessageId(String messageId) {
        this.messageId = messageId;
    }

    public String getRoomId() {
        return roomId;
    }

    public void setRoomId(String roomId) {
        this.roomId = roomId;
    }

    public MessengerUser getAuthor() {
        return author;
    }

    public void setAuthor(MessengerUser author) {
        this.author = author;
    }

    public Boolean getAvatarPrintable() {
        return avatarPrintable;
    }

    public void setAvatarPrintable(Boolean avatarPrintable) {
        this.avatarPrintable = avatarPrintable;
    }

    public String getMessageText() {
        return messageText;
    }

    public void setMessageText(String messageText) {
        this.messageText = messageText;
    }

    public MessageBody getMessageBody() {
        return messageBody;
    }

    public void setMessageBody(MessageBody messageBody) {
        this.messageBody = messageBody;
    }

    public String getFormattedTime() {
        return formattedTime;
    }

    public void setFormattedTime(String formattedTime) {
        this.formattedTime = formattedTime;
    }

    public MessengerMessage(JSONObject object) {
        try {
            if (object.has("type")) {
                this.type = object.getString("type");
            }
            if (object.has("id")) {
                this.id = object.getString("id");
            }
            if (object.has("attributes")) {
                JSONObject attributes = object.getJSONObject("attributes");

                if (attributes.has("body")) {
                    this.messageText = attributes.getString("body");
                }
                if (attributes.has("source")) {
                    this.messageSource = attributes.getString("source");
                }
                if (attributes.has("platformInfo")) {
                    this.messagePlatformInfo = attributes.getString("platformInfo");
                }
                if (attributes.has("type")) {
                    this.messageType = attributes.getString("type");
                }
                if (attributes.has("time")) {
                    this.messageTime = new Date(attributes.getLong("time"));
                }
            }


            if (object.has("relationships")) {
                JSONObject relationships = object.getJSONObject("relationships");

                if (relationships.has("room")) {
                    JSONObject room = relationships.getJSONObject("room");

                    if (room.has("data")) {
                        JSONObject data = room.getJSONObject("data");
                        if (data.has("id")) {
                            this.roomId = data.getString("id");
                        }
                    }
                }

                if (relationships.has("client")) {
                    this.author = new MessengerClient(relationships.getJSONObject("client"));
                }

                if (relationships.has("vendor")) {
                    this.author = new MessengerVendor(relationships.getJSONObject("vendor"));
                }

            }


        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public MessengerMessage() {
    }
}


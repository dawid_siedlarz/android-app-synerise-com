package com.synerise.synerise.app.crmFilters;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.synerise.synerise.R;
import com.synerise.synerise.app.adapter.SimpleFiltersArrayAdapter;
import com.synerise.synerise.app.model.FilterAttributes;

public class Filters extends AppCompatActivity {

    public static final int REQUEST_EXIT = 65;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filters);

        RecyclerView listView = (RecyclerView) findViewById(R.id.clientFilters);
        SimpleFiltersArrayAdapter adapter = new SimpleFiltersArrayAdapter(this, FilterAttributes.ITEMS);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        listView.setLayoutManager(linearLayoutManager);
        listView.setAdapter(adapter);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_EXIT) {
            if (resultCode == RESULT_OK) {
                this.finish();
            }
        }
    }
}

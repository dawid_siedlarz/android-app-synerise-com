package com.synerise.synerise.app.crmFilters;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.synerise.synerise.R;
import com.synerise.synerise.app.adapter.SimpleSegmentsArrayAdapter;
import com.synerise.synerise.app.fragment.CRMFragment;
import com.synerise.synerise.app.model.Segment;
import com.synerise.synerise.app.model.Tag;

public class SegmentsActivity extends AppCompatActivity {
    private SimpleSegmentsArrayAdapter segmentsAdapter;
    private ListView segmentsFilterList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_segments);

        segmentsFilterList = (ListView) findViewById(R.id.segmentsFilterList);
        segmentsAdapter = new SimpleSegmentsArrayAdapter(getApplicationContext(), Segment.ITEMS);
        segmentsFilterList.setAdapter(segmentsAdapter);
        segmentsFilterList.setOnItemClickListener(
                new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                        CRMFragment.listingChooseSmartSegmentchooseColumnValue.add(Tag.ITEMS.get(position).name);
                        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.mainContainer);
                        ((CRMFragment) fragment).getClients();
                        finish();
                    }
                }
        );
    }
}

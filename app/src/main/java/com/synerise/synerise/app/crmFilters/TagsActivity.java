package com.synerise.synerise.app.crmFilters;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.synerise.synerise.R;
import com.synerise.synerise.app.adapter.SimpleTagsArrayAdapter;
import com.synerise.synerise.app.fragment.CRMFragment;
import com.synerise.synerise.app.model.Tag;

public class TagsActivity extends AppCompatActivity {
    private SimpleTagsArrayAdapter tagsAdapter;
    private ListView tagListView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tags);

        tagListView = (ListView) findViewById(R.id.tagFilterList);
        tagsAdapter = new SimpleTagsArrayAdapter(getApplicationContext(), Tag.ITEMS);
        tagListView.setAdapter(tagsAdapter);
        tagListView.setOnItemClickListener(
                new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                        CRMFragment.listingChooseTagchooseColumnValue.add(Tag.ITEMS.get(position).name);
                        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.mainContainer);
                        ((CRMFragment) fragment).getClients();
                        finish();
                    }
                }
        );

    }
}

package com.synerise.synerise.app.model.campaign;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONException;
import org.json.JSONObject;

public class MobilePushCampaign extends Campaign implements Parcelable{
    private int read;
    private int delivered;
    private int sent;
    private String createdBySource;
    private String uuid;

    public MobilePushCampaign(JSONObject jsonObject) {
        super(jsonObject);

        try {
            read = jsonObject.getInt("read");
            delivered = jsonObject.getInt("delivered");
            sent = jsonObject.getInt("sent");
            createdBySource = jsonObject.getString("createdBySource");
            uuid = jsonObject.getString("uuid");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public int getRead() {
        return read;
    }

    public void setRead(int read) {
        this.read = read;
    }

    public int getDelivered() {
        return delivered;
    }

    public void setDelivered(int delivered) {
        this.delivered = delivered;
    }

    public int getSent() {
        return sent;
    }

    public void setSent(int sent) {
        this.sent = sent;
    }

    public String getCreatedBySource() {
        return createdBySource;
    }

    public void setCreatedBySource(String createdBySource) {
        this.createdBySource = createdBySource;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    protected MobilePushCampaign(Parcel in) {
        super(in);
        read = in.readInt();
        delivered = in.readInt();
        sent = in.readInt();
        createdBySource = in.readString();
        uuid = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);

        dest.writeInt(read);
        dest.writeInt(delivered);
        dest.writeInt(sent);
        dest.writeString(createdBySource);
        dest.writeString(uuid);
    }

    @Override
    public int describeContents() {
        return super.describeContents();
    }
}

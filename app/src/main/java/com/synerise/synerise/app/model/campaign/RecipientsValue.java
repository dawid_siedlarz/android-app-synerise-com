package com.synerise.synerise.app.model.campaign;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONException;
import org.json.JSONObject;

public class RecipientsValue implements Parcelable {
    private int id;
    private String name;

    public RecipientsValue(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public RecipientsValue(JSONObject object) {
        try {
            this.id = object.getInt("id");
            this.name = object.getString("name");

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    protected RecipientsValue(Parcel in) {
        id = in.readInt();
        name = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(name);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<RecipientsValue> CREATOR = new Creator<RecipientsValue>() {
        @Override
        public RecipientsValue createFromParcel(Parcel in) {
            return new RecipientsValue(in);
        }

        @Override
        public RecipientsValue[] newArray(int size) {
            return new RecipientsValue[size];
        }
    };

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}

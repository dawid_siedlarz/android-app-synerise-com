package com.synerise.synerise.app.syneview;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;

import com.synerise.synerise.R;

public class SyneTextView extends android.support.v7.widget.AppCompatTextView {
    private Context context;

    public SyneTextView(Context context) {
        super(context);
        this.context = context;
        initFont();
    }

    public SyneTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        initFont();
    }

    public SyneTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.context = context;
        initFont();
    }

    private void initFont() {
        Typeface tf = Typeface.createFromAsset(context.getAssets(),
                "fonts/Lato-Regular.ttf");
        setTypeface(tf);
        setTextColor(getResources().getColor(R.color.textDarkGray));
    }
}

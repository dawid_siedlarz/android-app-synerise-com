package com.synerise.synerise.app.syneview;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.synerise.synerise.R;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;

public class SyneChartTitle extends RelativeLayout {
    View view;
    TextView title;
    TextView counter;
    TextView unit;

    public static DecimalFormat formatter = new DecimalFormat("###,###,###");
    DecimalFormat floatFormatter;


    public SyneChartTitle(Context context, AttributeSet attrs) {
        super(context, attrs);
        initViews();
        TypedArray a = context.getTheme().obtainStyledAttributes(
                attrs,
                R.styleable.SyneChartTitle,
                0, 0);

        try {
            String titleText = a.getString(R.styleable.SyneChartTitle_title);
            String unitText = a.getString(R.styleable.SyneChartTitle_unit);

            title.setText(titleText);
            unit.setText(unitText);
        } finally {
            a.recycle();
        }

        DecimalFormatSymbols symbols = DecimalFormatSymbols.getInstance();
        symbols.setGroupingSeparator(' ');
        formatter = new DecimalFormat("###,###,###", symbols);
        floatFormatter = new DecimalFormat("###,###,###.00", symbols);
    }


    private void initViews() {
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view = inflater.inflate(R.layout.syne_chart_title, this, true);

        title = (TextView) view.findViewById(R.id.title);
        counter = (TextView) view.findViewById(R.id.counter);
        unit = (TextView) view.findViewById(R.id.unit);

        title.setText("");
        unit.setText("");
    }

    public void setIntCounter(int value) {
        counter.setText(formatter.format(value));
    }

    public void setFloatCounter(float value) {
        counter.setText(floatFormatter.format(value));
    }

    public void setDoubleCounter(double value) {
        counter.setText(floatFormatter.format(value));
    }

    public void setCounter(String value) {
        counter.setText(value);
    }

    public void setTitle(String titleText) {
        this.title.setText(titleText);
    }

    public void setUnit(String unitText){
        this.unit.setText(unitText);
    }

    public void clearCountAndUnit() {
        counter.setText("");
        unit.setAlpha(0);
        counter.setAlpha(0);
    }

    public void setStringCounter(String value){

    }

    public void hide(){
        view.setVisibility(GONE);
    }
}

package com.synerise.synerise.app.fragment.campaign.statistic;

import android.graphics.Color;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.text.style.TypefaceSpan;

import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;

import java.text.DecimalFormat;


public class PieChartSelectedValueListener implements OnChartValueSelectedListener {

    PieChart chart = null;


    public void setChart(PieChart chart) {
        this.chart = chart;
    }

    public PieChartSelectedValueListener(PieChart chart) {
        this.chart = chart;
    }

    @Override
    public void onValueSelected(Entry e, Highlight h) {
        drawCenter(chart, h);
    }

    @Override
    public void onNothingSelected() {
        chart.setCenterText("");
    }

    public void drawCenter(PieChart chart, Highlight h) {
        DecimalFormat df = new DecimalFormat("#.##");
        Entry e = chart.getData().getEntryForHighlight(h);
        PieEntry entry = (PieEntry) e;
        String label = entry.getLabel() + "\n";
        String value = (int) entry.getValue() + " ";
        String percent = df.format((double) ((entry.getValue() / chart.getData().getYValueSum()) * 100)) + "%";

        SpannableString s = new SpannableString(label + value + percent);
        s.setSpan(new TypefaceSpan("fonts/Lato-Thin.ttf"), 0, s.length(), 0);
        s.setSpan(new RelativeSizeSpan(1.0f), 0, label.length(), 0);
        s.setSpan(new RelativeSizeSpan(2.0f), label.length(), label.length() + value.length(), 0);
        s.setSpan(new ForegroundColorSpan(Color.BLACK), label.length(), label.length() + value.length(), 0);
        s.setSpan(new RelativeSizeSpan(0.7f), s.length() - percent.length(), s.length(), 0);

        chart.setCenterText(s);

        chart.setCenterTextSize(24);

        chart.setCenterTextColor(Color.parseColor("#647f97"));
    }

}

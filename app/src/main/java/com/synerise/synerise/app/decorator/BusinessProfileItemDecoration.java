package com.synerise.synerise.app.decorator;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.View;

import com.synerise.synerise.R;

import static android.support.v7.widget.RecyclerView.ItemDecoration;
import static android.support.v7.widget.RecyclerView.State;

public class BusinessProfileItemDecoration extends ItemDecoration {
    private Drawable mDivider;
    Float image;

    public BusinessProfileItemDecoration(){

    }

    public BusinessProfileItemDecoration(Context context) {
        mDivider = context.getResources().getDrawable(R.drawable.line_divider);
        image = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 108, context.getResources().getDisplayMetrics());
    }

    @Override
    public void onDrawOver(Canvas c, RecyclerView parent, State state) {
        int left = parent.getPaddingLeft();
        int right = parent.getWidth() - parent.getPaddingRight();
        
        int intrinsic = mDivider.getIntrinsicHeight();



        int childCount = parent.getChildCount();
        for (int i = 0; i < childCount; i++) {
            View child = parent.getChildAt(i);

            RecyclerView.LayoutParams params = (RecyclerView.LayoutParams) child.getLayoutParams();

            int bottom = child.getBottom() + params.bottomMargin;

            mDivider.setBounds(left, bottom, right, bottom + intrinsic*2);
            mDivider.draw(c);
        }
    }
}
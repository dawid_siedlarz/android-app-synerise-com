package com.synerise.synerise.app.model;

import java.util.ArrayList;

public class Tag {

    public static final ArrayList<TagModel> ITEMS = new ArrayList<TagModel>();

    public static void add(String name, String count) {
        ITEMS.add(new TagModel(name, count));
    }

    public static void clear() {
        ITEMS.clear();
    }

    public static class TagModel {
        public String name;
        public String count;

        public TagModel(String name, String count) {
                this.name = name;
                this.count = count;
        }

        @Override
        public String toString() {
            return name + " (" + count + ")";
        }
    }

}

package com.synerise.synerise.messenger.model.messageBody;

import android.content.Context;
import android.view.View;
import android.widget.TextView;

import com.synerise.synerise.app.syneview.SyneTextView;
import com.synerise.synerise.app.syneview.SyneTextViewWhite;
import com.synerise.synerise.messenger.model.MessengerClient;

public class TextBody implements MessageBody {

    TextView textView;

    public TextBody(Context context, String text, String type) {
        switch (type) {
            case MessengerClient.CLIENT_TYPE:
                textView = new SyneTextView(context);
                break;
            case MessengerClient.VENDOR_TYPE:
                textView = new SyneTextViewWhite(context);
                break;
            default:
                textView = new TextView(context);
        }
        textView.setText(text);
    }

    @Override
    public View getBody() {
        return textView;
    }
}

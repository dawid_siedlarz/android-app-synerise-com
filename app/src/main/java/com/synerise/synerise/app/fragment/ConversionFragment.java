package com.synerise.synerise.app.fragment;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.util.Pair;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.synerise.synerise.MainActivity;
import com.synerise.synerise.R;
import com.synerise.synerise.SyneriseOnFragmentInteractionListener;
import com.synerise.synerise.app.conversionCategory.ChooseConversionCategory;
import com.synerise.synerise.app.model.ClientTag;
import com.synerise.synerise.app.syneutil.SyneHttpCacheHandler;
import com.synerise.synerise.app.syneview.SyneBoldTextView;
import com.synerise.synerise.app.syneview.SyneComplexBarChart;
import com.synerise.synerise.app.syneview.SyneComplexLineChart;
import com.synerise.synerise.app.syneview.SyneTextView;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link SyneriseOnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ConversionFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ConversionFragment extends Fragment {

    public static ArrayList<ClientTag> availableTags = new ArrayList<>();
    public static ArrayList<ClientTag> availableSegments = new ArrayList<>();

    private Calendar dateFrom;
    private Calendar dateTo;
    private String group = "day";
    private String minPeriod = "DD";

    private TextView hour;
    private TextView day;
    private TextView week;
    private TextView month;
    private TextView year;

    private View hourLine;
    private View dayLine;
    private View weekLine;
    private View monthLine;
    private View yearLine;


    public static ArrayList<Pair<String, String>> categoryCampaigns = new ArrayList<>();

    private SyneriseOnFragmentInteractionListener mListener;

    private DecimalFormat formatter = new DecimalFormat("###,###,###");


    boolean isConversionInitialized = false;

    SyneComplexBarChart sumTransactionChart;
    SyneComplexLineChart avgTransactionChart;
    SyneComplexLineChart countTransactionChart;
    SyneComplexLineChart visitedPageChart;
    SyneComplexLineChart conversionChart;


    public ConversionFragment() {
        // Required empty public constructor
    }

    public static ConversionFragment newInstance(String param1, String param2) {
        return new ConversionFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_conversion, container, false);
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof SyneriseOnFragmentInteractionListener) {
            mListener = (SyneriseOnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public void getConversion() {
        new GetConversion().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        Activity activity = getActivity();
        ActionBar actionBar = ((AppCompatActivity) activity).getSupportActionBar();
        actionBar.show();
        actionBar.setSubtitle("Conversion");

        View rootView = getView();
        if (rootView == null) {
            return;
        }
        sumTransactionChart = (SyneComplexBarChart) rootView.findViewById(R.id.sumTransactionChart);
        avgTransactionChart = (SyneComplexLineChart) rootView.findViewById(R.id.avgTransactionChart);
        countTransactionChart = (SyneComplexLineChart) rootView.findViewById(R.id.countTransactionChart);
        visitedPageChart = (SyneComplexLineChart) rootView.findViewById(R.id.visitedPageChart);
        conversionChart = (SyneComplexLineChart) rootView.findViewById(R.id.conversionChart);

        new GetClientTags().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        getConversion();

        final SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");

        dateTo = Calendar.getInstance();
        dateFrom = Calendar.getInstance();
        dateFrom.add(Calendar.MONTH, -1);

        final TextView dateFromTextView = (TextView) rootView.findViewById(R.id.dateFrom);

        dateFromTextView.setText(format.format(dateFrom.getTime()));

        final DatePickerDialog dateFromPickerDialog = new DatePickerDialog(getContext(), new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                dateFrom.set(year, monthOfYear, dayOfMonth);
                dateFromTextView.setText(format.format(dateFrom.getTime()));
                getConversion();
            }

        }, dateFrom.get(Calendar.YEAR), dateFrom.get(Calendar.MONTH), dateFrom.get(Calendar.DAY_OF_MONTH));


        dateFromTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                dateFromPickerDialog.show();
            }
        });

        TextView dateToTextView = (TextView) rootView.findViewById(R.id.dateTo);

        final DatePickerDialog dateToPickerDialog = new DatePickerDialog(getContext(), new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                dateFrom.set(year, monthOfYear, dayOfMonth);
                dateFromTextView.setText(format.format(dateFrom.getTime()));
                getConversion();
            }

        }, dateFrom.get(Calendar.YEAR), dateFrom.get(Calendar.MONTH), dateFrom.get(Calendar.DAY_OF_MONTH));

        dateToTextView.setText(format.format(dateTo.getTime()));

        dateToTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                dateToPickerDialog.show();
            }
        });


        hour = (TextView) rootView.findViewById(R.id.datePickerHour);
        day = (TextView) rootView.findViewById(R.id.datePickerDay);
        week = (TextView) rootView.findViewById(R.id.datePickerWeek);
        month = (TextView) rootView.findViewById(R.id.datePickerMonth);
        year = (TextView) rootView.findViewById(R.id.datePickerYear);

        hourLine = rootView.findViewById(R.id.datePickerHourLine);
        dayLine = rootView.findViewById(R.id.datePickerDayLine);
        weekLine = rootView.findViewById(R.id.datePickerWeekLine);
        monthLine = rootView.findViewById(R.id.datePickerMonthLine);
        yearLine = rootView.findViewById(R.id.datePickerYearLine);

        setChoosed("day");


        hour.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                group = "hour";
                minPeriod = "hh";

                getConversion();

                setChoosed("hour");
                return true;
            }
        });


        day.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                group = "day";
                minPeriod = "DD";

                getConversion();

                setChoosed("day");
                return true;
            }
        });

        week.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                group = "weak";
                minPeriod = "DD";

                getConversion();

                setChoosed("weak");
                return true;
            }
        });


        month.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                group = "month";
                minPeriod = "MM";

                getConversion();
                setChoosed("month");

                return true;
            }
        });


        year.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                group = "year";
                minPeriod = "MM";

                getConversion();

                setChoosed("year");
                return true;
            }
        });

        Button chooseButton = (Button) rootView.findViewById(R.id.choose_conversion_category);
        chooseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), ChooseConversionCategory.class);
                getActivity().startActivity(intent);
            }
        });


    }


    private class GetConversion extends AsyncTask<Void, Void, String> {
        protected String doInBackground(Void... params) {
            try {


                String url = "https://api.synerise.com/rtom/stats/conversion/get-for-business-profile-in-time";
                HttpGet httpget = new HttpGet(url + getParams());

                SyneHttpCacheHandler handler = new SyneHttpCacheHandler();
                boolean cacheUsed = false;
                if (!isConversionInitialized) {
                    cacheUsed = MainActivity.cacheString.contains(SyneHttpCacheHandler.getCacheKeyFromUrl(createCacheKey(url)));
                }
                final String body = handler.executeWithCacheKey(httpget, createCacheKey(url), !isConversionInitialized, true);

                if (body == null) {
                    return "not ok";
                }




                JSONArray jsonArray = null;

                try {
                    jsonArray = new JSONArray(body);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                final JSONArray finalJSONArray = jsonArray;


                final ArrayList<String> labels = new ArrayList<String>();
                final ArrayList<BarEntry> sumTransaction = new ArrayList<BarEntry>();
                final ArrayList<Entry> avgTransaction = new ArrayList<Entry>();
                final ArrayList<Entry> countTransaction = new ArrayList<Entry>();
                final ArrayList<Entry> visitedPage = new ArrayList<Entry>();
                final ArrayList<Entry> conversion = new ArrayList<Entry>();

                final ArrayList<Float> sumTransactionArray = new ArrayList<>();
                final ArrayList<Float> avgTransactionArray = new ArrayList<>();
                final ArrayList<Float> countTransactionArray = new ArrayList<>();
                final ArrayList<Float> visitedPageArray = new ArrayList<>();
                final ArrayList<Float> conversionArray = new ArrayList<>();

                float allReturnProductsValue = 0.0f;
                float allReturnProducts = 0.0f;

                float sum;
                float avg;
                float count;
                float visited;
                float conv;


                try {
                    int j = 0;
                    for (int i = finalJSONArray.length() - 1; i >= 0; i--) {
//                    for (int i = 0; i < finalJSONArray.length(); i++) {
                        JSONObject jsonObject = finalJSONArray.getJSONObject(i);

                        sum = (float) (jsonObject.isNull("sumTransaction") ? 0 : jsonObject.getDouble("sumTransaction"));
                        avg = (float) (jsonObject.isNull("avgTransaction") ? 0 : jsonObject.getDouble("avgTransaction"));
                        count = (float) (jsonObject.isNull("countTransaction") ? 0 : jsonObject.getDouble("countTransaction"));
                        visited = (float) (jsonObject.isNull("visitedPage") ? 0 : jsonObject.getDouble("visitedPage"));
                        conv = (float) (jsonObject.isNull("conversion") ? 0 : jsonObject.getDouble("conversion"));


                        sumTransaction.add(new BarEntry(j, sum));
                        avgTransaction.add(new Entry(j, avg));
                        countTransaction.add(new Entry(j, count));
                        visitedPage.add(new Entry(j, visited));
                        conversion.add(new Entry(j, conv));

                        sumTransactionArray.add(sum);
                        avgTransactionArray.add(avg);
                        countTransactionArray.add(count);
                        visitedPageArray.add(visited);
                        conversionArray.add(conv);


                        j++;
                        labels.add(jsonObject.getString("humanDate"));

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                BarDataSet sumTransactionSet = new BarDataSet(sumTransaction, "Transaction Sum");
                LineDataSet avgTransactionSet = new LineDataSet(avgTransaction, "Transaction Average");
                LineDataSet countTransactionSet = new LineDataSet(countTransaction, "Transaction Count");
                LineDataSet visitedPageSet = new LineDataSet(visitedPage, "Visited Pages");
                LineDataSet conversionSet = new LineDataSet(conversion, "Conversion");


                sumTransactionSet.setColor(getResources().getColor(R.color.fancyOrange));
                avgTransactionSet.setColor(getResources().getColor(R.color.fancyMaterialPink));
                countTransactionSet.setColor(getResources().getColor(R.color.fancyBlue));
                visitedPageSet.setColor(getResources().getColor(R.color.fancyGreen));
                conversionSet.setColor(getResources().getColor(R.color.fancyPurple));

                SyneComplexLineChart.initLineDataSet(avgTransactionSet);
                SyneComplexLineChart.initLineDataSet(countTransactionSet);
                SyneComplexLineChart.initLineDataSet(visitedPageSet);
                SyneComplexLineChart.initLineDataSet(conversionSet);


                final BarData sumTransactionData = new BarData(sumTransactionSet);
                final LineData avgTransactionData = new LineData(avgTransactionSet);
                final LineData countTransactionData = new LineData(countTransactionSet);
                final LineData visitedPageData = new LineData(visitedPageSet);
                final LineData conversionData = new LineData(conversionSet);

                sumTransactionData.setDrawValues(false);
                avgTransactionData.setDrawValues(false);
                countTransactionData.setDrawValues(false);
                visitedPageData.setDrawValues(false);
                conversionData.setDrawValues(false);


                final Activity activity = getActivity();
                if (activity != null) {

                    final float finalAllReturnProductsValue = allReturnProductsValue;
                    final float finalAllReturnProducts = allReturnProducts;
                    final boolean finalCacheUsed = cacheUsed;
                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            sumTransactionChart.update(sumTransactionData, 0d, labels, new String[]{"Currency"}, isConversionInitialized, finalCacheUsed);
                            avgTransactionChart.update(avgTransactionData, 0d, labels, new String[]{"Value"}, isConversionInitialized, finalCacheUsed);
                            countTransactionChart.update(countTransactionData, 0d, labels, new String[]{"Value"}, isConversionInitialized, finalCacheUsed);
                            visitedPageChart.update(visitedPageData, 0d, labels, new String[]{"Value"}, isConversionInitialized, finalCacheUsed);
                            conversionChart.update(conversionData, 0d, labels, new String[]{"Value"}, isConversionInitialized, finalCacheUsed);


                            isConversionInitialized = true;
                            View rootView = getView();
                            if (rootView == null) {
                                return;
                            }
                            TableLayout table = (TableLayout) rootView.findViewById(R.id.conversionTable);
                            table.removeAllViewsInLayout();


                            TableRow head = new TableRow(getContext());

                            SyneBoldTextView dateHead = new SyneBoldTextView(getContext());
                            dateHead.setText("DATE");
                            dateHead.setGravity(Gravity.CENTER);
                            dateHead.setPadding(5, 5, 5, 5);

                            SyneBoldTextView transactionSumHead = new SyneBoldTextView(getContext());
                            transactionSumHead.setText("TRANSACTION SUM");
                            transactionSumHead.setGravity(Gravity.CENTER);
                            transactionSumHead.setPadding(5, 5, 5, 5);

                            SyneBoldTextView averageTransactionSumHead = new SyneBoldTextView(getContext());
                            averageTransactionSumHead.setText("AVERAGE TRANSACTION SUM");
                            averageTransactionSumHead.setGravity(Gravity.CENTER);
                            averageTransactionSumHead.setPadding(5, 5, 5, 5);

                            SyneBoldTextView transactionCountsHead = new SyneBoldTextView(getContext());
                            transactionCountsHead.setText("TRANSACTION COUNTS");
                            transactionCountsHead.setGravity(Gravity.CENTER);
                            transactionCountsHead.setPadding(5, 5, 5, 5);

                            SyneBoldTextView visitedPagesHead = new SyneBoldTextView(getContext());
                            visitedPagesHead.setText("VISITED PAGES");
                            visitedPagesHead.setGravity(Gravity.CENTER);
                            visitedPagesHead.setPadding(5, 5, 5, 5);

                            SyneBoldTextView conversionHead = new SyneBoldTextView(getContext());
                            conversionHead.setText("CONVERSION(%)");
                            conversionHead.setGravity(Gravity.CENTER);
                            conversionHead.setPadding(5, 5, 5, 5);

                            head.addView(dateHead);
                            head.addView(transactionSumHead);
                            head.addView(averageTransactionSumHead);
                            head.addView(transactionCountsHead);
                            head.addView(visitedPagesHead);
                            head.addView(conversionHead);

                            table.addView(head);


                            for (int i = 0; i < labels.size(); i++) {
                                TableRow tableRow = new TableRow(getContext());
                                SyneTextView date = new SyneTextView(getContext());
                                date.setText(labels.get(i));
                                date.setGravity(Gravity.CENTER);

                                SyneTextView sumTransaction = new SyneTextView(getContext());
                                sumTransaction.setText(sumTransactionArray.get(i).toString());
                                sumTransaction.setGravity(Gravity.CENTER);


                                SyneTextView avgTransaction = new SyneTextView(getContext());
                                avgTransaction.setText(avgTransactionArray.get(i).toString());
                                avgTransaction.setGravity(Gravity.CENTER);

                                SyneTextView countTransaction = new SyneTextView(getContext());
                                countTransaction.setText(countTransactionArray.get(i).toString());
                                countTransaction.setGravity(Gravity.CENTER);

                                SyneTextView visitedPage = new SyneTextView(getContext());
                                visitedPage.setText(visitedPageArray.get(i).toString());
                                visitedPage.setGravity(Gravity.CENTER);

                                SyneTextView conversion = new SyneTextView(getContext());
                                conversion.setText(conversionArray.get(i).toString());
                                conversion.setGravity(Gravity.CENTER);

                                tableRow.addView(date);
                                tableRow.addView(sumTransaction);
                                tableRow.addView(avgTransaction);
                                tableRow.addView(countTransaction);
                                tableRow.addView(visitedPage);
                                tableRow.addView(conversion);

                                table.addView(tableRow);
                            }

                        }
                    });
                }
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (NullPointerException e) {

            } catch (IllegalStateException e) {

            }
            return "ok";
        }

    }


    private class GetClientTags extends AsyncTask<Void, Void, String> {
        protected String doInBackground(Void... params) {
            try {

                HttpGet httpget = new HttpGet("https://app.synerise.com/rest/clients/tags/show-all");

                HttpResponse response = MainActivity.httpClient.execute(httpget);
                if (response == null) {
                    return "not OK";
                }

                HttpEntity entity = response.getEntity();

                final String body = EntityUtils.toString(entity);

                JSONArray jsonArray = null;
                try {
                    jsonArray = new JSONArray(body);
                } catch (JSONException e) {
                    e.printStackTrace();
                }


                try {
                    int j = 0;
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        ClientTag clientTag = new ClientTag(jsonObject);
                        if (clientTag.getType().compareToIgnoreCase("SEGMENTATION") == 0) {
                            availableSegments.add(clientTag);
                        } else {
                            availableTags.add(clientTag);
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }


            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (NullPointerException e) {

            } catch (IllegalStateException e) {

            }
            return "ok";
        }

    }


    void releaseButtons() {
        hour.setPressed(false);
        day.setPressed(false);
        month.setPressed(false);
        year.setPressed(false);
    }


    private String getParams() {

        String params = "?dateFrom=" + dateFrom.getTimeInMillis() + "&dateTo=" + dateTo.getTimeInMillis() + "&group=" + group + "&minPeriod=" + minPeriod + "&client=all";

        for (Pair<String, String> p :
                categoryCampaigns) {
            params += "&" + p.first + "=" + p.second;

        }

        for (int i = 0; i < availableTags.size(); i++) {
            ClientTag tag = availableTags.get(i);
            if (tag.getChoosed()) {
                params += "$tagId=" + tag.getId();
            }
        }

        for (int i = 0; i < availableSegments.size(); i++) {
            ClientTag tag = availableSegments.get(i);
            if (tag.getChoosed()) {
                params += "$tagId=" + tag.getId();
            }
        }

        return params;
    }

    private String createCacheKey(String key) {

        String params = key + "&group=" + group + "&minPeriod=" + minPeriod + "&client=all";

        for (Pair<String, String> p :
                categoryCampaigns) {
            params += "&" + p.first + "=" + p.second;

        }

        for (int i = 0; i < availableTags.size(); i++) {
            ClientTag tag = availableTags.get(i);
            if (tag.getChoosed()) {
                params += "$tagId=" + tag.getId();
            }
        }

        for (int i = 0; i < availableSegments.size(); i++) {
            ClientTag tag = availableSegments.get(i);
            if (tag.getChoosed()) {
                params += "$tagId=" + tag.getId();
            }
        }

        return params;

    }

    private void setChoosed(String type) {
        int gray = getResources().getColor(R.color.fancyGray);
        int blue = getResources().getColor(R.color.fancyBlue);
        hourLine.setBackgroundColor(gray);
        dayLine.setBackgroundColor(gray);
        weekLine.setBackgroundColor(gray);
        monthLine.setBackgroundColor(gray);
        yearLine.setBackgroundColor(gray);

        hour.setTextColor(gray);
        day.setTextColor(gray);
        week.setTextColor(gray);
        month.setTextColor(gray);
        year.setTextColor(gray);

        switch (type) {
            case "hour":
                hour.setTextColor(blue);
                hourLine.setBackgroundColor(blue);
                break;
            case "day":
                day.setTextColor(blue);
                dayLine.setBackgroundColor(blue);
                break;
            case "week":
                week.setTextColor(blue);
                weekLine.setBackgroundColor(blue);
                break;
            case "month":
                month.setTextColor(blue);
                monthLine.setBackgroundColor(blue);
                break;
            case "year":
                year.setTextColor(blue);
                yearLine.setBackgroundColor(blue);
                break;
        }
    }
}

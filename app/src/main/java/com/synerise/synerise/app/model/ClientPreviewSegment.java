package com.synerise.synerise.app.model;

import java.util.ArrayList;

public class ClientPreviewSegment {

    public static final ArrayList<Segment> ITEMS = new ArrayList<Segment>();

    public static void add(String id, String name) {
        ITEMS.add(new Segment(id, name));
    }

    public static void clear() {
        ITEMS.clear();

    }

    public static class Segment {
        public String id;
        public String name;

        public Segment(String id, String name) {
            this.id = id;
            this.name = name;

        }

        @Override
        public String toString() {
            return "#" + name;
        }
    }

}

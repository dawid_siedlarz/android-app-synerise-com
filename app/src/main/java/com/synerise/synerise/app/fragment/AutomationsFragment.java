package com.synerise.synerise.app.fragment;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;
import com.synerise.sdk.tracking.Tracker;
import com.synerise.sdk.tracking.TrackerParams;
import com.synerise.synerise.MainActivity;
import com.synerise.synerise.R;
import com.synerise.synerise.SyneriseOnFragmentInteractionListener;
import com.synerise.synerise.app.syneview.SyneBoldTextView;
import com.synerise.synerise.app.syneview.SyneTextView;
import com.synerise.synerise.app.syneview.SyneVerticalLine;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;


public class AutomationsFragment extends Fragment {
    private SyneriseOnFragmentInteractionListener mListener;

    public AutomationsFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment AutomationsFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static AutomationsFragment newInstance(String param1, String param2) {
        AutomationsFragment fragment = new AutomationsFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_automations, container, false);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof SyneriseOnFragmentInteractionListener) {
            mListener = (SyneriseOnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Tracker.trackScreen("Dashboard automations", new TrackerParams[]{});

        new GetAutomationPerformance().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        new GetActionsMadePerJourney().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        new GetClientsInStep().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        ActionBar actionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        actionBar.show();
        actionBar.setTitle(MainActivity.businessProfileName != null? MainActivity.businessProfileName : "Synerise");
        actionBar.setSubtitle("Automation");
        actionBar.setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.colorPrimary)));

    }

    private class GetAutomationPerformance extends AsyncTask<Void, Void, String> {
        protected String doInBackground(Void... params) {
            try {

                HttpGet httpget = new HttpGet("https://app.synerise.com/dashboard/stats-automations/");

                HttpResponse listGrowthResponse = MainActivity.httpClient.execute(httpget);
                if (listGrowthResponse == null) {
                    return "not OK";
                }

                HttpEntity listGrowthResponseEntity = listGrowthResponse.getEntity();

                final String body4 = EntityUtils.toString(listGrowthResponseEntity);

                JSONArray jsonArray = null;

                try {
                    jsonArray = new JSONArray(body4);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                final JSONArray finalJSONArray = jsonArray;


                final ArrayList<String> labels = new ArrayList<String>();
                final ArrayList<BarEntry> countOnTriggerValue = new ArrayList<BarEntry>();
                final ArrayList<BarEntry> countOnFirstStepValue = new ArrayList<BarEntry>();

                float countOnTriggerAll = 0.0f;
                float countOnFirstStepAll = 0.0f;

                float countOnTrigger = 0.0f;
                float countOnFirstStep = 0.0f;

                final ArrayList<String> started = new ArrayList<>();
                final ArrayList<String> finished = new ArrayList<>();

                try {
                    int j = 0;
                    for (int i = 0; i < finalJSONArray.length(); i++) {
                        JSONObject jsonObject = finalJSONArray.getJSONObject(i);

                        countOnTrigger = (float) jsonObject.getDouble("countOnTrigger");
                        countOnFirstStep = (float) jsonObject.getDouble("countOnFirstStep");

                        countOnTriggerAll += countOnTrigger;
                        countOnFirstStepAll += countOnFirstStep;

                        started.add((int) countOnTrigger + "");
                        finished.add((int) countOnFirstStep + "");

                        countOnTriggerValue.add(new BarEntry(j, countOnTrigger));
                        countOnFirstStepValue.add(new BarEntry(j, countOnFirstStep));

                        j++;
                        labels.add(jsonObject.getString("automationName"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                BarDataSet countOnTriggerSet = new BarDataSet(countOnTriggerValue, "countOnTriggerValue");
                BarDataSet countOnFirstStepSet = new BarDataSet(countOnFirstStepValue, "countOnFirstStepValue");


                Fragment fragment = getActivity().getSupportFragmentManager().findFragmentById(R.id.mainContainer);

                if (!(fragment instanceof AutomationsFragment)) {
                    return "not OK";
                }
                countOnTriggerSet.setColor(getResources().getColor(R.color.fancyBlue));
                countOnFirstStepSet.setColor(getResources().getColor(R.color.fancyGreen));

                ArrayList<IBarDataSet> setList = new ArrayList<>();
                setList.add(countOnTriggerSet);
                setList.add(countOnFirstStepSet);

                final BarData AutomationPerformanceData = new BarData(setList);

                double performance = 0;

                int i = 0;
                for (i = 0; i < started.size(); i++) {
                    performance += Double.valueOf(started.get(i)) / Double.valueOf(finished.get(i));
                }

                performance = performance / (i + 1);

                final Activity activity = getActivity();
                if (activity != null) {

                    final double finalPerformance = performance;
                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            View rootView = getView();
                            if (rootView == null) {
                                return;
                            }
                            ((TextView) rootView.findViewById(R.id.AutomationPerformanceValue)).setText(String.format("%.2f", finalPerformance));

                            BarChart AutomationPerformanceChart = ((BarChart) rootView.findViewById(R.id.AutomationPerformanceChart));

                            AutomationPerformanceChart.setData(AutomationPerformanceData);


                            AutomationPerformanceChart.setDrawGridBackground(false);
                            AutomationPerformanceChart.getAxisLeft().setDrawGridLines(true);
                            AutomationPerformanceChart.getAxisLeft().setGridColor(getResources().getColor(R.color.fancyGray));
                            AutomationPerformanceChart.getAxisLeft().setDrawAxisLine(false);
                            AutomationPerformanceChart.getXAxis().setDrawGridLines(false);
                            AutomationPerformanceChart.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM);
                            AutomationPerformanceChart.getAxisRight().setDrawAxisLine(false);
                            AutomationPerformanceChart.getAxisRight().setEnabled(false);
                            AutomationPerformanceChart.getAxisRight().setDrawGridLines(false);
                            AutomationPerformanceChart.setDrawBorders(false);
                            AutomationPerformanceChart.getLegend().setEnabled(false);



                            AutomationPerformanceChart.invalidate();

                            TableLayout table = (TableLayout) rootView.findViewById(R.id.customerJourneyTable);
                            table.removeAllViewsInLayout();


                            TableRow head = new TableRow(getContext());

                            SyneBoldTextView customerJourneyHead = new SyneBoldTextView(getContext());
                            customerJourneyHead.setText("CUSTOMER JOURNEY");
                            customerJourneyHead.setGravity(Gravity.CENTER);
                            customerJourneyHead.setPadding(5, 5, 5, 5);

                            SyneBoldTextView startedHead = new SyneBoldTextView(getContext());
                            startedHead.setText("STARTED");
                            startedHead.setGravity(Gravity.CENTER);
                            startedHead.setPadding(5, 5, 5, 5);

                            SyneBoldTextView finishedHead = new SyneBoldTextView(getContext());
                            finishedHead.setText("FINISHED");
                            finishedHead.setGravity(Gravity.CENTER);
                            finishedHead.setPadding(5, 5, 5, 5);

                            SyneBoldTextView performanceHead = new SyneBoldTextView(getContext());
                            performanceHead.setText("PERFORMANCE");
                            performanceHead.setGravity(Gravity.CENTER);
                            performanceHead.setPadding(5, 5, 5, 5);

                            SyneVerticalLine line = new SyneVerticalLine(getContext());
                            SyneVerticalLine line1 = new SyneVerticalLine(getContext());
                            SyneVerticalLine line2 = new SyneVerticalLine(getContext());


                            head.addView(customerJourneyHead);
                            head.addView(line);
                            head.addView(startedHead);
                            head.addView(line1);
                            head.addView(finishedHead);
                            head.addView(line2);
                            head.addView(performanceHead);

                            table.addView(head);


                            for (int i = 0; i < started.size(); i++) {
                                TableRow tableRow = new TableRow(getContext());
                                SyneTextView name = new SyneTextView(getContext());
                                name.setText(labels.get(i));

                                name.setGravity(Gravity.CENTER);
                                SyneTextView start = new SyneTextView(getContext());
                                start.setText(started.get(i));
                                start.setGravity(Gravity.CENTER);
                                SyneTextView finish = new SyneTextView(getContext());
                                finish.setText(finished.get(i));
                                finish.setGravity(Gravity.CENTER);
                                SyneTextView perform = new SyneTextView(getContext());
                                perform.setText(Double.valueOf(finished.get(i)) / Double.valueOf(started.get(i)) + "");
                                perform.setGravity(Gravity.CENTER);

                                SyneVerticalLine line3 = new SyneVerticalLine(getContext());
                                SyneVerticalLine line4 = new SyneVerticalLine(getContext());
                                SyneVerticalLine line5 = new SyneVerticalLine(getContext());

                                tableRow.addView(name);
                                tableRow.addView(line3);
                                tableRow.addView(start);
                                tableRow.addView(line4);
                                tableRow.addView(finish);
                                tableRow.addView(line5);
                                tableRow.addView(perform);

                                table.addView(tableRow);
                            }


                        }
                    });
                }
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (NullPointerException e) {

            } catch (IllegalStateException e) {

            }

            return "ok";
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            Handler myHandler = new Handler();
            myHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    Fragment fragment = getActivity().getSupportFragmentManager().findFragmentById(R.id.mainContainer);
                    if (getActivity() != null && fragment instanceof AutomationsFragment) {
                        new GetAutomationPerformance().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                    }
                }
            }, 3000);
        }
    }

    private class GetActionsMadePerJourney extends AsyncTask<Void, Void, String> {
        protected String doInBackground(Void... params) {
            try {

                HttpGet httpget = new HttpGet("https://app.synerise.com/dashboard/automations/sent-messages");

                HttpResponse listGrowthResponse = MainActivity.httpClient.execute(httpget);
                if (listGrowthResponse == null) {
                    return "not OK";
                }

                HttpEntity listGrowthResponseEntity = listGrowthResponse.getEntity();

                final String body4 = EntityUtils.toString(listGrowthResponseEntity);

                JSONArray jsonArray = null;

                try {
                    jsonArray = new JSONArray(body4);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                final JSONArray finalJSONArray = jsonArray;


                final ArrayList<String> labels = new ArrayList<String>();
                final ArrayList<Entry> aggregationValueValue = new ArrayList<Entry>();

                float aggregationValueAll = 0.0f;

                float aggregationValue = 0.0f;

                try {
                    int j = 0;
                    for (int i = 0; i < finalJSONArray.length(); i++) {
                        JSONObject jsonObject = finalJSONArray.getJSONObject(i);

                        aggregationValue = (float) jsonObject.getDouble("aggregationValue");

                        aggregationValueAll += aggregationValue;

                        aggregationValueValue.add(new Entry(j, aggregationValue));

                        j++;
                        labels.add(jsonObject.getString("humanDate"));

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                LineDataSet aggregationValueSet = new LineDataSet(aggregationValueValue, "aggregationValue");

                aggregationValueSet.setColor(getResources().getColor(R.color.fancyBlue));

                aggregationValueSet.setDrawCircles(false);

                final LineData aggregationValueData = new LineData(aggregationValueSet);


                aggregationValueData.setDrawValues(false);

                Activity activity = getActivity();
                if (activity != null) {

                    final float finalAggregationValueAll = aggregationValueAll;
                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            View rootView = getView();
                            if (rootView == null) {
                                return;
                            }
                            ((TextView) rootView.findViewById(R.id.ActionsMadePerJourneyValue)).setText(finalAggregationValueAll + "");

                            LineChart aggregationValueChart = ((LineChart) rootView.findViewById(R.id.ActionsMadePerJourneyChart));

                            aggregationValueChart.getXAxis().setValueFormatter(new IndexAxisValueFormatter(labels));

                            aggregationValueChart.invalidate();

                        }
                    });
                }


            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (NullPointerException e) {

            } catch (IllegalStateException e) {

            }
            return "ok";
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            Handler myHandler = new Handler();
            myHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    Fragment fragment = getActivity().getSupportFragmentManager().findFragmentById(R.id.mainContainer);
                    if (getActivity() != null && fragment instanceof AutomationsFragment) {
                        new GetActionsMadePerJourney().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                    }
                }
            }, 3000);
        }
    }


    private class GetClientsInStep extends AsyncTask<Void, Void, String> {
        protected String doInBackground(Void... params) {
            try {

                HttpGet httpget = new HttpGet("https://app.synerise.com/rest/automation/clients-on-step-global");

                HttpResponse listGrowthResponse = MainActivity.httpClient.execute(httpget);
                if (listGrowthResponse == null) {
                    return "not OK";
                }

                HttpEntity listGrowthResponseEntity = listGrowthResponse.getEntity();

                final String body4 = EntityUtils.toString(listGrowthResponseEntity);

                JSONArray jsonArray = null;

                try {
                    jsonArray = new JSONArray(body4);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                final JSONArray finalJSONArray = jsonArray;


                final ArrayList<String> labels = new ArrayList<String>();
                final ArrayList<ArrayList<String>> onStepNames = new ArrayList<>();
                final ArrayList<ArrayList<String>> onStepValues = new ArrayList<>();


                try {
                    for (int i = 0; i < finalJSONArray.length(); i++) {
                        JSONObject jsonObject = finalJSONArray.getJSONObject(i);

                        onStepNames.add(new ArrayList<String>());
                        onStepValues.add(new ArrayList<String>());
                        JSONArray steps = jsonObject.getJSONArray("steps");
                        JSONObject step;
                        for (int j = 0; j < steps.length(); j++) {
                            step = steps.getJSONObject(j);
                            onStepNames.get(i).add(step.getString("name"));
                            onStepValues.get(i).add(step.getString("count"));
                        }
                        labels.add(jsonObject.getString("name"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }


                final Activity activity = getActivity();
                if (activity != null) {

                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            View rootView = getView();
                            if (rootView == null) {
                                return;
                            }
                            LinearLayout container = rootView.findViewById(R.id.clientsInStep);
                            container.removeAllViewsInLayout();

                            LinearLayout diagram = new LinearLayout(getContext());


                            for (int i = 0; i < labels.size(); i++) {


                                TableLayout tableLayout = new TableLayout(getContext());

                                SyneBoldTextView name = new SyneBoldTextView(getContext());
                                name.setText(labels.get(i));
                                name.setGravity(Gravity.CENTER);
                                name.setTextSize(25);

                                TableRow headerRow = new TableRow(getContext());
                                SyneBoldTextView step = new SyneBoldTextView(getContext());
                                step.setText("STEP");
                                step.setPadding(5, 5, 5, 5);
                                step.setGravity(Gravity.CENTER);


                                SyneBoldTextView clientsInStep = new SyneBoldTextView(getContext());
                                clientsInStep.setText("CLIENTS IN STEP");
                                clientsInStep.setGravity(Gravity.CENTER);
                                clientsInStep.setPadding(5, 5, 5, 5);

                                headerRow.addView(step);
                                headerRow.addView(clientsInStep);

                                tableLayout.addView(headerRow);

                                ArrayList<String> names = onStepNames.get(i);
                                ArrayList<String> values = onStepValues.get(i);
                                for (int j = 0; j < names.size(); j++) {
                                    TableRow row = new TableRow(getContext());
                                    SyneTextView stepName = new SyneTextView(getContext());
                                    stepName.setText(names.get(j));
                                    stepName.setGravity(Gravity.CENTER);
                                    stepName.setPadding(5, 5, 5, 5);

                                    SyneTextView stepValue = new SyneTextView(getContext());
                                    stepValue.setText(values.get(j));
                                    stepValue.setGravity(Gravity.CENTER);
                                    stepValue.setPadding(5, 5, 5, 5);

                                    row.addView(stepName);
                                    row.addView(stepValue);
                                    tableLayout.addView(row);
                                }

                                container.addView(name);
                                container.addView(tableLayout);
                            }


                        }
                    });
                }
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (NullPointerException e) {

            } catch (IllegalStateException e) {

            }
            return "ok";
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            Handler myHandler = new Handler();
            myHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    Fragment fragment = getActivity().getSupportFragmentManager().findFragmentById(R.id.mainContainer);
                    if (getActivity() != null && fragment instanceof AutomationsFragment) {
                        new GetClientsInStep().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                    }
                }
            }, 3000);
        }
    }

}


package com.synerise.synerise.app.model;

import java.util.ArrayList;

public class FilterAttributes {

    public static final ArrayList<FilterAttribute> ITEMS = new ArrayList<FilterAttribute>();

    public static void add(String id, String name, String type) {
        ITEMS.add(new FilterAttribute(id, name, type));
    }

    public static void clear() {
        ITEMS.clear();

    }

    public static class FilterAttribute {
        public String id;
        public String name;
        public String type;
        public String option = null;
        public String value = null;

        public FilterAttribute(String id, String name, String type) {
            this.id = id;
            this.name = name;
            this.type = type;
        }

        @Override
        public String toString() {
            return name ;
        }
    }

}

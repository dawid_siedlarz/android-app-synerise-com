package com.synerise.synerise.messenger.factory.messageBody;

import android.content.Context;
import android.util.Log;

import com.synerise.synerise.messenger.model.MessengerMessage;
import com.synerise.synerise.messenger.model.messageBody.ImageBody;
import com.synerise.synerise.messenger.model.messageBody.MessageBody;
import com.synerise.synerise.messenger.model.messageBody.TextBody;
import com.synerise.synerise.messenger.model.messageBody.TextTemplateBody;

import org.json.JSONException;
import org.json.JSONObject;

public class AllInOneMessageBodyFactory implements MessageBodyFactory {

    Context context;

    public AllInOneMessageBodyFactory(Context context) {
        this.context = context;
    }

    @Override
    public MessageBody getMessageBody(MessengerMessage messengerMessage) {
        try {
            JSONObject jsonObject = new JSONObject(messengerMessage.getMessageText());
            return buildFromTemplate(messengerMessage, jsonObject);

        } catch (Exception e) {}

        return createTextMessageBody(messengerMessage);
    }

    private MessageBody createTextMessageBody(MessengerMessage messengerMessage) {
        return new TextBody(context, messengerMessage.getMessageText(), messengerMessage.getAuthor().getType());
    }

    private MessageBody buildFromTemplate(MessengerMessage message, JSONObject jsonObject) throws JSONException {
        JSONObject params = jsonObject.getJSONObject("params");

        switch (jsonObject.getString("template")) {
            case "templateDropEmailForm":
                if (params.keys().hasNext()) {
                    return new TextTemplateBody(context, "Bot request for drop email.", message.getAuthor().getType());
                } else {
                    return new TextTemplateBody(context, "Bot request for drop email.", message.getAuthor().getType());
                }
            case "templateUploadFile":
                Log.v("params", params.toString());
                Log.v("type", params.getString("type"));

                if (params.getString("type").contains("image")) {
                    return new ImageBody(context, params.getString("fileSrc"));
                }
        }
        throw new RuntimeException("unsupported template");
    }
}

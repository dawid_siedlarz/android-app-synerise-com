package com.synerise.synerise.app.model.campaign;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONException;
import org.json.JSONObject;

public class CampaignRecipients implements Parcelable {
    private int count = 0;
    private String type = "";
    private RecipientsValue recipientsValue;

    public CampaignRecipients(JSONObject jsonObject) {
        try {

            if (jsonObject.has("count")) {
                count = jsonObject.getInt("count");
            }

            if (jsonObject.has("type")) {
                type = jsonObject.getString("type");
            }

            if (jsonObject.has("value") && !jsonObject.isNull("value")) {
                recipientsValue = new RecipientsValue(jsonObject.getJSONObject("value"));
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public String toReadableString() {
        return type + " " +  (recipientsValue != null ? recipientsValue.getName() : "") + "(" + count + ")";
    }

    protected CampaignRecipients(Parcel in) {
        count = in.readInt();
        type = in.readString();
        recipientsValue = in.readParcelable(RecipientsValue.class.getClassLoader());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(count);
        dest.writeString(type);
        dest.writeParcelable(recipientsValue, flags);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<CampaignRecipients> CREATOR = new Creator<CampaignRecipients>() {
        @Override
        public CampaignRecipients createFromParcel(Parcel in) {
            return new CampaignRecipients(in);
        }

        @Override
        public CampaignRecipients[] newArray(int size) {
            return new CampaignRecipients[size];
        }
    };

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public RecipientsValue getRecipientsValue() {
        return recipientsValue;
    }

    public void setRecipientsValue(RecipientsValue recipientsValue) {
        this.recipientsValue = recipientsValue;
    }
}

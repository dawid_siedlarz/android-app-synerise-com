package com.synerise.synerise;

import android.app.Activity;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;

import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.synerise.synerise.app.ActionBarConfigurableFragment;
import com.synerise.synerise.app.model.Insight;
import com.synerise.synerise.app.syneview.SyneComplexBarChart;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class InsightPreview extends ActionBarConfigurableFragment {

    public static String ARG_INSIGHT = "insight";

    private Insight insight;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.activity_insight_preview, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (getArguments() != null) {
            insight = (Insight) getArguments().getParcelable(ARG_INSIGHT);
        }

        String sql = insight.sql_scheme;

        insightChart = (SyneComplexBarChart) view.findViewById(R.id.insightChart);
        insightChart.getTitle().setTitle(insight.title.toString());

        insightChart.getTitle().clearCountAndUnit();

        new GetInsight().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, sql);

    }

    @Override
    public void setUpActionBar() {
        ActionBar actionBar = (ActionBar) ((AppCompatActivity) getActivity()).getSupportActionBar();
        if (actionBar != null) {
            actionBar.show();
            actionBar.setTitle(MainActivity.businessProfileName != null ? MainActivity.businessProfileName : "Synerise");
            actionBar.setSubtitle("Insights");
            actionBar.setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.colorPrimaryInsights)));
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                Window window = getActivity().getWindow();
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                window.setStatusBarColor(getResources().getColor(R.color.colorPrimaryInsights));
            }
            actionBar.setElevation(0);
        }
    }

    SyneComplexBarChart insightChart;

    private class GetInsight extends AsyncTask<String, Void, String> {
        protected String doInBackground(String... params) {
            try {

                String sql = params[0];

                String sqlDecoded = new String(Base64.decode(sql, Base64.DEFAULT), "UTF-8");

                Calendar dateTo = Calendar.getInstance();
                Calendar dateFrom = Calendar.getInstance();
                dateFrom.add(Calendar.MONTH, -1);

                final SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");

                sqlDecoded = sqlDecoded.replace("##dateFrom##", format.format(dateFrom.getTime())).replace("##dateTo##", format.format(dateTo.getTime()));


                HttpPost httpost = new HttpPost("https://api.synerise.com/rtom/synerise-sqls/query");

                List<NameValuePair> nvps = new ArrayList<NameValuePair>();
                nvps.add(new BasicNameValuePair("sql", sqlDecoded));
                httpost.setEntity(new UrlEncodedFormEntity(nvps, HTTP.UTF_8));
                HttpResponse response2 = MainActivity.httpClient.execute(httpost);
                final String body = EntityUtils.toString(response2.getEntity());

                if (body == null) {
                    return "not ok";
                }


                JSONObject jsonObject = null;

                try {
                    jsonObject = new JSONObject(body);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                JSONArray jsonArray = jsonObject.getJSONArray("items");

                final List<String> labels = new ArrayList<String>();
                ArrayList<BarEntry> values = new ArrayList<BarEntry>();

                int minY = Integer.MAX_VALUE;
                int size = 0;

                try {

                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject object = jsonArray.getJSONObject(i);
                        values.add(new BarEntry(i, (float) object.getDouble("value")));
                        labels.add(object.getString("category"));
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                final String[] labelsa = new String[]{"model.title.toString()"};

                BarDataSet set = new BarDataSet(values, insight.title.toString());
                set.setStackLabels(labelsa);
                int[] colors = {
                        getResources().getColor(R.color.fancyBlueAlpha),
                        getResources().getColor(R.color.fancyPinkAlpha),
                        getResources().getColor(R.color.fancyDarkBlueAlpha),
                        getResources().getColor(R.color.fancyGreenAlpha),
                };
                set.setColors(colors);
                final BarData data = new BarData(set);

                data.setDrawValues(false);
                data.setBarWidth(0.6f);

                Activity activity = getActivity();
                if (activity == null) {
                    return "not ok";

                }
                final Typeface tf = Typeface.createFromAsset(activity.getAssets(),
                        "fonts/Lato-Thin.ttf");
                final Typeface btf = Typeface.createFromAsset(activity.getAssets(),
                        "fonts/Lato-Bold.ttf");


                final int finalMinY = minY;
                final int finalSize = size;
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        insightChart.update(data, 0d, labels, labelsa, false, false);
                        insightChart.getChart().getXAxis().setAxisMinimum(-3f);
                        insightChart.getChart().getXAxis().setAxisMaximum(labels.size() + 1);
                        insightChart.getTitle().clearCountAndUnit();
                    }
                });

            } catch (NullPointerException e) {

            } catch (IllegalStateException e) {

            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return "ok";
        }
    }
}

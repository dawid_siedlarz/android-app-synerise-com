package com.synerise.synerise.app.syneview;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.View;
import android.widget.RelativeLayout;

import com.synerise.synerise.R;

/**
 * Created by dsiedlarz on 27.02.2017.
 */

public class SyneVerticalLine extends View {
    public SyneVerticalLine(Context context) {
        super(context);
        setBackgroundColor(context.getResources().getColor(R.color.separatorGray));
        setLayoutParams(new RelativeLayout.LayoutParams(1, RelativeLayout.LayoutParams.MATCH_PARENT));
        setBackground(ContextCompat.getDrawable(context, R.drawable.line_divider));

    }

    public SyneVerticalLine(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

}

package com.synerise.synerise.app.controller;

import android.app.Activity;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;

import com.synerise.synerise.app.adapter.MessengerConversationRecyclerViewAdapter;
import com.synerise.synerise.app.adapter.MessengerRoomsRecyclerViewAdapter;
import com.synerise.synerise.messenger.fragment.MessengerFragment;
import com.synerise.synerise.messenger.model.MessengerClient;
import com.synerise.synerise.messenger.model.MessengerMessage;
import com.synerise.synerise.messenger.model.MessengerUser;
import com.synerise.synerise.messenger.model.MessengerVendor;
import com.synerise.synerise.app.syneutil.SyneHttpCacheHandler;

import org.apache.http.client.methods.HttpGet;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;

/**
 * Created by dsiedlarz on 22.04.2017.
 */

public class MessengerUserController {
    Activity activity;

    public MessengerUserController(Activity activity) {
        this.activity = activity;
    }

    public void setMessengerRoomHolder(MessengerRoomsRecyclerViewAdapter.RoomViewHolder holder) {
        MessengerUser owner = holder.getmRoom().getOwner();
        String initials = owner.getInitials();
        Bitmap avaratBitmap = owner.getAvatarBitmap();
        String name = owner.getName();


        if (initials != null) {
            holder.getmOwnerAvatarInitials().setText(owner.getInitials());
        }
        if (avaratBitmap != null) {
            holder.getmOwnerAvatar().setImageBitmap(avaratBitmap);
        }
        if (name != null) {
            holder.getmName().setText(name);
        }
        if (avaratBitmap == null || initials == null || name == null) {
            new GetUserData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, holder);
        }
    }

    public void setClientMessageViewHolder(MessengerConversationRecyclerViewAdapter.ClientMessageViewHolder holder, ArrayList<MessengerMessage> messages, MessengerClient client) {
        int position = holder.getAdapterPosition();
        if (isClientAvatarPrinted(position, messages)) {
            holder.getmOwnerAvatarBox().setVisibility(View.VISIBLE);
            MessengerClient owner = client;
            String initials = owner.getInitials();


//            Log.v("owner.getInitials()", initials);

            if (initials != null) {
                holder.getmOwnerAvatarInitials().setText(initials);
            }
            Bitmap avaratBitmap = owner.getAvatarBitmap();
            if (avaratBitmap != null) {
                holder.getmOwnerAvatar().setImageBitmap(avaratBitmap);
            }

            if (avaratBitmap == null || initials == null) {
                new GetUserDataInMessage().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, holder);
            }
        } else {
            holder.getmOwnerAvatarBox().setVisibility(View.INVISIBLE);
        }
    }

    private boolean isClientAvatarPrinted(int position, ArrayList<MessengerMessage> messages) {

        MessengerMessage message = messages.get(position);
//        Log.v("Message", message.getMessageBody());
        if(message.getAvatarPrintable() != null) {
            Log.v("Message return ", " " + message.getAvatarPrintable());
            return message.getAvatarPrintable();
        }

        if (position == 0) {
            message.setAvatarPrintable(true);
            Log.v("Message return ", "true" );
            return true;
        }

        Log.v("isClientAvatarPrinted", "Position: " + position +  "; size: " + messages.size()+ "; typeAbove: " + messages.get(position - 1).getAuthor().getType() );

        if (position > 0 && position <= messages.size()
                && messages.get(position - 1).getAuthor().getType().compareToIgnoreCase("CLIENT") != 0) {
            message.setAvatarPrintable(true);
            Log.v("Message return ", "true" );
            return  true;
        }

        message.setAvatarPrintable(false);
        Log.v("Message return ", "false" );

        return false;
    }

    private class GetUserData extends AsyncTask<MessengerRoomsRecyclerViewAdapter.RoomViewHolder, Void, Void> {
        @Override
        protected Void doInBackground(MessengerRoomsRecyclerViewAdapter.RoomViewHolder... params) {
            final MessengerRoomsRecyclerViewAdapter.RoomViewHolder holder = params[0];
            final MessengerUser owner = holder.getmRoom().getOwner();
            getDataFromApi(owner);

            if (owner.getAvatar() != null && owner.getAvatarBitmap() == null) {
                downloadAvatar(owner);
            }

            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Log.v("GetUserData initials", "initials: " + owner.getInitials());
                    holder.getmOwnerAvatarInitials().setText(owner.getInitials());
                    Log.v("GetUserData name", "name: " + owner.getName());
                    holder.getmName().setText(owner.getName());
                    holder.getmOwnerAvatar().setImageBitmap(owner.getAvatarBitmap());
                }
            });

            return null;
        }
    }


    private class GetUserDataInMessage extends AsyncTask<MessengerConversationRecyclerViewAdapter.ClientMessageViewHolder, Void, Void> {
        @Override
        protected Void doInBackground(MessengerConversationRecyclerViewAdapter.ClientMessageViewHolder... params) {
            final MessengerConversationRecyclerViewAdapter.ClientMessageViewHolder holder = params[0];
            final MessengerClient owner = (MessengerClient) holder.getmMessage().getAuthor();
            getDataFromApi(owner);

            if (owner.getAvatar() != null && owner.getAvatarBitmap() == null) {
                downloadAvatar(owner);
            }

            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Log.v("GetUserData initials", "initials: " + owner.getInitials());
                    holder.getmOwnerAvatarInitials().setText(owner.getInitials());
                    Log.v("GetUserData name", "name: " + owner.getName());
                    holder.getmOwnerAvatar().setImageBitmap(owner.getAvatarBitmap());
                }
            });

            return null;
        }
    }

    public MessengerUser getDataFromApi(MessengerUser owner) {
        String urlBase = "";
        switch (owner.getType()) {
            case "client":
                urlBase = MessengerFragment.API_ENDPOINT + "/clients/";
                break;
            case "vendor":
                urlBase = MessengerFragment.API_ENDPOINT + "/vendors/";
                break;
            default:
                return null;
        }


        HttpGet httpget = new HttpGet(urlBase + owner.getId());

        SyneHttpCacheHandler handler = new SyneHttpCacheHandler();
        final String body = handler.executeWithCache(httpget, true, true);
        if (body == null) {
            return null;
        }

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject = new JSONObject(body).getJSONObject("data");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        switch (owner.getType()) {
            case "client":
                ((MessengerClient) owner).parseJsonObject(jsonObject);
                break;
            case "vendor":
                ((MessengerVendor) owner).parseJsonObject(jsonObject);
                break;
            default:
                return null;
        }

        return owner;
    }

    private void downloadAvatar(MessengerUser user) {

        Bitmap bmp = null;

        try {
            bmp = SyneHttpCacheHandler.getBmp(user.getAvatar(), true);
            user.setAvatarBitmap(bmp);
        } catch (MalformedURLException e) {
            Log.v("SYNERISE", "Malformed URL " + user.getAvatar());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}

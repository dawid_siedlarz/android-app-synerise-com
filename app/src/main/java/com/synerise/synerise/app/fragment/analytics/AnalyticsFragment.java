package com.synerise.synerise.app.fragment.analytics;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;

import com.synerise.synerise.MainActivity;
import com.synerise.synerise.R;
import com.synerise.synerise.app.ActionBarConfigurableFragment;
import com.synerise.synerise.app.adapter.AnalyticsRecyclerViewAdapter;
import com.synerise.synerise.app.model.AnalyticsQuery;
import com.synerise.synerise.app.syneutil.EndlessRecyclerViewScrollListener;
import com.synerise.synerise.app.syneutil.SyneHttpCacheHandler;

import org.apache.http.client.methods.HttpGet;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


public class AnalyticsFragment extends ActionBarConfigurableFragment {

    private int listing_Paginator_offset = 0;
    private int listing_Paginator_limit = 150;
    private String listing_type_enumValue = "all";
    private String listing_search_value = "";

    private int counter = 0;

    private AnalyticsRecyclerViewAdapter adapter;
    private RecyclerView listView;
    private LinearLayoutManager linearLayoutManager;
    private RecyclerView.OnScrollListener scrollListener;
    private ArrayList<AnalyticsQuery> analytics;
    private boolean getFromCache = true;

    @Override
    public void setUpActionBar() {
        ActionBar actionBar = (ActionBar) ((AppCompatActivity) getActivity()).getSupportActionBar();
        if (actionBar != null) {
            actionBar.show();
            actionBar.setTitle(MainActivity.businessProfileName != null ? MainActivity.businessProfileName : "Synerise");
            actionBar.setSubtitle("Analytics");
            actionBar.setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.colorPrimaryAnalytics)));
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                Window window = getActivity().getWindow();
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                window.setStatusBarColor(getResources().getColor(R.color.colorPrimaryDarkAnalytics));
            }
            actionBar.setElevation(0);
        }
    }

    public AnalyticsFragment() {
        // Required empty public constructor
    }

    public static AnalyticsFragment newInstance() {
        AnalyticsFragment fragment = new AnalyticsFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_analytics, container, false);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }


    private void initList() {
        View rootView = getView();
        if (rootView == null) {
            return;
        }
        analytics = new ArrayList<>();
        adapter = new AnalyticsRecyclerViewAdapter(analytics, this);
        listView = rootView.findViewById(R.id.analyticsRecyclerView);
        linearLayoutManager = new LinearLayoutManager(getActivity());
        listView.setLayoutManager(linearLayoutManager);
        listView.setAdapter(adapter);
        scrollListener = new EndlessRecyclerViewScrollListener(linearLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                new GetAnalytics().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, listing_Paginator_limit);
            }
        };
        listView.addOnScrollListener(scrollListener);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initList();
        new GetAnalytics().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, 0);
    }

    private class GetAnalytics extends AsyncTask<Integer, Void, String> {
        protected String doInBackground(Integer... params) {
            try {

                listing_Paginator_offset = params[0] == 0 ? 0 : listing_Paginator_offset + params[0];
                counter = 0;

                String url = "https://app.synerise.com/rest/analytics/list" + getListingParams();

                SyneHttpCacheHandler handler = new SyneHttpCacheHandler();
                HttpGet httpget = new HttpGet(url);

                final String body = handler.executeWithCache(httpget, getFromCache, true);

                if (body == null) {
                    return "not OK";
                }

                Log.v("body", body);

                JSONArray jsonArray = null;

                try {
                    jsonArray = new JSONArray(body);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if (jsonArray == null) {
                    return null;
                }

                ArrayList<AnalyticsQuery> newAnalytics = new ArrayList<>();
                for (int i = 0; i < jsonArray.length(); i++) {

                    JSONObject query = jsonArray.getJSONObject(i);
                    if (AnalyticsQuery.isSupportedQuery(query.getInt("query_type"))) {
                        newAnalytics.add(new AnalyticsQuery(query));
                    }

                }

                if (listing_Paginator_offset == 0) {
                    analytics.clear();
                }
                analytics.addAll(newAnalytics);

                Activity activity = getActivity();
                if (activity != null) {

                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            adapter.notifyDataSetChanged();
                            if (getFromCache) {
                                new GetAnalytics().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, 0);
                            }
                            getFromCache = false;
                        }
                    });
                }

                saveToCacheFormatted(httpget, jsonArray);
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (NullPointerException | IllegalStateException e) {

            }
            return "ok";
        }
    }

    private void saveToCacheFormatted(HttpGet httpget, JSONArray jsonArray) {
        try {

            JSONArray jsonToCache = new JSONArray();
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject query = jsonArray.getJSONObject(i);
                if (AnalyticsQuery.isSupportedQuery(query.getInt("query_type"))) {
                    JSONObject jsonObject = new JSONObject();
                    for (String key :
                            AnalyticsQuery.keysToSave) {
                        jsonObject.put(key, query.get(key));
                    }
                    jsonToCache.put(jsonObject);
                }
            }

            String cacheKey = SyneHttpCacheHandler.getCacheKey(httpget);
            MainActivity.cacheString.put(cacheKey, jsonToCache.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private String getListingParams() {
        return "?listing[Paginator][limit]=" + listing_Paginator_limit + "&listing[Paginator][offset]=" + listing_Paginator_offset + "&listing[Type][enumValue]=" + listing_type_enumValue + "&listing[SimpleFilter][simpleFilterValue]=" + listing_search_value;
    }
}
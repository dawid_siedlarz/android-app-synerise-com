package com.synerise.synerise.app.model;

import java.util.ArrayList;

public class TagSelected {

    public static final ArrayList<Tag.TagModel> ITEMS = new ArrayList<Tag.TagModel>();

    public static void add(String name, String count) {
        ITEMS.add(new Tag.TagModel(name, count));
    }

    public static void clear() {
        ITEMS.clear();
    }


}

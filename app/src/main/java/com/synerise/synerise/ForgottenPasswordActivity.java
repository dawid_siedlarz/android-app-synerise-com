package com.synerise.synerise;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.synerise.synerise.app.model.BusinessProfile;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ForgottenPasswordActivity extends AppCompatActivity {

    private EditText email;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgotten_password);
       ActionBar bar = getSupportActionBar();
        if (bar != null){
            bar.hide();
        }
        email = (EditText)findViewById(R.id.emailForgotten);
    }




    public void remindPasswordAction(View v){
        new RemindPassword().execute(email.getText().toString());
    }


    public void createAccountAction(View v){
        Toast.makeText(this, "This option will be available soon", Toast.LENGTH_SHORT).show();
    }


    private class RemindPassword extends AsyncTask<String, Void, String> {
        protected String doInBackground(String... params) {
            try {

                HttpPost httpost = new HttpPost("https://app.synerise.com/resetting/send-email");

                List<NameValuePair> nvps = new ArrayList<NameValuePair>();
                nvps.add(new BasicNameValuePair("username", params[0]));


                httpost.setEntity(new UrlEncodedFormEntity(nvps, HTTP.UTF_8));
                HttpResponse response2 = MainActivity.httpClient.execute(httpost);
                if (response2 == null ){
                    return "not OK";
                }

                HttpEntity entity = response2.getEntity();

                final String body2 = EntityUtils.toString(entity);

                Document doc2 = Jsoup.parse(body2);

                BusinessProfile.clear();

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(ForgottenPasswordActivity.this, "Please check your email for further instructions", Toast.LENGTH_SHORT).show();
                        finish();
                    }
                });


            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {e.printStackTrace();}
            catch (NullPointerException e) {

            } catch (IllegalStateException e) {

            }return "ok";
        }
    }


}

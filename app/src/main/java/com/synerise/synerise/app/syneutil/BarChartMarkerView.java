package com.synerise.synerise.app.syneutil;

import android.content.Context;
import android.text.SpannableString;
import android.text.style.TypefaceSpan;
import android.widget.TextView;

import com.github.mikephil.charting.components.MarkerView;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.utils.MPPointF;
import com.synerise.synerise.R;

import java.util.List;

/**
 * Created by dsiedlarz on 16.02.2017.
 */

public class BarChartMarkerView extends MarkerView {

    private TextView tvContent;
    private String[] labels;
    private List<String> dates;


    public void setLabels(String[] labels) {
        this.labels = labels;
    }

    public void setDates(List<String> dates) {
        this.dates = dates;
    }

    public BarChartMarkerView(Context context, int layoutResource) {
        super(context, layoutResource);

        tvContent = (TextView) findViewById(R.id.tvContent);
    }

    // callbacks everytime the MarkerView is redrawn, can be used to update the
    // content (user-interface)
    @Override
    public void refreshContent(Entry e, Highlight highlight) {

//        if (e instanceof CandleEntry) {
//
//            CandleEntry ce = (CandleEntry) e;
//
//            tvContent.setText("" + Utils.formatNumber(ce.getHigh(), 0, true));
//        } else {
//
//            tvContent.setText("" + Utils.formatNumber(e.getY(), 0, true));
//        }

        BarEntry entry = (BarEntry) e;
        String x = null;
        String y = null;
        String date = null;

        if (entry.getYVals() != null) {
            y = String.valueOf((int)entry.getYVals()[highlight.getStackIndex()]);
            x = labels[highlight.getStackIndex()];
        }

        if (y == null || y.isEmpty()) {
            y = String.valueOf(entry.getY());
        }


        date = dates.get((int) entry.getX());

        SpannableString span =  new SpannableString( (x != null ? (x + " \n ") : "") + date + " \n " + y);
        span.setSpan(new TypefaceSpan("fonts/Lato-Thin.ttf"), 0, span.length(), 0);
        tvContent.setText(span);

        super.refreshContent(e, highlight);
    }

    @Override
    public MPPointF getOffset() {
        return new MPPointF(-(getWidth() / 2), -getHeight());
    }
}
package com.synerise.synerise.messenger.model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by dsiedlarz on 28.12.2016.
 */

public class MessengerClient extends MessengerUser {

    public static final String CLIENT_TYPE = "client";
    public static final String VENDOR_TYPE = "vendor";

    private String location;
    private ArrayList<String> tags = new ArrayList<>();
    private String lastViewedItem;
    private Date lastViewedDate;
    private Date createdTime;

    public MessengerClient(JSONObject object) {
        super(object);
        parseJsonObject(object);
    }

    public void parseJsonObject(JSONObject object) {
        super.parseJsonObject(object);
        if (getType() == null) {
            setUserType(USER_TYPE_CLIENTS);
        }
        try {
            if (object.has("attributes")) {
                JSONObject attributes = object.getJSONObject("attributes");
                if (attributes.has("location")) {
                    location = attributes.getString("location");
                }
                if (attributes.has("location")) {

                    lastViewedDate = new Date(attributes.getLong("last-viewed-time"));
                }
                if (attributes.has("created-time")) {
                    createdTime = new Date(attributes.getLong("created-time"));
                }
                if (attributes.has("last-viewed-item")) {
                    lastViewedItem = attributes.getString("last-viewed-item");
                }
                if (attributes.has("tags")) {
                    JSONArray tagsArray = attributes.getJSONArray("tags");
                    if (tags == null) {
                        tags = new ArrayList<>();
                    }
                    for (int i = 0; i < tagsArray.length(); i++) {
                        tags.add(tagsArray.getString(i));
                    }
                }
            }


        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public ArrayList<String> getTags() {
        return tags;
    }

    public void setTags(ArrayList<String> tags) {
        this.tags = tags;
    }

    public String getLastViewedItem() {
        return lastViewedItem;
    }

    public void setLastViewedItem(String lastViewedItem) {
        this.lastViewedItem = lastViewedItem;
    }

    public Date getLastViewedDate() {
        return lastViewedDate;
    }

    public void setLastViewedDate(Date lastViewedDate) {
        this.lastViewedDate = lastViewedDate;
    }

    public Date getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(Date createdTime) {
        this.createdTime = createdTime;
    }

}

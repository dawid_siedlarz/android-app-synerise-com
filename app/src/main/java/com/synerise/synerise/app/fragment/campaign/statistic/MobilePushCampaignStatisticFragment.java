package com.synerise.synerise.app.fragment.campaign.statistic;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.highlight.Highlight;
import com.synerise.synerise.MainActivity;
import com.synerise.synerise.R;
import com.synerise.synerise.app.ActionBarConfigurableFragment;
import com.synerise.synerise.app.model.campaign.MobilePushCampaign;
import com.synerise.synerise.app.syneutil.SyneValueFormatter;
import com.synerise.synerise.app.syneview.SyneChartTitle;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class MobilePushCampaignStatisticFragment extends ActionBarConfigurableFragment {
    public static final String ARG_CAMPAIGN = "campaign";
    private MobilePushCampaign mobilePushCampaign;
    PieChart mobilePushStatisticCtrChart;
    SyneChartTitle mobilePushStatisticCtrTitle;

    private TextView mobilePushCampaignStatisticTitle;
    private TextView mobilePushCampaignStatisticStatus;
    private TextView mobilePushCampaignStatisticRecipients;
    private TextView mobilePushCampaignStatisticSent;
    private TextView mobilePushStatisticDelivered;
    private TextView mobilePushStatisticRead;
    private TextView mobilePushStatisticDateSend;

    public MobilePushCampaignStatisticFragment() {
        // Required empty public constructor
    }


    @Override
    public void setUpActionBar() {
        ActionBar actionBar = (ActionBar) ((AppCompatActivity) getActivity()).getSupportActionBar();
        if (actionBar != null) {
            actionBar.show();
            actionBar.setTitle(MainActivity.businessProfileName != null ? MainActivity.businessProfileName : "Synerise");
            actionBar.setSubtitle("Mobil push campaign");
            actionBar.setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.colorPrimaryCampaigns)));
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                Window window = getActivity().getWindow();
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                window.setStatusBarColor(getResources().getColor(R.color.colorPrimaryDarkCampaigns));
            }
            actionBar.setElevation(0);
        }
    }

    public static MobilePushCampaignStatisticFragment newInstance(MobilePushCampaign mobilePushCampaign) {
        MobilePushCampaignStatisticFragment fragment = new MobilePushCampaignStatisticFragment();
        Bundle args = new Bundle();
        args.putParcelable(ARG_CAMPAIGN, mobilePushCampaign);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mobilePushCampaign = getArguments().getParcelable(ARG_CAMPAIGN);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_mobile_push_campaign_statistic, container, false);
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        findHolders();
        fillHolders();
        displayLineChart();

    }

    private void findHolders() {
        View rootView = getView();
        if (rootView == null) {
            return;
        }
        mobilePushStatisticCtrChart = rootView.findViewById(R.id.mobilePushStatisticCtrChart);
        mobilePushStatisticCtrTitle = rootView.findViewById(R.id.mobilePushStatisticCtrTitle);
        mobilePushCampaignStatisticTitle = rootView.findViewById(R.id.mobilePushCampaignStatisticTitle);
        mobilePushCampaignStatisticStatus = rootView.findViewById(R.id.mobilePushCampaignStatisticStatus);
        mobilePushCampaignStatisticRecipients = rootView.findViewById(R.id.mobilePushCampaignStatisticRecipients);
        mobilePushCampaignStatisticSent = rootView.findViewById(R.id.mobilePushCampaignStatisticSent);
        mobilePushStatisticDelivered = rootView.findViewById(R.id.mobilePushStatisticDelivered);
        mobilePushStatisticRead = rootView.findViewById(R.id.mobilePushStatisticRead);
        mobilePushStatisticDateSend = rootView.findViewById(R.id.mobilePushStatisticDateSend);
    }

    private void fillHolders() {
        DecimalFormat df = new DecimalFormat("###,###,###.##");

        mobilePushCampaignStatisticTitle.setText(mobilePushCampaign.getTitle());
        mobilePushCampaignStatisticStatus.setText(mobilePushCampaign.getStatus());
        mobilePushCampaignStatisticRecipients.setText(mobilePushCampaign.getRecipientsReadableString());
        mobilePushCampaignStatisticSent.setText(df.format(mobilePushCampaign.getSent()));
        mobilePushStatisticDelivered.setText(df.format(mobilePushCampaign.getDelivered()));
        mobilePushStatisticRead.setText(df.format(mobilePushCampaign.getRead()));

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy.MM.dd");
        mobilePushStatisticDateSend.setText(dateFormat.format(mobilePushCampaign.getDateSend()));
    }

    private void displayLineChart() {
        float ctr = mobilePushCampaign.getDelivered() != 0 ? ((float) mobilePushCampaign.getRead()) / mobilePushCampaign.getDelivered() : 0;
        mobilePushStatisticCtrTitle.setCounter(String.format("%.2f", ctr * 100));

        if (ctr == 0) {
            mobilePushStatisticCtrChart.setVisibility(View.GONE);
            return;
        }

        List<PieEntry> entries = new ArrayList<>();
        entries.add(new PieEntry(mobilePushCampaign.getRead(), "Read", 0));
        entries.add(new PieEntry(mobilePushCampaign.getDelivered() - mobilePushCampaign.getRead(), "Others", 0));

        PieDataSet set = new PieDataSet(entries, "");

        Context context = getContext();
        set.setColors(new int[]{R.color.fancyGreenAlpha, R.color.fancyGrayAlpha}, context);

        set.setValueLinePart1OffsetPercentage(100.0f);
        set.setValueLinePart1Length(0.15f);
        set.setValueLinePart2Length(0.0f);

        set.setValueLineColor(Color.parseColor("#00000000"));
        set.setYValuePosition(PieDataSet.ValuePosition.OUTSIDE_SLICE);

        PieData data = new PieData(set);

        data.setValueFormatter(new SyneValueFormatter(0));
        data.setValueTextSize(14f);
        data.setValueTextColor(Color.BLACK);

        data.setValueTypeface(Typeface.createFromAsset(getContext().getAssets(), "fonts/Lato-Thin.ttf"));

        mobilePushStatisticCtrChart.setData(data);

        mobilePushStatisticCtrChart.getDescription().setEnabled(false);

        mobilePushStatisticCtrChart.setCenterText("");
        mobilePushStatisticCtrChart.setDrawEntryLabels(false);
        mobilePushStatisticCtrChart.setExtraOffsets(15f, 15f, 15f, 15f);
        mobilePushStatisticCtrChart.setTransparentCircleRadius(90);
        mobilePushStatisticCtrChart.setHoleRadius(90);
        mobilePushStatisticCtrChart.setTransparentCircleAlpha(100);
        PieChartSelectedValueListener mobilePushStatisticCtrChartListener = new PieChartSelectedValueListener(mobilePushStatisticCtrChart);
        mobilePushStatisticCtrChart.setOnChartValueSelectedListener(mobilePushStatisticCtrChartListener);

        mobilePushStatisticCtrChart.disableScroll();
        mobilePushStatisticCtrChart.getLegend().setEnabled(false);

        mobilePushStatisticCtrChart.invalidate(); // refresh
//                                mobilePushStatisticCtrChart.animateY(1400, Easing.EasingOption.EaseInOutQuad);

        Highlight h = new Highlight(0, 0, 0);
        mobilePushStatisticCtrChart.highlightValue(h);
        mobilePushStatisticCtrChartListener.drawCenter(mobilePushStatisticCtrChart, h);

    }
}

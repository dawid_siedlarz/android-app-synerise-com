package com.synerise.synerise.app.model;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;

import com.synerise.synerise.R;

import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.ocpsoft.prettytime.PrettyTime;

import java.util.Date;
import java.util.Locale;

public class Event {
    String action;
    public String avatarUrl;
    public String city;
    public String clientId;
    public String created;
    public String createdOriginal;
    public String data;
    public String event;
    public JSONObject eventParams;

    public String externalAvatarUrl;
    public String firstname;
    public boolean isAnonymous;
    public String lastname;
    public String translation;
    public String uuid;

    public boolean hasModal = false;
    public String modalContent;
    public Spanned titleContent;
    public String titleHtml;
    public String nameContent;
    public String timeContent;
    public Drawable icon;

    public Event(JSONObject jsonObject, Context context) {
        try {
            this.action = jsonObject.getString("action");
            this.avatarUrl = jsonObject.getString("avatarUrl");
            this.city = jsonObject.getString("city");
            this.clientId = jsonObject.getString("clientId");
            this.created = jsonObject.getString("created");
            this.createdOriginal = jsonObject.getString("createdOriginal");
            this.data = jsonObject.getString("data");
            this.event = jsonObject.getString("event");
            this.eventParams = jsonObject.get("data") != null ? jsonObject.getJSONObject("eventParams") : null;
            this.externalAvatarUrl = jsonObject.getString("externalAvatarUrl");
            this.firstname = jsonObject.getString("firstname");
            this.isAnonymous = jsonObject.getString("isAnonymous").compareToIgnoreCase("") == 0 || jsonObject.getBoolean("isAnonymous");
            this.lastname = jsonObject.getString("lastname");
            this.translation = jsonObject.getString("translation");
            this.uuid = jsonObject.getString("uuid");

        } catch (JSONException e) {
            e.printStackTrace();
        }

        setIconNew(this.action, context);
        Document document = Jsoup.parse(this.translation);
        for (Element element : document.select("div.modal-content")) {
            this.modalContent = element.html().replace("hide", "").replace("modal-content", "");
            this.hasModal = true;
            element.remove();
        }

        for (Element element : document.select("button.more")) {
            element.remove();
        }

        this.titleContent = Html.fromHtml(document.toString());
        this.titleHtml = document.toString();

        this.nameContent = (this.isAnonymous ? "Anonymous" + (this.city.compareToIgnoreCase("null") == 0 ? "" : " from " + this.city) : this.firstname + " " + (this.lastname.compareToIgnoreCase("null") == 0 ? "" : this.lastname));


        Date date = null;
        try {
            date = new Date(this.eventParams.getLong("time"));
        } catch (JSONException e) {

        }
        this.timeContent = (new PrettyTime().setLocale(Locale.ENGLISH).format(date));
    }

    private void setIcon(String category, Context context) {
        switch (category) {
            case "CLIENT_SIGNED_IN_BY_NEWSLETTER":
                this.icon = context.getResources().getDrawable(R.drawable.cmr_add_or_update);
                break;
            case "CLIENT_CONFIRM_NEWSLETTER":
                this.icon = context.getResources().getDrawable(R.drawable.int_email_messages);
                break;
            case "CLIENT_FRESHMAIL_ACTION_HARD_BOUNCE":
                this.icon = context.getResources().getDrawable(R.drawable.cpg_email_messages);
                break;
            case "CLIENT_FRESHMAIL_ACTION_SOFT_BOUNCE":
                this.icon = context.getResources().getDrawable(R.drawable.cpg_email_messages);
                break;
            case "CLIENT_SENT_CONTACT_EMAIL":
                this.icon = context.getResources().getDrawable(R.drawable.cpg_email_messages);
                break;
            case "CLIENT_CAME_IN_BEACON_RANGE":
                this.icon = context.getResources().getDrawable(R.drawable.tec_beacon_range_in);
                break;
            case "CLIENT_CAME_OUT_BEACON_RANGE":
                this.icon = context.getResources().getDrawable(R.drawable.tec_beacon_range_out);
                break;
            case "CLIENT_CANCEL_NOTIFICATION":
                this.icon = context.getResources().getDrawable(R.drawable.tec_notification);
                break;
            case "CLIENT_CLICK_NOTIFICATION":
                this.icon = context.getResources().getDrawable(R.drawable.tec_notification);
                break;
            case "CLIENT_SHOW_NOTIFICATION":
                this.icon = context.getResources().getDrawable(R.drawable.tec_notification);
                break;
            case "CLIENT_BEACON_NOTIFICATION_SEND":
                this.icon = context.getResources().getDrawable(R.drawable.tec_notification);
                break;
            case "CLIENT_CANCEL_PUSH_MESSAGE":
                this.icon = context.getResources().getDrawable(R.drawable.cpg_push_messages);
                break;
            case "CLIENT_CLICK_PUSH_MESSAGE":
                this.icon = context.getResources().getDrawable(R.drawable.cpg_push_messages);
                break;
            case "CLIENT_SHOW_PUSH_MESSAGE":
                this.icon = context.getResources().getDrawable(R.drawable.cpg_push_messages);
                break;
            case "CLIENT_SUBSCRIBE_NOTIFICATION":
                this.icon = context.getResources().getDrawable(R.drawable.cpg_email_messages);
                break;
            case "CLIENT_LOG_IN_MOBILE":
                this.icon = context.getResources().getDrawable(R.drawable.cmr_login);
                break;
            case "CLIENT_LOG_OUT_MOBILE":
                this.icon = context.getResources().getDrawable(R.drawable.cmr_login);
                break;
            case "CLIENT_ACTIVATED_COUPON":
                this.icon = context.getResources().getDrawable(R.drawable.cpg_coupons);
                break;
            case "CLIENT_REDEEMED_COUPON":
                this.icon = context.getResources().getDrawable(R.drawable.cpg_coupons);
                break;
            case "CLIENT_MOBILE_EVENT":
                this.icon = context.getResources().getDrawable(R.drawable.tec_mobile_info);
                break;
            case "CLIENT_CHANGED_MOBILE_DISPLAY":
                this.icon = context.getResources().getDrawable(R.drawable.tec_mobile_info);
                break;
            case "CLIENT_MOBILE_VISITED_PAGE":
                this.icon = context.getResources().getDrawable(R.drawable.tec_mobile_info);
                break;
            case "CLIENT_SENT_FORM":
                this.icon = context.getResources().getDrawable(R.drawable.frm_form_update);
                break;
            case "CLIENT_MAIL_ACTION_CLICK":
                this.icon = context.getResources().getDrawable(R.drawable.cpg_email_messages);
                break;
            case "CLIENT_MAIL_ACTION_OPEN":
                this.icon = context.getResources().getDrawable(R.drawable.cpg_email_messages);
                break;
            case "CLIENT_MAIL_ACTION_UNSUBSCRIBE":
                this.icon = context.getResources().getDrawable(R.drawable.cpg_email_messages);
                break;
            case "CLIENT_FRESHMAIL_ACTION_CLICK":
                this.icon = context.getResources().getDrawable(R.drawable.cpg_email_messages);
                break;
            case "CLIENT_FRESHMAIL_ACTION_OPEN":
                this.icon = context.getResources().getDrawable(R.drawable.cpg_email_messages);
                break;
            case "CLIENT_FRESHMAIL_ACTION_UNSUBSCRIBE":
                this.icon = context.getResources().getDrawable(R.drawable.cpg_email_messages);
                break;
            case "CLIENT_ADD_AVAIL_NOTIFICATION":
                this.icon = context.getResources().getDrawable(R.drawable.tec_notification);
                break;
            case "CLIENT_ADDED":
                this.icon = context.getResources().getDrawable(R.drawable.cmr_add_or_update);
                break;
            case "CLIENT_REGISTERED":
                this.icon = context.getResources().getDrawable(R.drawable.cmr_add_or_update);
                break;
            case "CLIENT_CLUB_SIGNUP":
                this.icon = context.getResources().getDrawable(R.drawable.cmr_add_or_update);
                break;
            case "CLIENT_VISITED_PAGE":
                this.icon = context.getResources().getDrawable(R.drawable.off_page_visit);
                break;
            case "CLIENT_NOTIFICATION_CONFIRMATION":
                this.icon = context.getResources().getDrawable(R.drawable.int_email_messages);
                break;
            case "CLIENT_IGNORE_NOTIFICATION":
                this.icon = context.getResources().getDrawable(R.drawable.int_custom_event);
                break;
            case "CLIENT_NOTIFICATION_SEND":
                this.icon = context.getResources().getDrawable(R.drawable.int_custom_event);
                break;
            case "CLIENT_COMMENTED_ON_FACEBOOK":
                this.icon = context.getResources().getDrawable(R.drawable.cmr_click_tap);
                break;
            case "CLIENT_SEND_MAIL_AFTER_COUPON_ACTIVATE":
                this.icon = context.getResources().getDrawable(R.drawable.cpg_coupons);
                break;
            case "CLIENT_GET_NOTIFICATION":
                this.icon = context.getResources().getDrawable(R.drawable.tec_notification);
                break;
            case "CLIENT_SEND_PUSH_MESSAGE":
                this.icon = context.getResources().getDrawable(R.drawable.int_push_messages);
                break;
            case "CLIENT_ADD_NOTE":
                this.icon = context.getResources().getDrawable(R.drawable.wrk_note);
                break;
            case "CLIENT_SMS_SEND":
                this.icon = context.getResources().getDrawable(R.drawable.int_sms_messages);
                break;
            case "CLIENT_SMS_BOUNCE":
                this.icon = context.getResources().getDrawable(R.drawable.int_sms_messages);
                break;
            case "CLIENT_SMS_DELIVER":
                this.icon = context.getResources().getDrawable(R.drawable.int_sms_messages);
                break;
            case "CLIENT_SEND_EMAIL":
                this.icon = context.getResources().getDrawable(R.drawable.int_email_messages);
                break;
            case "CLIENT_START_PROMOTION":
                this.icon = context.getResources().getDrawable(R.drawable.cpg_promotions);
                break;
            case "CLIENT_DATA_CHANGED":
                this.icon = context.getResources().getDrawable(R.drawable.cmr_add_or_update);
                break;
            case "CLIENT_ADDED_SOURCE_WEB":
                this.icon = context.getResources().getDrawable(R.drawable.cmr_add_or_update);
                break;
            case "CLIENT_ADD_POINTS":
                this.icon = context.getResources().getDrawable(R.drawable.cmr_score);
                break;
            case "CLIENT_ADDED_SOURCE_API":
                this.icon = context.getResources().getDrawable(R.drawable.cmr_add_or_update);
                break;
            case "CLIENT_ADDED_SOURCE_IMPORT":
                this.icon = context.getResources().getDrawable(R.drawable.cmr_add_or_update);
                break;
            case "CLIENT_ADDED_SOURCE_TRACKING":
                this.icon = context.getResources().getDrawable(R.drawable.cmr_add_or_update);
                break;
            case "CLIENT_ADDED_SOURCE_INTEGRATION":
                this.icon = context.getResources().getDrawable(R.drawable.cmr_add_or_update);
                break;
            case "CLIENT_ADDED_SOURCE_IMPORTINTEGRATION":
                this.icon = context.getResources().getDrawable(R.drawable.cmr_add_or_update);
                break;
            case "CLIENT_PRODUCT_ADDED_TO_CART":
                this.icon = context.getResources().getDrawable(R.drawable.off_product_availability);
                break;
            case "CLIENT_PRODUCT_REMOVED_FROM_CART":
                this.icon = context.getResources().getDrawable(R.drawable.off_product_availability);
                break;
            case "CLIENT_TRANSACTION_CHARGE":
                this.icon = context.getResources().getDrawable(R.drawable.tsn_buy);
                break;
            case "CLIENT_RETAIL_POS_CORE":
                this.icon = context.getResources().getDrawable(R.drawable.tsn_buy);
                break;
            case "CLIENT_CREATE_OR_UPDATE":
                this.icon = context.getResources().getDrawable(R.drawable.cmr_add_or_update);
                break;
            case "CLIENT_AUTOMATION_IS_IN_STEP":
                this.icon = context.getResources().getDrawable(R.drawable.atm_step);
                break;
            case "CLIENT_AUTOMATION_HAS_FINISHED":
                this.icon = context.getResources().getDrawable(R.drawable.atm_end);
                break;
            case "CLIENT_MARKETING_AGREEMENT_TURN_ON":
                this.icon = context.getResources().getDrawable(R.drawable.int_email_messages);
                break;
            case "CLIENT_MARKETING_AGREEMENT_TURN_OFF":
                this.icon = context.getResources().getDrawable(R.drawable.int_email_messages);
                break;
            case "CLIENT_ADD_TAG":
                this.icon = context.getResources().getDrawable(R.drawable.int_tags_add_remove);
                break;
            case "CLIENT_DYNAMIC_CONTENT_SHOW":
                this.icon = context.getResources().getDrawable(R.drawable.cpg_dynamic_content);
                break;
            case "CLIENT_BIRTHDAY_TODAY":
                this.icon = context.getResources().getDrawable(R.drawable.cmr_view_message);
                break;
            case "CLIENT_LOG_IN_WEB":
                this.icon = context.getResources().getDrawable(R.drawable.event_api_wrongresponse);
                break;
            default:
                this.icon = context.getResources().getDrawable(R.drawable.int_custom_event);
        }
    }

    private void setIconNew(String action, Context context) {
        try {
            int resId = context.getResources().getIdentifier("event_" + action.replace("-", "_").toLowerCase(), "drawable", context.getPackageName());
            if (resId > 0) {
                this.icon = context.getResources().getDrawable(resId);

            } else {
                this.icon = context.getResources().getDrawable(R.drawable.int_custom_event);
            }
        } catch (Exception e) {
            this.icon = context.getResources().getDrawable(R.drawable.int_custom_event);
        }

    }
}


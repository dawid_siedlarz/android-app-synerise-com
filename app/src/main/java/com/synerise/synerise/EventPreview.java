package com.synerise.synerise;

import android.app.Activity;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebChromeClient;
import android.webkit.WebView;

import com.synerise.synerise.app.ActionBarConfigurableFragment;

public class EventPreview extends ActionBarConfigurableFragment {
    private WebView webView;


    @Override
    public void setUpActionBar() {
        ActionBar actionBar = (ActionBar) ((AppCompatActivity) getActivity()).getSupportActionBar();
        if (actionBar != null) {
            actionBar.show();
            actionBar.setTitle(MainActivity.businessProfileName != null ? MainActivity.businessProfileName : "Synerise");
            actionBar.setSubtitle("Event preview");
            actionBar.setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.colorPrimary)));
            getActivity().setTheme(R.style.AppThemeInsights);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                Window window = getActivity().getWindow();
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                window.setStatusBarColor(getResources().getColor(R.color.colorPrimary));
            }
            actionBar.setElevation(0);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.activity_event_preview, container, false);
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        String body = getArguments().getString("body");
        String header = getArguments().getString("header");

        View rootView = getView();
        if (rootView == null) {
            return;
        }
        webView = (WebView) rootView.findViewById(R.id.eventPreviewWebView);
        WebChromeClient chromeClient = new WebChromeClient();
        webView.setWebChromeClient(chromeClient);


        final StringBuilder htmlData = new StringBuilder();
        htmlData.append("<head><style>");

        htmlData.append(css);
        htmlData.append(".snrs-popup--content {\n" +
                "    box-shadow: 0 0 0 rgba(16, 27, 36, 0.2) !important;\n" +
                "    display: block !important;\n" +
                "}");
        htmlData.append(".snrs-popup--event-body {\n" +
                "    padding: 15px 10px !important;\n" +
                "}");
        htmlData.append(".snrs-popup--footer {\n" +
                "    display: none !important; }");
        htmlData.append("img {\n" +
                "    width: 400px; " +
                "    display: block !important;\n" +
                "    position: relative !important; }");
        htmlData.append("button {\n" +
                "    display: none !important; }");
        htmlData.append("</style><script>");
//            htmlData.append(headJs);
//            htmlData.append(" ");
//            htmlData.append(modalEventJs);

        htmlData.append("</script></head>");
        htmlData.append("<body style=\"background:#FFF;\">");
        htmlData.append("<div class=\"snrs-popup--content modal-content\"><div class=\"snrs-popup--event-header\" id=\"snrsPopupHeader\">Activity details</div><div class=\"snrs-popup--event-subheader\" id=\"snrsPopupSubHeader\">" + header + "</div><div class=\"snrs-popup--event-scroll\"><div class=\"snrs-popup--event-body snrs-ta-left\" id=\"snrsPopupBody\">\n");
        htmlData.append(body);
        htmlData.append("</div></div></div>\n");
        htmlData.append("</body>");


        Activity activity = getActivity();
        if (activity == null) {
            return;
        }

        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                webView.loadDataWithBaseURL(null, htmlData.toString(), "text/html", "utf-8", null);
            }
        });
    }


    String css = "@font-face {\n" +
            "  font-family: 'LatoRegular';\n" +
            "  src: url('https://app.synerise.com/assets/font/Lato-Regular.eot');\n" +
            "  src: url('https://app.synerise.com/assets/font/Lato-Regular.eot?#iefix') format('embedded-opentype'), url('https://app.synerise.com/assets/font/Lato-Regular.woff2') format('woff2'), url('https://app.synerise.com/assets/font/Lato-Regular.woff') format('woff'), url('https://app.synerise.com/assets/font/Lato-Regular.ttf') format('truetype');\n" +
            "  font-weight: normal;\n" +
            "  font-style: normal;\n" +
            "}\n" +
            "body {\n" +
            "  font-family: \"LatoRegular\", sans-serif;\n" +
            "  -webkit-font-smoothing: antialiased;\n" +
            "  -moz-osx-font-smoothing: grayscale;\n" +
            "  background: #fff;\n" +
            "}\n" +
            ".snrs-popup--event-subheader .ico {\n" +
            "  box-shadow: none;\n" +
            "  margin-top: -22px;\n" +
            "  left: -1px;\n" +
            "}\n" +
            ".snrs-popup--event-subheader .snrs-activity--list-default {\n" +
            "  margin: 0;\n" +
            "  padding: 0;\n" +
            "}\n" +
            ".snrs-popup--event-subheader .snrs-activity--list-name a {\n" +
            "  font-size: 12px;\n" +
            "  color: #6c7780;\n" +
            "  display: block;\n" +
            "  text-align: left;\n" +
            "  margin: 5px 0 0;\n" +
            "  font-weight: normal;\n" +
            "}\n" +
            "html body .snrs-popup--event-subheader .snrs-activity--list-name {\n" +
            "  font-size: 12px;\n" +
            "  color: #6c7780;\n" +
            "  display: block;\n" +
            "  text-align: left;\n" +
            "  margin: 5px 0 0;\n" +
            "  font-weight: normal;\n" +
            "}\n" +
            ".snrs-popup--event-subheader .snrs-activity--list-activity {\n" +
            "  color: #2b373c;\n" +
            "  font-size: 15px;\n" +
            "  font-weight: bold;\n" +
            "}\n" +
            ".snrs-popup--event-subheader .snrs-activity--list-data-item {\n" +
            "  height: auto;\n" +
            "}\n" +
            ".snrs-popup--event-subheader .snrs-activity--list-data-item > div {\n" +
            "  margin: 5px 0 0;\n" +
            "}\n" +
            "\n" +
            ".snrs-popup.snrs-tutorial-modal.snrs-automation-tutorial-modal {\n" +
            "  margin-top: -300px;\n" +
            "  padding: 45px!important;\n" +
            "  margin-left: -380px;\n" +
            "  width: auto;\n" +
            "  height: auto;\n" +
            "  overflow: hidden;\n" +
            "}\n" +
            ".snrs-tutorial-modal.snrs-automation-tutorial-modal .snrs-popup--wrapper {\n" +
            "  position: static;\n" +
            "}\n" +
            ".snrs-tutorial-modal.snrs-automation-tutorial-modal .snrs-popup--aligner {\n" +
            "  padding: 0;\n" +
            "}\n" +
            ".snrs-popup.snrs-tutorial-modal {\n" +
            "  margin-top: -323px;\n" +
            "  padding: 45px!important;\n" +
            "  margin-left: -262px;\n" +
            "  top: 50%;\n" +
            "  left: 50%;\n" +
            "  bottom: auto;\n" +
            "  right: auto!important;\n" +
            "  -webkit-box-shadow: none;\n" +
            "  -moz-box-shadow: none;\n" +
            "  box-shadow: none;\n" +
            "  outline: none;\n" +
            "}\n" +
            ".snrs-popup.snrs-tutorial-modal .snrs-popup--content {\n" +
            "  min-width: inherit;\n" +
            "}\n" +
            ".snrs-popup.snrs-tutorial-modal .snrs-popup--body {\n" +
            "  padding: 0;\n" +
            "}\n" +
            ".snrs-popup.snrs-tutorial-modal .modal-dialog {\n" +
            "  width: 420px;\n" +
            "}\n" +
            "\n" +
            ".snrs-popup {\n" +
            "  z-index: 999999 !important;\n" +
            "  /* ==========================================================================\n" +
            "\t\t\t Modal Event\n" +
            "\t\t\t ========================================================================== */\n" +
            "}\n" +
            ".snrs-popup .modal-backdrop {\n" +
            "  background: rgba(16, 27, 36, 0.7);\n" +
            "  position: fixed !important;\n" +
            "  z-index: 99999 !important;\n" +
            "  height: 100% !important;\n" +
            "}\n" +
            ".snrs-popup .modal-backdrop.in.fade {\n" +
            "  display: block;\n" +
            "}\n" +
            ".snrs-popup .modal-backdrop {\n" +
            "  z-index: 99 !important;\n" +
            "}\n" +
            ".snrs-popup--overlay {\n" +
            "  position: fixed;\n" +
            "  width: 100%;\n" +
            "  height: 100%;\n" +
            "  overflow: auto;\n" +
            "  left: 0;\n" +
            "  bottom: 0;\n" +
            "  display: none;\n" +
            "  z-index: 99999;\n" +
            "}\n" +
            ".snrs-popup--wrapper {\n" +
            "  padding: 20px 0;\n" +
            "  display: table;\n" +
            "  left: 0;\n" +
            "  top: 0;\n" +
            "  width: 100%;\n" +
            "  height: 100%;\n" +
            "  position: absolute;\n" +
            "  -webkit-box-sizing: border-box;\n" +
            "  -moz-box-sizing: border-box;\n" +
            "  box-sizing: border-box;\n" +
            "}\n" +
            ".snrs-popup--aligner {\n" +
            "  display: table-cell;\n" +
            "  vertical-align: middle;\n" +
            "  width: 100%;\n" +
            "  text-align: center;\n" +
            "  padding: 80px 0 80px 0;\n" +
            "}\n" +
            ".snrs-ta-centered.snrs-popup--body,\n" +
            ".snrs-ta-centered.snrs-popup--footer,\n" +
            ".snrs-ta-centered.snrs-popup--header {\n" +
            "  text-align: center;\n" +
            "}\n" +
            ".snrs-ta-right.snrs-popup--body,\n" +
            ".snrs-ta-right.snrs-popup--footer,\n" +
            ".snrs-ta-right.snrs-popup--header {\n" +
            "  text-align: right;\n" +
            "}\n" +
            ".snrs-ta-left.snrs-popup--body,\n" +
            ".snrs-ta-left.snrs-popup--footer,\n" +
            ".snrs-ta-left.snrs-popup--header {\n" +
            "  text-align: left;\n" +
            "}\n" +
            ".snrs-popup--close {\n" +
            "  width: 18px;\n" +
            "  height: 18px;\n" +
            "  position: absolute;\n" +
            "  top: 32px;\n" +
            "  right: 30px;\n" +
            "  background-size: 100%;\n" +
            "  cursor: pointer;\n" +
            "  -webkit-transition: ease-out all 0.4s;\n" +
            "  -moz-transition: ease-out all 0.4s;\n" +
            "  -o-transition: ease-out all 0.4s;\n" +
            "  transition: ease-out all 0.4s;\n" +
            "}\n" +
            ".snrs-popup--close:hover {\n" +
            "  background-size: 100%;\n" +
            "  -webkit-transition: ease-out all 0.4s;\n" +
            "  -moz-transition: ease-out all 0.4s;\n" +
            "  -o-transition: ease-out all 0.4s;\n" +
            "  transition: ease-out all 0.4s;\n" +
            "}\n" +
            ".snrs-popup .snrs-header--inline {\n" +
            "  padding: 30px;\n" +
            "}\n" +
            ".snrs-popup .snrs-header--inline h1,\n" +
            ".snrs-popup .snrs-header--inline h2,\n" +
            ".snrs-popup .snrs-header--inline h3 {\n" +
            "  padding: 0;\n" +
            "  font-weight: 400;\n" +
            "  font-size: 22px;\n" +
            "  text-align: left;\n" +
            "  margin: 0;\n" +
            "}\n" +
            ".modal .snrs-popup--header h1,\n" +
            ".modal .snrs-popup--header h2,\n" +
            ".modal .snrs-popup--header h3 {\n" +
            "  padding: 0;\n" +
            "  font-weight: 400;\n" +
            "  font-size: 22px;\n" +
            "  text-align: left;\n" +
            "  margin: 0;\n" +
            "}\n" +
            ".modal .snrs-popup--header .m-title {\n" +
            "  padding-right: 20px;\n" +
            "  position: relative;\n" +
            "}\n" +
            ".modal .snrs-popup--header .m-title:before {\n" +
            "  position: absolute;\n" +
            "  right: 0;\n" +
            "  top: 8px;\n" +
            "}\n" +
            ".modal .snrs-popup--body {\n" +
            "  padding: 15px 30px;\n" +
            "}\n" +
            "body .modal .snrs-popup--body .default-wrapper {\n" +
            "  margin: 0;\n" +
            "  padding: 0;\n" +
            "}\n" +
            ".modal .snrs-popup--footer {\n" +
            "  padding: 15px 30px 30px 30px;\n" +
            "}\n" +
            ".modal .snrs-popup--footer .snrs-popup--help-text {\n" +
            "  font-size: 13px;\n" +
            "  color: #6c7780;\n" +
            "}\n" +
            ".modal .snrs-popup--footer .snrs-popup--help-text p {\n" +
            "  margin-top: 25px;\n" +
            "  font-size: 13px;\n" +
            "}\n" +
            ".modal .snrs-popup--content {\n" +
            "  -webkit-box-sizing: border-box;\n" +
            "  -moz-box-sizing: border-box;\n" +
            "  box-sizing: border-box;\n" +
            "  -webkit-box-shadow: 0 0 50px 1px rgba(16, 27, 36, 0.2);\n" +
            "  -moz-box-shadow: 0 0 50px 1px rgba(16, 27, 36, 0.2);\n" +
            "  box-shadow: 0 0 50px 1px rgba(16, 27, 36, 0.2);\n" +
            "  background: #fff;\n" +
            "  color: #2f3a43;\n" +
            "  display: inline-block;\n" +
            "  text-align: left;\n" +
            "  -webkit-border-radius: 0;\n" +
            "  -webkit-background-clip: padding-box;\n" +
            "  -moz-border-radius: 0;\n" +
            "  -moz-background-clip: padding;\n" +
            "  border-radius: 0;\n" +
            "  background-clip: padding-box;\n" +
            "  border: none;\n" +
            "  min-width: 600px;\n" +
            "  display: block;\n" +
            "}\n" +
            ".modal .snrs-popup--content .snrs-header--inline,\n" +
            ".modal .snrs-popup--content .snrs-title--primary {\n" +
            "  font-weight: 400!important;\n" +
            "  font-style: normal;\n" +
            "  font-variant: normal;\n" +
            "  text-rendering: auto!important;\n" +
            "}\n" +
            ".modal .snrs-popup--content p {\n" +
            "  margin-bottom: 0;\n" +
            "  margin-top: 0;\n" +
            "}\n" +
            ".modal .snrs-popup--content .snrs-popup--help-text {\n" +
            "  font-size: 13px;\n" +
            "  color: #6c7780;\n" +
            "  margin-top: 25px;\n" +
            "}\n" +
            ".modal .snrs-popup--content .snrs-popup--help-text p {\n" +
            "  margin-top: 25px;\n" +
            "}\n" +
            ".snrs-popup--dialog {\n" +
            "  z-index: 999999;\n" +
            "  opacity: 0;\n" +
            "  -webkit-transition: 0.4s;\n" +
            "  -moz-transition: 0.4s;\n" +
            "  -o-transition: 0.4s;\n" +
            "  transition: 0.4s;\n" +
            "  margin-top: 0!important;\n" +
            "}\n" +
            ".modal .snrs-popup--loader {\n" +
            "  height: 100%;\n" +
            "  width: 100%;\n" +
            "  display: table;\n" +
            "  vertical-align: middle;\n" +
            "  text-align: center;\n" +
            "  top: 0;\n" +
            "  left: 0;\n" +
            "  padding: 0;\n" +
            "  margin: 0;\n" +
            "  margin-left: 0;\n" +
            "  min-height: 250px;\n" +
            "  position: relative;\n" +
            "}\n" +
            ".snrs-popup--loader-inner {\n" +
            "  text-align: center;\n" +
            "  vertical-align: middle;\n" +
            "  display: table-cell;\n" +
            "}\n" +
            ".snrs-popup--event {\n" +
            "  width: 1024px;\n" +
            "}\n" +
            ".snrs-popup--event-list {\n" +
            "  margin: 0;\n" +
            "  padding: 0;\n" +
            "  list-style: none;\n" +
            "  display: table;\n" +
            "  text-align: left;\n" +
            "}\n" +
            ".snrs-popup--event-list li {\n" +
            "  display: table-row;\n" +
            "  line-height: 1.5;\n" +
            "}\n" +
            ".snrs-popup--event-list p {\n" +
            "  font-size: 13px;\n" +
            "  color: #6c7780;\n" +
            "  display: table-cell;\n" +
            "  padding: 3px 40px 3px 0;\n" +
            "  word-wrap: break-word;\n" +
            "}\n" +
            ".snrs-popup--event-list-key {\n" +
            "  text-transform: capitalize;\n" +
            "}\n" +
            "body .snrs-popup--event-list-value {\n" +
            "  font-weight: bold;\n" +
            "  color: #2f3a43;\n" +
            "}\n" +
            ".snrs-popup--event-scroll {\n" +
            "  width: 100%;\n" +
            "  overflow: auto;\n" +
            "}\n" +
            ".snrs-popup--event-scroll .jspVerticalBar {\n" +
            "  width: 8px;\n" +
            "  right: 1px;\n" +
            "}\n" +
            ".snrs-popup--event-scroll .jspDrag {\n" +
            "  width: 6px;\n" +
            "  background: #afb2bc;\n" +
            "  -webkit-border-radius: 8px;\n" +
            "  -webkit-background-clip: padding-box;\n" +
            "  -moz-border-radius: 8px;\n" +
            "  -moz-background-clip: padding;\n" +
            "  border-radius: 8px;\n" +
            "  background-clip: padding-box;\n" +
            "}\n" +
            ".snrs-popup--event .product-line {\n" +
            "  margin-bottom: 45px;\n" +
            "}\n" +
            ".snrs-popup--event .product-line img {\n" +
            "  position: absolute;\n" +
            "  top: 0;\n" +
            "  width: 95px;\n" +
            "  max-width: 100%;\n" +
            "  right: 0;\n" +
            "}\n" +
            ".snrs-popup--event .product-line img.product-show {\n" +
            "  width: 100%;\n" +
            "  position: relative;\n" +
            "}\n" +
            ".snrs-popup--event .product-line table {\n" +
            "  width: 100%;\n" +
            "}\n" +
            ".snrs-popup--event .product-line td {\n" +
            "  vertical-align: top;\n" +
            "}\n" +
            ".snrs-popup--event .product-line td.images {\n" +
            "  width: 200px;\n" +
            "}\n" +
            ".snrs-popup--event-header {\n" +
            "  padding: 30px;\n" +
            "  -webkit-box-flex: 0;\n" +
            "  -moz-box-flex: 0;\n" +
            "  -webkit-flex: 0 1 auto;\n" +
            "  -ms-flex: 0 1 auto;\n" +
            "  flex: 0 1 auto;\n" +
            "  display: -webkit-box;\n" +
            "  display: -moz-box;\n" +
            "  display: -webkit-flex;\n" +
            "  display: -ms-flexbox;\n" +
            "  display: flex;\n" +
            "  -moz-justify-content: space-between;\n" +
            "  -ms-justify-content: space-between;\n" +
            "  -o-justify-content: space-between;\n" +
            "  -webkit-box-pack: justify;\n" +
            "  -moz-box-pack: justify;\n" +
            "  -ms-flex-pack: justify;\n" +
            "  -webkit-justify-content: space-between;\n" +
            "  justify-content: space-between;\n" +
            "}\n" +
            ".snrs-popup--event-header .btn-close {\n" +
            "  background: transparent;\n" +
            "  padding: 0;\n" +
            "  border: 0;\n" +
            "  margin-right: -1px;\n" +
            "}\n" +
            ".snrs-popup--event-header .btn-close:after {\n" +
            "  content: \"\";\n" +
            "  width: 20px;\n" +
            "  height: 20px;\n" +
            "  display: block;\n" +
            "  background-image: url(\"data:image/svg+xml;charset=UTF-8,%3C%3Fxml%20version%3D%221.0%22%20encoding%3D%22utf-8%22%3F%3E%0A%3C!--%20Generator%3A%20Adobe%20Illustrator%2019.2.1%2C%20SVG%20Export%20Plug-In%20.%20SVG%20Version%3A%206.00%20Build%200)%20%20--%3E%0A%3Csvg%20version%3D%221.1%22%20id%3D%22Warstwa_1%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20xmlns%3Axlink%3D%22http%3A%2F%2Fwww.w3.org%2F1999%2Fxlink%22%20x%3D%220px%22%20y%3D%220px%22%0A%09%20viewBox%3D%220%200%2036%2036%22%20style%3D%22enable-background%3Anew%200%200%2036%2036%3B%22%20xml%3Aspace%3D%22preserve%22%3E%0A%3Cstyle%20type%3D%22text%2Fcss%22%3E%0A%09.st0%7Bopacity%3A0%3Bfill-rule%3Aevenodd%3Bclip-rule%3Aevenodd%3B%7D%0A%09.st1%7Bfill%3Anone%3Bstroke%3A%23AFB3BC%3Bstroke-width%3A4%3Bstroke-linecap%3Around%3Bstroke-linejoin%3Around%3Bstroke-miterlimit%3A10%3B%7D%0A%3C%2Fstyle%3E%0A%3Crect%20class%3D%22st0%22%20width%3D%2236%22%20height%3D%2236%22%2F%3E%0A%3Cline%20class%3D%22st1%22%20x1%3D%225.5%22%20y1%3D%225.6%22%20x2%3D%2231.5%22%20y2%3D%2231.6%22%2F%3E%0A%3Cline%20class%3D%22st1%22%20x1%3D%225.5%22%20y1%3D%2231.6%22%20x2%3D%2231.5%22%20y2%3D%225.6%22%2F%3E%0A%3C%2Fsvg%3E%0A\");\n" +
            "  background-repeat: no-repeat;\n" +
            "}\n" +
            ".snrs-popup--event-header-title {\n" +
            "  margin: 0;\n" +
            "  line-height: 1;\n" +
            "}\n" +
            ".snrs-popup--event-body {\n" +
            "  padding: 15px 100px;\n" +
            "  overflow: auto;\n" +
            "}\n" +
            ".snrs-popup--event-subheader {\n" +
            "  background: #fafafa;\n" +
            "  padding: 30px;\n" +
            "}\n" +
            "body .snrs-popup--event .more-info-track {\n" +
            "  margin: 20px 0;\n" +
            "  display: none;\n" +
            "}\n" +
            ".snrs-popup--event .more-info-track a {\n" +
            "  color: #6c7780;\n" +
            "  font-size: 13px;\n" +
            "  padding: 7px 16px;\n" +
            "  border-style: solid;\n" +
            "  margin: 2px;\n" +
            "  font-weight: 700;\n" +
            "  -webkit-border-radius: 3px;\n" +
            "  -webkit-background-clip: padding-box;\n" +
            "  -moz-border-radius: 3px;\n" +
            "  -moz-background-clip: padding;\n" +
            "  border-radius: 3px;\n" +
            "  background-clip: padding-box;\n" +
            "  -webkit-transition: ease-in 0.2s;\n" +
            "  -moz-transition: ease-in 0.2s;\n" +
            "  -o-transition: ease-in 0.2s;\n" +
            "  transition: ease-in 0.2s;\n" +
            "  -webkit-transition-property: background, border, -webkit-box-shadow, opacity, color, scale;\n" +
            "  -moz-transition-property: background, border, -moz-box-shadow, opacity, color, scale;\n" +
            "  -o-transition-property: background, border, box-shadow, opacity, color, scale;\n" +
            "  transition-property: background, border, box-shadow, opacity, color, scale;\n" +
            "  border-width: 1px;\n" +
            "  outline: none;\n" +
            "  display: inline-block;\n" +
            "  vertical-align: middle;\n" +
            "  background: #fff;\n" +
            "  color: #69757e;\n" +
            "  border-color: #d7d9dd;\n" +
            "  padding: 3px 12px;\n" +
            "  font-size: 12px;\n" +
            "  line-height: 18px;\n" +
            "  margin: 0;\n" +
            "  text-transform: capitalize;\n" +
            "}\n" +
            ".snrs-popup--event .more-info-track a.snrs-button--enable-icon {\n" +
            "  padding: 0;\n" +
            "}\n" +
            ".snrs-popup--event .more-info-track a.snrs-button--enable-icon.inverse span:first-child:before,\n" +
            ".snrs-popup--event .more-info-track a.snrs-button--enable-icon.snrs-button--invert span:first-child:before {\n" +
            "  /*opacity:1;*/\n" +
            "}\n" +
            ".snrs-popup--event .more-info-track a.snrs-button--enable-icon span {\n" +
            "  display: inline-block;\n" +
            "  vertical-align: middle;\n" +
            "  padding: 7px 16px;\n" +
            "  color: inherit;\n" +
            "  font-size: inherit;\n" +
            "  font-weight: inherit;\n" +
            "  text-align: center;\n" +
            "  text-transform: capitalize;\n" +
            "}\n" +
            ".snrs-popup--event .more-info-track a.snrs-button--enable-icon span:first-child {\n" +
            "  position: relative;\n" +
            "}\n" +
            ".snrs-popup--event .more-info-track a.snrs-button--enable-icon span:first-child:before {\n" +
            "  position: absolute;\n" +
            "  height: 100%;\n" +
            "  width: 0;\n" +
            "  content: '';\n" +
            "  border-right: 1px solid;\n" +
            "  opacity: 0.2;\n" +
            "  top: 0;\n" +
            "  right: 0;\n" +
            "}\n" +
            ".snrs-popup--event .more-info-track a.snrs-button--enable-icon span i {\n" +
            "  width: 20px;\n" +
            "  margin: 0 -10px;\n" +
            "  color: inherit;\n" +
            "  font-size: inherit;\n" +
            "  font-weight: inherit;\n" +
            "}\n" +
            ".snrs-popup--event .more-info-track a:hover {\n" +
            "  outline: none;\n" +
            "  -webkit-transition: ease-in 0.2s;\n" +
            "  -moz-transition: ease-in 0.2s;\n" +
            "  -o-transition: ease-in 0.2s;\n" +
            "  transition: ease-in 0.2s;\n" +
            "  -webkit-transition-property: background, border, -webkit-box-shadow, opacity, color, scale;\n" +
            "  -moz-transition-property: background, border, -moz-box-shadow, opacity, color, scale;\n" +
            "  -o-transition-property: background, border, box-shadow, opacity, color, scale;\n" +
            "  transition-property: background, border, box-shadow, opacity, color, scale;\n" +
            "  -webkit-box-shadow: 0 0px 0px rgba(0, 0, 0, 0.08), 1px 3px 9px -2px rgba(0, 0, 0, 0.2);\n" +
            "  -moz-box-shadow: 0 0px 0px rgba(0, 0, 0, 0.08), 1px 3px 9px -2px rgba(0, 0, 0, 0.2);\n" +
            "  box-shadow: 0 0px 0px rgba(0, 0, 0, 0.08), 1px 3px 9px -2px rgba(0, 0, 0, 0.2);\n" +
            "}\n" +
            ".snrs-popup--event .more-info-track a:focus,\n" +
            ".snrs-popup--event .more-info-track a:active {\n" +
            "  -webkit-box-shadow: none;\n" +
            "  -moz-box-shadow: none;\n" +
            "  box-shadow: none;\n" +
            "}\n" +
            ".snrs-popup--event .more-info-track a:focus:hover,\n" +
            ".snrs-popup--event .more-info-track a:active:hover {\n" +
            "  -webkit-box-shadow: none;\n" +
            "  -moz-box-shadow: none;\n" +
            "  box-shadow: none;\n" +
            "}\n" +
            ".snrs-popup--event .more-info-track a.disable {\n" +
            "  -webkit-box-shadow: none;\n" +
            "  -moz-box-shadow: none;\n" +
            "  box-shadow: none;\n" +
            "  pointer-events: none;\n" +
            "  cursor: default;\n" +
            "  background: #afb3bb;\n" +
            "  border-color: #afb3bb;\n" +
            "  color: #fff;\n" +
            "}\n" +
            ".snrs-popup--event .more-info-track a.disable:hover {\n" +
            "  -webkit-box-shadow: none;\n" +
            "  -moz-box-shadow: none;\n" +
            "  box-shadow: none;\n" +
            "  pointer-events: none;\n" +
            "  cursor: default;\n" +
            "  background: #afb3bb;\n" +
            "  border-color: #afb3bb;\n" +
            "  color: #fff;\n" +
            "}\n" +
            ".snrs-popup--event .more-info-track a.disable:hover:focus,\n" +
            ".snrs-popup--event .more-info-track a.disable:hover:active {\n" +
            "  -webkit-box-shadow: none;\n" +
            "  -moz-box-shadow: none;\n" +
            "  box-shadow: none;\n" +
            "  pointer-events: none;\n" +
            "  cursor: default;\n" +
            "  background: #afb3bb;\n" +
            "  border-color: #afb3bb;\n" +
            "  color: #fff;\n" +
            "}\n" +
            ".snrs-popup--event .more-info-track a.disabled {\n" +
            "  -webkit-box-shadow: none;\n" +
            "  -moz-box-shadow: none;\n" +
            "  box-shadow: none;\n" +
            "  pointer-events: none;\n" +
            "  cursor: default;\n" +
            "  background: #afb3bb;\n" +
            "  border-color: #afb3bb;\n" +
            "  color: #fff;\n" +
            "}\n" +
            ".snrs-popup--event .more-info-track a.disabled:hover {\n" +
            "  -webkit-box-shadow: none;\n" +
            "  -moz-box-shadow: none;\n" +
            "  box-shadow: none;\n" +
            "  pointer-events: none;\n" +
            "  cursor: default;\n" +
            "  background: #afb3bb;\n" +
            "  border-color: #afb3bb;\n" +
            "  color: #fff;\n" +
            "}\n" +
            ".snrs-popup--event .more-info-track a.disabled:hover:focus,\n" +
            ".snrs-popup--event .more-info-track a.disabled:hover:active {\n" +
            "  -webkit-box-shadow: none;\n" +
            "  -moz-box-shadow: none;\n" +
            "  box-shadow: none;\n" +
            "  pointer-events: none;\n" +
            "  cursor: default;\n" +
            "  background: #afb3bb;\n" +
            "  border-color: #afb3bb;\n" +
            "  color: #fff;\n" +
            "}\n" +
            ".snrs-popup--event .more-info-track a:disable {\n" +
            "  -webkit-box-shadow: none;\n" +
            "  -moz-box-shadow: none;\n" +
            "  box-shadow: none;\n" +
            "  pointer-events: none;\n" +
            "  cursor: default;\n" +
            "  background: #afb3bb;\n" +
            "  border-color: #afb3bb;\n" +
            "  color: #fff;\n" +
            "}\n" +
            ".snrs-popup--event .more-info-track a:disable:hover {\n" +
            "  -webkit-box-shadow: none;\n" +
            "  -moz-box-shadow: none;\n" +
            "  box-shadow: none;\n" +
            "  pointer-events: none;\n" +
            "  cursor: default;\n" +
            "  background: #afb3bb;\n" +
            "  border-color: #afb3bb;\n" +
            "  color: #fff;\n" +
            "}\n" +
            ".snrs-popup--event .more-info-track a:disable:hover:focus,\n" +
            ".snrs-popup--event .more-info-track a:disable:hover:active {\n" +
            "  -webkit-box-shadow: none;\n" +
            "  -moz-box-shadow: none;\n" +
            "  box-shadow: none;\n" +
            "  pointer-events: none;\n" +
            "  cursor: default;\n" +
            "  background: #afb3bb;\n" +
            "  border-color: #afb3bb;\n" +
            "  color: #fff;\n" +
            "}\n" +
            ".snrs-popup--event .more-info-track a[disabled=\"disabled\"] {\n" +
            "  -webkit-box-shadow: none;\n" +
            "  -moz-box-shadow: none;\n" +
            "  box-shadow: none;\n" +
            "  pointer-events: none;\n" +
            "  cursor: default;\n" +
            "  background: #afb3bb;\n" +
            "  border-color: #afb3bb;\n" +
            "  color: #fff;\n" +
            "}\n" +
            ".snrs-popup--event .more-info-track a[disabled=\"disabled\"]:hover {\n" +
            "  -webkit-box-shadow: none;\n" +
            "  -moz-box-shadow: none;\n" +
            "  box-shadow: none;\n" +
            "  pointer-events: none;\n" +
            "  cursor: default;\n" +
            "  background: #afb3bb;\n" +
            "  border-color: #afb3bb;\n" +
            "  color: #fff;\n" +
            "}\n" +
            ".snrs-popup--event .more-info-track a[disabled=\"disabled\"]:hover:focus,\n" +
            ".snrs-popup--event .more-info-track a[disabled=\"disabled\"]:hover:active {\n" +
            "  -webkit-box-shadow: none;\n" +
            "  -moz-box-shadow: none;\n" +
            "  box-shadow: none;\n" +
            "  pointer-events: none;\n" +
            "  cursor: default;\n" +
            "  background: #afb3bb;\n" +
            "  border-color: #afb3bb;\n" +
            "  color: #fff;\n" +
            "}\n" +
            ".snrs-popup--event .more-info-track a.medium {\n" +
            "  padding: 5px 14px;\n" +
            "  font-size: 13px;\n" +
            "  line-height: 18px;\n" +
            "}\n" +
            ".snrs-popup--event .more-info-track a.medium.snrs-button--enable-icon {\n" +
            "  padding: 0;\n" +
            "}\n" +
            ".snrs-popup--event .more-info-track a.medium.snrs-button--enable-icon span {\n" +
            "  padding: 5px 14px;\n" +
            "}\n" +
            ".snrs-popup--event .more-info-track a.small {\n" +
            "  padding: 3px 12px;\n" +
            "  font-size: 12px;\n" +
            "  line-height: 18px;\n" +
            "}\n" +
            ".snrs-popup--event .more-info-track a.small.snrs-button--enable-icon {\n" +
            "  padding: 0;\n" +
            "}\n" +
            ".snrs-popup--event .more-info-track a.small.snrs-button--enable-icon span {\n" +
            "  padding: 3px 12px;\n" +
            "}\n" +
            ".snrs-popup--event .more-info-track a.small-x {\n" +
            "  padding: 3px 6px;\n" +
            "  font-size: 12px;\n" +
            "  line-height: 15px;\n" +
            "}\n" +
            ".snrs-popup--event .more-info-track a.small-x.snrs-button--enable-icon {\n" +
            "  padding: 0;\n" +
            "}\n" +
            ".snrs-popup--event .more-info-track a.small-x.snrs-button--enable-icon span {\n" +
            "  padding: 3px 6px;\n" +
            "}\n" +
            ".snrs-popup--event .more-info-track a:hover,\n" +
            ".snrs-popup--event .more-info-track a:focus {\n" +
            "  background: #fff;\n" +
            "  border-color: #afb3bb;\n" +
            "  color: #69757e;\n" +
            "}\n" +
            ".snrs-popup--event .more-info-track a.active,\n" +
            ".snrs-popup--event .more-info-track a.wysihtml5-command-active {\n" +
            "  background: #afb3bb;\n" +
            "  border-color: #afb3bb;\n" +
            "  color: #fff;\n" +
            "}\n" +
            ".snrs-popup--event .more-info-track a:focus:hover,\n" +
            ".snrs-popup--event .more-info-track a:active:hover,\n" +
            ".snrs-popup--event .more-info-track a.wysihtml5-command-active:hover {\n" +
            "  background: #fff;\n" +
            "  border-color: #afb3bb;\n" +
            "}\n" +
            ".snrs-popup--event .more-info-track a.active:hover,\n" +
            ".snrs-popup--event .more-info-track a.wysihtml5-command-active:hover {\n" +
            "  background: #afb3bb;\n" +
            "  border-color: #afb3bb;\n" +
            "  color: #fff;\n" +
            "}\n" +
            ".snrs-popup--event .more-info-track a.disable {\n" +
            "  border-color: #d7d9dd;\n" +
            "  color: #afb3bb;\n" +
            "  background: #fff;\n" +
            "  -webkit-box-shadow: none;\n" +
            "  -moz-box-shadow: none;\n" +
            "  box-shadow: none;\n" +
            "}\n" +
            ".snrs-popup--event .more-info-track a.disable:hover {\n" +
            "  border-color: #d7d9dd;\n" +
            "  color: #afb3bb;\n" +
            "  background: #fff;\n" +
            "  -webkit-box-shadow: none;\n" +
            "  -moz-box-shadow: none;\n" +
            "  box-shadow: none;\n" +
            "}\n" +
            ".snrs-popup--event .more-info-track a.disable:hover:focus,\n" +
            ".snrs-popup--event .more-info-track a.disable:hover:active {\n" +
            "  border-color: #d7d9dd;\n" +
            "  color: #afb3bb;\n" +
            "  background: #fff;\n" +
            "  -webkit-box-shadow: none;\n" +
            "  -moz-box-shadow: none;\n" +
            "  box-shadow: none;\n" +
            "}\n" +
            ".snrs-popup--event .more-info-track a.disabled {\n" +
            "  border-color: #d7d9dd;\n" +
            "  color: #afb3bb;\n" +
            "  background: #fff;\n" +
            "  -webkit-box-shadow: none;\n" +
            "  -moz-box-shadow: none;\n" +
            "  box-shadow: none;\n" +
            "}\n" +
            ".snrs-popup--event .more-info-track a.disabled:hover {\n" +
            "  border-color: #d7d9dd;\n" +
            "  color: #afb3bb;\n" +
            "  background: #fff;\n" +
            "  -webkit-box-shadow: none;\n" +
            "  -moz-box-shadow: none;\n" +
            "  box-shadow: none;\n" +
            "}\n" +
            ".snrs-popup--event .more-info-track a.disabled:hover:focus,\n" +
            ".snrs-popup--event .more-info-track a.disabled:hover:active {\n" +
            "  border-color: #d7d9dd;\n" +
            "  color: #afb3bb;\n" +
            "  background: #fff;\n" +
            "  -webkit-box-shadow: none;\n" +
            "  -moz-box-shadow: none;\n" +
            "  box-shadow: none;\n" +
            "}\n" +
            ".snrs-popup--event .more-info-track a:disable {\n" +
            "  border-color: #d7d9dd;\n" +
            "  color: #afb3bb;\n" +
            "  background: #fff;\n" +
            "  -webkit-box-shadow: none;\n" +
            "  -moz-box-shadow: none;\n" +
            "  box-shadow: none;\n" +
            "}\n" +
            ".snrs-popup--event .more-info-track a:disable:hover {\n" +
            "  border-color: #d7d9dd;\n" +
            "  color: #afb3bb;\n" +
            "  background: #fff;\n" +
            "  -webkit-box-shadow: none;\n" +
            "  -moz-box-shadow: none;\n" +
            "  box-shadow: none;\n" +
            "}\n" +
            ".snrs-popup--event .more-info-track a:disable:hover:focus,\n" +
            ".snrs-popup--event .more-info-track a:disable:hover:active {\n" +
            "  border-color: #d7d9dd;\n" +
            "  color: #afb3bb;\n" +
            "  background: #fff;\n" +
            "  -webkit-box-shadow: none;\n" +
            "  -moz-box-shadow: none;\n" +
            "  box-shadow: none;\n" +
            "}\n" +
            ".snrs-popup--event .more-info-track a[disabled=\"disabled\"] {\n" +
            "  border-color: #d7d9dd;\n" +
            "  color: #afb3bb;\n" +
            "  background: #fff;\n" +
            "  -webkit-box-shadow: none;\n" +
            "  -moz-box-shadow: none;\n" +
            "  box-shadow: none;\n" +
            "}\n" +
            ".snrs-popup--event .more-info-track a[disabled=\"disabled\"]:hover {\n" +
            "  border-color: #d7d9dd;\n" +
            "  color: #afb3bb;\n" +
            "  background: #fff;\n" +
            "  -webkit-box-shadow: none;\n" +
            "  -moz-box-shadow: none;\n" +
            "  box-shadow: none;\n" +
            "}\n" +
            ".snrs-popup--event .more-info-track a[disabled=\"disabled\"]:hover:focus,\n" +
            ".snrs-popup--event .more-info-track a[disabled=\"disabled\"]:hover:active {\n" +
            "  border-color: #d7d9dd;\n" +
            "  color: #afb3bb;\n" +
            "  background: #fff;\n" +
            "  -webkit-box-shadow: none;\n" +
            "  -moz-box-shadow: none;\n" +
            "  box-shadow: none;\n" +
            "}\n" +
            ".in .snrs-popup--dialog {\n" +
            "  opacity: 1;\n" +
            "  -webkit-transform: translate(0, 0);\n" +
            "  -moz-transform: translate(0, 0);\n" +
            "  -o-transform: translate(0, 0);\n" +
            "  -ms-transform: translate(0, 0);\n" +
            "  transform: translate(0, 0);\n" +
            "}\n" +
            "\n" +
            ".snrs-popup .snrs-tab--wrapper ul {\n" +
            "  border-bottom: 1px solid #ececed;\n" +
            "  padding: 0 10px 0 10px;\n" +
            "}\n";


}

package com.synerise.synerise.app.model;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.Date;

public class AnalyticsQuery implements Parcelable {
    private int id;
    private int business_profile_id;
    private int user_id;
    private int query_type;
    private int reload_iteration;
    private String author_name;
    private String created;
    private String query_category;
    private String query_dashboard;
    private String query_data;
    private String query_description;
    private String query_html;
    private String query_name;
    private String query_sql;
    private String query_sql_by_client;
    private String query_sql_list_client;
    private String status;
    private Date reload_time;
    private Date updated;

    public static String [] keysToSave = new String[]{"id", "query_type", "author_name", "query_data", "query_description", "query_name", "query_sql", "status"};


    public AnalyticsQuery(JSONObject jsonObject) {
        try {
            id = jsonObject.getInt("id");
//            business_profile_id = jsonObject.getInt("business_profile_id");
//            user_id = jsonObject.getInt("user_id");
            query_type = jsonObject.getInt("query_type");
//            reload_iteration = jsonObject.getInt("reload_iteration");
            author_name = jsonObject.getString("author_name");
//            created = jsonObject.getString("created");
//            query_category = jsonObject.getString("query_category");
//            query_dashboard = jsonObject.getString("query_dashboard");
            query_data = jsonObject.getString("query_data");
            query_description = jsonObject.getString("query_description");
//            query_html = jsonObject.getString("query_html");
            query_name = jsonObject.getString("query_name");
            query_sql = jsonObject.getString("query_sql");
//            query_sql_by_client = jsonObject.getString("query_sql_by_client");
//            query_sql_list_client = jsonObject.getString("query_sql_list_client");
            status = jsonObject.getString("status");

////            ISO8601 iso8601 = new ISO8601();
//            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.000Z");
//            reload_time = format.parse(jsonObject.getString("reload_time"));
//            updated =  format.parse(jsonObject.getString("updated"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    protected AnalyticsQuery(Parcel in) {
        id = in.readInt();
        business_profile_id = in.readInt();
        user_id = in.readInt();
        query_type = in.readInt();
        reload_iteration = in.readInt();
        author_name = in.readString();
        created = in.readString();
        query_category = in.readString();
        query_dashboard = in.readString();
        query_data = in.readString();
        query_description = in.readString();
        query_html = in.readString();
        query_name = in.readString();
        query_sql = in.readString();
        query_sql_by_client = in.readString();
        query_sql_list_client = in.readString();
        status = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeInt(business_profile_id);
        dest.writeInt(user_id);
        dest.writeInt(query_type);
        dest.writeInt(reload_iteration);
        dest.writeString(author_name);
        dest.writeString(created);
        dest.writeString(query_category);
        dest.writeString(query_dashboard);
        dest.writeString(query_data);
        dest.writeString(query_description);
        dest.writeString(query_html);
        dest.writeString(query_name);
        dest.writeString(query_sql);
        dest.writeString(query_sql_by_client);
        dest.writeString(query_sql_list_client);
        dest.writeString(status);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<AnalyticsQuery> CREATOR = new Creator<AnalyticsQuery>() {
        @Override
        public AnalyticsQuery createFromParcel(Parcel in) {
            return new AnalyticsQuery(in);
        }

        @Override
        public AnalyticsQuery[] newArray(int size) {
            return new AnalyticsQuery[size];
        }
    };

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getBusiness_profile_id() {
        return business_profile_id;
    }

    public void setBusiness_profile_id(int business_profile_id) {
        this.business_profile_id = business_profile_id;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public int getQuery_type() {
        return query_type;
    }

    public void setQuery_type(int query_type) {
        this.query_type = query_type;
    }

    public int getReload_iteration() {
        return reload_iteration;
    }

    public void setReload_iteration(int reload_iteration) {
        this.reload_iteration = reload_iteration;
    }

    public String getAuthor_name() {
        return author_name;
    }

    public void setAuthor_name(String author_name) {
        this.author_name = author_name;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getQuery_category() {
        return query_category;
    }

    public void setQuery_category(String query_category) {
        this.query_category = query_category;
    }

    public String getQuery_dashboard() {
        return query_dashboard;
    }

    public void setQuery_dashboard(String query_dashboard) {
        this.query_dashboard = query_dashboard;
    }

    public String getQuery_data() {
        return query_data;
    }

    public void setQuery_data(String query_data) {
        this.query_data = query_data;
    }

    public String getQuery_description() {
        return query_description;
    }

    public void setQuery_description(String query_description) {
        this.query_description = query_description;
    }

    public String getQuery_html() {
        return query_html;
    }

    public void setQuery_html(String query_html) {
        this.query_html = query_html;
    }

    public String getQuery_name() {
        return query_name;
    }

    public void setQuery_name(String query_name) {
        this.query_name = query_name;
    }

    public String getQuery_sql() {
        return query_sql;
    }

    public void setQuery_sql(String query_sql) {
        this.query_sql = query_sql;
    }

    public String getQuery_sql_by_client() {
        return query_sql_by_client;
    }

    public void setQuery_sql_by_client(String query_sql_by_client) {
        this.query_sql_by_client = query_sql_by_client;
    }

    public String getQuery_sql_list_client() {
        return query_sql_list_client;
    }

    public void setQuery_sql_list_client(String query_sql_list_client) {
        this.query_sql_list_client = query_sql_list_client;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getReload_time() {
        return reload_time;
    }

    public void setReload_time(Date reload_time) {
        this.reload_time = reload_time;
    }

    public Date getUpdated() {
        return updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }

    public static boolean isSupportedQuery(int type){
        return Arrays.asList(0,1,2,3,4,5).contains(type);
    }
}

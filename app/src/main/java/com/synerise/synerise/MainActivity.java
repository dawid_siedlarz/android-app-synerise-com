package com.synerise.synerise;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.accounts.AccountManagerCallback;
import android.accounts.AccountManagerFuture;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.multidex.MultiDex;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.synerise.sdk.tracking.Tracker;
import com.synerise.sdk.tracking.TrackerParams;
import com.synerise.synerise.app.ActionBarConfigurableFragment;
import com.synerise.synerise.app.fragment.BusinessProfileFragment;
import com.synerise.synerise.app.fragment.CRMFragment;
import com.synerise.synerise.app.fragment.CommerceFragment;
import com.synerise.synerise.app.fragment.InsightsFragment;
import com.synerise.synerise.app.fragment.ListGrowthFragment;
import com.synerise.synerise.app.fragment.LiveStreamFragment;
import com.synerise.synerise.app.fragment.LoginFragment;
import com.synerise.synerise.app.fragment.analytics.AnalyticsFragment;
import com.synerise.synerise.app.fragment.campaign.listing.CampaignChooserFragment;
import com.synerise.synerise.app.syneutil.SyneBitmapCacheUtil;
import com.synerise.synerise.app.syneutil.SyneHttpClient;
import com.synerise.synerise.app.syneutil.SyneStringCacheUtil;
import com.synerise.synerise.messenger.fragment.MessengerFragment;
import com.vincentbrison.openlibraries.android.dualcache.Builder;
import com.vincentbrison.openlibraries.android.dualcache.DualCache;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, SyneriseOnFragmentInteractionListener {


    public String csrf = "";
    public static String mail = "";
    public static SyneHttpClient httpClient = null;
    public static SearchView searchView;
    public static int businessProfileId;
    public static String businessProfileName;
    public static MenuItem searchItem;
    private static FragmentManager fragmentManager;

    public static DualCache<String> cacheString;
    public static DualCache<Bitmap> cacheBitmap;

    private static final String CACHE_STRING_NAME = "CACHE_STRING_NAME";
    private static final String CACHE_BITMAP_NAME = "CACHE_BITMAP_NAME";

    private static DrawerLayout drawer;
    private static ActionBarDrawerToggle toggle;

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        final int memClass = ((ActivityManager) getSystemService(Context.ACTIVITY_SERVICE)).getMemoryClass();
        // Use 1/8th of the available memory for this memory cache.
        final int cacheSize = 1024 * 1024 * memClass / 8;

        cacheString = new Builder<String>(CACHE_STRING_NAME, 1)
                .enableLog()
                .useReferenceInRam(cacheSize, new SyneStringCacheUtil())
                .useSerializerInDisk(cacheSize, true, new SyneStringCacheUtil(), getApplicationContext())
                .build();

//        cacheString.invalidate();

        cacheBitmap = new Builder<Bitmap>(CACHE_BITMAP_NAME, 1)
                .enableLog()
                .useReferenceInRam(cacheSize, new SyneBitmapCacheUtil())
                .useSerializerInDisk(cacheSize, true, new SyneBitmapCacheUtil(), getApplicationContext())
                .build();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().hide();
        fragmentManager = getSupportFragmentManager();

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();
        drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        toggle.onDrawerStateChanged(DrawerLayout.STATE_IDLE);
        toggle.setDrawerIndicatorEnabled(false);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        httpClient = new SyneHttpClient();
        httpClient.setAppContext(this);


        Tracker.initialize(getApplicationContext());
        new PrepareLogin().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            int count = getSupportFragmentManager().getBackStackEntryCount();
            if (count == 0) {
                super.onBackPressed();
            } else {
                getSupportFragmentManager().popBackStack();
                getSupportFragmentManager().executePendingTransactions();

                Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.mainContainer);
                if (fragment instanceof ActionBarConfigurableFragment)
                    try {

                        ((ActionBarConfigurableFragment) fragment).setUpActionBar();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
            }
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);

        searchItem = menu.findItem(R.id.action_search);
        searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
        SearchManager searchManager =
                (SearchManager) getSystemService(Context.SEARCH_SERVICE);

        searchView.setSearchableInfo(
                searchManager.getSearchableInfo(getComponentName()));
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                CRMFragment.listing_SimpleFilter_simpleFilterValue = java.net.URLEncoder.encode(query);
                Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.mainContainer);
                if (fragment instanceof CRMFragment) {
                    ((CRMFragment) fragment).clearClientsArray();
                    CRMFragment.adapter.notifyDataSetChanged();
                    ((CRMFragment) fragment).getClients();
                }

                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });


        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {


        return super.onPrepareOptionsMenu(menu);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP_MR1)
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_logout) {
            AccountManager accountManager = AccountManager.get(getApplicationContext());
            if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.GET_ACCOUNTS) == PackageManager.PERMISSION_GRANTED) {

                Account[] accounts = accountManager.getAccountsByType(getResources().getString(R.string.account_type));
                for (int i = 0; i < accounts.length; i++) {
//                    accountManager.removeAccountExplicitly(accounts[i]);
                    accountManager.removeAccount(accounts[i], new AccountManagerCallback<Boolean>() {
                        @Override
                        public void run(AccountManagerFuture<Boolean> future) {
                            try {
                                if (future.getResult()) {
                                    // do something
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                        }
                    }, null);


                }
            }

            Fragment newfragment = new LoginFragment();
            addFragment(newfragment);

            setDrawerState(false);


            new Logout().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            return true;
        }
        if (id == R.id.action_choose_business_profile) {
            Fragment newfragment = new BusinessProfileFragment();
            addFragment(newfragment);
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        Fragment f2 = null;

        searchItem.setVisible(false);

        if (id == R.id.menuLiveStream) {
            f2 = new LiveStreamFragment();
        } else if (id == R.id.menuListGrowth) {
            f2 = new ListGrowthFragment();
        } else if (id == R.id.menuCommerce) {
            f2 = new CommerceFragment();

        } else if (id == R.id.menuInsights) {
            f2 = new InsightsFragment();

        } else if (id == R.id.menuCampaigns) {
            f2 = new CampaignChooserFragment();
        } else if (id == R.id.menuCRM) {
            searchItem.setVisible(true);
            f2 = new CRMFragment();
        } else if (id == R.id.menuMessenger) {
            f2 = new MessengerFragment();
        } else if (id == R.id.menuAnalytics) {
            f2 = new AnalyticsFragment();
        }
        //        } else if (id == R.id.menuAutomations) {
//            f2 = new AutomationsFragment();
//        } else if (id == R.id.menuEngagment) {
//            f2 = new EngagementFragment();
//        } else if (id == R.id.menuConversion) {
//            f2 = new ConversionFragment();
//        }  else if (id == R.idmenuEmailCampaigns) {
//            f2 = new MobilePushCampaignsFragment();
//        }


        if (f2 != null) {
            addFragment(f2);
        }


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    public static void addFragment(Fragment fragment) {
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right, R.anim.slide_in_left, R.anim.slide_out_right);
        fragmentTransaction.add(R.id.mainContainer, fragment);
        fragmentTransaction.addToBackStack("SyneriseFragment");
        fragmentTransaction.commit();

    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    public void loginAction(View view) {
        String email = ((TextView) findViewById(R.id.email)).getText().toString();
        String password = ((TextView) findViewById(R.id.password)).getText().toString();


        new LoginCheck().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, email, password);
    }

    public void forgottenPasswordAction(View view) {
        Intent intent = new Intent(this, ForgottenPasswordActivity.class);
        startActivity(intent);
    }


    public void prepareLogin() {

        new PrepareLogin().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    private class PrepareLogin extends AsyncTask<Void, Void, String> {
        protected String doInBackground(Void... params) {
            try {
                HttpGet httpget = new HttpGet("https://app.synerise.com/login");

                HttpResponse response = httpClient.execute(httpget);
                if (response == null) {
                    Activity activity = new Activity();
                    if (activity != null) {
                        activity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                new PrepareLogin().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                            }
                        });
                    }
                    return "not OK";
                }


                HttpEntity entity = response.getEntity();

                String body = EntityUtils.toString(entity);

                Document doc = Jsoup.parse(body);
                csrf = doc.select("[name='_csrf_token']").attr("value");


                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        final AccountManager am = AccountManager.get(getApplicationContext());
                        if (ActivityCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.GET_ACCOUNTS) == PackageManager.PERMISSION_GRANTED) {
                            Account[] accounts = am.getAccountsByType(getResources().getString(R.string.account_type));
                            if (accounts.length > 0) {
                                new LoginCheck().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, accounts[0].name, am.getPassword(accounts[0]));
                                return;
                            }

                        } else {
                        }

                        Fragment fragment = new LoginFragment();

                        FragmentManager fragmentManager = getSupportFragmentManager();
                        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                        fragmentTransaction.setCustomAnimations(R.anim.fade_out, R.anim.fade_out);
                        fragmentTransaction.replace(R.id.mainContainer, fragment);
                        fragmentTransaction.commit();

                    }
                });


            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (NullPointerException e) {

            } catch (IllegalStateException e) {

            }
            return "ok";
        }

        protected void onProgressUpdate(Integer... progress) {

        }

        protected void onPostExecute(Long result) {
        }
    }


    private class Logout extends AsyncTask<Void, Void, String> {
        protected String doInBackground(Void... params) {
            try {
                HttpGet httpget = new HttpGet("https://app.synerise.com/logout");

                HttpResponse response = httpClient.execute(httpget);
                if (response == null) {
                    return "not OK";
                }


                HttpEntity entity = response.getEntity();

            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (NullPointerException e) {

            } catch (IllegalStateException e) {

            }
            return "ok";
        }

        protected void onProgressUpdate(Integer... progress) {

        }

        protected void onPostExecute(Long result) {
        }
    }

    private class LoginCheck extends AsyncTask<String, Void, String> {
        protected String doInBackground(final String... params) {
            try {

                mail = params[0];
                HttpPost httpost = new HttpPost("https://app.synerise.com/login_check");

                List<NameValuePair> nvps = new ArrayList<NameValuePair>();
                nvps.add(new BasicNameValuePair("_username", params[0]));
                nvps.add(new BasicNameValuePair("_password", params[1]));
                nvps.add(new BasicNameValuePair("_csrf_token", csrf));

                httpost.setEntity(new UrlEncodedFormEntity(nvps, HTTP.UTF_8));
                HttpParams param = new BasicHttpParams();
                param.setParameter("http.protocol.handle-redirects", false);

                httpost.setParams(param);

                HttpResponse response2 = httpClient.executeWithoutProcessing(httpost);
                if (response2 == null) {
                    return "not OK";
                }


                Header locationHeader = response2.getFirstHeader("location");
                if (locationHeader != null) {

                    String redirectLocation = locationHeader.getValue();
                    if (redirectLocation.contains("login")) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                RelativeLayout error = (RelativeLayout) findViewById(R.id.loginFormError);
                                error.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
                                return;
                            }
                        });

                    } else {

                        Tracker.client(new TrackerParams[]{new TrackerParams("email", params[0])});

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                final AccountManager am = AccountManager.get(getApplicationContext());

                                Account account = new Account(params[0], getResources().getString(R.string.account_type));
                                Bundle userdata = new Bundle();
                                am.addAccountExplicitly(account, params[1], null);

                                String bpId = am.getUserData(account, "bpId");
                                String bpBmpUrl = am.getUserData(account, "bpBmpUrl");
                                String name = am.getUserData(account, "bpName");
                                Log.v("BP_DEBUG", "load from memory " + name);


                                if (bpId != null) {
                                    MainActivity.businessProfileId = Integer.valueOf(bpId);
                                    MainActivity.businessProfileName = name;

                                    Log.v("businessProfileName", " " + name);

                                    chooseProfile(Integer.valueOf(bpId), bpBmpUrl, name);
                                } else {
                                    Fragment fragment = new BusinessProfileFragment();

                                    FragmentManager fragmentManager = getSupportFragmentManager();
                                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                                    fragmentTransaction.setCustomAnimations(R.anim.fade_out, R.anim.fade_out);
                                    fragmentTransaction.replace(R.id.mainContainer, fragment);
                                    fragmentTransaction.commit();
                                }
                            }
                        });
                    }
                }


            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (NullPointerException e) {

            } catch (IllegalStateException e) {

            }
            return "ok";
        }
    }

    public static void setListViewHeightBasedOnChildren(ListView listView) {

        ListAdapter mAdapter = listView.getAdapter();


        int totalHeight = 0;

        for (int i = 0; i < mAdapter.getCount(); i++) {
            View mView = mAdapter.getView(i, null, listView);

            mView.measure(
                    View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED),

                    View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));

            totalHeight += mView.getMeasuredHeight() + mView.getPaddingTop() + mView.getPaddingBottom();

        }

        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight
                + (listView.getDividerHeight() * (mAdapter.getCount() - 1));
        listView.setLayoutParams(params);
        listView.requestLayout();
    }

    public static void setDrawerState(boolean isEnabled) {
        if (isEnabled) {
            drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
            toggle.onDrawerStateChanged(DrawerLayout.STATE_IDLE);
            toggle.setDrawerIndicatorEnabled(true);
            toggle.syncState();

        } else {
            drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
            toggle.onDrawerStateChanged(DrawerLayout.STATE_DRAGGING);
            toggle.setDrawerIndicatorEnabled(false);
            toggle.syncState();
        }
    }

    public void tryReconnectAction(View v) {
        new PrepareLogin().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    public void chooseProfile(Integer bpId, String bpBmpUrl, String name) {


        final AccountManager am = AccountManager.get(getApplicationContext());
        if (ActivityCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.GET_ACCOUNTS) == PackageManager.PERMISSION_GRANTED) {
            Account[] accounts = am.getAccountsByType(getResources().getString(R.string.account_type));
            if (accounts.length > 0) {
                am.setUserData(accounts[0], "bpId", String.valueOf(bpId));
                am.setUserData(accounts[0], "bpBmpUrl", bpBmpUrl);
                Log.v("BP_DEBUG", "save to memory " + name);
                am.setUserData(accounts[0], "bpName", name);
            }

        } else {
        }

        MainActivity.businessProfileId = bpId;
        MainActivity.businessProfileName = name;

        new ChooseProfile().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, bpId);
        new LoadImageToNavbar().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, bpBmpUrl);
    }

    private class ChooseProfile extends AsyncTask<Integer, Void, String> {
        protected String doInBackground(final Integer... params) {
            try {
                HttpGet httpget3 = new HttpGet("https://app.synerise.com/panel/profile/set/" + params[0]);

                HttpResponse response3 = MainActivity.httpClient.execute(httpget3);
                if (response3 == null) {
                    Activity activity = new Activity();
                    if (activity != null) {
                        activity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                new ChooseProfile().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, params[0]);
                            }
                        });
                    }
                    return "not OK";
                }

                MainActivity.businessProfileId = params[0];


                Fragment newFragment = new LiveStreamFragment();

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        MainActivity.setDrawerState(true);
                    }
                });

                ImageView splash = findViewById(R.id.synerise_android_splash);


                if (splash != null) {
                    FragmentManager fragmentManager = getSupportFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    Log.v("MainActivity", "setAnimation choose profile");
                    fragmentTransaction.replace(R.id.mainContainer, newFragment);
                    Animation animation = new AlphaAnimation(1, 0);
                    animation.setDuration(1000);
                    fragmentTransaction.setCustomAnimations(R.animator.fade_out_animator, R.animator.fade_out_animator);
                    fragmentTransaction.commit();
                } else {
                    addFragment(newFragment);
                }

            } catch (NullPointerException | IllegalStateException e) {

            } catch (IOException e) {
                e.printStackTrace();
            }
            return "ok";
        }
    }

    private class LoadImageToNavbar extends AsyncTask<String, Void, Void> {

        @Override
        protected Void doInBackground(String... params) {

            Bitmap bmp = null;


            try {
                bmp = BitmapFactory.decodeStream(new URL(params[0]).openConnection().getInputStream());

// bmp = Bitmap.createScaledBitmap(bmp, 150, 150, false);
            } catch (MalformedURLException e) {
                Log.v("SYNERISE", "Malformed URL " + params[0]);
            } catch (IOException e) {
                e.printStackTrace();
            }

            final Bitmap finalBmp = bmp;

            Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.mainContainer);
            if (fragment == null) {
                return null;
            }
            Activity activity = fragment.getActivity();
            if (activity != null) {

                fragment.getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        ((ImageView) findViewById(R.id.navBarLogo)).setImageBitmap(finalBmp);
                    }
                });

            }
            return null;
        }

    }

    public void createAccountAction(View v) {
        Toast.makeText(this, "This option will be available soon", Toast.LENGTH_SHORT).show();
    }
}

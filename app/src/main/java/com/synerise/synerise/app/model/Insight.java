package com.synerise.synerise.app.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.Html;
import android.text.Spanned;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class Insight implements Parcelable {
    public String id;
    public Spanned title;
    public Spanned desc;
    public String sql_scheme;
    public String timedivider;
    public String summarynumber;
    public String insightcategory;
    public String insightdate;


    public Insight(JSONObject jsonObject) {
        try {
            this.id = jsonObject.getString("id");
            this.title = Html.fromHtml(jsonObject.getString("title"));
            this.desc = Html.fromHtml(jsonObject.getString("desc"));
            this.sql_scheme = jsonObject.getString("sql_scheme");
            this.timedivider = jsonObject.getString("timedivider");
            this.summarynumber = jsonObject.getString("summarynumber");
            this.insightcategory = jsonObject.getString("insightcategory");
            this.insightdate = jsonObject.getString("insightdate");

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    protected Insight(Parcel in) {
        id = in.readString();
        sql_scheme = in.readString();
        timedivider = in.readString();
        summarynumber = in.readString();
        insightcategory = in.readString();
        insightdate = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(sql_scheme);
        dest.writeString(timedivider);
        dest.writeString(summarynumber);
        dest.writeString(insightcategory);
        dest.writeString(insightdate);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Insight> CREATOR = new Creator<Insight>() {
        @Override
        public Insight createFromParcel(Parcel in) {
            return new Insight(in);
        }

        @Override
        public Insight[] newArray(int size) {
            return new Insight[size];
        }
    };
}


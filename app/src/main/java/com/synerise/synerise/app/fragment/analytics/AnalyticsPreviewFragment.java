package com.synerise.synerise.app.fragment.analytics;

import android.content.Context;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;
import com.synerise.synerise.MainActivity;
import com.synerise.synerise.R;
import com.synerise.synerise.app.ActionBarConfigurableFragment;
import com.synerise.synerise.app.model.AnalyticsQuery;
import com.synerise.synerise.app.syneutil.BarChartMarkerView;
import com.synerise.synerise.app.syneview.SyneComplexBarChart;

import org.apache.http.cookie.Cookie;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.net.ssl.HttpsURLConnection;


public class AnalyticsPreviewFragment extends ActionBarConfigurableFragment {
    public static String ARG_QUERY = "query";

    private SyneComplexBarChart barChart;
    private TextView analyticsChartTitle;
    private TextView analyticsChartDescription;

    private AnalyticsQuery analyticsQuery;
    int[] colors;

    @Override
    public void setUpActionBar() {
        ActionBar actionBar = (ActionBar) ((AppCompatActivity) getActivity()).getSupportActionBar();
        if (actionBar != null) {
            actionBar.show();
            actionBar.setTitle(MainActivity.businessProfileName != null ? MainActivity.businessProfileName : "Synerise");
            actionBar.setSubtitle("Analytics");
            actionBar.setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.colorPrimaryAnalytics)));
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                Window window = getActivity().getWindow();
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                window.setStatusBarColor(getResources().getColor(R.color.colorPrimaryDarkAnalytics));
            }
            actionBar.setElevation(0);
        }
    }

    public AnalyticsPreviewFragment() {
        // Required empty public constructor
    }

    public static AnalyticsFragment newInstance() {
        AnalyticsFragment fragment = new AnalyticsFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            analyticsQuery = (AnalyticsQuery) getArguments().getParcelable(ARG_QUERY);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_analytics_preview, container, false);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        View rootView = getView();
        if (rootView == null) {
            return;
        }
        colors = new int[]{
                getResources().getColor(R.color.fancyBlueAlpha),
                getResources().getColor(R.color.fancyOrangeAlpha),
                getResources().getColor(R.color.fancyGrayAlpha),
                getResources().getColor(R.color.fancyPinkAlpha),
                getResources().getColor(R.color.fancyDarkBlueAlpha),
                getResources().getColor(R.color.fancyGreenAlpha),
                getResources().getColor(R.color.fancyNavyBlueAlpha),
        };

        barChart = rootView.findViewById(R.id.analyticsChart);
        analyticsChartTitle = rootView.findViewById(R.id.analyticsChartTitle);
        analyticsChartDescription = rootView.findViewById(R.id.analyticsChartDescription);
        barChart.getTitle().hide();
        analyticsChartTitle.setText(analyticsQuery.getQuery_name());
        analyticsChartDescription.setText(analyticsQuery.getQuery_description());

        switch (analyticsQuery.getQuery_type()) {
            case 0://segmentatnion
            case 1://funnels
            case 3://sqlquery
                new GetQueryData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                break;
            case 4://metrics
                new GetMetric().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                break;
            case 2://trends
                new GetTrends().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                break;

        }
    }

    private String getCookieString() {
        List<Cookie> cookies = MainActivity.httpClient.httpContext.getCookieStore().getCookies();

        String cookieText = "";
        for (Cookie c :
                cookies) {
            cookieText += c.getName() + "=" + c.getValue() + "; ";
        }
        return cookieText;
    }

    private String postData(String urlAddress, JSONObject requestData) {
        String responseString = "[]";
        try {

            URL url = new URL(urlAddress);
            HttpsURLConnection connection = (HttpsURLConnection) url.openConnection();

            connection.setRequestMethod("POST");
            connection.setRequestProperty("User-Agent", "Synerise android app");
            connection.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
            connection.setRequestProperty("Content-type", "application/json");
            connection.setRequestProperty("Cookie", getCookieString());

            connection.setDoOutput(true);
            DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
            wr.writeBytes(requestData.toString());
            wr.flush();
            wr.close();

            int responseCode = connection.getResponseCode();
            Log.v("URL", "\nSending 'POST' request to URL : " + url);
            Log.v("URL", "Post parameters : " + requestData.toString());
            Log.v("URL", "Response Code : " + responseCode);

            BufferedReader in = new BufferedReader(
                    new InputStreamReader(connection.getInputStream()));
            String inputLine;
            StringBuilder response = new StringBuilder();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();


            Log.v("URL", response.toString());
            responseString = response.toString();
        } catch (ProtocolException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return responseString;
    }

    private class GetQueryData extends AsyncTask<Void, Void, String> {
        protected String doInBackground(Void... params) {
            try {


                JSONObject requestData = new JSONObject();
                requestData.put("sql", analyticsQuery.getQuery_sql());

                String body = postData("https://api.synerise.com/rtom/synerise-sqls/query", requestData);

                if (body == null) {
                    return "not OK";
                }
                JSONObject jsonObject = null;

                try {
                    jsonObject = new JSONObject(body);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                final JSONArray jsonArray = jsonObject.getJSONArray("items");

                final List<String> labels = new ArrayList<String>();
                ArrayList<BarEntry> values = new ArrayList<BarEntry>();

                int minY = Integer.MAX_VALUE;
                int size = 0;

                try {

                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject object = jsonArray.getJSONObject(i);
                        String key = object.keys().next();
                        values.add(new BarEntry(i, (float) object.getInt(key)));
                        labels.add(key);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                final String[] labelsa = new String[]{"model.title.toString()"};

                BarDataSet set = new BarDataSet(values, analyticsQuery.getQuery_name());
                set.setStackLabels(labelsa);

                set.setColors(colors);
                final BarData data = new BarData(set);

                data.setDrawValues(false);
                data.setBarWidth(0.6f);
                final Typeface tf = Typeface.createFromAsset(getActivity().getAssets(),
                        "fonts/Lato-Thin.ttf");
                final Typeface btf = Typeface.createFromAsset(getActivity().getAssets(),
                        "fonts/Lato-Bold.ttf");


                final int finalMinY = minY;
                final int finalSize = size;
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        barChart.update(data, 0d, labels, labelsa, false, false);
                        barChart.getChart().getXAxis().setAxisMinimum(-3f);
                        barChart.getChart().getXAxis().setAxisMaximum(labels.size() + 1);

                        if (jsonArray.length() == 1) {
                            barChart.getChart().getBarData().setBarWidth(3);
                        }
                        barChart.getTitle().clearCountAndUnit();
                    }
                });
//
            } catch (NullPointerException | IllegalStateException e) {

            } catch (JSONException e) {
                e.printStackTrace();
            }
            return "ok";
        }
    }


    private class GetMetric extends AsyncTask<Void, Void, String> {
        protected String doInBackground(Void... params) {
            try {
                JSONObject requestData = new JSONObject();
                requestData.put("metricJSON", analyticsQuery.getQuery_data());
                requestData.put("aggregateScale", "DAY");
                Date currentDate = new Date();
                Calendar c = Calendar.getInstance();
                c.setTime(currentDate);
                c.add(Calendar.DATE, 1);
                c.set(Calendar.HOUR, 0);
                c.set(Calendar.MINUTE, 0);
                c.set(Calendar.SECOND, 0);

                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd  HH:mm:ss");

                requestData.put("rangeDateTo", format.format(c.getTime()));
                c.add(Calendar.DATE, -7);
                requestData.put("rangeDateFrom", format.format(c.getTime()));

                String body = postData("https://api.synerise.com/rtom/analytics-query/execute-metric-query", requestData);

                if (body == null) {
                    return "not OK";
                }
                JSONObject jsonObject = null;

                try {
                    jsonObject = new JSONObject(body);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                final JSONArray jsonArray = jsonObject.getJSONArray("resultForChart");

                final List<String> labels = new ArrayList<String>();
                ArrayList<BarEntry> values = new ArrayList<BarEntry>();

                int minY = Integer.MAX_VALUE;
                int size = 0;

                try {

                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject object = jsonArray.getJSONObject(i);
                        String key = object.keys().next();
                        values.add(new BarEntry(i, (float) object.getDouble("value")));
                        labels.add(object.getString("date"));
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                final String[] labelsa = new String[]{"model.title.toString()"};

                BarDataSet set = new BarDataSet(values, analyticsQuery.getQuery_name());
                set.setStackLabels(labelsa);

                set.setColors(colors);
                final BarData data = new BarData(set);

                data.setDrawValues(false);
                data.setBarWidth(0.6f);
                final Typeface tf = Typeface.createFromAsset(getActivity().getAssets(),
                        "fonts/Lato-Thin.ttf");
                final Typeface btf = Typeface.createFromAsset(getActivity().getAssets(),
                        "fonts/Lato-Bold.ttf");


                final int finalMinY = minY;
                final int finalSize = size;
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        barChart.update(data, 0d, labels, labelsa, false, false);
                        barChart.getChart().getXAxis().setAxisMinimum(-3f);
                        barChart.getChart().getXAxis().setAxisMaximum(labels.size() + 1);

                        if (jsonArray.length() == 1) {
                            barChart.getChart().getBarData().setBarWidth(3);
                        }
                        barChart.getTitle().clearCountAndUnit();
                    }
                });
//
            } catch (NullPointerException | IllegalStateException e) {

            } catch (JSONException e) {
                e.printStackTrace();
            }
            return "ok";
        }
    }


    private class GetTrends extends AsyncTask<Void, Void, String> {
        protected String doInBackground(Void... params) {
            try {

                JSONObject requestData = new JSONObject();
                requestData.put("segmentJSON", analyticsQuery.getQuery_data());
                requestData.put("aggregateScale", "DAY");
                Date currentDate = new Date();
                Calendar c = Calendar.getInstance();
                c.setTime(currentDate);
                c.add(Calendar.DATE, 1);
                c.set(Calendar.HOUR, 0);
                c.set(Calendar.MINUTE, 0);
                c.set(Calendar.SECOND, 0);

                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd  HH:mm:ss");

                requestData.put("rangeDateTo", format.format(c.getTime()));
                c.add(Calendar.DATE, -7);
                requestData.put("rangeDateFrom", format.format(c.getTime()));

                String body = postData("https://api.synerise.com/rtom/analytics-query/execute-trends-query", requestData);

                if (body == null) {
                    return "not OK";
                }
                JSONObject jsonObject = null;

                try {
                    jsonObject = new JSONObject(body);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                final JSONArray jsonArray = jsonObject.getJSONArray("resultForChart");

                final List<String> labels = new ArrayList<String>();
                Map<String, ArrayList<BarEntry>> values = new HashMap<>();

                int minY = Integer.MAX_VALUE;
                int size = 0;
                ArrayList<String> stackLabels = null;

                try {
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject object = jsonArray.getJSONObject(i);
                        Iterator<String> keys = object.keys();
                        ArrayList<Float> yVals = new ArrayList<>();
                        stackLabels = new ArrayList<>();

                        while (keys.hasNext()) {
                            String key = keys.next();
                            if (key.compareToIgnoreCase("date") != 0) {
                                stackLabels.add(key);
                                ArrayList<BarEntry> currentValues = null;
                                if (values.containsKey(key)) {
                                    currentValues = values.get(key);
                                } else {
                                    currentValues = new ArrayList<>();
                                    values.put(key, currentValues);
                                }
//                                yVals.add((float) object.getDouble(key));

                                currentValues.add(new BarEntry(i, (float) object.getDouble(key)));
                            }
                            labels.add(object.getString("date"));
                        }

//                        float[] floatValues = new float[yVals.size()];
//                        for (int j = 0; j < yVals.size(); j++) {
//                            floatValues[j] = yVals.get(j);
//                        }
//
//                        values.add(new BarEntry(i, floatValues));
//                        labels.add(object.getString("date"));
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                final String[] labelsa = new String[stackLabels.size()];

                final ArrayList<BarDataSet> barDataSets = new ArrayList<>();
                Set<String> keys = values.keySet();
                for (String key:
                        keys) {
                    barDataSets.add(new BarDataSet(values.get(key), key));
                }
//                BarDataSet set = new BarDataSet(values);
//                set.setStackLabels(stackLabels.toArray(labelsa));

                for (int i = 0; i < barDataSets.size() ; i++) {
                    barDataSets.get(i).setColor(colors[i % stackLabels.size()]);
                }
                final BarData data = new BarData(barDataSets.toArray(new IBarDataSet[barDataSets.size()]));

                data.setDrawValues(false);
                data.setBarWidth(0.6f);
                final Typeface tf = Typeface.createFromAsset(getActivity().getAssets(),
                        "fonts/Lato-Thin.ttf");
                final Typeface btf = Typeface.createFromAsset(getActivity().getAssets(),
                        "fonts/Lato-Bold.ttf");


                final int finalMinY = minY;
                final int finalSize = size;
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        barChart.update(data, 0d, labels, labelsa, false, false);

                        BarChart chart = barChart.getChart();
                        BarChartMarkerView mv = new BarChartMarkerView(getContext(), R.layout.custom_marker_view);
                        mv.setChartView(chart); // For bounds control
                        mv.setDates(labels);
                        mv.setLabels(labelsa);
                        chart.setMarker(mv);

                        chart.setData(data);
                        chart.getXAxis().setValueFormatter(new IndexAxisValueFormatter(labels));
                        chart.getLegend().setEnabled(true);

                        chart.getLegend().setEnabled(true);

                        chart.getLegend().setOrientation(Legend.LegendOrientation.HORIZONTAL);
                        chart.getLegend().setDrawInside(false);
                        chart.getLegend().setHorizontalAlignment(Legend.LegendHorizontalAlignment.CENTER);
                        chart.getLegend().setVerticalAlignment(Legend.LegendVerticalAlignment.TOP);
                        chart.setExtraBottomOffset(150);
//                        chart.getLegend().setYOffset(-40);

                        chart.groupBars(0,(float) 0.8/barDataSets.size(),(float) 0.2/barDataSets.size());

                        barChart.getChart().getXAxis().setAxisMinimum(-3f);
                        barChart.getChart().getXAxis().setAxisMaximum(labels.size() + 1);
                        barChart.invalidate();
                    }
                });
//
            } catch (NullPointerException | IllegalStateException e) {

            } catch (JSONException e) {
                e.printStackTrace();
            }
            return "ok";
        }
    }
}

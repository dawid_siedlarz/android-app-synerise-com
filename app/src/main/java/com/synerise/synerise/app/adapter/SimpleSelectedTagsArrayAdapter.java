package com.synerise.synerise.app.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.synerise.synerise.R;
import com.synerise.synerise.app.model.Tag;
import com.synerise.synerise.app.model.TagSelected;

import java.util.ArrayList;

/**
 * Created by dsiedlarz on 15.10.2016.
 */
public class SimpleSelectedTagsArrayAdapter extends ArrayAdapter<Tag.TagModel> {
    private final Context context;
    private final ArrayList<Tag.TagModel> values;
    private TextView mAvatar;
    private Tag.TagModel tag;



    public SimpleSelectedTagsArrayAdapter(Context context, ArrayList<Tag.TagModel> values) {
        super(context, -1, values);
        this.context = context;
        this.values = values;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        tag = TagSelected.ITEMS.get(position);
        View rowView = inflater.inflate(R.layout.list_element_tag_selected, parent, false);
        TextView mTagName = (TextView) rowView.findViewById(R.id.tagName);

        mTagName.setText(tag.name);
        return rowView;
    }

}

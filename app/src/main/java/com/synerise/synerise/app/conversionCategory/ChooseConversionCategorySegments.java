package com.synerise.synerise.app.conversionCategory;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.synerise.synerise.app.fragment.ConversionFragment;
import com.synerise.synerise.R;
import com.synerise.synerise.app.adapter.ConversionSegmentsRecyclerAdapter;

public class ChooseConversionCategorySegments extends AppCompatActivity {
    private RecyclerView eventsList;
    private ConversionSegmentsRecyclerAdapter adapter;
    private LinearLayoutManager linearLayoutManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_conversion_category_segments);


        eventsList = (RecyclerView) findViewById(R.id.conversion_choose_segment_list);
        adapter = new ConversionSegmentsRecyclerAdapter(this, ConversionFragment.availableSegments);
        eventsList.setNestedScrollingEnabled(false);
        linearLayoutManager = new LinearLayoutManager(getApplicationContext());
        eventsList.setLayoutManager(linearLayoutManager);
        eventsList.setAdapter(adapter);
        System.out.println(adapter.getItemCount());
    }

}
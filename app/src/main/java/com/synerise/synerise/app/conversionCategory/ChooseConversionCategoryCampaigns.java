package com.synerise.synerise.app.conversionCategory;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.util.Pair;
import android.support.v7.app.AppCompatActivity;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Switch;

import com.synerise.synerise.R;
import com.synerise.synerise.app.fragment.ConversionFragment;

import java.util.Iterator;

public class ChooseConversionCategoryCampaigns extends AppCompatActivity {

    private Switch utmSourceSwitch;
    private Switch utmMediumSwitch;
    private Switch utmCampaignSwitch;
    private Switch utmTermSwitch;
    private Switch utmContentSwitch;
    private Switch snrSourceSwitch;
    private Switch snrMediumSwitch;
    private Switch snrCampaignSwitch;
    private Switch snrTermSwitch;
    private Switch snrContentSwitch;

    private EditText utmSourceText;
    private EditText utmMediumText;
    private EditText utmCampaignText;
    private EditText utmTermText;
    private EditText utmContentText;
    private EditText snrSourceText;
    private EditText snrMediumText;
    private EditText snrCampaignText;
    private EditText snrTermText;
    private EditText snrContentText;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_conversion_category_campaigns);

        utmSourceSwitch = (Switch) findViewById(R.id.utmSourceSwitch);
        utmMediumSwitch = (Switch) findViewById(R.id.utmMediumSwitch);
        utmCampaignSwitch = (Switch) findViewById(R.id.utmCampaignSwitch);
        utmTermSwitch = (Switch) findViewById(R.id.utmTermSwitch);
        utmContentSwitch = (Switch) findViewById(R.id.utmContentSwitch);
        snrSourceSwitch = (Switch) findViewById(R.id.snrSourceSwitch);
        snrMediumSwitch = (Switch) findViewById(R.id.snrMediumSwitch);
        snrCampaignSwitch = (Switch) findViewById(R.id.snrCampaignSwitch);
        snrTermSwitch = (Switch) findViewById(R.id.snrTermSwitch);
        snrContentSwitch = (Switch) findViewById(R.id.snrContentSwitch);

        utmSourceText = (EditText) findViewById(R.id.utmSourceText);
        utmMediumText = (EditText) findViewById(R.id.utmMediumText);
        utmCampaignText = (EditText) findViewById(R.id.utmCampaignText);
        utmTermText = (EditText) findViewById(R.id.utmTermText);
        utmContentText = (EditText) findViewById(R.id.utmContentText);
        snrSourceText = (EditText) findViewById(R.id.snrSourceText);
        snrMediumText = (EditText) findViewById(R.id.snrMediumText);
        snrCampaignText = (EditText) findViewById(R.id.snrCampaignText);
        snrTermText = (EditText) findViewById(R.id.snrTermText);
        snrContentText = (EditText) findViewById(R.id.snrContentText);


        for (int i = 0; i < ConversionFragment.categoryCampaigns.size(); i++) {
            switch (ConversionFragment.categoryCampaigns.get(i).first) {
                case "utmSource":
                    utmSourceSwitch.setChecked(true);
                    utmSourceText.setText(ConversionFragment.categoryCampaigns.get(i).second);
                    break;
                case "utmMedium":
                    utmMediumSwitch.setChecked(true);
                    utmMediumText.setText(ConversionFragment.categoryCampaigns.get(i).second);
                    break;
                case "utmCampaign":
                    utmCampaignSwitch.setChecked(true);
                    utmCampaignText.setText(ConversionFragment.categoryCampaigns.get(i).second);
                    break;
                case "utmTerm":
                    utmTermSwitch.setChecked(true);
                    utmTermText.setText(ConversionFragment.categoryCampaigns.get(i).second);
                    break;
                case "utmContent":
                    utmContentSwitch.setChecked(true);
                    utmContentText.setText(ConversionFragment.categoryCampaigns.get(i).second);
                    break;
                case "snrSource":
                    snrSourceSwitch.setChecked(true);
                    snrSourceText.setText(ConversionFragment.categoryCampaigns.get(i).second);
                    break;
                case "snrMedium":
                    snrMediumSwitch.setChecked(true);
                    snrMediumText.setText(ConversionFragment.categoryCampaigns.get(i).second);
                    break;
                case "snrCampaign":
                    snrCampaignSwitch.setChecked(true);
                    snrCampaignText.setText(ConversionFragment.categoryCampaigns.get(i).second);
                    break;
                case "snrTerm":
                    snrTermSwitch.setChecked(true);
                    snrTermText.setText(ConversionFragment.categoryCampaigns.get(i).second);
                    break;
                case "snrContent":
                    snrContentSwitch.setChecked(true);
                    snrContentText.setText(ConversionFragment.categoryCampaigns.get(i).second);
                    break;
            }
        }


        utmSourceSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    ConversionFragment.categoryCampaigns.add(new Pair<String, String>("utmSource", utmSourceText.getText().toString()));
                } else {
                    deleteCampaing("utmSource");
                }
                Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.mainContainer);
                ((ConversionFragment) fragment).getConversion();
            }
        });

        utmMediumSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    ConversionFragment.categoryCampaigns.add(new Pair<String, String>("utmMedium", utmMediumText.getText().toString()));
                } else {
                    deleteCampaing("utmMedium");
                }
                Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.mainContainer);
                ((ConversionFragment) fragment).getConversion();
            }
        });

        utmCampaignSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    ConversionFragment.categoryCampaigns.add(new Pair<String, String>("utmCampaign", utmCampaignText.getText().toString()));
                } else {
                    deleteCampaing("utmCampaign");
                }
                Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.mainContainer);
                ((ConversionFragment) fragment).getConversion();
            }
        });

        utmTermSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    ConversionFragment.categoryCampaigns.add(new Pair<String, String>("utmTerm", utmTermText.getText().toString()));
                } else {
                    deleteCampaing("utmTerm");
                }
                Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.mainContainer);
                ((ConversionFragment) fragment).getConversion();
            }
        });

        utmContentSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    ConversionFragment.categoryCampaigns.add(new Pair<String, String>("utmContent", utmContentText.getText().toString()));
                } else {
                    deleteCampaing("utmContent");
                }
                Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.mainContainer);
                ((ConversionFragment) fragment).getConversion();
            }
        });

        snrSourceSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    ConversionFragment.categoryCampaigns.add(new Pair<String, String>("snrSource", snrSourceText.getText().toString()));
                } else {
                    deleteCampaing("snrSource");
                }
                Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.mainContainer);
                ((ConversionFragment) fragment).getConversion();
            }
        });

        snrMediumSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    ConversionFragment.categoryCampaigns.add(new Pair<String, String>("snrMedium", snrMediumText.getText().toString()));
                } else {
                    deleteCampaing("snrMedium");
                }
                Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.mainContainer);
                ((ConversionFragment) fragment).getConversion();
            }
        });

        snrCampaignSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    ConversionFragment.categoryCampaigns.add(new Pair<String, String>("snrCampaign", snrCampaignText.getText().toString()));
                } else {
                    deleteCampaing("snrCampaign");
                }
                Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.mainContainer);
                ((ConversionFragment) fragment).getConversion();
            }
        });

        snrTermSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    ConversionFragment.categoryCampaigns.add(new Pair<String, String>("snrTerm", snrTermText.getText().toString()));
                } else {
                    deleteCampaing("snrTerm");
                }
                Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.mainContainer);
                ((ConversionFragment) fragment).getConversion();
            }
        });

        snrContentSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    ConversionFragment.categoryCampaigns.add(new Pair<String, String>("snrContent", snrContentText.getText().toString()));
                } else {
                    deleteCampaing("snrContent");
                }
                Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.mainContainer);
                ((ConversionFragment) fragment).getConversion();
            }
        });


    }

    public void deleteCampaing(String key) {
        Iterator<Pair<String, String>> i = ConversionFragment.categoryCampaigns.iterator();
        while (i.hasNext()) {
            Pair<String, String> s = i.next();
            if (s.first.compareToIgnoreCase(key) == 0) {
                i.remove();
            }
        }
    }
}

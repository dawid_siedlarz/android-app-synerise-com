package com.synerise.synerise.app.adapter;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.synerise.synerise.MainActivity;
import com.synerise.synerise.R;
import com.synerise.synerise.app.fragment.campaign.statistic.BeaconHintCampaignStatisticsFragment;
import com.synerise.synerise.app.fragment.campaign.statistic.DynamicContentCampaignStatisticsFragment;
import com.synerise.synerise.app.fragment.campaign.statistic.EmailCampaignStatisticsFragment;
import com.synerise.synerise.app.fragment.campaign.statistic.LandingPageCampaignStatisticsFragment;
import com.synerise.synerise.app.fragment.campaign.statistic.MobilePushCampaignStatisticFragment;
import com.synerise.synerise.app.fragment.campaign.statistic.SmsCampaignStatisticsFragment;
import com.synerise.synerise.app.model.campaign.BeaconHintCampaign;
import com.synerise.synerise.app.model.campaign.Campaign;
import com.synerise.synerise.app.model.campaign.CampaignRecipients;
import com.synerise.synerise.app.model.campaign.DynamicContentCampaign;
import com.synerise.synerise.app.model.campaign.EmailCampaign;
import com.synerise.synerise.app.model.campaign.LandingPageCampaign;
import com.synerise.synerise.app.model.campaign.MobilePushCampaign;
import com.synerise.synerise.app.model.campaign.SmsCampaign;

import java.text.SimpleDateFormat;
import java.util.List;


public class CampaignRecyclerViewAdapter extends RecyclerView.Adapter<CampaignRecyclerViewAdapter.ViewHolder> {

    private final List<Campaign> mValues;
    private final Fragment fragment;

    private Drawable greenRing;
    private Drawable orangeRing;
    private Drawable redRing;

    private SimpleDateFormat dateFormat;

    public CampaignRecyclerViewAdapter(List<Campaign> items, Fragment fragment) {
        mValues = items;
        this.fragment = fragment;

        greenRing = ResourcesCompat.getDrawable(fragment.getResources(), R.drawable.ring_green, null);
        orangeRing = ResourcesCompat.getDrawable(fragment.getResources(), R.drawable.ring_orange, null);
        redRing = ResourcesCompat.getDrawable(fragment.getResources(), R.drawable.ring_red, null);
        dateFormat = new SimpleDateFormat("yyyy.MM.dd");
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_element_campaign, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final Campaign campaign = mValues.get(position);

        holder.mItem = campaign;
        holder.title.setText(campaign.getName() != null ? campaign.getName() : campaign.getTitle());
        setStatus(holder);
        CampaignRecipients recipients = campaign.getRecipients();
        if (recipients != null) {
            holder.recipients.setText(recipients.toReadableString());
        } else {
            holder.recipients.setText("Recipients not available");
        }
        holder.date.setText(campaign.getDateSend() != null ? dateFormat.format(campaign.getDateSend()) : "not available");

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment newFragment = null;

                if ((campaign instanceof EmailCampaign)) {
                    newFragment = new EmailCampaignStatisticsFragment();
                }

                if ((campaign instanceof MobilePushCampaign)) {
                    newFragment = new MobilePushCampaignStatisticFragment();
                }

                if ((campaign instanceof BeaconHintCampaign)) {
                    newFragment = new BeaconHintCampaignStatisticsFragment();
                }

                if ((campaign instanceof SmsCampaign)) {
                    newFragment = new SmsCampaignStatisticsFragment();
                }

                if ((campaign instanceof DynamicContentCampaign)) {
                    newFragment = new DynamicContentCampaignStatisticsFragment();
                }

                if ((campaign instanceof LandingPageCampaign)) {
                    newFragment = new LandingPageCampaignStatisticsFragment();
                }

                if (newFragment == null) {
                    return;
                }

                if (campaign.getDateSend() == null && !(campaign instanceof BeaconHintCampaign)  && !(campaign instanceof DynamicContentCampaign)) {
                    View rootView = fragment.getView();
                    if (rootView == null) {
                        return;
                    }
                    Snackbar.make(rootView.findViewById(R.id.mainContainer), "Statistics not available for \n" + campaign.getName(), Snackbar.LENGTH_SHORT).show();

                    return;
                }

                Bundle bundle = new Bundle();
                bundle.putParcelable("campaign", campaign);
                newFragment.setArguments(bundle);

                 MainActivity.addFragment(newFragment);
            }
        });
    }

    private void setStatus(ViewHolder holder) {
        Campaign campaign = holder.mItem;
        String status = campaign.getStatus() != null ? campaign.getStatus() : "";
        if (status.length() > 1) {
            holder.status.setText(status.substring(0, 1).toUpperCase() + status.substring(1).toLowerCase());
        }
        switch (status) {
            case "SENT":
            case "Active":
            case "Public":
                holder.statusCricle.setBackground(greenRing);
                break;
            case "DRAFT":
                holder.statusCricle.setBackground(orangeRing);
                break;
            default:
                holder.statusCricle.setBackground(redRing);
        }
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView title;
        public final TextView status;
        public final View statusCricle;
        public final TextView recipients;
        public final TextView date;

        public Campaign mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            title = (TextView) view.findViewById(R.id.emailCampaignTitle);
            status = (TextView) view.findViewById(R.id.emailCampaignStatus);
            statusCricle = view.findViewById(R.id.emailCampaignStatusCircle);
            recipients = (TextView) view.findViewById(R.id.emailCampaignRecipients);
            date = (TextView) view.findViewById(R.id.emailCampaignDate);
        }
    }
}


package com.synerise.synerise.app.fragment.campaign.statistic;

import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.synerise.synerise.MainActivity;
import com.synerise.synerise.R;
import com.synerise.synerise.app.ActionBarConfigurableFragment;
import com.synerise.synerise.app.model.campaign.SmsCampaign;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;

public class SmsCampaignStatisticsFragment extends ActionBarConfigurableFragment {
    public static final String ARG_CAMPAIGN = "campaign";
    private SmsCampaign smsCampaign;

    private TextView smsCampaignStatisticTitle;
    private TextView smsCampaignStatisticStatus;
    private TextView smsCampaignStatisticRecipients;
    private TextView smsCampaignStatisticSent;
    private TextView smsStatisticDateSend;

    public SmsCampaignStatisticsFragment() {
        // Required empty public constructor
    }


    @Override
    public void setUpActionBar() {
        ActionBar actionBar = (ActionBar) ((AppCompatActivity) getActivity()).getSupportActionBar();
        if (actionBar != null) {
            actionBar.show();
            actionBar.setTitle(MainActivity.businessProfileName != null ? MainActivity.businessProfileName : "Synerise");
            actionBar.setSubtitle("SMS campaign");
            actionBar.setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.colorPrimaryCampaigns)));
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                Window window = getActivity().getWindow();
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                window.setStatusBarColor(getResources().getColor(R.color.colorPrimaryDarkCampaigns));
            }
            actionBar.setElevation(0);
        }
    }

    public static SmsCampaignStatisticsFragment newInstance(SmsCampaign smsCampaign) {
        SmsCampaignStatisticsFragment fragment = new SmsCampaignStatisticsFragment();
        Bundle args = new Bundle();
        args.putParcelable(ARG_CAMPAIGN, smsCampaign);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            smsCampaign = getArguments().getParcelable(ARG_CAMPAIGN);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_sms_campaign_statistics, container, false);
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        findHolders();
        fillHolders();
    }

    private void findHolders() {
        View rootView = getView();
        if (rootView == null) {
            return;
        }
        smsCampaignStatisticTitle = rootView.findViewById(R.id.smsCampaignStatisticTitle);
        smsCampaignStatisticStatus = rootView.findViewById(R.id.smsCampaignStatisticStatus);
        smsCampaignStatisticRecipients = rootView.findViewById(R.id.smsCampaignStatisticRecipients);
        smsCampaignStatisticSent = rootView.findViewById(R.id.smsCampaignStatisticSent);
        smsStatisticDateSend = rootView.findViewById(R.id.smsStatisticDateSend);
    }

    private void fillHolders() {
        DecimalFormat df = new DecimalFormat("###,###,###.##");

        smsCampaignStatisticTitle.setText(smsCampaign.getTitle());
        smsCampaignStatisticStatus.setText(smsCampaign.getStatus());
        smsCampaignStatisticRecipients.setText(smsCampaign.getRecipientsReadableString());
        smsCampaignStatisticSent.setText(df.format(smsCampaign.getSent()));

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy.MM.dd");
        smsStatisticDateSend.setText(dateFormat.format(smsCampaign.getDateSend()));
    }
}

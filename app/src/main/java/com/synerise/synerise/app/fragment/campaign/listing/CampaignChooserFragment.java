package com.synerise.synerise.app.fragment.campaign.listing;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.synerise.synerise.MainActivity;
import com.synerise.synerise.R;
import com.synerise.synerise.app.ActionBarConfigurableFragment;
import com.synerise.synerise.app.syneutil.SyneHttpCacheHandler;

import org.apache.http.client.methods.HttpGet;
import org.json.JSONException;
import org.json.JSONObject;

public class CampaignChooserFragment extends ActionBarConfigurableFragment {

    private TextView emailCampaignsCounter;
    private TextView smsCampaignsCounter;
    private TextView mobilePushCampaignsCounter;
    private TextView beaconCampaignsCounter;
    private TextView landingPageCampaignsCounter;
    private TextView dynamicContentCampaignsCounter;

    public CampaignChooserFragment() {
        // Required empty public constructor
    }

    @Override
    public void setUpActionBar() {
        ActionBar actionBar = (ActionBar) ((AppCompatActivity) getActivity()).getSupportActionBar();
        if (actionBar != null) {
            actionBar.show();
            actionBar.setTitle(MainActivity.businessProfileName != null ? MainActivity.businessProfileName : "Synerise");
            actionBar.setSubtitle("Campaign Chooser");
            actionBar.setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.colorPrimaryCampaigns)));
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                Window window = getActivity().getWindow();
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                window.setStatusBarColor(getResources().getColor(R.color.colorPrimaryDarkCampaigns));
            }
            actionBar.setElevation(0);
        }
    }


    public static CampaignChooserFragment newInstance(String param1, String param2) {
        CampaignChooserFragment fragment = new CampaignChooserFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_campaign_chooser, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        View rootView = getView();
        if (rootView == null) {
            return;
        }
        View emailBox = rootView.findViewById(R.id.emailBox);
        View smsBox = rootView.findViewById(R.id.smsBox);
        View mobilePushBox = rootView.findViewById(R.id.mobilePushBox);
        View beaconBox = rootView.findViewById(R.id.beaconBox);
        View dynamicContentBox = rootView.findViewById(R.id.dynamicContentBox);
        View landingPageBox = rootView.findViewById(R.id.landingPageBox);

        emailCampaignsCounter = rootView.findViewById(R.id.emailCounter);
        smsCampaignsCounter = rootView.findViewById(R.id.smsCounter);
        mobilePushCampaignsCounter = rootView.findViewById(R.id.mobilePushCounter);
        beaconCampaignsCounter = rootView.findViewById(R.id.beaconCounter);
        landingPageCampaignsCounter = rootView.findViewById(R.id.landingPageCampaignsCounter);
        dynamicContentCampaignsCounter = rootView.findViewById(R.id.dynamicContentCampaignsCounter);

        new EmailCampaignsCounterGetter().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        new SmsCampaignsCounterGetter().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        new MobilePushCampaignsCounterGetter().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        new BeaconCampaignsCounterGetter().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        new DynamicContentCampaignsCounterGetter().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        new LandingPageCampaignsCounterGetter().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

        emailBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment = new EmailCampaignsFragment();
                MainActivity.addFragment(fragment);
            }
        });


        smsBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment = new SmsCampaignsFragment();
                MainActivity.addFragment(fragment);

            }
        });

        mobilePushBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment = new MobilePushCampaignsFragment();
                MainActivity.addFragment(fragment);
            }
        });

        beaconBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment = new BeaconHintCampaignsFragment();
                MainActivity.addFragment(fragment);
            }
        });


        dynamicContentBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment = new DynamicContentCampaignsFragment();
                MainActivity.addFragment(fragment);
            }
        });


        landingPageBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment = new LandingPageCampaignsFragment();
                MainActivity.addFragment(fragment);
            }
        });
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }


    private class EmailCampaignsCounterGetter extends AsyncTask<Integer, Void, String> {
        protected String doInBackground(Integer... params) {
            try {

                SyneHttpCacheHandler handler = new SyneHttpCacheHandler();
                HttpGet httpget = new HttpGet("https://app.synerise.com/em/rest/campaigns/?listing[Paginator][limit]=1&listing[Paginator][offset]=0");

                final String body = handler.executeWithCache(httpget, true, true);

                if (body == null) {
                    return "not OK";
                }


                JSONObject jsonObject = null;

                try {
                    jsonObject = new JSONObject(body);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if (jsonObject == null) {
                    return null;
                }

                final int counter = jsonObject.getInt("counter");


                Activity activity = getActivity();
                if (activity != null) {

                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            emailCampaignsCounter.setText(counter + (counter == 1 ? " campaign" : " campaigns"));
                        }
                    });
                }


            } catch (JSONException e) {
                e.printStackTrace();
            } catch (NullPointerException | IllegalStateException e) {

            }
            return "ok";
        }
    }

    private class SmsCampaignsCounterGetter extends AsyncTask<Integer, Void, String> {
        protected String doInBackground(Integer... params) {
            try {

                SyneHttpCacheHandler handler = new SyneHttpCacheHandler();
                HttpGet httpget = new HttpGet("https://app.synerise.com/rest/sms-message/?listing[Paginator][limit]=50&listing[Paginator][offset]=0");

                final String body = handler.executeWithCache(httpget, true, true);

                if (body == null) {
                    return "not OK";
                }


                JSONObject jsonObject = null;

                try {
                    jsonObject = new JSONObject(body);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if (jsonObject == null) {
                    return null;
                }

                final int counter = jsonObject.getInt("counter");


                Activity activity = getActivity();
                if (activity != null) {

                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            smsCampaignsCounter.setText(counter + (counter == 1 ? " campaign" : " campaigns"));
                        }
                    });
                }


            } catch (JSONException e) {
                e.printStackTrace();
            } catch (NullPointerException | IllegalStateException e) {

            }
            return "ok";
        }
    }

    private class MobilePushCampaignsCounterGetter extends AsyncTask<Integer, Void, String> {
        protected String doInBackground(Integer... params) {
            try {

                SyneHttpCacheHandler handler = new SyneHttpCacheHandler();
                HttpGet httpget = new HttpGet("https://app.synerise.com/rest/push_message/list?listing[Paginator][limit]=50&listing[Paginator][offset]=0");

                final String body = handler.executeWithCache(httpget, true, true);

                if (body == null) {
                    return "not OK";
                }


                JSONObject jsonObject = null;

                try {
                    jsonObject = new JSONObject(body);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if (jsonObject == null) {
                    return null;
                }

                final int counter = jsonObject.getInt("counter");


                Activity activity = getActivity();
                if (activity != null) {

                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            mobilePushCampaignsCounter.setText(counter + (counter == 1 ? " campaign" : " campaigns"));
                        }
                    });
                }


            } catch (JSONException e) {
                e.printStackTrace();
            } catch (NullPointerException | IllegalStateException e) {

            }
            return "ok";
        }
    }

    private class BeaconCampaignsCounterGetter extends AsyncTask<Integer, Void, String> {
        protected String doInBackground(Integer... params) {
            try {

                SyneHttpCacheHandler handler = new SyneHttpCacheHandler();
                HttpGet httpget = new HttpGet("https://app.synerise.com/hints?listing[Paginator][limit]=50&listing[Paginator][offset]=0");

                final String body = handler.executeWithCache(httpget, true, true);

                if (body == null) {
                    return "not OK";
                }


                JSONObject jsonObject = null;

                try {
                    jsonObject = new JSONObject(body);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if (jsonObject == null) {
                    return null;
                }

                final int counter = jsonObject.getInt("counter");


                Activity activity = getActivity();
                if (activity != null) {

                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            beaconCampaignsCounter.setText(counter + (counter == 1 ? " campaign" : " campaigns"));
                        }
                    });
                }


            } catch (JSONException e) {
                e.printStackTrace();
            } catch (NullPointerException | IllegalStateException e) {

            }
            return "ok";
        }
    }

    private class LandingPageCampaignsCounterGetter extends AsyncTask<Integer, Void, String> {
        protected String doInBackground(Integer... params) {
            try {

                SyneHttpCacheHandler handler = new SyneHttpCacheHandler();
                HttpGet httpget = new HttpGet("https://app.synerise.com/rest/landing-pages/?listing[Paginator][limit]=50&listing[Paginator][offset]=0");

                final String body = handler.executeWithCache(httpget, true, true);

                if (body == null) {
                    return "not OK";
                }


                JSONObject jsonObject = null;

                try {
                    jsonObject = new JSONObject(body);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if (jsonObject == null) {
                    return null;
                }

                final int counter = jsonObject.getInt("counter");


                Activity activity = getActivity();
                if (activity != null) {

                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            landingPageCampaignsCounter.setText(counter + (counter == 1 ? " campaign" : " campaigns"));
                        }
                    });
                }


            } catch (JSONException e) {
                e.printStackTrace();
            } catch (NullPointerException | IllegalStateException e) {

            }
            return "ok";
        }
    }

    private class DynamicContentCampaignsCounterGetter extends AsyncTask<Integer, Void, String> {
        protected String doInBackground(Integer... params) {
            try {

                SyneHttpCacheHandler handler = new SyneHttpCacheHandler();
                HttpGet httpget = new HttpGet("https://app.synerise.com/rest/dynamic-contents/list?listing[Paginator][limit]=50&listing[Paginator][offset]=0");

                final String body = handler.executeWithCache(httpget, true, true);

                if (body == null) {
                    return "not OK";
                }


                JSONObject jsonObject = null;

                try {
                    jsonObject = new JSONObject(body);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if (jsonObject == null) {
                    return null;
                }

                final int counter = jsonObject.getInt("counter");


                Activity activity = getActivity();
                if (activity != null) {

                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            dynamicContentCampaignsCounter.setText(counter + (counter == 1 ? " campaign" : " campaigns"));
                        }
                    });
                }


            } catch (JSONException e) {
                e.printStackTrace();
            } catch (NullPointerException | IllegalStateException e) {

            }
            return "ok";
        }
    }
}

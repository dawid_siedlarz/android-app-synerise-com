package com.synerise.synerise.app.syneutil;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

import com.synerise.synerise.MainActivity;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.net.URL;

public class SyneHttpCacheHandler {

    public String executeWithCache(HttpUriRequest request, boolean loadFromCache, boolean saveToCache) {
        String response = null;
        if (loadFromCache) {

            response = MainActivity.cacheString.get(getCacheKey(request));

            Log.v("SyneHttpCacheHandler", "request: " + request.getURI().toASCIIString());
            Log.v("SyneHttpCacheHandler", "responseLoadedFromCache " + response);

            if (response != null && response.compareToIgnoreCase("null") != 0)
            {
                Log.v("getFromCache URL", request.getURI().toString());
                Log.v("getFromCache Data", response);
                return response;
            }
        }


        HttpResponse httpResponse = null;
        try {
            httpResponse = MainActivity.httpClient.execute(request);
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (httpResponse != null) {
            HttpEntity lastCampaignsEntity = httpResponse.getEntity();

            try {
                response = EntityUtils.toString(lastCampaignsEntity);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        Log.v("SyneHttpCacheHandler", "saveToCache?? " + (saveToCache && response != null && !response.isEmpty() && (response.compareToIgnoreCase("[]")!=0)));

        if (saveToCache && response != null && !response.isEmpty() && (response.compareToIgnoreCase("[]")!=0)) {
            Log.v("SyneHttpCacheHandler", "request: " + request.getURI().toASCIIString());
            Log.v("SyneHttpCacheHandler", "saveToCache " + response);
            MainActivity.cacheString.put(getCacheKey(request), response);
        }
        return response;
    }

    public String executeWithCacheKey(HttpUriRequest request, String key, boolean loadFromCache, boolean saveToCache) throws IOException {
        String response = null;

        if (loadFromCache) {
            try {
                response = MainActivity.cacheString.get(getCacheKeyFromUrl(key));
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
            if (response != null && response.compareToIgnoreCase("null") != 0){
                return response;
            }
        }
        HttpResponse httpResponse = MainActivity.httpClient.execute(request);
        if (httpResponse != null) {
            HttpEntity lastCampaignsEntity = httpResponse.getEntity();

            response = EntityUtils.toString(lastCampaignsEntity);
        }

        if (saveToCache && response != null && !response.isEmpty() && (response.compareToIgnoreCase("[]")!=0)) {
            MainActivity.cacheString.put(getCacheKeyFromUrl(key), response);
        }
        return response;
    }


    public static String getCacheKey(HttpUriRequest request) {
        return String.valueOf((MainActivity.businessProfileId + request.getURI().toASCIIString()).hashCode());
    }

    public static String getCacheKeyFromUrl(String url) {
        return String.valueOf(url.hashCode());
    }



    public static Bitmap getBmp(String url, boolean fromCache) throws IOException {
        Bitmap bmp = null;

        if( fromCache){
            bmp = MainActivity.cacheBitmap.get(SyneHttpCacheHandler.getCacheKeyFromUrl(url));
        }
        if (bmp == null) {
            bmp = BitmapFactory.decodeStream(new URL(url).openConnection().getInputStream());
            MainActivity.cacheBitmap.put(SyneHttpCacheHandler.getCacheKeyFromUrl(url), bmp);
        }
        return bmp;
    }


}

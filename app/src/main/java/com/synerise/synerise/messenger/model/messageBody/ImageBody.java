package com.synerise.synerise.messenger.model.messageBody;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.View;
import android.widget.ImageView;

import com.synerise.synerise.app.syneutil.SyneHttpCacheHandler;

import java.io.IOException;

public class ImageBody implements MessageBody {

    ImageView imageView;

    public ImageBody(Context context, String fileSrc) {
        imageView = new ImageView(context);
        try {
            Bitmap bitmap = SyneHttpCacheHandler.getBmp(fileSrc, true);
            imageView.setImageBitmap(bitmap);
            imageView.setAdjustViewBounds(true);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public View getBody() {
        return imageView;
    }
}

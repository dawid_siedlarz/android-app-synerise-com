package com.synerise.synerise.app.fragment;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.Switch;
import android.widget.TextView;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;
import com.synerise.sdk.tracking.Tracker;
import com.synerise.sdk.tracking.TrackerParams;
import com.synerise.synerise.MainActivity;
import com.synerise.synerise.R;
import com.synerise.synerise.SyneriseOnFragmentInteractionListener;
import com.synerise.synerise.app.adapter.EventRecyclerViewAdapter;
import com.synerise.synerise.app.factory.EventFactory;
import com.synerise.synerise.app.model.Event;
import com.synerise.synerise.app.syneutil.SyneHttpCacheHandler;
import com.synerise.synerise.app.syneview.SyneChartTitle;
import com.synerise.synerise.app.syneview.SyneComplexBarChart;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;


public class CampaignsFragment extends Fragment {

    private SyneriseOnFragmentInteractionListener mListener;
    private RecyclerView eventsList;
    private EventRecyclerViewAdapter adapter;
    private LinearLayoutManager linearLayoutManager;
    private ArrayList<Event> events;
    private EventFactory eventFactory;

    private void initEventsList() {
        View rootView = getView();
        if (rootView == null) {
            return;
        }
        eventsList = (RecyclerView) rootView.findViewById(R.id.campaignsEventsListView);
        events = new ArrayList<>();
        adapter = new EventRecyclerViewAdapter((MainActivity) getActivity(), events);
        eventsList.setNestedScrollingEnabled(false);
        linearLayoutManager = new LinearLayoutManager(getContext());
        eventsList.setLayoutManager(linearLayoutManager);
        eventsList.setAdapter(adapter);
    }


    //charts
    SyneComplexBarChart lastCampaignsChart;
    SyneComplexBarChart ratesChart;

    private ArrayList<TextView> campaignNames;
    private ArrayList<TextView> campaignClicks;
    private ArrayList<TextView> campaignOpened;
    private ArrayList<TextView> campaignUnsubscribed;
    private ArrayList<TextView> campaignHardBounces;
    private ArrayList<TextView> campaignSoftBounces;
    private ArrayList<TextView> campaignUniqueOpens;
    private ArrayList<TextView> campaignUniqueClicks;
    private ArrayList<TextView> campaignCTRs;
    private ArrayList<TextView> campaignOpenRates;
    private ArrayList<TextView> campaignSents;

    private Typeface   tf;

    boolean isScreenActive = false;
    boolean isCampaignChartInitialized = false;


    @Override
    public void onResume() {
        super.onResume();
        isScreenActive = true;
        System.out.println("onResume");
        new GetCampaigns().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        initEventsList();

        new GetCampaignsEventList().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    @Override
    public void onPause() {
        super.onPause();
        isScreenActive = false;
    }

    @Override
    public void onStop() {
        super.onStop();
        isScreenActive = false;
    }

    public CampaignsFragment() {
    }

    public static CampaignsFragment newInstance() {
        CampaignsFragment fragment = new CampaignsFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_campaigns, container, false);
    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        eventFactory = new EventFactory(getContext());

        Tracker.trackScreen("Campaigns", new TrackerParams[]{});
        ActionBar actionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        actionBar.show();
        actionBar.setTitle(MainActivity.businessProfileName != null? MainActivity.businessProfileName : "Synerise");
        actionBar.setSubtitle("Campaign");
        actionBar.setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.colorPrimary)));
        View rootView = getView();
        if (rootView == null) {
            return;
        }
        lastCampaignsChart = ((SyneComplexBarChart) rootView.findViewById(R.id.CampaignsChart));
        lastCampaignsChart.getTitle().clearCountAndUnit();
        ratesChart = (SyneComplexBarChart) rootView.findViewById(R.id.CampaignsRatesChart);
        ratesChart.getTitle().clearCountAndUnit();
        tf = Typeface.createFromAsset(getContext().getAssets(),
                "fonts/Lato-Thin.ttf");



        final ImageButton button = (ImageButton) rootView.findViewById(R.id.refreshTrafficButton);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new GetCampaignsEventList().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                /*Animation animation = AnimationUtils.loadAnimation(getContext(), R.anim.rotate);  animation.setRepeatCount(-1);;               animation.setDuration(1000);                 button.startAnimation(animation); */            }         });

        Switch autoload = (Switch) rootView.findViewById(R.id.autoloadTrafficSwitch);
        autoload.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    new GetCampaignsEventList().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                }
            }
        });



    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof SyneriseOnFragmentInteractionListener) {
            mListener = (SyneriseOnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    private class GetCampaigns extends AsyncTask<Void, Void, String> {
        protected String doInBackground(Void... params) {
            try {

                HttpGet httpgetLastCampaigns = new HttpGet("https://app.synerise.com/em/rest/campaigns/last");

                SyneHttpCacheHandler handler = new SyneHttpCacheHandler();
                boolean cacheUsed = false;
                if(!isCampaignChartInitialized){
                    cacheUsed = MainActivity.cacheString.contains(SyneHttpCacheHandler.getCacheKey(httpgetLastCampaigns));
                }
                final String lastCampaigns = handler.executeWithCache(httpgetLastCampaigns, !isCampaignChartInitialized, true);

                if (lastCampaigns == null) {
                    return "not OK";
                }

                JSONArray lastCampaignsArray = null;

                try {
                    lastCampaignsArray = new JSONArray(lastCampaigns);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if (lastCampaignsArray == null) {
                    return null;
                }

                String hashes = "";
                for (int i = 0; i < lastCampaignsArray.length(); i++) {
                    hashes += "ids[" + i + "]=" + lastCampaignsArray.getString(i) + "&";
                }

                HttpGet httpget = new HttpGet("https://app.synerise.com/rest/dashboard/campaign-statistics-flat/email?" + hashes + "&date%5BdateFormat%5D=hour&date%5BminPeriod%5D=hh&client=all&campaignId=");

                SyneHttpCacheHandler handler2 = new SyneHttpCacheHandler();


                final String body4 = handler2.executeWithCache(httpget, true, true );


                if (body4 == null) {
                    return "not OK";
                }

                System.out.println("body4");
                System.out.println(body4);

                JSONArray jsonArray = null;

                try {
                    JSONObject jsonObject = new JSONObject(body4);
                    jsonArray = jsonObject.getJSONArray("items");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if (jsonArray == null) {
                    return null;
                }

                final JSONArray finalJSONArray = jsonArray;

                final ArrayList<String> labels = new ArrayList<String>();
                final ArrayList<BarEntry> sentEntries = new ArrayList<BarEntry>();
                final ArrayList<BarEntry> openedEntries = new ArrayList<BarEntry>();
                final ArrayList<BarEntry> clickedEntries = new ArrayList<BarEntry>();
                final ArrayList<BarEntry> uniqueClickedEntries = new ArrayList<BarEntry>();
                final ArrayList<BarEntry> uniqueOpenedEntries = new ArrayList<BarEntry>();
                final ArrayList<BarEntry> ctrEntries = new ArrayList<>();
                final ArrayList<BarEntry> openRatesEntries = new ArrayList<>();

                double sent = 0.0;
                double opened = 0.0;
                double clicked = 0.0;
                double uniqueClicked = 0.0;
                double uniqueOpened = 0.0;
                double unsubscribed = 0.0;
                double openRate = 0.0;
                double ctr = 0.0;

                double sentAll = 0.0;
                double openedAll = 0.0;
                double clickedAll = 0.0;
                double uniqueClickedAll = 0.0;
                double uniqueOpenedAll = 0.0;
                double unsubscribedAll = 0.0;
                double openRateAll = 0.0;
                double ctrAll = 0.0;

                final ArrayList<String> campaignClicksValues = new ArrayList<>();
                final ArrayList<String> campaignOpenedValues = new ArrayList<>();
                final ArrayList<String> campaignUnsubscribedValues = new ArrayList<>();
                final ArrayList<String> campaignHardBouncesValues = new ArrayList<>();
                final ArrayList<String> campaignSoftBouncesValues = new ArrayList<>();
                final ArrayList<String> campaignUniqueOpensValues = new ArrayList<>();
                final ArrayList<String> campaignUniqueClicksValues = new ArrayList<>();
                final ArrayList<String> campaignCTRsValues = new ArrayList<>();
                final ArrayList<String> campaignOpenRatesValues = new ArrayList<>();
                final ArrayList<String> campaignSentsValues = new ArrayList<>();

                ArrayList<BarEntry> yVals1 = new ArrayList<BarEntry>();
                ArrayList<BarEntry> yVals2 = new ArrayList<BarEntry>();


                try {
                    int j = 0;
                    for (int i = 0; i < finalJSONArray.length(); i++) {
                        JSONObject jsonObject = finalJSONArray.getJSONObject(i);
                        sent = !jsonObject.isNull("sent") ? jsonObject.getDouble("sent") : 0;
                        opened = !jsonObject.isNull("opened") ? jsonObject.getDouble("opened") : 0;
                        clicked = !jsonObject.isNull("clicked") ? jsonObject.getDouble("clicked") : 0;
                        uniqueClicked = !jsonObject.isNull("uniqueClicked") ? jsonObject.getDouble("uniqueClicked") : 0;
                        uniqueOpened = !jsonObject.isNull("uniqueOpened") ? jsonObject.getDouble("uniqueOpened") : 0;
                        unsubscribed = !jsonObject.isNull("unsubscribed") ? jsonObject.getDouble("unsubscribed") : 0;
                        openRate = !jsonObject.isNull("openrate") ? jsonObject.getDouble("openrate") : 0;
                        ctr = !jsonObject.isNull("ctr") ? jsonObject.getDouble("ctr") : 0;

                        sentAll += sent;
                        openedAll += opened;
                        clickedAll += clicked;
                        uniqueClickedAll += uniqueClicked;
                        uniqueOpenedAll += uniqueOpened;
                        unsubscribedAll += unsubscribed;
                        openRateAll += openRate;
                        ctrAll += ctr;

                        sentEntries.add(new BarEntry(j, (float) sent));
                        openedEntries.add(new BarEntry(j, (float) opened));
                        clickedEntries.add(new BarEntry(j, (float) clicked));
                        uniqueClickedEntries.add(new BarEntry(j, (float) uniqueClicked));
                        uniqueOpenedEntries.add(new BarEntry(j, (float) uniqueOpened));

                        yVals1.add(new BarEntry(j, new float[]{(float)sent, (float)opened, (float)clicked, (float)uniqueClicked, (float)uniqueOpened}));

                        openRatesEntries.add(new BarEntry(j, (float) openRate));
                        ctrEntries.add(new BarEntry(j, (float) ctr));

                        yVals2.add(new BarEntry(j, new float[]{(float)openRate, (float)ctr}));


                        campaignClicksValues.add((int) clicked + "");
                        campaignOpenedValues.add((int) opened + "");
                        campaignUnsubscribedValues.add((int) unsubscribed + "");
                        campaignHardBouncesValues.add((int) jsonObject.getDouble("hardbounced") + "");
                        campaignSoftBouncesValues.add((int) jsonObject.getDouble("softbounced") + "");
                        campaignUniqueOpensValues.add((int) uniqueOpened + "");
                        campaignUniqueClicksValues.add((int) uniqueClicked + "");
                        campaignCTRsValues.add((int) ctr + " %");
                        campaignOpenRatesValues.add((int) openRate + " %");
                        campaignSentsValues.add((int) sent + "");
                        j++;
                        labels.add(jsonObject.getString("name"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }


                final String [] labelsY = new String[]{"Sent","Opened", "Clicked","Unique Clicked","UniqueOpened"};
                BarDataSet sentSet = new BarDataSet(sentEntries, "Sent");
                BarDataSet openedSet = new BarDataSet(openedEntries, "Opened");
                BarDataSet clickedSet = new BarDataSet(clickedEntries, "Clicked");
                BarDataSet uniqueClickedSet = new BarDataSet(uniqueClickedEntries, "Unique Clicked");
                BarDataSet uniqueOpenedSet = new BarDataSet(uniqueOpenedEntries, "UniqueOpened");

                final String [] labelsYCtrOr = new String[]{"Open rate","CTR"};
                BarDataSet openRateSet = new BarDataSet(openRatesEntries, "Open rate");
                BarDataSet ctrSet = new BarDataSet(ctrEntries, "CTR");


                sentSet.setColor(Color.parseColor("#00bdf4"));
                openedSet.setColor(Color.parseColor("#f87b16"));
                clickedSet.setColor(Color.parseColor("#ffb606"));
                uniqueClickedSet.setColor(Color.parseColor("#dd0b9f"));
                uniqueOpenedSet.setColor(Color.parseColor("#f87b16"));

                openRateSet.setColor(Color.parseColor("#ffb606"));
                ctrSet.setColor(Color.parseColor("#dd0b9f"));

                ArrayList<LineDataSet> sets = new ArrayList<LineDataSet>();

//                sets.add(openRateSet);
//                sets.add(ctrSet);
//
//                for (LineDataSet set : sets) {
//                    set.setDrawCircles(false);
//                    set.setMode(LineDataSet.Mode.CUBIC_BEZIER);
//                    set.setDrawHighlightIndicators(true);
//                    set.setDrawHorizontalHighlightIndicator(false);
//                    set.setDrawVerticalHighlightIndicator(false);
//                }


                ArrayList<IBarDataSet> setList = new ArrayList<>();

                setList.add(sentSet);
                setList.add(openedSet);
                setList.add(clickedSet);
                setList.add(uniqueClickedSet);
                setList.add(uniqueOpenedSet);

                ArrayList<IBarDataSet> ratesList = new ArrayList<>();

                ratesList.add(openRateSet);
                ratesList.add(ctrSet);

                final BarData ratesData = new BarData( ratesList);
                ratesData.setDrawValues(false);

//                final BarData barData = new BarData( setList);


                int [] colors = new int [] {
                        Color.parseColor("#00bdf4"),
                        Color.parseColor("#f87b16"),
                        Color.parseColor("#ffb606"),
                        Color.parseColor("#dd0b9f"),
                        Color.parseColor("#f87b16")
                };

                int [] colors2 = new int [] {
                        getResources().getColor(R.color.legendCtr),
                        getResources().getColor(R.color.legendOr)
                };

                BarDataSet set1 = new BarDataSet(yVals1, "Last campaigns");
                set1.setColors(colors);
                set1.setDrawValues(false);
                set1.setStackLabels(labels.toArray(new String[labels.size()]));

                ArrayList<IBarDataSet> dataSets = new ArrayList<IBarDataSet>();
                dataSets.add(set1);

                final BarData barData = new BarData(dataSets);
//                barData.setValueFormatter(new MyValueFormatter());
                barData.setValueTextColor(Color.WHITE);

                BarDataSet set2 = new BarDataSet(yVals2, "CTR/OR");
                set2.setColors(colors2);
                set2.setDrawValues(false);
                set2.setStackLabels(labels.toArray(new String[labels.size()]));

                ArrayList<IBarDataSet> dataSets2 = new ArrayList<IBarDataSet>();
                dataSets2.add(set2);

                final BarData barData2 = new BarData(dataSets2);


                Activity activity = getActivity();
                if (activity != null) {


                    final double finalUniqueOpenedAll = uniqueOpenedAll;
                    final double finalSentAll = sentAll;
                    final double finalUnsubscribedAll = unsubscribedAll;
                    final double finalClickedAll = clickedAll;
                    final double finalOpenedAll = openedAll;
                    final double finalOpenRateAll = openRateAll;
                    final double finalCtrAll = ctrAll;


                    final boolean finalCacheUsed = cacheUsed;
                    final boolean finalCacheUsed1 = cacheUsed;
                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {



                            lastCampaignsChart.update(barData, 0d, labels, labelsY ,isCampaignChartInitialized, finalCacheUsed1);
                            ratesChart.update(barData2, 0d, labels, labelsY ,isCampaignChartInitialized, finalCacheUsed1);

                            isCampaignChartInitialized = true;

                            loadTable();

                            for (int i = 0; i < campaignNames.size(); i++) {
                                campaignNames.get(i).setText(labels.get(i));
                                campaignClicks.get(i).setText(campaignClicksValues.get(i));
                                campaignOpened.get(i).setText(campaignOpenedValues.get(i));
                                campaignUnsubscribed.get(i).setText(campaignUnsubscribedValues.get(i));
                                campaignHardBounces.get(i).setText(campaignHardBouncesValues.get(i));
                                campaignSoftBounces.get(i).setText(campaignSoftBouncesValues.get(i));
                                campaignUniqueOpens.get(i).setText(campaignUniqueOpensValues.get(i));
                                campaignUniqueClicks.get(i).setText(campaignUniqueClicksValues.get(i));
                                campaignCTRs.get(i).setText(campaignCTRsValues.get(i));
                                campaignOpenRates.get(i).setText(campaignOpenRatesValues.get(i));
                                campaignSents.get(i).setText(campaignSentsValues.get(i));
                            }

                            View rootView = getView();
                            if (rootView == null) {
                                return;
                            }
                            ((TextView) rootView.findViewById(R.id.UnsubscribedValue)).setText((int) finalUnsubscribedAll + "");
                            ((TextView) rootView.findViewById(R.id.AllUniqueInteractionsValue)).setText(0 + "");
                            ((TextView) rootView.findViewById(R.id.AllUniqueOpensValue)).setText((int) finalUniqueOpenedAll + "");
                            ((TextView) rootView.findViewById(R.id.AllSentValue)).setText((int) finalSentAll + "");
                            ((TextView) rootView.findViewById(R.id.AllClicksValue)).setText((int) finalClickedAll + "");
                            ((TextView) rootView.findViewById(R.id.AllOpensValue)).setText((int) finalOpenedAll + "");



                            ((TextView) rootView.findViewById(R.id.OpenRatesAverageValue)).setText(SyneChartTitle.formatter.format( finalOpenRateAll / 5) + " %");
                            ((TextView) rootView.findViewById(R.id.ctrAverageValue)).setText(SyneChartTitle.formatter.format( finalCtrAll / 5) + " %");


                        }
                    });
                }
            }  catch (JSONException e) {
                e.printStackTrace();
            } catch (NullPointerException e) {

            } catch (IllegalStateException e) {

            }
            return "ok";
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            Handler myHandler = new Handler();
            myHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    Fragment fragment = getActivity().getSupportFragmentManager().findFragmentById(R.id.mainContainer);

                    if (isScreenActive && getActivity() != null && fragment instanceof CampaignsFragment) {
                        new GetCampaigns().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                    }
                }
            }, 3000);
        }
    }


    private class GetCampaignsEventList extends AsyncTask<Void, Void, String> {
        protected String doInBackground(Void... params) {
            try {

                HttpGet httpget = new HttpGet("https://app.synerise.com/activity-histories/stream-for-business-profile/?category=client.web.browser.mail");

                HttpResponse eventList = MainActivity.httpClient.execute(httpget);
                if (eventList == null) {
                    return "not OK";
                }

                HttpEntity eventListEntity = eventList.getEntity();

                final String body = EntityUtils.toString(eventListEntity);

                JSONArray jsonArray = null;

                try {
                    jsonArray = new JSONArray(body);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if (jsonArray == null) {
                    return null;
                }

                eventFactory.createEventsArray(events, jsonArray);

                Activity activity = getActivity();
                if (activity != null) {
                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            adapter.notifyDataSetChanged();
                        }
                    });
                }

            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (NullPointerException e) {

            } catch (IllegalStateException e) {

            }
            return "ok";
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            Handler myHandler = new Handler();
            myHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    View rootView = getView();
                    if (rootView == null) {
                        return;
                    }
                    Fragment fragment = getActivity().getSupportFragmentManager().findFragmentById(R.id.mainContainer);
                    if (isScreenActive && getActivity() != null && ((Switch) rootView.findViewById(R.id.autoloadTrafficSwitch)).isChecked() && fragment instanceof CampaignsFragment) {
                        new GetCampaignsEventList().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                    }
                }
            }, 5000);

        }

    }


    private void loadTable() {
        Activity activity = getActivity();
        campaignNames = new ArrayList<>();
        campaignClicks = new ArrayList<>();
        campaignOpened = new ArrayList<>();
        campaignUnsubscribed = new ArrayList<>();
        campaignHardBounces = new ArrayList<>();
        campaignSoftBounces = new ArrayList<>();
        campaignUniqueOpens = new ArrayList<>();
        campaignUniqueClicks = new ArrayList<>();
        campaignCTRs = new ArrayList<>();
        campaignOpenRates = new ArrayList<>();
        campaignSents = new ArrayList<>();

        View rootView = getView();
        if (rootView == null) {
            return;
        }
        campaignNames.add((TextView) rootView.findViewById(R.id.campaignName0));
        campaignClicks.add((TextView) rootView.findViewById(R.id.campaignClicks0));
        campaignOpened.add((TextView) rootView.findViewById(R.id.campaignOpened0));
        campaignUnsubscribed.add((TextView) rootView.findViewById(R.id.campaignUnsubscribed0));
        campaignHardBounces.add((TextView) rootView.findViewById(R.id.campaignHardBounce0));
        campaignSoftBounces.add((TextView) rootView.findViewById(R.id.campaignSoftBounce0));
        campaignUniqueOpens.add((TextView) rootView.findViewById(R.id.campaignUniqueOpened0));
        campaignUniqueClicks.add((TextView) rootView.findViewById(R.id.campaignUniueClicks0));
        campaignCTRs.add((TextView) rootView.findViewById(R.id.campaignCTR0));
        campaignOpenRates.add((TextView) rootView.findViewById(R.id.campaignOpenRate0));
        campaignSents.add((TextView) rootView.findViewById(R.id.campaignSent0));

        campaignNames.add((TextView) rootView.findViewById(R.id.campaignName1));
        campaignClicks.add((TextView) rootView.findViewById(R.id.campaignClicks1));
        campaignOpened.add((TextView) rootView.findViewById(R.id.campaignOpened1));
        campaignUnsubscribed.add((TextView) rootView.findViewById(R.id.campaignUnsubscribed1));
        campaignHardBounces.add((TextView) rootView.findViewById(R.id.campaignHardBounce1));
        campaignSoftBounces.add((TextView) rootView.findViewById(R.id.campaignSoftBounce1));
        campaignUniqueOpens.add((TextView) rootView.findViewById(R.id.campaignUniqueOpened1));
        campaignUniqueClicks.add((TextView) rootView.findViewById(R.id.campaignUniueClicks1));
        campaignCTRs.add((TextView) rootView.findViewById(R.id.campaignCTR1));
        campaignOpenRates.add((TextView) rootView.findViewById(R.id.campaignOpenRate1));
        campaignSents.add((TextView) rootView.findViewById(R.id.campaignSent1));

        campaignNames.add((TextView) rootView.findViewById(R.id.campaignName2));
        campaignClicks.add((TextView) rootView.findViewById(R.id.campaignClicks2));
        campaignOpened.add((TextView) rootView.findViewById(R.id.campaignOpened2));
        campaignUnsubscribed.add((TextView) rootView.findViewById(R.id.campaignUnsubscribed2));
        campaignHardBounces.add((TextView) rootView.findViewById(R.id.campaignHardBounce2));
        campaignSoftBounces.add((TextView) rootView.findViewById(R.id.campaignSoftBounce2));
        campaignUniqueOpens.add((TextView) rootView.findViewById(R.id.campaignUniqueOpened2));
        campaignUniqueClicks.add((TextView) rootView.findViewById(R.id.campaignUniueClicks2));
        campaignCTRs.add((TextView) rootView.findViewById(R.id.campaignCTR2));
        campaignOpenRates.add((TextView) rootView.findViewById(R.id.campaignOpenRate2));
        campaignSents.add((TextView) rootView.findViewById(R.id.campaignSent2));

        campaignNames.add((TextView) rootView.findViewById(R.id.campaignName3));
        campaignClicks.add((TextView) rootView.findViewById(R.id.campaignClicks3));
        campaignOpened.add((TextView) rootView.findViewById(R.id.campaignOpened3));
        campaignUnsubscribed.add((TextView) rootView.findViewById(R.id.campaignUnsubscribed3));
        campaignHardBounces.add((TextView) rootView.findViewById(R.id.campaignHardBounce3));
        campaignSoftBounces.add((TextView) rootView.findViewById(R.id.campaignSoftBounce3));
        campaignUniqueOpens.add((TextView) rootView.findViewById(R.id.campaignUniqueOpened3));
        campaignUniqueClicks.add((TextView) rootView.findViewById(R.id.campaignUniueClicks3));
        campaignCTRs.add((TextView) rootView.findViewById(R.id.campaignCTR3));
        campaignOpenRates.add((TextView) rootView.findViewById(R.id.campaignOpenRate3));
        campaignSents.add((TextView) rootView.findViewById(R.id.campaignSent3));

        campaignNames.add((TextView) rootView.findViewById(R.id.campaignName4));
        campaignClicks.add((TextView) rootView.findViewById(R.id.campaignClicks4));
        campaignOpened.add((TextView) rootView.findViewById(R.id.campaignOpened4));
        campaignUnsubscribed.add((TextView) rootView.findViewById(R.id.campaignUnsubscribed4));
        campaignHardBounces.add((TextView) rootView.findViewById(R.id.campaignHardBounce4));
        campaignSoftBounces.add((TextView) rootView.findViewById(R.id.campaignSoftBounce4));
        campaignUniqueOpens.add((TextView) rootView.findViewById(R.id.campaignUniqueOpened4));
        campaignUniqueClicks.add((TextView) rootView.findViewById(R.id.campaignUniueClicks4));
        campaignCTRs.add((TextView) rootView.findViewById(R.id.campaignCTR4));
        campaignOpenRates.add((TextView) rootView.findViewById(R.id.campaignOpenRate4));
        campaignSents.add((TextView) rootView.findViewById(R.id.campaignSent4));
    }



    public  void initBarChart(BarChart chart) {
        chart.setDescription(new Description());
        chart.getDescription().setEnabled(false);
        chart.setDrawGridBackground(false);
        chart.setDrawGridBackground(false);
        chart.getXAxis().setTypeface(tf);
        chart.getAxisLeft().setDrawGridLines(true);
        chart.getAxisLeft().setGridColor(getResources().getColor(R.color.fancyGray));
        chart.getAxisLeft().setDrawAxisLine(false);
        chart.getAxisLeft().setTypeface(tf);
        chart.getXAxis().setDrawGridLines(false);
        chart.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM);
        chart.getXAxis().setAxisMinimum(-1);
        chart.getAxisRight().setDrawAxisLine(false);
        chart.getAxisRight().setEnabled(false);
        chart.getAxisRight().setDrawGridLines(false);
        chart.setDrawBorders(false);
        chart.getAxisRight().setEnabled(false);

        XAxis xAxis = chart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        chart.setBackgroundColor(Color.parseColor("#FFFFFF"));
        chart.setDrawGridBackground(false);

        chart.setViewPortOffsets(0f, 40f, 0f, 40f);


        chart.getAxisLeft().setPosition(YAxis.YAxisLabelPosition.INSIDE_CHART);
        chart.getAxisLeft().setYOffset(-10);

        Legend l = chart.getLegend();
        l.setEnabled(false);
    }


    public void initLineChart(LineChart chart, Collection<String> labels) {
        chart.setDescription(new Description());
        chart.getDescription().setEnabled(false);
        chart.setDrawGridBackground(false);
        chart.setDrawGridBackground(false);
        chart.getXAxis().setValueFormatter(new IndexAxisValueFormatter(labels));
        chart.getXAxis().setTypeface(tf);
        chart.getAxisLeft().setDrawGridLines(true);
        chart.getAxisLeft().setGridColor(getResources().getColor(R.color.fancyGray));
        chart.getAxisLeft().setDrawAxisLine(false);
        chart.getAxisLeft().setTypeface(tf);
        chart.getXAxis().setDrawGridLines(false);
        chart.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM);
        chart.getXAxis().setLabelCount(3);
        chart.getAxisRight().setDrawAxisLine(false);
        chart.getAxisRight().setEnabled(false);
        chart.getAxisRight().setDrawGridLines(false);
        chart.setDrawBorders(false);
        chart.getAxisRight().setEnabled(false);

        XAxis xAxis = chart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        chart.setBackgroundColor(Color.parseColor("#FFFFFF"));
        chart.setDrawGridBackground(false);

        chart.setViewPortOffsets(0f, 40f, 0f, 40f);


        chart.getAxisLeft().setPosition(YAxis.YAxisLabelPosition.INSIDE_CHART);
        chart.getAxisLeft().setYOffset(-10);

        Legend l = chart.getLegend();
        l.setEnabled(false);


//        l.setVerticalAlignment(Legend.LegendVerticalAlignment.BOTTOM);
//        l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.CENTER);
//        l.setOrientation(Legend.LegendOrientation.VERTICAL);
//        l.setTypeface(tf);
//        l.setTextSize(16);
//        l.setDrawInside(false);


    }


}

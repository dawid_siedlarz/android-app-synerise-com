package com.synerise.synerise.app.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.synerise.synerise.MainActivity;
import com.synerise.synerise.R;
import com.synerise.synerise.app.clientPreview.ClientPreview;
import com.synerise.synerise.app.model.Client;
import com.synerise.synerise.app.model.FilterAttributes;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;



public class ClientRecyclerViewAdapter extends RecyclerView.Adapter<ClientRecyclerViewAdapter.ClientViewHolder> {
    private final Context context;
    private final Activity activity;
    private final ArrayList<Client> values;

    private FilterAttributes.FilterAttribute filterAttribute;


    public ClientRecyclerViewAdapter(Activity activity, ArrayList<Client> values) {
        this.activity = activity;
        this.context = activity.getApplicationContext();
        this.values = values;
    }

    @Override
    public ClientViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_element_client, null);
        ClientViewHolder viewHolder = new ClientViewHolder(view);
        return viewHolder;
    }




    @Override
    public void onBindViewHolder(ClientViewHolder holder, final int position) {

        final Client clientModel = values.get(position);
        holder.mItem = clientModel;
        holder.mDisplayName.setText(clientModel.getDisplayName());
        loadAvatar(holder.mAvatar, clientModel.firstname, clientModel.lastname);
        loadAvatarImage(holder);

        holder.mView.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Fragment newFragment = new ClientPreview();
                Bundle bundle = new Bundle();
                bundle.putParcelable("client", clientModel);
                newFragment.setArguments(bundle);

                MainActivity.addFragment(newFragment);
            }


        });
    }

    @Override
    public int getItemCount() {
        return values.size();
    }

//    @Override
//    public View getView(int position, View convertView, ViewGroup parent) {
//        LayoutInflater inflater = (LayoutInflater) context
//                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//
//        filterAttribute = FilterAttributes.ITEMS.get(position);
//        View rowView = inflater.inflate(R.layout.list_element_filter, parent, false);
//        TextView mTagName = (TextView) rowView.findViewById(R.id.filterName);
//
//        mTagName.setText(filterAttribute.name);
//        return rowView;
//    }

    class ClientViewHolder extends RecyclerView.ViewHolder {
        private TextView mDisplayName;
        private TextView mAvatar;
        private ImageView mAvatarImage;
        private Bitmap avatarBmp;
        private Client mItem;
        public View mView;

        public ClientViewHolder(View view) {
            super(view);
            this.mView = view;
            this.mDisplayName = (TextView) view.findViewById(R.id.clientDisplayName);
            this.mAvatar = (TextView) view.findViewById(R.id.clientAvatar);
            this.mAvatarImage = (ImageView) view.findViewById(R.id.clientAvatarImage);
        }
    }


    private void loadAvatar(TextView mAvatar, String firstname, String lastname) {
        if (firstname == null || firstname.isEmpty()) {
            mAvatar.setBackgroundResource(R.drawable.avatar_default);
        } else {
            char letter = firstname.charAt(0);
            switch (letter) {
                case 'A':
                    mAvatar.setBackgroundResource(R.drawable.avatar_a);
                    break;
                case 'B':
                    mAvatar.setBackgroundResource(R.drawable.avatar_b);
                    break;
                case 'C':
                    mAvatar.setBackgroundResource(R.drawable.avatar_c);
                    break;
                case 'D':
                    mAvatar.setBackgroundResource(R.drawable.avatar_d);
                    break;
                case 'E':
                    mAvatar.setBackgroundResource(R.drawable.avatar_e);
                    break;
                case 'F':
                    mAvatar.setBackgroundResource(R.drawable.avatar_f);
                    break;
                case 'G':
                    mAvatar.setBackgroundResource(R.drawable.avatar_g);
                    break;
                case 'H':
                    mAvatar.setBackgroundResource(R.drawable.avatar_h);
                    break;
                case 'I':
                    mAvatar.setBackgroundResource(R.drawable.avatar_i);
                    break;
                case 'J':
                    mAvatar.setBackgroundResource(R.drawable.avatar_j);
                    break;
                case 'K':
                    mAvatar.setBackgroundResource(R.drawable.avatar_k);
                    break;
                case 'L':
                    mAvatar.setBackgroundResource(R.drawable.avatar_l);
                    break;
                case 'M':
                    mAvatar.setBackgroundResource(R.drawable.avatar_m);
                    break;
                case 'N':
                    mAvatar.setBackgroundResource(R.drawable.avatar_n);
                    break;
                case 'o':
                    mAvatar.setBackgroundResource(R.drawable.avatar_o);
                    break;
                case 'p':
                    mAvatar.setBackgroundResource(R.drawable.avatar_p);
                    break;
                case 'r':
                    mAvatar.setBackgroundResource(R.drawable.avatar_r);
                    break;
                case 's':
                    mAvatar.setBackgroundResource(R.drawable.avatar_s);
                    break;
                case 't':
                    mAvatar.setBackgroundResource(R.drawable.avatar_t);
                    break;
                case 'u':
                    mAvatar.setBackgroundResource(R.drawable.avatar_u);
                    break;
                case 'v':
                    mAvatar.setBackgroundResource(R.drawable.avatar_v);
                    break;
                case 'w':
                    mAvatar.setBackgroundResource(R.drawable.avatar_w);
                    break;
                case 'x':
                    mAvatar.setBackgroundResource(R.drawable.avatar_x);
                    break;
                case 'y':
                    mAvatar.setBackgroundResource(R.drawable.avatar_y);
                    break;
                case 'z':
                    mAvatar.setBackgroundResource(R.drawable.avatar_z);
                    break;
                default:
                    mAvatar.setBackgroundResource(R.drawable.avatar_default);
            }
        }

        String avatar = "" + (firstname == null || firstname.isEmpty() ? "AA" : firstname.charAt(0)) + (lastname == null || lastname.isEmpty() ? "" : lastname.charAt(0));
        mAvatar.setText(avatar);
    }


    private class LoadImageInBackground extends AsyncTask<ClientRecyclerViewAdapter.ClientViewHolder, Void, Void> {

        @Override
        protected Void doInBackground(ClientViewHolder... params) {

            final ClientViewHolder holder = params[0];
            Bitmap bmp = null;

       String url = "";

            Client clientModel = holder.mItem;
                try {
//                    if(clientModel.externalAvatarUrl!= null && clientModel.externalAvatarUrl.compareToIgnoreCase("null") != 0){
//                        url = clientModel.externalAvatarUrl;
//                    } else {
                        url = clientModel.avatarUrl;
//                    }
                    bmp = BitmapFactory.decodeStream(new URL(url).openConnection().getInputStream());

// bmp = Bitmap.createScaledBitmap(bmp, 150, 150, false);
                } catch (MalformedURLException e) {
                    Log.v("SYNERISE", "Malformed URL " + url);
                } catch (IOException e) {
                    e.printStackTrace();
                }

            if ( bmp != null) {
                bmp = getCroppedBitmap(bmp);
            }
            final Bitmap finalBmp = bmp;

            clientModel.avatarBmp = finalBmp;

            if (activity != null) {

                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try{
                            holder.mAvatarImage.setImageBitmap(finalBmp);
                        }
                        catch (IndexOutOfBoundsException e){
                            //ignore
                        }
                    }
                });

            }
            return null;
        }

    }


    private void loadAvatarImage(ClientViewHolder holder){
        if (holder.mItem.avatarBmp != null){
            holder.mAvatarImage.setImageBitmap(holder.mItem.avatarBmp);
        }
        else {
            new LoadImageInBackground().execute(holder);
        }
    }


    private Bitmap getCroppedBitmap(Bitmap bitmap) {
        Bitmap output = Bitmap.createBitmap(bitmap.getWidth(),
                bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(output);

        final int color = 0xff424242;
        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());

        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        // canvas.drawRoundRect(rectF, roundPx, roundPx, paint);
        canvas.drawCircle(bitmap.getWidth() / 2, bitmap.getHeight() / 2,
                bitmap.getWidth() / 2, paint);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);
        //Bitmap _bmp = Bitmap.createScaledBitmap(output, 60, 60, false);
        //return _bmp;
        return output;
    }


    }

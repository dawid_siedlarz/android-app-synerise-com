package com.synerise.synerise.app;

import android.support.v4.app.Fragment;
import android.util.Log;

import com.synerise.synerise.MainActivity;

public abstract class ActionBarConfigurableFragment extends Fragment {
    abstract public void setUpActionBar();

    @Override
    public void onResume() {
        Log.v("ActionBarConfig", this.getClass().getCanonicalName());
        super.onResume();
        if (MainActivity.searchItem !=null) {
            MainActivity.searchItem.setVisible(false);
        }

        setUpActionBar();
    }
}

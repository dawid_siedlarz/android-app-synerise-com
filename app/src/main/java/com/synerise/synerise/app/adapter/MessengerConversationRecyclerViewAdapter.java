package com.synerise.synerise.app.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.synerise.synerise.R;
import com.synerise.synerise.app.controller.MessengerUserController;
import com.synerise.synerise.messenger.model.MessengerClient;
import com.synerise.synerise.messenger.model.MessengerMessage;
import com.synerise.synerise.messenger.model.MessengerRoom;

import java.util.ArrayList;


public class MessengerConversationRecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private final Context context;
    private final Activity activity;
    private final ArrayList<MessengerMessage> values;
    private MessengerUserController messengerUserController;
    private MessengerRoom room;

    private final int CLIENT = 0, VENDOR = 1;


    public MessengerConversationRecyclerViewAdapter(Activity activity, ArrayList<MessengerMessage> values, MessengerRoom room) {
        this.activity = activity;
        this.context = activity.getApplicationContext();
        this.values = values;
        this.messengerUserController = new MessengerUserController(activity);
        this.room = room;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        if (viewType == CLIENT) {
            View v1 = inflater.inflate(R.layout.list_element_messenger_message_client, parent, false);
            viewHolder = new ClientMessageViewHolder(v1);
        } else {
            View v2 = inflater.inflate(R.layout.list_element_messenger_message_vendor, parent, false);
            viewHolder = new VendorMessageViewHolder(v2);
        }

        return viewHolder;
    }

    @Override
    public int getItemViewType(int position) {
        if (this.values.get(position).getAuthor() instanceof MessengerClient) {
            return CLIENT;
        }
        return VENDOR;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

        final MessengerMessage messageModel = this.values.get(position);


        if (holder.getItemViewType() == CLIENT) {
            ClientMessageViewHolder clientMessageViewHolder = (ClientMessageViewHolder) holder;
            clientMessageViewHolder.mMessage = messageModel;
            clientMessageViewHolder.mTime.setText(messageModel.getFormattedTime());

            clientMessageViewHolder.mBody.removeAllViewsInLayout();
            ViewGroup parent = (ViewGroup) messageModel.getMessageBody().getBody().getParent();
            if (parent != null) {
                parent.removeAllViewsInLayout();
            }
            clientMessageViewHolder.mBody.addView(messageModel.getMessageBody().getBody());

            messengerUserController.setClientMessageViewHolder(clientMessageViewHolder, values, (MessengerClient) room.getOwner());

        } else {
            VendorMessageViewHolder vendorMessageViewHolder = (VendorMessageViewHolder) holder;
            vendorMessageViewHolder.mMessage = messageModel;
            vendorMessageViewHolder.mTime.setText(messageModel.getFormattedTime());
            vendorMessageViewHolder.mBody.removeAllViewsInLayout();
            ViewGroup parent = (ViewGroup) messageModel.getMessageBody().getBody().getParent();
            if (parent != null) {
                parent.removeAllViewsInLayout();
            }
            vendorMessageViewHolder.mBody.addView(messageModel.getMessageBody().getBody());
        }


//        holder.mView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Bundle bundle = new Bundle();
//                bundle.putString(ConversationFragment.ARG_ROOM_ID, roomModel.getId());
//                MainActivity.fragment = new ConversationFragment();
//                MainActivity.fragment.setArguments(bundle);
//
//                ((ViewGroup) rootView.findViewById(R.id.mainContainer)).removeAllViewsInLayout();
//
//                FragmentManager fragmentManager = ((MainActivity)activity).getSupportFragmentManager();
//                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
//                fragmentTransaction.replace(R.id.mainContainer, MainActivity.fragment);
//                fragmentTransaction.commit();
//            }
//        });
    }

    @Override
    public int getItemCount() {
        return this.values.size();
    }

    public class ClientMessageViewHolder extends RecyclerView.ViewHolder {
        private MessengerMessage mMessage;
        private TextView mTime;
        private ViewGroup mBody;
        private ImageView mOwnerAvatar;
        private TextView mOwnerAvatarInitials;
        private RelativeLayout mOwnerAvatarBox;
        private View mView;

        ClientMessageViewHolder(View view) {
            super(view);
            this.mView = view;
            this.mTime = (TextView) view.findViewById(R.id.messageTime);
            this.mBody = (ViewGroup) view.findViewById(R.id.messageBody);
            this.mOwnerAvatar = (ImageView) view.findViewById(R.id.ownerAvatar);
            this.mOwnerAvatarInitials = (TextView) view.findViewById(R.id.ownerAvatarInitials);
            this.mOwnerAvatarBox = (RelativeLayout) view.findViewById(R.id.ownerAvatarBox);
        }

        public MessengerMessage getmMessage() {
            return mMessage;
        }

        public void setmMessage(MessengerMessage mMessage) {
            this.mMessage = mMessage;
        }

        public TextView getmTime() {
            return mTime;
        }

        public void setmTime(TextView mTime) {
            this.mTime = mTime;
        }

        public ViewGroup getmBody() {
            return mBody;
        }

        public void setmBody(ViewGroup mBody) {
            this.mBody = mBody;
        }

        public ImageView getmOwnerAvatar() {
            return mOwnerAvatar;
        }

        public void setmOwnerAvatar(ImageView mOwnerAvatar) {
            this.mOwnerAvatar = mOwnerAvatar;
        }

        public TextView getmOwnerAvatarInitials() {
            return mOwnerAvatarInitials;
        }

        public void setmOwnerAvatarInitials(TextView mOwnerAvatarInitials) {
            this.mOwnerAvatarInitials = mOwnerAvatarInitials;
        }

        public View getmView() {
            return mView;
        }

        public void setmView(View mView) {
            this.mView = mView;
        }

        public RelativeLayout getmOwnerAvatarBox() {
            return mOwnerAvatarBox;
        }

        public void setmOwnerAvatarBox(RelativeLayout mOwnerAvatarBox) {
            this.mOwnerAvatarBox = mOwnerAvatarBox;
        }
    }


    private class VendorMessageViewHolder extends RecyclerView.ViewHolder {
        private MessengerMessage mMessage;
        private TextView mTime;
        private ViewGroup mBody;
        private View mView;

        VendorMessageViewHolder(View view) {
            super(view);
            this.mView = view;
            this.mTime = (TextView) view.findViewById(R.id.messageTime);
            this.mBody = (ViewGroup) view.findViewById(R.id.messageBody);
        }
    }


//    private class GetName extends AsyncTask<MessageViewHolder, Void, Void> {
//        @Override
//        protected Void doInBackground(MessageViewHolder... params) {
//            final MessageViewHolder holder = params[0];
//            final String name = holder.mMessage.getAuthor().getNameFromApi();
//            activity.runOnUiThread(new Runnable() {
//                @Override
//                public void run() {
//                    holder.mAuthor.setText(name);
//                }
//            });
//
//            return null;
//        }
//    }
}

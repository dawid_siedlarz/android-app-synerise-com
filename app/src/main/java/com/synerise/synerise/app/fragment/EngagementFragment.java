package com.synerise.synerise.app.fragment;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter;
import com.synerise.sdk.tracking.Tracker;
import com.synerise.sdk.tracking.TrackerParams;
import com.synerise.synerise.MainActivity;
import com.synerise.synerise.R;
import com.synerise.synerise.SyneriseOnFragmentInteractionListener;
import com.synerise.synerise.app.syneutil.BarChartMarkerView;
import com.synerise.synerise.app.syneutil.SyneHttpCacheHandler;
import com.synerise.synerise.app.syneview.LineChartMarkerView;
import com.synerise.synerise.app.syneview.SyneBarChart;
import com.synerise.synerise.app.syneview.SyneChartTitle;
import com.synerise.synerise.app.syneview.SyneComplexLineChart;
import com.synerise.synerise.app.syneview.SyneLineChart;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;


public class EngagementFragment extends Fragment {

    private Calendar dateFrom;
    private Calendar dateTo;
    private String peridod;
    private String groupDate = "yyyy-MM-dd";
    private String minPeriod = "DD";
    private String dateFateFormat = "yyyy-MM-dd";
    private String dateMinPeriod = "DD";
    private String client = "all";

    private TextView hour;
    private TextView day;
    private TextView week;
    private TextView month;
    private TextView year;

    private View hourLine;
    private View dayLine;
    private View weekLine;
    private View monthLine;
    private View yearLine;

    DecimalFormat formatter = new DecimalFormat("###,###,###");

    private SyneLineChart scoringChart;
    private SyneChartTitle scoringTitle;

    private SyneBarChart top10ClientsChart;
    private SyneChartTitle top10ClientsTitle;

    private SyneBarChart top10CategoriesChart;
    private SyneChartTitle top10CategoriesTitle;


    boolean isScreenActive = false;

    private boolean isScoringInitialized = false;
    private boolean isTop10ClientsInitialized = false;
    private boolean isTop10CategoriesInitialized = false;


    private SyneriseOnFragmentInteractionListener mListener;

    public EngagementFragment() {
        // Required empty public constructor
    }

    @Override
    public void onResume() {
        super.onResume();
        isScreenActive = true;
        loadCharts();
    }

    @Override
    public void onPause() {
        super.onPause();
        isScreenActive = false;
    }

    @Override
    public void onStop() {
        super.onStop();
        isScreenActive = false;
    }


    public static EngagementFragment newInstance(String param1, String param2) {
        EngagementFragment fragment = new EngagementFragment();

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        dateTo = Calendar.getInstance();
        dateFrom = Calendar.getInstance();
        dateFrom.add(Calendar.YEAR, -1);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_engagement, container, false);
    }


    private void loadCharts() {
        new GetScoring().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        new GetTop10Clients().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        new GetTop10Categories().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Tracker.trackScreen("Dashboard engagement", new TrackerParams[]{});

        ActionBar actionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        actionBar.show();
        actionBar.setTitle(MainActivity.businessProfileName != null ? MainActivity.businessProfileName : "Synerise");
        actionBar.setSubtitle("Engagement");
        actionBar.setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.colorPrimary)));

        loadCharts();

        Activity activity = getActivity();

        View rootView = getView();
        if (rootView == null) {
            return;
        }
        scoringTitle = (SyneChartTitle) rootView.findViewById(R.id.scoringTitle);
        scoringChart = ((SyneLineChart) rootView.findViewById(R.id.scoringChart));

        top10ClientsTitle = (SyneChartTitle) rootView.findViewById(R.id.top10ClientsTitle);
        top10ClientsChart = (SyneBarChart) rootView.findViewById(R.id.top10ClientsChart);

        top10CategoriesTitle = (SyneChartTitle) rootView.findViewById(R.id.top10CategoriesTitle);
        top10CategoriesChart = (SyneBarChart) rootView.findViewById(R.id.top10CategoriesChart);

        final SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");

        final TextView dateFromTextView = (TextView) rootView.findViewById(R.id.dateFrom);

        dateFromTextView.setText(format.format(dateFrom.getTime()));

        final DatePickerDialog dateFromPickerDialog = new DatePickerDialog(getContext(), new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                dateFrom.set(year, monthOfYear, dayOfMonth);
                dateFromTextView.setText(format.format(dateFrom.getTime()));
                loadCharts();
            }

        }, dateFrom.get(Calendar.YEAR), dateFrom.get(Calendar.MONTH), dateFrom.get(Calendar.DAY_OF_MONTH));


        dateFromTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                dateFromPickerDialog.show();
            }
        });

        TextView dateToTextView = (TextView) rootView.findViewById(R.id.dateTo);

        final DatePickerDialog dateToPickerDialog = new DatePickerDialog(getContext(), new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                dateFrom.set(year, monthOfYear, dayOfMonth);
                dateFromTextView.setText(format.format(dateFrom.getTime()));
                loadCharts();
            }

        }, dateFrom.get(Calendar.YEAR), dateFrom.get(Calendar.MONTH), dateFrom.get(Calendar.DAY_OF_MONTH));

        dateToTextView.setText(format.format(dateTo.getTime()));

        dateToTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                dateToPickerDialog.show();
            }
        });


        hour = (TextView) rootView.findViewById(R.id.datePickerHour);
        day = (TextView) rootView.findViewById(R.id.datePickerDay);
        week = (TextView) rootView.findViewById(R.id.datePickerWeek);
        month = (TextView) rootView.findViewById(R.id.datePickerMonth);
        year = (TextView) rootView.findViewById(R.id.datePickerYear);

        hourLine = rootView.findViewById(R.id.datePickerHourLine);
        dayLine = rootView.findViewById(R.id.datePickerDayLine);
        weekLine = rootView.findViewById(R.id.datePickerWeekLine);
        monthLine = rootView.findViewById(R.id.datePickerMonthLine);
        yearLine = rootView.findViewById(R.id.datePickerYearLine);

        ((RelativeLayout) rootView.findViewById(R.id.datePickerWeekBox)).setVisibility(View.GONE);


        setChoosed("day");

        hour.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                groupDate = "yyyy-MM-dd+HH";
                minPeriod = "hh";

                loadCharts();

                setChoosed("hour");
                return true;
            }
        });


        day.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                groupDate = "yyyy-MM-dd";
                minPeriod = "DD";

                loadCharts();

                setChoosed("day");
                return true;
            }
        });

        week.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                //// TODO: 19.02.2017 ogarnąć
                groupDate = "yyyy-MM";
                minPeriod = "DD";

                loadCharts();

                setChoosed("week");

                return true;
            }
        });


        month.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                groupDate = "yyyy-MM";
                minPeriod = "DD";

                loadCharts();

                setChoosed("month");

                return true;
            }
        });


        year.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                groupDate = "yyyy";
                minPeriod = "MM";

                loadCharts();

                setChoosed("year");
                return true;
            }
        });

    }

    void releaseButtons() {
        hour.setPressed(false);
        day.setPressed(false);
        month.setPressed(false);
        year.setPressed(false);

    }


    private void setChoosed(String type) {
        int gray = getResources().getColor(R.color.fancyGray);
        int blue = getResources().getColor(R.color.fancyBlue);
        hourLine.setBackgroundColor(gray);
        dayLine.setBackgroundColor(gray);
        weekLine.setBackgroundColor(gray);
        monthLine.setBackgroundColor(gray);
        yearLine.setBackgroundColor(gray);

        hour.setTextColor(gray);
        day.setTextColor(gray);
        week.setTextColor(gray);
        month.setTextColor(gray);
        year.setTextColor(gray);

        switch (type) {
            case "hour":
                hour.setTextColor(blue);
                hourLine.setBackgroundColor(blue);
                break;
            case "day":
                day.setTextColor(blue);
                dayLine.setBackgroundColor(blue);
                break;
            case "week":
                week.setTextColor(blue);
                weekLine.setBackgroundColor(blue);
                break;
            case "month":
                month.setTextColor(blue);
                monthLine.setBackgroundColor(blue);
                break;
            case "year":
                year.setTextColor(blue);
                yearLine.setBackgroundColor(blue);
                break;
        }
    }

    private String getParams() {
        return "?groupDate=" + groupDate + "&dateTo=" + dateTo.getTimeInMillis() / 1000 + "&dateFrom=" + dateFrom.getTimeInMillis() / 1000 + "&minPeriod=" +
                minPeriod + "&date[dateFormat]=" + dateFateFormat + "&date[dateTo]=" + dateTo.getTimeInMillis() / 1000 + "&date[dateFrom]=" + dateFrom.getTimeInMillis() / 1000 +
                "&date[minPeriod]=" + dateMinPeriod + "&client=" + client;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof SyneriseOnFragmentInteractionListener) {
            mListener = (SyneriseOnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    private class GetScoring extends AsyncTask<Void, Void, String> {
        protected String doInBackground(Void... params) {
            try {
                String url = "https://app.synerise.com/dashboard/stats-engagement/total/all/";

                HttpGet httpget = new HttpGet(url + dateFrom.getTimeInMillis() / 1000 + "/" + dateTo.getTimeInMillis() / 1000 + getParams());

                SyneHttpCacheHandler handler = new SyneHttpCacheHandler();
                boolean cacheUsed = false;
                if (!isScoringInitialized) {
                    cacheUsed = MainActivity.cacheString.contains(SyneHttpCacheHandler.getCacheKeyFromUrl(createCacheKey(url)));
                }
                final String body = handler.executeWithCacheKey(httpget, createCacheKey(url), !isScoringInitialized, true);

                if (body == null) {
                    return "not ok";
                }

                JSONArray jsonArray = null;

                try {
                    jsonArray = new JSONArray(body);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                final JSONArray finalJSONArray = jsonArray;


                final ArrayList<String> labels = new ArrayList<String>();
                final ArrayList<Entry> values = new ArrayList<Entry>();

                float aggregationValueAll = 0.0f;

                float aggregationValue = 0.0f;

                try {
                    int j = 0;
                    for (int i = 0; i < finalJSONArray.length(); i++) {
                        JSONObject jsonObject = finalJSONArray.getJSONObject(i);

                        aggregationValue = (float) jsonObject.getDouble("value");

                        aggregationValueAll += aggregationValue;

                        values.add(new Entry(j, aggregationValue));

                        j++;
                        labels.add(jsonObject.getString("date"));

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                LineDataSet valueSet = new LineDataSet(values, "scoring");

                int blue = Color.parseColor("#00bdf4");

                valueSet.setColor(blue);

                SyneComplexLineChart.initLineDataSet(valueSet);


                final LineData aggregationValueData = new LineData(valueSet);


                aggregationValueData.setDrawValues(false);

                Activity activity = getActivity();
                if (activity != null) {

                    final float finalAggregationValueAll = aggregationValueAll;
                    final boolean finalCacheUsed = cacheUsed;
                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            scoringTitle.setFloatCounter(finalAggregationValueAll);

                            scoringChart.setData(aggregationValueData);
                            scoringChart.getXAxis().setValueFormatter(new IndexAxisValueFormatter(labels));


                            LineChartMarkerView mv = new LineChartMarkerView(getContext(), R.layout.custom_marker_view);
                            mv.setChartView(scoringChart); // For bounds control
                            mv.setDates(labels);
                            mv.setLabels(new String[]{"Points"});
                            scoringChart.setMarker(mv);

                            if (!isTop10ClientsInitialized) {
                                scoringChart.invalidate();
                                if (finalCacheUsed) {
                                    scoringChart.setAlpha(0.4f);
                                } else {
                                    scoringChart.animateY(1500);
                                }
                                isTop10ClientsInitialized = true;
                            } else {
                                scoringChart.setAlpha(1f);
                                scoringChart.notifyDataSetChanged();
                            }
                        }
                    });
                }


            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (NullPointerException e) {

            } catch (IllegalStateException e) {

            }
            return "ok";
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            Handler myHandler = new Handler();
            myHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    Fragment fragment = getActivity().getSupportFragmentManager().findFragmentById(R.id.mainContainer);
                    if (isScreenActive && getActivity() != null && fragment instanceof EngagementFragment) {
                        new GetScoring().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                    }
                }
            }, 3000);
        }
    }


    private class GetTop10Clients extends AsyncTask<Void, Void, String> {
        protected String doInBackground(Void... params) {
            try {

                String url = "https://app.synerise.com/dashboard/stats-engagement/top-clients/all/";

                HttpGet httpget = new HttpGet(url + dateFrom.getTimeInMillis() / 1000 + "/" + dateTo.getTimeInMillis() / 1000 + getParams());

                SyneHttpCacheHandler handler = new SyneHttpCacheHandler();
                boolean cacheUsed = false;
                if (!isTop10CategoriesInitialized) {
                    cacheUsed = MainActivity.cacheString.contains(SyneHttpCacheHandler.getCacheKeyFromUrl(createCacheKey(url)));
                }
                final String body = handler.executeWithCacheKey(httpget, createCacheKey(url), !isTop10CategoriesInitialized, true);

                if (body == null) {
                    return "not ok";
                }


                JSONArray jsonArray = null;

                try {
                    jsonArray = new JSONArray(body);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                final JSONArray finalJSONArray = jsonArray;


                final ArrayList<String> labels = new ArrayList<String>();
                final ArrayList<BarEntry> values = new ArrayList<BarEntry>();


                float valuesAll = 0.0f;

                float value = 0.0f;

                try {
                    int j = 0;
                    for (int i = 0; i < finalJSONArray.length(); i++) {
                        JSONObject jsonObject = finalJSONArray.getJSONObject(i);
                        value = (float) jsonObject.getDouble("value");

                        valuesAll += value;

                        values.add(new BarEntry(j, value));

                        j++;
                        labels.add(jsonObject.getString("title"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                BarDataSet valuesSet = new BarDataSet(values, "countOnTriggerValue");


                valuesSet.setColor(Color.parseColor("#00bdf4"));
                valuesSet.setDrawValues(false);

                final BarData AutomationPerformanceData = new BarData(valuesSet);

                Activity activity = getActivity();
                if (activity != null) {


                    final float finalValuesAll = valuesAll;
                    final boolean finalCacheUsed = cacheUsed;
                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            top10ClientsTitle.setFloatCounter(finalValuesAll);

                            top10ClientsChart.setData(AutomationPerformanceData);
                            top10ClientsChart.getXAxis().setValueFormatter(new IndexAxisValueFormatter(labels));

                            BarChartMarkerView mv = new BarChartMarkerView(getContext(), R.layout.custom_marker_view);
                            mv.setChartView(top10ClientsChart); // For bounds control
                            mv.setDates(labels);
                            mv.setLabels(new String[]{"Points"});
                            top10ClientsChart.setMarker(mv);

                            if (!isTop10CategoriesInitialized) {

                                top10ClientsChart.getXAxis().setAxisMinimum(-3f);
                                top10ClientsChart.getXAxis().setAxisMaximum(labels.size() + 1);
                                top10ClientsChart.invalidate();
                                if (finalCacheUsed) {
                                    top10ClientsChart.setAlpha(0.4f);
                                } else {
                                    top10ClientsChart.animateY(1500);
                                }
                                isTop10CategoriesInitialized = true;
                            } else {
                                top10ClientsChart.setAlpha(1f);
                                top10ClientsChart.notifyDataSetChanged();
                            }
                        }
                    });
                }
            } catch (IOException e) {
                e.printStackTrace();
            } catch (NullPointerException | IllegalStateException e) {

            }
            return "ok";
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            Handler myHandler = new Handler();
            myHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    Fragment fragment = getActivity().getSupportFragmentManager().findFragmentById(R.id.mainContainer);
                    if (isScreenActive && getActivity() != null && fragment instanceof EngagementFragment) {
                        new GetTop10Clients().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                    }
                }
            }, 3000);
        }
    }


    private class GetTop10Categories extends AsyncTask<Void, Void, String> {
        protected String doInBackground(Void... params) {
            try {

                String url = "https://app.synerise.com/dashboard/stats-engagement/top-categories/all/";
                Log.v("DAWID", url);
                HttpGet httpget = new HttpGet(url + dateFrom.getTimeInMillis() / 1000 + "/" + dateTo.getTimeInMillis() / 1000 + getParams());

                SyneHttpCacheHandler handler = new SyneHttpCacheHandler();
                boolean cacheUsed = false;
                if (!isTop10ClientsInitialized) {
                    cacheUsed = MainActivity.cacheString.contains(SyneHttpCacheHandler.getCacheKeyFromUrl(createCacheKey(url)));
                }

                final String body = handler.executeWithCacheKey(httpget, createCacheKey(url), !isTop10ClientsInitialized, true);
                Log.v("DAWID", body);

                if (body == null) {
                    return "not ok";
                }

                JSONArray jsonArray = null;

                try {
                    jsonArray = new JSONArray(body);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                final JSONArray finalJSONArray = jsonArray;


                final ArrayList<String> labels = new ArrayList<String>();
                final ArrayList<BarEntry> values = new ArrayList<BarEntry>();


                float valuesAll = 0.0f;

                float value = 0.0f;


                try {
                    int j = 0;
                    for (int i = 0; i < finalJSONArray.length(); i++) {
                        JSONObject jsonObject = finalJSONArray.getJSONObject(i);

                        value = (float) jsonObject.getDouble("value");

                        valuesAll += value;

                        values.add(new BarEntry(j, value));

                        j++;
                        labels.add(jsonObject.getString("title"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                BarDataSet valuesSet = new BarDataSet(values, "countOnTriggerValue");
                valuesSet.setDrawValues(false);


                valuesSet.setColor(Color.parseColor("#00bdf4"));


                final BarData AutomationPerformanceData = new BarData(valuesSet);


                Activity activity = getActivity();
                if (activity != null) {


                    final float finalValuesAll = valuesAll;
                    final boolean finalCacheUsed = cacheUsed;
                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            top10CategoriesTitle.setFloatCounter(finalValuesAll);

                            top10CategoriesChart.setData(AutomationPerformanceData);

                            top10CategoriesChart.getXAxis().setValueFormatter(new IndexAxisValueFormatter(labels));

                            BarChartMarkerView mv = new BarChartMarkerView(getContext(), R.layout.custom_marker_view);
                            mv.setChartView(top10CategoriesChart); // For bounds control
                            mv.setDates(labels);
                            mv.setLabels(new String[]{"Points"});
                            top10CategoriesChart.setMarker(mv);

                            if (!isScoringInitialized) {
                                top10CategoriesChart.getXAxis().setAxisMinimum(-3f);
                                top10CategoriesChart.getXAxis().setAxisMaximum(labels.size() + 1);
                                top10CategoriesChart.getAxisLeft().setAxisMinimum(0);
                                top10CategoriesChart.invalidate();
                                if (finalCacheUsed) {
                                    top10CategoriesChart.setAlpha(0.4f);
                                } else {
                                    top10CategoriesChart.animateY(1500);
                                }
                                isScoringInitialized = true;
                            } else {
                                top10CategoriesChart.setAlpha(1f);
                                top10CategoriesChart.notifyDataSetChanged();
                            }

                        }
                    });
                }
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (NullPointerException e) {

            } catch (IllegalStateException e) {

            }

            return "ok";
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            Handler myHandler = new Handler();
            myHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    Fragment fragment = getActivity().getSupportFragmentManager().findFragmentById(R.id.mainContainer);
                    if (isScreenActive && getActivity() != null && fragment instanceof EngagementFragment) {
                        new GetTop10Categories().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                    }
                }
            }, 3000);
        }
    }

    private String createCacheKey(String key) {
        return "?groupDate=" + groupDate +
                minPeriod + "&date[dateFormat]=" + dateFateFormat +
                "&date[minPeriod]=" + dateMinPeriod + "&client=" + client;
    }
}

package com.synerise.synerise.app.adapter;

import android.app.Activity;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.synerise.synerise.app.fragment.BusinessProfileFragment;
import com.synerise.synerise.MainActivity;
import com.synerise.synerise.R;
import com.synerise.synerise.SyneriseOnFragmentInteractionListener;
import com.synerise.synerise.app.model.BusinessProfile;
import com.synerise.synerise.app.syneutil.SyneHttpCacheHandler;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.List;


public class MyBusinessProfileRecyclerViewAdapter extends RecyclerView.Adapter<MyBusinessProfileRecyclerViewAdapter.ViewHolder> {

    private final List<BusinessProfile.BusinessProfileEntity> mValues;
    private final SyneriseOnFragmentInteractionListener mListener;
    private final BusinessProfileFragment fragment;

    public MyBusinessProfileRecyclerViewAdapter(List<BusinessProfile.BusinessProfileEntity> items, SyneriseOnFragmentInteractionListener listener, BusinessProfileFragment fragment) {
        mValues = items;
        mListener = listener;
        this.fragment = fragment;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_businessprofile, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        holder.mItem = mValues.get(position);

        holder.name.setText(holder.mItem.name);

//        holder.createdOn.setText(holder.mItem.createdOn);

        new LoadImageInBackground().execute(holder);

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity) fragment.getActivity()).chooseProfile(holder.mItem.id, holder.mItem.imgSrc, holder.mItem.name);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final ImageView image;
        public final TextView name;
        //        public final TextView createdOn;
        public BusinessProfile.BusinessProfileEntity mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            image = (ImageView) view.findViewById(R.id.businessProfileLogo);
            name = (TextView) view.findViewById(R.id.businessProfileName);
//            createdOn = (TextView) view.findViewById(R.id.createdOn);
        }


    }

    private class LoadImageInBackground extends AsyncTask<ViewHolder, Void, Void> {

        @Override
        protected Void doInBackground(ViewHolder... params) {

            final ViewHolder holder = params[0];
            Bitmap bmp = null;

            if (holder.mItem.img != null) {
                bmp = holder.mItem.img;
            } else {
                try {
                    bmp = SyneHttpCacheHandler.getBmp(holder.mItem.imgSrc, true);
// bmp = Bitmap.createScaledBitmap(bmp, 150, 150, false);
                } catch (MalformedURLException e) {
                    Log.v("SYNERISE", "Malformed URL " + holder.mItem.imgSrc);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            final Bitmap finalBmp = bmp;

            Activity activity = fragment.getActivity();
            if (activity != null) {
                holder.mItem.img = finalBmp;

                fragment.getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        holder.image.setImageBitmap(finalBmp);
                    }
                });

            }
            return null;
        }

    }


}


package com.synerise.synerise.app.conversionCategory;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.synerise.synerise.R;

public class ChooseConversionCategory extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_conversion_category);

        Button chooseCampaign = (Button)findViewById(R.id.chooseCategoryCampaigns);
        chooseCampaign.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), ChooseConversionCategoryCampaigns.class);
                startActivity(intent);
            }
        });
        Button chooseTag = (Button)findViewById(R.id.chooseCategoryTags);
        chooseTag.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), ChooseConversionCategoryTags.class);
                startActivity(intent);
            }
        });
        Button chooseSegment = (Button)findViewById(R.id.chooseCategorySegments);
        chooseSegment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Intent intent = new Intent(getApplicationContext(), ChooseConversionCategorySegments.class);
                startActivity(intent);
            }
        });
    }
}

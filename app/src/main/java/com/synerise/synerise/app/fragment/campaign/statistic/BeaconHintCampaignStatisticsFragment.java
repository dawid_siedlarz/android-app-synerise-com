package com.synerise.synerise.app.fragment.campaign.statistic;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.highlight.Highlight;
import com.synerise.synerise.MainActivity;
import com.synerise.synerise.R;
import com.synerise.synerise.app.ActionBarConfigurableFragment;
import com.synerise.synerise.app.model.campaign.BeaconHintCampaign;
import com.synerise.synerise.app.syneutil.SyneValueFormatter;
import com.synerise.synerise.app.syneview.SyneChartTitle;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class BeaconHintCampaignStatisticsFragment extends ActionBarConfigurableFragment {
    public static final String ARG_CAMPAIGN = "campaign";
    private BeaconHintCampaign beaconHintCampaign;
    PieChart beaconHintStatisticCtrChart;
    SyneChartTitle beaconHintStatisticCtrTitle;

    private TextView beaconHintCampaignStatisticTitle;
    private TextView beaconHintCampaignStatisticStatus;
    private TextView beaconHintCampaignStatisticRecipients;
    private TextView beaconHintCampaignStatisticSent;
    private TextView beaconHintStatisticDelivered;
    private TextView beaconHintStatisticRead;
    private TextView beaconHintStatisticDateSend;

    public BeaconHintCampaignStatisticsFragment() {
        // Required empty public constructor
    }


    @Override
    public void setUpActionBar() {
        ActionBar actionBar = (ActionBar) ((AppCompatActivity) getActivity()).getSupportActionBar();
        if (actionBar != null) {
            actionBar.show();
            actionBar.setTitle(MainActivity.businessProfileName != null ? MainActivity.businessProfileName : "Synerise");
            actionBar.setSubtitle("Beacon hint campaign");
            actionBar.setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.colorPrimaryCampaigns)));
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                Window window = getActivity().getWindow();
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                window.setStatusBarColor(getResources().getColor(R.color.colorPrimaryDarkCampaigns));
            }
            actionBar.setElevation(0);
        }
    }

    public static BeaconHintCampaignStatisticsFragment newInstance(BeaconHintCampaign beaconHintCampaign) {
        BeaconHintCampaignStatisticsFragment fragment = new BeaconHintCampaignStatisticsFragment();
        Bundle args = new Bundle();
        args.putParcelable(ARG_CAMPAIGN, beaconHintCampaign);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            beaconHintCampaign = getArguments().getParcelable(ARG_CAMPAIGN);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_beacon_hint_campaign_statistics, container, false);
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        findHolders();
        fillHolders();
        displayLineChart();

    }

    private void findHolders() {
        View rootView = getView();
        if (rootView == null) {
            return;
        }
        beaconHintStatisticCtrChart = rootView.findViewById(R.id.beaconHintStatisticCtrChart);
        beaconHintStatisticCtrTitle = rootView.findViewById(R.id.beaconHintStatisticCtrTitle);
        beaconHintCampaignStatisticTitle = rootView.findViewById(R.id.beaconHintCampaignStatisticTitle);
        beaconHintCampaignStatisticStatus = rootView.findViewById(R.id.beaconHintCampaignStatisticStatus);
        beaconHintCampaignStatisticRecipients = rootView.findViewById(R.id.beaconHintCampaignStatisticRecipients);
        beaconHintCampaignStatisticSent = rootView.findViewById(R.id.beaconHintCampaignStatisticSent);
        beaconHintStatisticDelivered = rootView.findViewById(R.id.beaconHintStatisticDelivered);
        beaconHintStatisticRead = rootView.findViewById(R.id.beaconHintStatisticRead);
        beaconHintStatisticDateSend = rootView.findViewById(R.id.beaconHintStatisticDateSend);
    }

    private void fillHolders() {
        DecimalFormat df = new DecimalFormat("###,###,###.##");

        if (beaconHintCampaign.getStatus() != null) {
            beaconHintCampaignStatisticStatus.setText(beaconHintCampaign.getStatus());
        } else {
            View rootView = getView();
            if (rootView == null) {
                return;
            }
            rootView.findViewById(R.id.beaconHintCampaignStatisticStatusBox).setVisibility(View.GONE);
        }

        beaconHintCampaignStatisticTitle.setText(beaconHintCampaign.getName());
        beaconHintCampaignStatisticRecipients.setText(beaconHintCampaign.getRecipientsReadableString());
        beaconHintCampaignStatisticSent.setText(df.format(beaconHintCampaign.getSent()));
        beaconHintStatisticDelivered.setText(df.format(beaconHintCampaign.getDelivered()));
        beaconHintStatisticRead.setText(df.format(beaconHintCampaign.getRead()));

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy.MM.dd");
        beaconHintStatisticDateSend.setText(beaconHintCampaign.getDateSend() != null ? dateFormat.format(beaconHintCampaign.getDateSend()) : "not available");
    }

    private void displayLineChart() {
        float ctr = beaconHintCampaign.getDelivered() != 0 ? ((float) beaconHintCampaign.getRead()) / beaconHintCampaign.getDelivered() : 0;
        beaconHintStatisticCtrTitle.setCounter(String.format("%.2f", ctr * 100));

        if (ctr == 0) {
            beaconHintStatisticCtrChart.setVisibility(View.GONE);
            return;
        }

        List<PieEntry> entries = new ArrayList<>();
        entries.add(new PieEntry(beaconHintCampaign.getRead(), "Read", 0));
        entries.add(new PieEntry(beaconHintCampaign.getDelivered() - beaconHintCampaign.getRead(), "Others", 0));

        PieDataSet set = new PieDataSet(entries, "");

        Context context = getContext();
        set.setColors(new int[]{R.color.fancyGreenAlpha, R.color.fancyGrayAlpha}, context);

        set.setValueLinePart1OffsetPercentage(100.0f);
        set.setValueLinePart1Length(0.15f);
        set.setValueLinePart2Length(0.0f);

        set.setValueLineColor(Color.parseColor("#00000000"));
        set.setYValuePosition(PieDataSet.ValuePosition.OUTSIDE_SLICE);

        PieData data = new PieData(set);

        data.setValueFormatter(new SyneValueFormatter(0));
        data.setValueTextSize(14f);
        data.setValueTextColor(Color.BLACK);

        data.setValueTypeface(Typeface.createFromAsset(getContext().getAssets(), "fonts/Lato-Thin.ttf"));

        beaconHintStatisticCtrChart.setData(data);

        beaconHintStatisticCtrChart.getDescription().setEnabled(false);

        beaconHintStatisticCtrChart.setCenterText("");
        beaconHintStatisticCtrChart.setDrawEntryLabels(false);
        beaconHintStatisticCtrChart.setExtraOffsets(15f, 15f, 15f, 15f);
        beaconHintStatisticCtrChart.setTransparentCircleRadius(90);
        beaconHintStatisticCtrChart.setHoleRadius(90);
        beaconHintStatisticCtrChart.setTransparentCircleAlpha(100);
        PieChartSelectedValueListener beaconHintStatisticCtrChartListener = new PieChartSelectedValueListener(beaconHintStatisticCtrChart);
        beaconHintStatisticCtrChart.setOnChartValueSelectedListener(beaconHintStatisticCtrChartListener);

        beaconHintStatisticCtrChart.disableScroll();
        beaconHintStatisticCtrChart.getLegend().setEnabled(false);

        beaconHintStatisticCtrChart.invalidate(); // refresh
//                                beaconHintStatisticCtrChart.animateY(1400, Easing.EasingOption.EaseInOutQuad);

        Highlight h = new Highlight(0, 0, 0);
        beaconHintStatisticCtrChart.highlightValue(h);
        beaconHintStatisticCtrChartListener.drawCenter(beaconHintStatisticCtrChart, h);

    }
}

package com.synerise.synerise.app.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.Switch;

import com.synerise.synerise.R;
import com.synerise.synerise.app.fragment.ConversionFragment;
import com.synerise.synerise.app.model.ClientTag;

import java.util.ArrayList;

/**
 * Created by dsiedlarz on 15.10.2016.
 */
public class ConversionTagRecyclerAdapter extends RecyclerView.Adapter<ConversionTagRecyclerAdapter.ClientTagViewHolder> {
    private final Context context;
    private final Activity activity;
    private final ArrayList<ClientTag> values;
    private Typeface font;

    public ConversionTagRecyclerAdapter(Activity activity, ArrayList<ClientTag> values) {
        this.activity = activity;
        this.context = activity.getApplicationContext();
        this.values = values;
        this.font = Typeface.createFromAsset(context.getAssets(), "fonts/action-icons-up.ttf");
    }

    @Override
    public int getItemCount() {
        return values.size();
    }

    @Override
    public ClientTagViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_element_choose_client_tag, null);
        ClientTagViewHolder viewHolder = new ClientTagViewHolder(view);
        return viewHolder;
    }



    @Override
    public void onBindViewHolder(final ClientTagViewHolder holder, final int position) {

        final ClientTag clientTag = ConversionFragment.availableTags.get(position);
        holder.mTagName.setText( clientTag.getName());
        holder.mTagName.setChecked(clientTag.getChoosed());

        holder.mTagName.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                ConversionFragment.availableTags.get(holder.getAdapterPosition()).setChoosed(isChecked);
                Fragment fragment = ((AppCompatActivity)activity).getSupportFragmentManager().findFragmentById(R.id.mainContainer);
                ((ConversionFragment) fragment).getConversion();
            }
        });

//
//        holder.mView.setOnClickListener(new View.OnClickListener() {
//
//
//            @Override
//            public void onClick(View v) {
//                clientTag.setChoosed(!clientTag.getChoosed());
//                holder.mTagName.setChecked(clientTag.getChoosed());
//
//                System.out.println(ConversionFragment.availableTags);
//            }
//        });
    }


    public class ClientTagViewHolder extends RecyclerView.ViewHolder {
        private Switch mTagName;

        View mView;

        public ClientTagViewHolder(View view) {
            super(view);
            this.mView = view;
            this.mTagName = (Switch) view.findViewById(R.id.tagName);


        }
    }

}



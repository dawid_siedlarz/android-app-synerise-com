package com.synerise.synerise.app.fragment;

import android.app.Activity;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.Switch;

import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.synerise.sdk.tracking.Tracker;
import com.synerise.sdk.tracking.TrackerParams;
import com.synerise.synerise.MainActivity;
import com.synerise.synerise.R;
import com.synerise.synerise.app.ActionBarConfigurableFragment;
import com.synerise.synerise.app.adapter.EventRecyclerViewAdapter;
import com.synerise.synerise.app.factory.EventFactory;
import com.synerise.synerise.app.model.Event;
import com.synerise.synerise.app.syneutil.SyneHttpCacheHandler;
import com.synerise.synerise.app.syneview.SyneComplexBarChart;

import org.apache.http.client.methods.HttpGet;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.helper.StringUtil;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


public class ListGrowthFragment extends ActionBarConfigurableFragment {

    private RecyclerView eventsList;
    private EventRecyclerViewAdapter adapter;
    private LinearLayoutManager linearLayoutManager;

    private ArrayList<Event> events;
    private EventFactory eventFactory;

//    BarChart listGrowthChart;
//    SyneChartTitle listGrowthTitle;

    SyneComplexBarChart listGrowthChart;


    boolean isScreenActive = false;
    private boolean isListGrowthInitialized = false;
    private boolean isGetEventsInitialized = false;

    private void initEventsList() {
        eventFactory = new EventFactory(getContext());
        View rootView = getView();
        if (rootView == null) {
            return;
        }
        eventsList = (RecyclerView) rootView.findViewById(R.id.listGrowEventsListView);
        events = new ArrayList<>();
        adapter = new EventRecyclerViewAdapter(getActivity(), events);
        eventsList.setNestedScrollingEnabled(false);
        linearLayoutManager = new LinearLayoutManager(getContext());
        eventsList.setLayoutManager(linearLayoutManager);
        eventsList.setAdapter(adapter);
    }


    @Override
    public void onResume() {
        super.onResume();
        isScreenActive = true;
        new GetListGrowth().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        getEvents();
    }

    @Override
    public void onPause() {
        super.onPause();
        isScreenActive = false;
    }

    @Override
    public void onStop() {
        super.onStop();
        isScreenActive = false;
    }

    public ListGrowthFragment() {
    }

    public static ListGrowthFragment newInstance(String param1, String param2) {
        ListGrowthFragment fragment = new ListGrowthFragment();
        return fragment;
    }

    @Override
    public void setUpActionBar() {
        ActionBar actionBar = (ActionBar) ((AppCompatActivity) getActivity()).getSupportActionBar();
        if (actionBar != null) {
            actionBar.show();
            actionBar.setTitle(MainActivity.businessProfileName != null ? MainActivity.businessProfileName : "Synerise");
            actionBar.setSubtitle("List Growth");
            actionBar.setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.colorPrimary)));
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                Window window = getActivity().getWindow();
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                window.setStatusBarColor(getResources().getColor(R.color.colorPrimary));
            }
            getActivity().setTheme(R.style.AppThemeDashboard);
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Tracker.trackScreen("List Growth", new TrackerParams[]{});


        initEventsList();

        Activity activity = getActivity();

        View rootView = getView();
        if (rootView == null) {
            return;
        }
        listGrowthChart = (SyneComplexBarChart) rootView.findViewById(R.id.listGrowthChart);
//        listGrowthTitle = (SyneChartTitle) rootView.findViewById(R.id.listGrowthTitle);

        final ImageButton button = (ImageButton) rootView.findViewById(R.id.refreshTrafficButton);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new GetClientEventList().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                /*Animation animation = AnimationUtils.loadAnimation(getContext(), R.anim.rotate);  animation.setRepeatCount(-1);;               animation.setDuration(1000);                 button.startAnimation(animation); */
            }
        });

        Switch autoload = (Switch) rootView.findViewById(R.id.autoloadTrafficSwitch);
        autoload.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    new GetClientEventList().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                }
            }
        });
    }

    private void getEvents() {
        new GetClientEventList().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_list_growth, container, false);
    }


    private class GetListGrowth extends AsyncTask<Void, Void, String> {
        protected String doInBackground(Void... params) {
            try {

                HttpGet httpget = new HttpGet("https://app.synerise.com/listgrowth-actions/clients");

                SyneHttpCacheHandler handler = new SyneHttpCacheHandler();
                boolean cacheUsed = false;
                if (!isListGrowthInitialized) {
                    cacheUsed = MainActivity.cacheString.contains(SyneHttpCacheHandler.getCacheKey(httpget));
                }
                final String body = handler.executeWithCache(httpget, !isListGrowthInitialized, true);

                if (body == null) {
                    return "not ok";
                }

                JSONObject jsonObjectAll = null;

                try {
                    jsonObjectAll = new JSONObject(body);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                final List<String> labels = new ArrayList<String>();
                List<BarEntry> entries = new ArrayList<BarEntry>();
                ArrayList<Float> existing = new ArrayList<Float>();
                ArrayList<Float> web_mobile = new ArrayList<Float>();
                ArrayList<Float> web_desktop = new ArrayList<Float>();
                ArrayList<Float> app_mobile = new ArrayList<Float>();
                ArrayList<Float> import_integration = new ArrayList<Float>();

                int minY = Integer.MAX_VALUE;
                int size = 0;

                try {

                    Iterator<?> keys = jsonObjectAll.keys();
                    while (keys.hasNext()) {
                        String key = (String) keys.next();
                        if (StringUtil.isNumeric(key)) {
                            size++;
                            JSONObject jsonObject = jsonObjectAll.getJSONObject(key);
                            int existingValue = jsonObject.getInt("existing");
                            minY = minY > existingValue ? existingValue : minY;
                            existing.add((float) existingValue);
                            web_mobile.add((float) jsonObject.getInt("web_mobile"));
                            web_desktop.add((float) jsonObject.getInt("web_desktop"));
                            app_mobile.add((float) jsonObject.getInt("app_mobile"));
                            import_integration.add((float) jsonObject.getInt("import_integration"));

                            labels.add(jsonObject.getString("year"));
                        }
                    }

                    for (int i = 0; i < size; i++) {
                        entries.add(new BarEntry(i, new float[]{
                                import_integration.get(i), web_desktop.get(i), app_mobile.get(i), web_mobile.get(i)
                        }, labels.get(i)));
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                final float valueAll = existing.get(size - 1) + web_mobile.get(size - 1) + web_desktop.get(size - 1) + app_mobile.get(size - 1) + import_integration.get(size - 1);

                final String[] labelsa = new String[]{"Import Integration", "Web", "Mobile App", "Mobile Site"};

                BarDataSet set = new BarDataSet(entries, "clients");
                set.setStackLabels(labelsa);
                int[] colors = {
                        getResources().getColor(R.color.fancyBlueAlpha),
                        getResources().getColor(R.color.fancyPinkAlpha),
                        getResources().getColor(R.color.fancyDarkBlueAlpha),
                        getResources().getColor(R.color.fancyGreenAlpha),
                };
                set.setColors(colors);
                final BarData data = new BarData(set);

                data.setDrawValues(false);
                data.setBarWidth(0.6f);
                final Typeface tf = Typeface.createFromAsset(getContext().getAssets(),
                        "fonts/Lato-Thin.ttf");
                final Typeface btf = Typeface.createFromAsset(getContext().getAssets(),
                        "fonts/Lato-Bold.ttf");
                Activity activity = getActivity();

                if (activity != null) {
                    final int finalMinY = minY;
                    final int finalSize = size;
                    final boolean finalCacheUsed = cacheUsed;
                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {


                            listGrowthChart.update(data, (double) valueAll, labels, labelsa, isListGrowthInitialized, finalCacheUsed);
                            listGrowthChart.getChart().getXAxis().setAxisMinimum(-3f);
                            listGrowthChart.getChart().getXAxis().setAxisMaximum(finalSize + 1);
                            listGrowthChart.getTitle().setIntCounter((int) valueAll);
                            isListGrowthInitialized = true;


                        }
                    });
                }
            } catch (NullPointerException e) {

            } catch (IllegalStateException e) {

            }
            return "ok";
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            Handler myHandler = new Handler();
            myHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    Fragment fragment = getActivity().getSupportFragmentManager().findFragmentById(R.id.mainContainer);
                    if (isScreenActive && getActivity() != null && fragment instanceof ListGrowthFragment) {
                        new GetListGrowth().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                    }
                }
            }, 3000);

        }

    }


    private class GetClientEventList extends AsyncTask<Void, Void, String> {
        protected String doInBackground(Void... params) {
            try {
                HttpGet httpget = new HttpGet("https://app.synerise.com/activity-histories/stream-for-business-profile/?action=client.createOrUpdate");
                SyneHttpCacheHandler handler = new SyneHttpCacheHandler();

                final String body = handler.executeWithCache(httpget, !isGetEventsInitialized, true);

                if (body == null) {
                    return "not ok";
                }

                JSONArray jsonArray = null;

                try {
                    jsonArray = new JSONArray(body);
                } catch (JSONException e) {
                    e.printStackTrace();
                }


                assert jsonArray != null;

                if (jsonArray.length() == 0) {
                    return "not ok";
                }

                eventFactory.createEventsArray(events, jsonArray);

                Activity activity = getActivity();
                if (activity != null) {
                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            adapter.notifyDataSetChanged();
                            isGetEventsInitialized = true;
                        }
                    });
                }
            } catch (NullPointerException e) {

            } catch (IllegalStateException e) {

            } catch (JSONException e) {
                e.printStackTrace();
            }
            return "ok";
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            Handler myHandler = new Handler();
            myHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    View rootView = getView();
                    if (rootView == null) {
                        return;
                    }
                    Fragment fragment = getActivity().getSupportFragmentManager().findFragmentById(R.id.mainContainer);

                    if (isScreenActive && ((Switch) rootView.findViewById(R.id.autoloadTrafficSwitch)).isChecked() && fragment instanceof ListGrowthFragment) {
                        new GetClientEventList().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                    }

                }
            }, 5000);

        }
    }
}

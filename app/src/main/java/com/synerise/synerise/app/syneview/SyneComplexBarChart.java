package com.synerise.synerise.app.syneview;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter;
import com.synerise.synerise.R;
import com.synerise.synerise.app.syneutil.BarChartMarkerView;

import java.util.List;

/**
 * Created by dsiedlarz on 22.02.2017.
 */

public class SyneComplexBarChart extends RelativeLayout {
    View view;

    SyneChartTitle title;
    BarChart chart;

    private static Typeface tf;
    static int fancyGray = Color.parseColor("#C7C7C7");


    public SyneComplexBarChart(Context context, AttributeSet attrs) {
        super(context, attrs);
        initViews();
        TypedArray a = context.getTheme().obtainStyledAttributes(
                attrs,
                R.styleable.SyneComplexBarChart,
                0, 0);

        try {
            String titleText = a.getString(R.styleable.SyneComplexBarChart_titleBarText);
            String unitText = a.getString(R.styleable.SyneComplexBarChart_unitBarText);

            title.setTitle(titleText);
            title.setUnit(unitText);

        } finally {
            a.recycle();
        }
        tf = Typeface.createFromAsset(getContext().getAssets(),
                "fonts/Lato-Thin.ttf");

        initBarChart(chart);
    }


    private void initViews() {
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view = inflater.inflate(R.layout.syne_complex_bar_chart, this, true);

        chart = (BarChart) view.findViewById(R.id.complexBarChart);
        title = (SyneChartTitle) view.findViewById(R.id.complexBarTitle);

    }

    public SyneChartTitle getTitle() {
        return title;
    }

    public BarChart getChart() {
        return chart;
    }

    public  void initBarChart(BarChart chart) {
        chart.getDescription().setEnabled(false);
        chart.getLegend().setEnabled(false);

        chart.setDrawGridBackground(false);
        chart.setDrawGridBackground(false);
        chart.getXAxis().setTypeface(tf);
        chart.getXAxis().setTextColor(getResources().getColor(R.color.textDarkGray));
        chart.getXAxis().setTextSize(8f);
        chart.getAxisLeft().setDrawGridLines(true);
        chart.getAxisLeft().setGridColor(getResources().getColor(R.color.fancyGray));
        chart.getAxisLeft().setDrawAxisLine(false);
        chart.getAxisLeft().setTypeface(tf);
        chart.getAxisLeft().setTextSize(8f);
        chart.getAxisLeft().setTextColor(getResources().getColor(R.color.textGray));
        chart.getAxisLeft().setYOffset(-10);
        chart.getAxisLeft().setXOffset(10);
        chart.getAxisLeft().setAxisMinimum(0);

        chart.getXAxis().setDrawGridLines(false);
        chart.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM);
       
     

        chart.getAxisRight().setDrawAxisLine(false);
        chart.getAxisRight().setEnabled(false);
        chart.getAxisRight().setDrawGridLines(false);
        chart.getAxisLeft().setPosition(YAxis.YAxisLabelPosition.INSIDE_CHART);
        chart.setDrawBorders(false);
        chart.setViewPortOffsets(0f, 20f, 0f, 40f);
        chart.getAxisRight().setEnabled(false);
        chart.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM);
        chart.setBackgroundColor(Color.parseColor("#FFFFFF"));
        chart.setDrawGridBackground(false);
    }

    public void update(BarData data, Double counter, List<String> xLabels, String[] lineLabels, boolean isChartInitialized, boolean isCacheUsed) {

        BarChartMarkerView mv = new BarChartMarkerView(getContext(), R.layout.custom_marker_view);
        mv.setChartView(chart); // For bounds control
        mv.setDates(xLabels);
        mv.setLabels(lineLabels);
        chart.setMarker(mv);

        chart.setData(data);
        title.setDoubleCounter(counter);
        chart.getXAxis().setValueFormatter(new IndexAxisValueFormatter(xLabels));

        if(!isChartInitialized) {
            if(isCacheUsed) {
                chart.setAlpha(0.5f);
            } else {
                chart.animateY(1000);
            }
            chart.invalidate();
        } else {
            chart.setAlpha(1f);
            chart.notifyDataSetChanged();
        }
    }

    public static void initLineDataSet(BarDataSet set) {
     set.setDrawValues(false);
    }
}

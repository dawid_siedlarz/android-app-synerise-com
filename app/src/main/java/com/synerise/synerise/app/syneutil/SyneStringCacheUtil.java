package com.synerise.synerise.app.syneutil;

import com.vincentbrison.openlibraries.android.dualcache.CacheSerializer;
import com.vincentbrison.openlibraries.android.dualcache.SizeOf;

/**
 * Created by dsiedlarz on 21.02.2017.
 */

public class SyneStringCacheUtil implements SizeOf<String> , CacheSerializer<String> {
    @Override
    public int sizeOf(String object) {
        return object.getBytes().length;
    }

    @Override
    public String fromString(String data) {
        return data;
    }

    @Override
    public String toString(String object) {
        return (String) object;
    }
}

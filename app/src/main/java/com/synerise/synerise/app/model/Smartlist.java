package com.synerise.synerise.app.model;

import java.util.ArrayList;

public class Smartlist {

    public static final ArrayList<SmartlistModel> ITEMS = new ArrayList<SmartlistModel>();

    public static void add(String name, String id) {
        ITEMS.add(new SmartlistModel(name, id));
    }

    public static void clear() {
        ITEMS.clear();

    }

    public static class SmartlistModel {
        public String name;
        public String id;

        public SmartlistModel(String name, String id) {
                this.name = name;
                this.id = id;
        }
    }

}

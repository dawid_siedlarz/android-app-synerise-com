package com.synerise.synerise.app.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.synerise.synerise.R;
import com.synerise.synerise.app.model.Tag;

import java.util.ArrayList;

/**
 * Created by dsiedlarz on 15.10.2016.
 */
public class SimpleTagsArrayAdapter extends ArrayAdapter<Tag.TagModel> {
    private final Context context;
    private final ArrayList<Tag.TagModel> values;
    private TextView mAvatar;
    private Tag.TagModel tag;



    public SimpleTagsArrayAdapter(Context context, ArrayList<Tag.TagModel> values) {
        super(context, -1, values);
        this.context = context;
        this.values = values;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        tag = Tag.ITEMS.get(position);
        View rowView = inflater.inflate(R.layout.list_element_tag, parent, false);
        TextView mTagName = (TextView) rowView.findViewById(R.id.tagName);
        TextView mTagCount = (TextView) rowView.findViewById(R.id.tagCount);

        mTagName.setText(tag.name);
        mTagCount.setText("(" + tag.count + ")");
        return rowView;
    }



//    @Override
//    public View getDropDownView(int position, View convertView,
//                                ViewGroup parent) {
//        tag = Tag.ITEMS.get(position);
//        LayoutInflater inflater = ((Activity)context).getLayoutInflater();
//        View dropDownView = inflater.inflate(R.layout.list_element_tag, parent, false);
//
//        TextView mTagName = (TextView) dropDownView.findViewById(R.id.tagName);
//        TextView mTagCount = (TextView) dropDownView.findViewById(R.id.tagCount);
//        mTagName.setText(tag.name);
//        mTagCount.setText("(" + tag.count + ")");
//
//        return dropDownView;
//
//    }

}

package com.synerise.synerise.messenger.model.messageBody;

import android.view.View;

public interface MessageBody {
     View getBody();
}

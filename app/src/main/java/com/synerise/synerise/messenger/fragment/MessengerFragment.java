package com.synerise.synerise.messenger.fragment;

import android.app.Activity;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.synerise.sdk.tracking.Tracker;
import com.synerise.sdk.tracking.TrackerParams;
import com.synerise.synerise.MainActivity;
import com.synerise.synerise.R;
import com.synerise.synerise.SyneriseOnFragmentInteractionListener;
import com.synerise.synerise.app.ActionBarConfigurableFragment;
import com.synerise.synerise.app.adapter.MessengerRoomsRecyclerViewAdapter;
import com.synerise.synerise.messenger.factory.messageBody.AllInOneMessageBodyFactory;
import com.synerise.synerise.messenger.model.MessengerRoom;
import com.synerise.synerise.messenger.model.messageBody.MessageBody;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import io.socket.client.Socket;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link SyneriseOnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link MessengerFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MessengerFragment extends ActionBarConfigurableFragment {

    private Socket mSocket;

    private RecyclerView insightsList;
    private MessengerRoomsRecyclerViewAdapter adapter;
    private LinearLayoutManager linearLayoutManager;

    private ArrayList<MessengerRoom> rooms;


    private void initMessengerRoomsList() {
        View rootView = getView();
        if (rootView == null) {
            return;
        }
        insightsList = (RecyclerView) rootView.findViewById(R.id.roomsRecyclerView);
        rooms = new ArrayList<>();
        adapter = new MessengerRoomsRecyclerViewAdapter((MainActivity) getActivity(), rooms);
        insightsList.setNestedScrollingEnabled(false);
        linearLayoutManager = new LinearLayoutManager(getContext());
        insightsList.setLayoutManager(linearLayoutManager);
        insightsList.setAdapter(adapter);
    }

    public static final String WEBSOCKET_ENDPOINT = "https://messenger.synerise.com";
    public static final String API_ENDPOINT = "https://api.synerise.com/rtom";

    private TextView textView;


    public MessengerFragment() {
        // Required empty public constructor
    }

    public static MessengerFragment newInstance() {
        MessengerFragment fragment = new MessengerFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        boolean fDialogMode = getActivity().getIntent().hasExtra("dialog_mode");

        Log.v("DAWID", String.valueOf(fDialogMode));

        if (!fDialogMode) {
            getActivity().setTheme(R.style.AppThemeMessenger);
        }
        super.onCreate(savedInstanceState);

    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Tracker.trackScreen("Messenger", new TrackerParams[]{});

        initMessengerRoomsList();
        new GetRooms().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    @Override
    public void onResume() {
        super.onResume();
    }


    @Override
    public void setUpActionBar() {
        ActionBar actionBar = (ActionBar) ((AppCompatActivity) getActivity()).getSupportActionBar();
        if (actionBar != null) {
            actionBar.show();
            actionBar.setTitle("Messenger");
            actionBar.setSubtitle("");
            actionBar.setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.colorPrimaryMessenger)));
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                Window window = getActivity().getWindow();
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                window.setStatusBarColor(getResources().getColor(R.color.colorPrimaryMessenger));
            }
            getActivity().setTheme(R.style.AppThemeMessenger);
        }
    }

    private class GetRooms extends AsyncTask<Void, Void, String> {
        protected String doInBackground(Void... params) {

            HttpGet httpget = new HttpGet("https://messenger.synerise.com/rtom/rooms?limit=1500&offset=0");

            HttpResponse eventList = null;
            try {
                eventList = MainActivity.httpClient.execute(httpget);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (eventList == null) {
                return "not OK";
            }

            HttpEntity eventListEntity = eventList.getEntity();
            String body = "";
            try {
                body = EntityUtils.toString(eventListEntity);
            } catch (IOException e) {
                e.printStackTrace();
            }

            JSONArray array = new JSONArray();
            try {
                array = new JSONObject(body).getJSONArray("data");
            } catch (JSONException e) {
                e.printStackTrace();
            }

            AllInOneMessageBodyFactory allInOneMessageBodyFactory = new AllInOneMessageBodyFactory(getContext());

            ArrayList<MessengerRoom> newRooms = new ArrayList<>();

            try {
                for (int i = 0; i < array.length(); i++) {
                    MessengerRoom messengerRoom = new MessengerRoom(array.getJSONObject(i));
                    MessageBody messageBody = allInOneMessageBodyFactory.getMessageBody(messengerRoom.getLastMessage());
                    messengerRoom.getLastMessage().setMessageBody(messageBody);
                    newRooms.add(messengerRoom);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            rooms.clear();
            rooms.addAll(newRooms);
            Activity activity = getActivity();
            if (activity != null) {
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        adapter.notifyDataSetChanged();
                    }
                });
            }

            return "ok";

        }
    }

    private class GetVendors extends AsyncTask<Void, Void, String> {
        protected String doInBackground(Void... params) {


            HttpGet httpget = new HttpGet("https://api.synerise.com/rtom/vendors");


            HttpResponse eventList = null;
            try {
                eventList = MainActivity.httpClient.execute(httpget);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (eventList == null) {
                return "not OK";
            }

            HttpEntity eventListEntity = eventList.getEntity();
            String body = "";
            try {
                body = EntityUtils.toString(eventListEntity);
            } catch (IOException e) {
                e.printStackTrace();
            }

            final String finalBody = body;
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    textView.setText(finalBody);
                }
            });
            return "ok";

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_messenger, container, false);
    }
}

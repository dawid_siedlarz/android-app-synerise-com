package com.synerise.synerise.app.fragment.campaign.statistic;

import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.synerise.synerise.MainActivity;
import com.synerise.synerise.R;
import com.synerise.synerise.app.ActionBarConfigurableFragment;
import com.synerise.synerise.app.model.campaign.LandingPageCampaign;

import java.text.SimpleDateFormat;

public class LandingPageCampaignStatisticsFragment extends ActionBarConfigurableFragment {
    public static final String ARG_CAMPAIGN = "campaign";
    private LandingPageCampaign landingPageCampaign;


    private TextView landingPageCampaignStatisticTitle;
    private TextView landingPageCampaignStatisticStatus;
    private TextView landingPageCampaignStatisticRecipients;
    private TextView landingPageCampaignStatisticUpdateTime;

    public LandingPageCampaignStatisticsFragment() {
        // Required empty public constructor
    }


    @Override
    public void setUpActionBar() {
        ActionBar actionBar = (ActionBar) ((AppCompatActivity) getActivity()).getSupportActionBar();
        if (actionBar != null) {
            actionBar.show();
            actionBar.setTitle(MainActivity.businessProfileName != null ? MainActivity.businessProfileName : "Synerise");
            actionBar.setSubtitle("Landing page campaign");
            actionBar.setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.colorPrimaryCampaigns)));
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                Window window = getActivity().getWindow();
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                window.setStatusBarColor(getResources().getColor(R.color.colorPrimaryDarkCampaigns));
            }
            actionBar.setElevation(0);
        }
    }

    public static LandingPageCampaignStatisticsFragment newInstance(LandingPageCampaign landingPageCampaign) {
        LandingPageCampaignStatisticsFragment fragment = new LandingPageCampaignStatisticsFragment();
        Bundle args = new Bundle();
        args.putParcelable(ARG_CAMPAIGN, landingPageCampaign);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            landingPageCampaign = getArguments().getParcelable(ARG_CAMPAIGN);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_landing_page_campaign_statistics, container, false);
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        findHolders();
        fillHolders();
        attachButtonListener();
    }

    private void attachButtonListener() {
        View rootView = getView();
        if (rootView == null) {
            return;
        }
        Button button = rootView.findViewById(R.id.previewButton);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Bundle bundle = new Bundle();
                bundle.putString(LandingPagePreviewFragment.ARG_NAME, landingPageCampaign.getName());
                bundle.putString(LandingPagePreviewFragment.ARG_URL, "http://lp.synerise.com/page/" + landingPageCampaign.getSearchHash() + "/" + landingPageCampaign.getUrl());

                Fragment fragment = new LandingPagePreviewFragment();
                fragment.setArguments(bundle);

                MainActivity.addFragment(fragment);
            }
        });
    }

    private void findHolders() {
        View rootView = getView();
        if (rootView == null) {
            return;
        }
        landingPageCampaignStatisticTitle = rootView.findViewById(R.id.landingPageCampaignStatisticTitle);
        landingPageCampaignStatisticStatus = rootView.findViewById(R.id.landingPageCampaignStatisticStatus);
        landingPageCampaignStatisticRecipients = rootView.findViewById(R.id.landingPageCampaignStatisticRecipients);
        landingPageCampaignStatisticUpdateTime = rootView.findViewById(R.id.landingPageCampaignStatisticUpdateTime);
    }

    private void fillHolders() {
        landingPageCampaignStatisticTitle.setText(landingPageCampaign.getTitle());
        landingPageCampaignStatisticStatus.setText(landingPageCampaign.getStatus());
        landingPageCampaignStatisticRecipients.setText(landingPageCampaign.getRecipientsReadableString());
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        landingPageCampaignStatisticUpdateTime.setText(format.format(landingPageCampaign.getDateSend()));
    }
}

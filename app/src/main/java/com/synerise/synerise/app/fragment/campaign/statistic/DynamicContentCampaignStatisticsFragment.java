package com.synerise.synerise.app.fragment.campaign.statistic;

import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.synerise.synerise.MainActivity;
import com.synerise.synerise.R;
import com.synerise.synerise.app.ActionBarConfigurableFragment;
import com.synerise.synerise.app.model.campaign.DynamicContentCampaign;

import java.text.DecimalFormat;

public class DynamicContentCampaignStatisticsFragment extends ActionBarConfigurableFragment {
    public static final String ARG_CAMPAIGN = "campaign";
    private DynamicContentCampaign dynamicContentCampaign;


    private TextView dynamicContentCampaignStatisticTitle;
    private TextView dynamicContentCampaignStatisticStatus;
    private TextView dynamicContentCampaignStatisticRecipients;
    private TextView dynamicContentCampaignRecipientsType;
    private TextView dynamicContentStatisticSelector;
    private TextView dynamicContentStatisticType;
    private TextView dynamicContentStatisticViewsPageUrl;
    private TextView dynamicContentStatisticViewsExcludePageUrl;

    public DynamicContentCampaignStatisticsFragment() {
        // Required empty public constructor
    }


    @Override
    public void setUpActionBar() {
        ActionBar actionBar = (ActionBar) ((AppCompatActivity) getActivity()).getSupportActionBar();
        if (actionBar != null) {
            actionBar.show();
            actionBar.setTitle(MainActivity.businessProfileName != null ? MainActivity.businessProfileName : "Synerise");
            actionBar.setSubtitle("Dynamic content campaign");
            actionBar.setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.colorPrimaryCampaigns)));
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                Window window = getActivity().getWindow();
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                window.setStatusBarColor(getResources().getColor(R.color.colorPrimaryDarkCampaigns));
            }
            actionBar.setElevation(0);
        }
    }

    public static DynamicContentCampaignStatisticsFragment newInstance(DynamicContentCampaign dynamicContentCampaign) {
        DynamicContentCampaignStatisticsFragment fragment = new DynamicContentCampaignStatisticsFragment();
        Bundle args = new Bundle();
        args.putParcelable(ARG_CAMPAIGN, dynamicContentCampaign);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            dynamicContentCampaign = getArguments().getParcelable(ARG_CAMPAIGN);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_dynamic_content_campaign_statistics, container, false);
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        findHolders();
        fillHolders();
    }

    private void findHolders() {
        View rootView = getView();
        if (rootView == null) {
            return;
        }
        dynamicContentCampaignStatisticTitle = rootView.findViewById(R.id.dynamicContentCampaignStatisticTitle);
        dynamicContentCampaignStatisticStatus = rootView.findViewById(R.id.dynamicContentCampaignStatisticStatus);
        dynamicContentCampaignStatisticRecipients = rootView.findViewById(R.id.dynamicContentCampaignStatisticRecipients);
        dynamicContentCampaignRecipientsType = rootView.findViewById(R.id.dynamicContentCampaignRecipientsType);
        dynamicContentStatisticSelector = rootView.findViewById(R.id.dynamicContentStatisticSelector);
        dynamicContentStatisticType = rootView.findViewById(R.id.dynamicContentStatisticType);
        dynamicContentStatisticViewsPageUrl = rootView.findViewById(R.id.dynamicContentStatisticViewsPageUrl);
        dynamicContentStatisticViewsExcludePageUrl = rootView.findViewById(R.id.dynamicContentStatisticViewsExcludePageUrl);
    }

    private void fillHolders() {
        DecimalFormat df = new DecimalFormat("###,###,###.##");

        dynamicContentCampaignStatisticTitle.setText(dynamicContentCampaign.getName());
        dynamicContentCampaignStatisticStatus.setText(dynamicContentCampaign.getStatus());
        dynamicContentCampaignStatisticRecipients.setText(dynamicContentCampaign.getRecipientsReadableString());
        dynamicContentCampaignRecipientsType.setText(dynamicContentCampaign.getRecipientsType() != null ? dynamicContentCampaign.getRecipientsType() : "Not available");
        dynamicContentStatisticSelector.setText(dynamicContentCampaign.getSelector() != null ? dynamicContentCampaign.getSelector() : "Not available");
        dynamicContentStatisticType.setText(dynamicContentCampaign.getType() != null ? dynamicContentCampaign.getType() : "Not available");
        dynamicContentStatisticViewsPageUrl.setText(dynamicContentCampaign.getViewsPageUrl() != null ? dynamicContentCampaign.getViewsPageUrl() : "Not available");
        dynamicContentStatisticViewsExcludePageUrl.setText(dynamicContentCampaign.getViewsExcludePageUrl() != null ? dynamicContentCampaign.getViewsExcludePageUrl() : "Not available");
    }
}

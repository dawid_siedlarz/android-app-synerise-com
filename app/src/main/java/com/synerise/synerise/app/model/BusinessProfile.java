package com.synerise.synerise.app.model;

import android.graphics.Bitmap;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Helper class for providing sample content for user interfaces created by
 * Android template wizards.
 * <p/>
 * TODO: Replace all uses of this class before publishing your app.
 */
public class BusinessProfile {

    public static final List<BusinessProfileEntity> ITEMS = new ArrayList<BusinessProfileEntity>();

    public static void add(JSONObject element){
        ITEMS.add(new BusinessProfileEntity(element));
    }

    public static void clear(){
        ITEMS.clear();

    }

    public static class BusinessProfileEntity {
        public Bitmap img;
        public int id;
        public String imgSrc;
        public String name;
        public String createdOn;
        public String description;

        public BusinessProfileEntity(JSONObject element) {
            try {
                this.id = element.getInt("id");
                this.imgSrc = "https://app.synerise.com/upload/profile/" + element.getString("logo");
                this.name = element.getString("name");
                this.createdOn = element.getJSONObject("created").getString("date");
                this.description = element.getString("description");
            } catch (JSONException e) {
                e.printStackTrace();
            }


        }


    }


}

package com.synerise.synerise.app.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.synerise.synerise.app.crmFilters.Attribute;
import com.synerise.synerise.app.crmFilters.Filters;
import com.synerise.synerise.R;
import com.synerise.synerise.app.model.FilterAttributes;

import java.util.ArrayList;

/**
 * Created by dsiedlarz on 15.10.2016.
 */
public class SimpleFiltersArrayAdapter extends RecyclerView.Adapter<SimpleFiltersArrayAdapter.CustomViewHolder> {
    private final Context context;
    private final ArrayList<FilterAttributes.FilterAttribute> values;

    private FilterAttributes.FilterAttribute filterAttribute;



    public SimpleFiltersArrayAdapter(Context context, ArrayList<FilterAttributes.FilterAttribute> values) {
        this.context = context;
        this.values = values;
    }

    @Override
    public SimpleFiltersArrayAdapter.CustomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_element_filter, null);
        CustomViewHolder viewHolder = new CustomViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(SimpleFiltersArrayAdapter.CustomViewHolder holder, final int position) {

        holder.textView.setText(FilterAttributes.ITEMS.get(position).name);
        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, Attribute.class);
                intent.putExtra("attributeId", position);
                intent.putExtra("attributeId", position);
                ((AppCompatActivity)context).startActivityForResult(intent, Filters.REQUEST_EXIT);

            }
        });

    }

    @Override
    public int getItemCount() {
        return FilterAttributes.ITEMS.size();
    }

//    @Override
//    public View getView(int position, View convertView, ViewGroup parent) {
//        LayoutInflater inflater = (LayoutInflater) context
//                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//
//        filterAttribute = FilterAttributes.ITEMS.get(position);
//        View rowView = inflater.inflate(R.layout.list_element_filter, parent, false);
//        TextView mTagName = (TextView) rowView.findViewById(R.id.filterName);
//
//        mTagName.setText(filterAttribute.name);
//        return rowView;
//    }

    class CustomViewHolder extends RecyclerView.ViewHolder {
        protected TextView textView;
        public View mView;

        public CustomViewHolder(View view) {
            super(view);
            this.mView = view;
            this.textView = (TextView) view.findViewById(R.id.filterName);
        }
    }

}

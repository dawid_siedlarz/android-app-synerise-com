package com.synerise.synerise.app.syneview;

import android.content.Context;
import android.text.SpannableString;
import android.text.style.TypefaceSpan;
import android.widget.TextView;

import com.github.mikephil.charting.components.MarkerView;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.utils.MPPointF;
import com.synerise.synerise.R;

import java.text.DecimalFormat;
import java.util.List;

/**
 * Created by dsiedlarz on 19.02.2017.
 */

public class LineChartMarkerView extends MarkerView {

    private TextView tvContent;
    private String[] labels;
    private List<String> dates;


    public void setLabels(String[] labels) {
        this.labels = labels;
    }

    public void setDates(List<String> dates) {
        this.dates = dates;
    }

    public LineChartMarkerView(Context context, int layoutResource) {
        super(context, layoutResource);

        tvContent = (TextView) findViewById(R.id.tvContent);
    }

    // callbacks everytime the MarkerView is redrawn, can be used to update the
    // content (user-interface)
    @Override
    public void refreshContent(Entry e, Highlight highlight) {

//        if (e instanceof CandleEntry) {
//
//            CandleEntry ce = (CandleEntry) e;
//
//            tvContent.setText("" + Utils.formatNumber(ce.getHigh(), 0, true));
//        } else {
//
//            tvContent.setText("" + Utils.formatNumber(e.getY(), 0, true));
//        }

        String x = "";
        String y = "";
        String date;

        DecimalFormat df = new DecimalFormat("###,###,###.##");

        y = df.format((double)e.getY());
        x = labels[highlight.getDataSetIndex()];


        date = dates.get((int) e.getX());

        SpannableString span = new SpannableString(x + " \n " + date + " \n " + y);
        span.setSpan(new TypefaceSpan("fonts/Lato-Regular.ttf"), 0, span.length(), 0);

        tvContent.setText(span);
        super.refreshContent(e, highlight);
    }

    @Override
    public MPPointF getOffset() {
        return new MPPointF(-(getWidth() / 2), -getHeight());
    }
}
package com.synerise.synerise.app.clientPreview;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.webkit.WebChromeClient;
import android.webkit.WebView;

import com.synerise.synerise.MainActivity;
import com.synerise.synerise.R;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.util.EntityUtils;

import java.io.IOException;

public class AdminDataActivity extends AppCompatActivity {
    private WebView webView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_data);

        webView = (WebView) findViewById(R.id.adminDataWebView);
        WebChromeClient chromeClient = new WebChromeClient();
        webView.setWebChromeClient(chromeClient);

        Bundle extras = getIntent().getExtras();
        String clientId = extras.getString("clientId");
        new GetAdminData().execute(clientId);

    }

    private class GetAdminData extends AsyncTask<String, Void, String> {
        protected String doInBackground(String... params) {
            try {

                HttpGet httpget = new HttpGet("https://app.synerise.com/admin/debug/client/" + params[0]);

                HttpResponse eventList = MainActivity.httpClient.execute(httpget);

                HttpEntity eventListEntity = eventList.getEntity();

                final String body = EntityUtils.toString(eventListEntity);



                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                     webView.loadDataWithBaseURL(null, body, "text/html", "utf-8", null);
                    }
                });
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (NullPointerException e) {

            } catch (IllegalStateException e) {

            } catch (IOException e) {
                e.printStackTrace();
            }
            return "ok";
        }

    }


}

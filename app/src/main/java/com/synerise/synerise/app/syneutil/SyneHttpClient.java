package com.synerise.synerise.app.syneutil;

import android.content.Context;
import android.net.ConnectivityManager;
import android.support.v4.app.Fragment;

import com.synerise.synerise.MainActivity;
import com.synerise.synerise.app.fragment.OfflineFragment;

import org.apache.http.Header;
import org.apache.http.HttpHost;
import org.apache.http.HttpRequest;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.CookieStore;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.cookie.Cookie;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.HttpContext;

import java.io.IOException;
import java.util.Collections;
import java.util.List;

/**
 * Created by dsiedlarz on 19.11.2016.
 */
public class SyneHttpClient implements HttpClient {

    private static final Object LOCK_1 = new Object();

    private int businessProfileId;
    private static Context context;
    private static MainActivity activity;

    private volatile String PHPSESSID;
    private volatile String MESSENGER_TOKEN;

    HttpClient httpClient = new DefaultHttpClient();
    public static HttpClientContext httpContext = HttpClientContext.create();
    CookieStore cookieStore = new BasicCookieStore();


    public void setAppContext(MainActivity activity) {
        this.context = activity.getApplicationContext();
        this.activity = activity;
        httpClient.getParams();
        httpContext.setCookieStore(cookieStore);
    }

    @Override
    public HttpParams getParams() {
        return null;
    }

    @Override
    public ClientConnectionManager getConnectionManager() {
        return null;
    }

    @Override
    public HttpResponse execute(HttpUriRequest request) throws IOException, ClientProtocolException {

        HttpClient tmpClient = new DefaultHttpClient();
        if (!isNetworkAvailable()) {
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Fragment fragment = new OfflineFragment();
                    MainActivity.addFragment(fragment);
                }
            });
        }
        HttpParams param = new BasicHttpParams();
        param.setParameter("http.protocol.handle-redirects", false);

        request.setParams(param);
        final HttpResponse response = tmpClient.execute(request, httpContext);
        return processResponse(response) ? response : null;
    }


    public HttpResponse executeWithoutProcessing(HttpUriRequest request) throws IOException, ClientProtocolException {
        if (!isNetworkAvailable()) {
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Fragment fragment = new OfflineFragment();
                    MainActivity.addFragment(fragment);
                }
            });
        }
        final HttpResponse response = httpClient.execute(request, httpContext);

        return response;
    }


    public String getPHPSESSID() {
        return PHPSESSID;
    }

    public String getMESSENGER_TOKEN() {
        return MESSENGER_TOKEN;
    }

    private boolean processResponse(final HttpResponse response) {

        try {

            synchronized (LOCK_1) {
                switch (response.getStatusLine().getStatusCode()) {
                    case 200:
                        List<Cookie> cookies = Collections.synchronizedList(httpContext.getCookieStore().getCookies());

                        for (Cookie cookie :
                                cookies) {
                            if (cookie.getName().compareToIgnoreCase("PHPSESSID") == 0) {
                                PHPSESSID = cookie.getValue();
                                continue;
                            }
                            if (cookie.getName().compareToIgnoreCase("messenger_token") == 0) {
                                MESSENGER_TOKEN = cookie.getValue();
                                continue;
                            }
                        }

                        break;
                    case 302:
                        Header locationHeader = response.getFirstHeader("location");
                        if (locationHeader != null) {
                            String redirectLocation = locationHeader.getValue();
                            if (redirectLocation.contains("login")) {
                                activity.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        activity.prepareLogin();
                                    }
                                });

                                return false;
                            }
                        }
                        break;
                    case 500:
//                activity.runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
//
//                        MainActivity.fragment = new Error500Fragment();
//
//                        FragmentManager fragmentManager = activity.getSupportFragmentManager();
//                        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
//                        fragmentTransaction.replace(R.id.mainContainer, MainActivity.fragment);
//                        fragmentTransaction.commit();
//                    }
//                });
                        return false;
                    default:
//                activity.runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
//                        Toast.makeText(activity, "Something went wrong: " + response.getStatusLine().getReasonPhrase(), Toast.LENGTH_SHORT).show();
//                    }
//                });
                        return false;

                }

                return true;
            }
        } catch (Exception e) {
            return false;
        }

    }

    public boolean isNetworkAvailable() {
        ConnectivityManager cm = (ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE);

// test for connection
        if (cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isAvailable()
                && cm.getActiveNetworkInfo().isConnected()) {
            return true;
        } else {
            return false;
        }
    }


    @Override
    public HttpResponse execute(HttpUriRequest httpUriRequest, HttpContext httpContext) throws IOException, ClientProtocolException {
        return null;
    }

    @Override
    public HttpResponse execute(HttpHost httpHost, HttpRequest httpRequest) throws IOException, ClientProtocolException {
        return null;
    }

    @Override
    public HttpResponse execute(HttpHost httpHost, HttpRequest httpRequest, HttpContext httpContext) throws IOException, ClientProtocolException {
        return null;
    }

    @Override
    public <T> T execute(HttpUriRequest httpUriRequest, ResponseHandler<? extends T> responseHandler) throws IOException, ClientProtocolException {
        return null;
    }

    @Override
    public <T> T execute(HttpUriRequest httpUriRequest, ResponseHandler<? extends T> responseHandler, HttpContext httpContext) throws IOException, ClientProtocolException {
        return null;
    }

    @Override
    public <T> T execute(HttpHost httpHost, HttpRequest httpRequest, ResponseHandler<? extends T> responseHandler) throws IOException, ClientProtocolException {
        return null;
    }

    @Override
    public <T> T execute(HttpHost httpHost, HttpRequest httpRequest, ResponseHandler<? extends T> responseHandler, HttpContext httpContext) throws IOException, ClientProtocolException {
        return null;
    }
}

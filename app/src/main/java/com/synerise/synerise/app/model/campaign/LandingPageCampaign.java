package com.synerise.synerise.app.model.campaign;

import android.os.Parcel;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;

public class LandingPageCampaign extends Campaign {
    private String hash;
    private String searchHash;
    private String url;

    public LandingPageCampaign(JSONObject jsonObject) {
        super(jsonObject);

        try {
            hash = jsonObject.getString("hash");
            searchHash = jsonObject.getString("searchHash");
            url = jsonObject.getString("url");

            if (jsonObject.getInt("isPublished") == 1) {
                status = "Public";
            } else {
                status = "Hidden";
            }

            if (jsonObject.has("upsertTime") && !jsonObject.isNull("upsertTime")) {
                try {
                    dateSend = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(jsonObject.getString("upsertTime"));
                } catch (Exception e) {
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    protected LandingPageCampaign(Parcel in) {
        super(in);

        hash = in.readString();
        searchHash = in.readString();
        url = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);

        dest.writeString(hash);
        dest.writeString(searchHash);
        dest.writeString(url);
    }

    @Override
    public int describeContents() {
        return super.describeContents();
    }

    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    public String getSearchHash() {
        return searchHash;
    }

    public void setSearchHash(String searchHash) {
        this.searchHash = searchHash;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}

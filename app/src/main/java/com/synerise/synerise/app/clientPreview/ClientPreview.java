package com.synerise.synerise.app.clientPreview;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.synerise.sdk.tracking.Tracker;
import com.synerise.sdk.tracking.TrackerParams;
import com.synerise.synerise.MainActivity;
import com.synerise.synerise.R;
import com.synerise.synerise.app.ActionBarConfigurableFragment;
import com.synerise.synerise.app.adapter.EventRecyclerViewAdapter;
import com.synerise.synerise.app.adapter.SimpleClientPrevievSegmentArrayAdapter;
import com.synerise.synerise.app.adapter.SimpleClientPrevievTagArrayAdapter;
import com.synerise.synerise.app.model.ActivityFilters;
import com.synerise.synerise.app.model.Client;
import com.synerise.synerise.app.model.ClientPreviewSegment;
import com.synerise.synerise.app.model.ClientPreviewTag;
import com.synerise.synerise.app.model.Event;
import com.synerise.synerise.app.syneutil.EndlessRecyclerViewScrollListener;
import com.synerise.synerise.app.syneview.SyneBoldTextView;
import com.synerise.synerise.app.syneview.SyneTextView;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.helper.StringUtil;
import org.ocpsoft.prettytime.PrettyTime;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

public class ClientPreview extends ActionBarConfigurableFragment {

    private Client clientModel;
    private ListView tagsList;
    private ListView smartSegmentsList;
    private LinearLayout contactInformationList;
    private LinearLayout geolocationList;
    private LinearLayout additionalAttributesList;
    private ImageView avatarImage;

    private SimpleClientPrevievTagArrayAdapter tagsAdapter;
    private SimpleClientPrevievSegmentArrayAdapter segmentsAdapter;

    public static String listingPaginatorlimit = "20";
    public static String listingPaginatoroffset = "0";
    public static String listingPaginatorsinceDate = System.currentTimeMillis() + "";
    public static String listingEnumenumValue = "";

    private RecyclerView eventsList;
    private EventRecyclerViewAdapter adapter;
    private LinearLayoutManager linearLayoutManager;
    private TextView clientLocation;
    private EndlessRecyclerViewScrollListener scrollListener;

    private CardView avatarCardView;

    private ArrayList<Event> events;

    private void initEventsList() {
        events = new ArrayList<>();
        View rootView = getView();
        if (rootView == null) {
            return;
        }
        eventsList = (RecyclerView) rootView.findViewById(R.id.clientEventListView);
        adapter = new EventRecyclerViewAdapter(getActivity(), events);
        linearLayoutManager = new LinearLayoutManager(getContext());
//        eventsList.setNestedScrollingEnabled(false);
//        linearLayoutManager.setAutoMeasureEnabled(true);
        eventsList.setLayoutManager(linearLayoutManager);
        eventsList.setAdapter(adapter);

        scrollListener = new EndlessRecyclerViewScrollListener(linearLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                // Triggered only when new data needs to be appended to the list
                // Add whatever code is needed to append new items to the bottom of the list
                new GetMoreClientEvents().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            }
        };
        eventsList.addOnScrollListener(scrollListener);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.activity_client_preview, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        if (getArguments() != null) {
            clientModel = (Client) getArguments().getParcelable("client");
        }

        if (clientModel == null) {
            Toast.makeText(getContext(), "Client not found", Toast.LENGTH_SHORT).show();
        }

        Tracker.trackScreen("Client Details - " + clientModel.getDisplayName(), new TrackerParams[]{new TrackerParams("clientId", String.valueOf(clientModel.getId()))});

        View rootView = getView();
        if (rootView == null) {
            return;
        }

        tagsList = (ListView) rootView.findViewById(R.id.tagsList);
        smartSegmentsList = (ListView) rootView.findViewById(R.id.smartSegmentsList);
        contactInformationList = (LinearLayout) rootView.findViewById(R.id.contactInformationList);
        geolocationList = (LinearLayout) rootView.findViewById(R.id.geolocationList);
        additionalAttributesList = (LinearLayout) rootView.findViewById(R.id.additionalAttributesList);
        clientLocation = (TextView) rootView.findViewById(R.id.clientLocation);

        tagsAdapter = new SimpleClientPrevievTagArrayAdapter(getContext(), ClientPreviewTag.ITEMS);
        segmentsAdapter = new SimpleClientPrevievSegmentArrayAdapter(getContext(), ClientPreviewSegment.ITEMS);

        tagsList.setAdapter(tagsAdapter);

        tagsList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
                new AlertDialog.Builder(getContext())
                        .setTitle("Untag customer")
                        .setMessage("Do you really want to untag " + clientModel.firstname + " " + clientModel.lastname + "?")
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface dialog, int whichButton) {
                                String tagName = ClientPreviewTag.ITEMS.get(position).name;
                                new UntagClient().execute(String.valueOf(clientModel.id), tagName);
                                ClientPreviewTag.ITEMS.remove(position);
                                tagsAdapter.notifyDataSetChanged();

                            }
                        })
                        .setNegativeButton(android.R.string.no, null).show();

            }
        });

        smartSegmentsList.setAdapter(segmentsAdapter);


        Tracker.trackScreen("Live stream", new TrackerParams[]{new TrackerParams("cliendId", clientModel.id + ""), new TrackerParams("clientFirstame", clientModel.firstname), new TrackerParams("clientLastname", clientModel.firstname), new TrackerParams("clientEmail", clientModel.email)});


        ClientPreviewTag.clear();
        ClientPreviewSegment.clear();

        TextView avatar = (TextView) rootView.findViewById(R.id.clientPreviewAvatar);

        avatarImage = (ImageView) rootView.findViewById(R.id.clientPreviewAvatarImage);
        avatarCardView = (CardView) rootView.findViewById(R.id.avatarCardView);

        TextView clientName = rootView.findViewById(R.id.clientName);
        clientName.setEllipsize(TextUtils.TruncateAt.END);
        clientName.setMaxLines(1);
//        clientName.setText("" + clientModel.firstname + " " + clientModel.lastname);
//        SmoothCollapsingToolbarLayout collapsingToolbarLayout = (SmoothCollapsingToolbarLayout)rootView.findViewById(R.id.expandingToolbar);
//        TextView title = (TextView)rootView.findViewById(R.id.expandedTitle);
//        title.setText("" + clientModel.firstname + " " + clientModel.lastname);

        setAvatarBackground(clientModel.firstname.isEmpty() ? '-' : clientModel.firstname.charAt(0));

        avatar.setText("" + (clientModel.firstname == null || clientModel.firstname.isEmpty() ? "Anonymous" : clientModel.firstname.charAt(0)) + (clientModel.lastname == null || clientModel.lastname.isEmpty() ? "" : clientModel.lastname.charAt(0)));

        ISO8601 conv = new ISO8601();

        Date created = new Date();
        Date lastAction = new Date();
        try {
            Log.v("clientModel.created", clientModel.created);
            created = conv.toCalendar(clientModel.created).getTime();
            lastAction = conv.toCalendar(clientModel.lastColumn).getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }


        SimpleDateFormat dateFormat = new SimpleDateFormat("MMM d,yy HH:mm");


        ((TextView) rootView.findViewById(R.id.sinceTime)).setText(dateFormat.format(created));
        ((TextView) rootView.findViewById(R.id.lastUseTime)).setText(new PrettyTime().setLocale(Locale.ENGLISH).format(lastAction));
//        ((TextView) rootView.findViewById(R.id.scoringValue)).setText(clientModel.points + "");

        new LoadImageInBackground().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

        new GetClientData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

        new GetSubsriptions().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

        initEventsList();

        new GetClientEvents().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

//        ((FloatingActionButton) rootView.findViewById(R.id.clientMenuActivities)).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                LinearLayout container = ((LinearLayout) rootView.findViewById(R.id.clientMenuContent));
//                container.removeAllViewsInLayout();
//                LayoutInflater layoutInflater = (LayoutInflater)
//                        getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//                container.addView(layoutInflater.inflate(R.layout.client_preview_activities, container, false));
//
//                new GetClientEvents().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
//            }
//        });
//
//        ((FloatingActionButton) rootView.findViewById(R.id.clientMenuMessage)).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                Toast.makeText(ClientPreview.this, "This option will be available soon", Toast.LENGTH_SHORT).show();
//            }
//        });
//

        Spinner spinner = (Spinner) rootView.findViewById(R.id.activityFilterSpinner);
        ArrayAdapter adapter = new ArrayAdapter(getContext(), android.R.layout.simple_spinner_dropdown_item, ActivityFilters.ITEMS);
        spinner.setAdapter(adapter);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                listingEnumenumValue = ActivityFilters.ITEMS.get(position).value;
                new GetClientEvents().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });


        TextView loadAdminData = (TextView) rootView.findViewById(R.id.loadAdminData);
        loadAdminData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent1 = new Intent(getContext(), AdminDataActivity.class);
                intent1.putExtra("clientId", clientModel.id + "");
                startActivity(intent1);
            }
        });

    }

    @Override
    public void setUpActionBar() {
        ActionBar actionBar = (ActionBar) ((AppCompatActivity) getActivity()).getSupportActionBar();
        if (actionBar != null) {
            actionBar.show();
            actionBar.setTitle(MainActivity.businessProfileName != null ? MainActivity.businessProfileName : "Synerise");
            actionBar.setSubtitle(clientModel.firstname + " " + clientModel.lastname);
            actionBar.setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.colorPrimaryCRM)));
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                Window window = getActivity().getWindow();
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                window.setStatusBarColor(getResources().getColor(R.color.colorPrimaryCRM));
            }
            getActivity().setTheme(R.style.AppThemeCRM);
            actionBar.setElevation(0);
        }
    }


    private class GetSubsriptions extends AsyncTask<Void, Void, String> {
        protected String doInBackground(Void... params) {
            try {

                HttpPost httpPost = new HttpPost("https://app.synerise.com/clients/subscriptions/" + clientModel.id);

                HttpResponse eventList = MainActivity.httpClient.execute(httpPost);

                HttpEntity eventListEntity = eventList.getEntity();

                final String body = EntityUtils.toString(eventListEntity);

                JSONObject jsonObject = null;
                JSONArray jsonArray = null;


                try {
                    jsonObject = new JSONObject(body);
                    jsonArray = jsonObject.getJSONArray("data");
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if (jsonArray == null) {
                    return null;
                }

                final ArrayList<String> agreements = new ArrayList<>();

                for (int i = 0; i < jsonArray.length(); i++) {
                    agreements.add(jsonArray.getString(i));
                }

                Activity activity = getActivity();
                if (activity == null) {
                    return "not ok";
                }

                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        View rootView = getView();
                        if (rootView == null) {
                            return;
                        }
                        ((TextView) rootView.findViewById(R.id.agreementsValue)).setText(StringUtil.join(agreements, ", "));
                    }
                });

            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (NullPointerException e) {

            } catch (IllegalStateException e) {

            }
//            catch (JSONException e) {
//                e.printStackTrace();
//            }
            return "ok";
        }

    }

    private class GetClientEvents extends AsyncTask<Void, Void, String> {
        protected String doInBackground(Void... params) {
            try {

                HttpGet httpget = new HttpGet("https://app.synerise.com/activity-histories?clientId=" + clientModel.id + getListingParams());

                HttpResponse eventList = MainActivity.httpClient.execute(httpget);

                HttpEntity eventListEntity = eventList.getEntity();

                final String body = EntityUtils.toString(eventListEntity);

                JSONArray jsonArray = null;

                try {
                    jsonArray = new JSONArray(body);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if (jsonArray == null) {
                    return null;
                }
                ArrayList<Event> newEvents = new ArrayList<>();

                for (int i = 0; i < jsonArray.length(); i++) {
                    newEvents.add(new Event(jsonArray.getJSONObject(i), getContext()));
                }

                events.clear();
                events.addAll(newEvents);

                Activity activity = getActivity();
                if (activity == null) {
                    return "not ok";
                }

                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        adapter.notifyDataSetChanged();
                    }
                });
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (NullPointerException e) {

            } catch (IllegalStateException e) {

            }
            return "ok";
        }

    }

    private class GetMoreClientEvents extends AsyncTask<Void, Void, String> {
        protected String doInBackground(Void... params) {
            try {

                Log.v("GetMoreClientEvents", "GetMoreClientEvents");
                listingPaginatorsinceDate = events.get(events.size() - 1).createdOriginal;

                HttpGet httpget = new HttpGet("https://app.synerise.com/activity-histories?clientId=" + clientModel.id + getListingParams());

                HttpResponse eventList = MainActivity.httpClient.execute(httpget);

                HttpEntity eventListEntity = eventList.getEntity();

                final String body = EntityUtils.toString(eventListEntity);

                Log.v("GetMoreClientEventsbdy", body);


                JSONArray jsonArray = null;

                try {
                    jsonArray = new JSONArray(body);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if (jsonArray == null) {
                    return null;
                }
                ArrayList<Event> newEvents = new ArrayList<>();


                if (jsonArray.length() == 0) {
                    scrollListener.setEnabled(false);
                    return "";

                }

                for (int i = 0; i < jsonArray.length(); i++) {
                    newEvents.add(new Event(jsonArray.getJSONObject(i), getContext()));
                }

                Log.v("new Events", newEvents.toString());
                events.addAll(newEvents);
                Log.v("events", events.toString());

//                Log.v("new Events")

                Activity activity = getActivity();
                if (activity == null) {
                    return "not ok";
                }

                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        adapter.notifyDataSetChanged();
                    }
                });
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (NullPointerException e) {

            } catch (IllegalStateException e) {

            }
            return "ok";
        }

    }

    private class GetClientData extends AsyncTask<Void, Void, String> {
        protected String doInBackground(Void... params) {
            try {


                HttpGet httpget = new HttpGet(String.format("https://app.synerise.com/clients/%d/mobileSummary", clientModel.id));

                HttpResponse clientData = MainActivity.httpClient.execute(httpget);

                HttpEntity clientDataEntity = clientData.getEntity();

                final String body = EntityUtils.toString(clientDataEntity);


                JSONObject jsonObject = null;

                try {
                    jsonObject = new JSONObject(body);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if (jsonObject == null) {
                    return null;
                }

                final JSONArray segments = jsonObject.getJSONArray("segments");
                final JSONArray tags = jsonObject.getJSONArray("tags");
                final JSONObject contactInformation = jsonObject.getJSONObject("contactInformation");
                final JSONObject geolocation = jsonObject.getJSONObject("geolocation");
                JSONObject attributes = null;
                try {
                    attributes = jsonObject.getJSONObject("attributes");
                } catch (JSONException e) {
                }
                for (int i = 0; i < tags.length(); i++) {
                    JSONObject object = tags.getJSONObject(i);
                    ClientPreviewTag.add(object.getString("id"), object.getString("name"));
                }

                for (int i = 0; i < segments.length(); i++) {
                    JSONObject object = segments.getJSONObject(i);
                    ClientPreviewSegment.add(object.getString("id"), object.getString("name"));
                }


                final JSONObject finalAttributes = attributes;

                Activity activity = getActivity();
                if (activity == null) {
                    return "not ok";
                }

                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        tagsAdapter.notifyDataSetChanged();
                        segmentsAdapter.notifyDataSetChanged();


                        MainActivity.setListViewHeightBasedOnChildren(tagsList);
                        MainActivity.setListViewHeightBasedOnChildren(smartSegmentsList);
                        String location = "";
                        try {
                            if (!contactInformation.isNull("mail")) {
                                SyneTextView mail = new SyneTextView(getContext());
                                mail.setText(contactInformation.getString("mail"));
                                mail.setTextColor(getResources().getColor(R.color.fancyBlue));
                                contactInformationList.addView(mail);
                            }
                            if (!contactInformation.isNull("phone")) {
                                SyneTextView phone = new SyneTextView(getContext());
                                phone.setText(contactInformation.getString("phone"));
                                phone.setTextColor(getResources().getColor(R.color.fancyBlue));
                                contactInformationList.addView(phone);
                            }

                            if (!contactInformation.isNull("address")) {
                                SyneTextView address = new SyneTextView(getContext());
                                address.setText(contactInformation.getString("address"));
//                                contactInformationList.addView(address);
                                location = address.getText().toString();
                            }
                            if (!contactInformation.isNull("city")) {
                                SyneTextView city = new SyneTextView(getContext());
                                city.setText(contactInformation.getString("city"));
//                                contactInformationList.addView(city);
                                if (!location.isEmpty()) {
                                    location += ", ";
                                }
                                location += city.getText().toString();

                            }

//                            if (!contactInformation.isNull("zipCode")) {
//                                SyneTextView zipCode = new SyneTextView(getContext());
//                                zipCode.setText(contactInformation.getString("zipCode"));
//                                contactInformationList.addView(zipCode);
//                            }
//
//                            if (!contactInformation.isNull("province")) {
//                                SyneTextView province = new SyneTextView(getContext());
//                                province.setText(contactInformation.getString("province"));
//                                contactInformationList.addView(province);
//                            }

                            addGeolocation(geolocation, "geoLocIp", "IP address: ");
                            addGeolocation(geolocation, "geoLocIsp", "ISP: ");
                            addGeolocation(geolocation, "geoLocAs", "AS: ");
                            addGeolocation(geolocation, "geoLocCity", "City: ");
                            addGeolocation(geolocation, "geoLocProvince", "Province: ");
                            addGeolocation(geolocation, "geoLocCountry", "Country: ");
                            addGeolocation(geolocation, "getLocZipCode", "Postal code: ");

                            final String finalLocation = location;
                            Activity activity = getActivity();
                            if (activity == null) {
                                return;
                            }

                            activity.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    clientLocation.setText(finalLocation);
                                }
                            });

                            if (finalAttributes != null) {
                                Iterator<?> keys = finalAttributes.keys();
                                while (keys.hasNext()) {
                                    String key = (String) keys.next();
                                    addAttribute(key, finalAttributes.getString(key));
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                });
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (NullPointerException e) {

            } catch (IllegalStateException e) {

            }
            return "ok";
        }

    }

    private void addGeolocation(JSONObject geolocation, String key, String lab) {
        if (!geolocation.isNull(key)) {
            LinearLayout linearLayout = new LinearLayout(getContext());
            linearLayout.setOrientation(LinearLayout.HORIZONTAL);
            SyneTextView label = new SyneTextView(getContext());
            label.setText(lab);
            SyneBoldTextView ip = new SyneBoldTextView(getContext());
            try {
                ip.setText(geolocation.getString(key));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            linearLayout.addView(label);
            linearLayout.addView(ip);
            geolocationList.addView(linearLayout);
        }
    }

    private void addAttribute(String key, String val) {
        LinearLayout linearLayout = new LinearLayout(getContext());
        linearLayout.setOrientation(LinearLayout.HORIZONTAL);
        SyneTextView label = new SyneTextView(getContext());
        label.setText(key + ": ");
        SyneBoldTextView value = new SyneBoldTextView(getContext());
        value.setText(val);
        linearLayout.addView(label);
        linearLayout.addView(value);
        additionalAttributesList.addView(linearLayout);

    }

    private String getListingParams() {
        return "&listing[Paginator][limit]=" + listingPaginatorlimit +
                "&listing[Paginator][offset]=" + listingPaginatoroffset +
                "&listing[Paginator][sinceDate]=" + listingPaginatorsinceDate +
                "&listing[Enum][enumValue]=" + java.net.URLEncoder.encode(listingEnumenumValue);
    }


    private class LoadImageInBackground extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {
            Bitmap bmp = null;

            String url = "";

            try {
//                if (clientModel.externalAvatarUrl != null && clientModel.externalAvatarUrl.compareToIgnoreCase("null") != 0) {
//                    url = clientModel.externalAvatarUrl;
//                } else {
                url = clientModel.avatarUrl;
//                }
                bmp = BitmapFactory.decodeStream(new URL(url).openConnection().getInputStream());

// bmp = Bitmap.createScaledBitmap(bmp, 150, 150, false);
            } catch (MalformedURLException e) {
                Log.v("SYNERISE", "Malformed URL " + url);
            } catch (IOException e) {
                e.printStackTrace();
            }

            final Bitmap finalBmp = bmp;

            Activity activity = getActivity();
            if (activity == null) {
                return null;
            }

            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    avatarImage.setImageBitmap(finalBmp);
                }
            });
            return null;
        }
    }

    private void setAvatarBackground(char first) {
        switch (first) {
            case 'a':
            case 'A':
                avatarCardView.setBackgroundColor(Color.parseColor("#A8f44336"));
                break;
            case 'b':
            case 'B':
                avatarCardView.setBackgroundColor(Color.parseColor("#A8E91E63"));
                break;
            case 'c':
            case 'C':
                avatarCardView.setBackgroundColor(Color.parseColor("#A89C27B0"));
                break;
            case 'd':
            case 'D':
                avatarCardView.setBackgroundColor(Color.parseColor("#A8673AB7"));
                break;
            case 'e':
            case 'E':
                avatarCardView.setBackgroundColor(Color.parseColor("#A83F51B5"));
                break;
            case 'f':
            case 'F':
                avatarCardView.setBackgroundColor(Color.parseColor("#A82196F3"));
                break;
            case 'g':
            case 'G':
                avatarCardView.setBackgroundColor(Color.parseColor("#A803A9F4"));
                break;
            case 'h':
            case 'H':
                avatarCardView.setBackgroundColor(Color.parseColor("#A800BCD4"));
                break;
            case 'i':
            case 'I':
                avatarCardView.setBackgroundColor(Color.parseColor("#A8009688"));
                break;
            case 'j':
            case 'J':
                avatarCardView.setBackgroundColor(Color.parseColor("#A84CAF50"));
                break;
            case 'k':
            case 'K':
                avatarCardView.setBackgroundColor(Color.parseColor("#A88BC34A"));
                break;
            case 'l':
            case 'L':
                avatarCardView.setBackgroundColor(Color.parseColor("#A8CDDC39"));
                break;
            case 'm':
            case 'M':
                avatarCardView.setBackgroundColor(Color.parseColor("#A8FFC107"));
                break;
            case 'n':
            case 'N':
                avatarCardView.setBackgroundColor(Color.parseColor("#A8FF9800"));
                break;
            case 'o':
            case 'O':
                avatarCardView.setBackgroundColor(Color.parseColor("#A8FF5722"));
                break;
            case 'p':
            case 'P':
                avatarCardView.setBackgroundColor(Color.parseColor("#A8795548"));
                break;
            case 'q':
            case 'Q':
                avatarCardView.setBackgroundColor(Color.parseColor("#A89E9E9E"));
                break;
            case 'r':
            case 'R':
                avatarCardView.setBackgroundColor(Color.parseColor("#A89E9E9E"));
                break;
            case 's':
            case 'S':
                avatarCardView.setBackgroundColor(Color.parseColor("#A8607D8B"));
                break;
            case 't':
            case 'T':
                avatarCardView.setBackgroundColor(Color.parseColor("#A8ff1744"));
                break;
            case 'u':
            case 'U':
                avatarCardView.setBackgroundColor(Color.parseColor("#A864DD17"));
                break;
            case 'v':
            case 'V':
                avatarCardView.setBackgroundColor(Color.parseColor("#A80091EA"));
                break;
            case 'w':
            case 'W':
                avatarCardView.setBackgroundColor(Color.parseColor("#A8F50057"));
                break;
            case 'x':
            case 'X':
                avatarCardView.setBackgroundColor(Color.parseColor("#A8FFAB00"));
                break;
            case 'y':
            case 'Y':
                avatarCardView.setBackgroundColor(Color.parseColor("#A8a7d975"));
                break;
            case 'z':
            case 'Z':
                avatarCardView.setBackgroundColor(Color.parseColor("#A8FFAB40"));
                break;
            default:
                avatarCardView.setBackgroundColor(Color.parseColor("#A8f44336"));
                break;
        }
    }


    private class UntagClient extends AsyncTask<String, Void, String> {
        protected String doInBackground(String... params) {
            try {

                HttpPost httpPost = new HttpPost("https://app.synerise.com/rest/clients/tags/untag-many");
                List<NameValuePair> nvps = new ArrayList<NameValuePair>();
                nvps.add(new BasicNameValuePair("entitysIds[]", params[0]));
                nvps.add(new BasicNameValuePair("tagsNames[]", params[1]));

                httpPost.setEntity(new UrlEncodedFormEntity(nvps, HTTP.UTF_8));

                HttpResponse response = MainActivity.httpClient.execute(httpPost);

                HttpEntity entity = response.getEntity();

                if (entity == null) {
                    return null;
                }

                Activity activity = getActivity();
                if( activity == null) {
                    return "not ok";
                }

                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getContext(), "Success", Toast.LENGTH_SHORT).show();
                    }
                });
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (NullPointerException e) {

            } catch (IllegalStateException e) {

            }
//            catch (JSONException e) {
//                e.printStackTrace();
//            }
            return "ok";
        }

    }

    public final class ISO8601 {
        /**
         * Transform Calendar to ISO 8601 string.
         */
        public String fromCalendar(final Calendar calendar) {
            Date date = calendar.getTime();
            String formatted = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")
                    .format(date);
            return formatted.substring(0, 22) + ":" + formatted.substring(22);
        }

        /**
         * Get current date and time formatted as ISO 8601 string.
         */
        public String now() {
            return fromCalendar(GregorianCalendar.getInstance());
        }

        /**
         * Transform ISO 8601 string to Calendar.
         */
        public Calendar toCalendar(final String iso8601string)
                throws ParseException {
            Calendar calendar = GregorianCalendar.getInstance();
            String s = iso8601string.replace("Z", "+00:00");
            try {
                s = s.substring(0, 22) + s.substring(23);  // to get rid of the ":"
            } catch (IndexOutOfBoundsException e) {
                throw new ParseException("Invalid length", 0);
            }
            Date date = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ").parse(s);
            calendar.setTime(date);
            return calendar;
        }
    }

}

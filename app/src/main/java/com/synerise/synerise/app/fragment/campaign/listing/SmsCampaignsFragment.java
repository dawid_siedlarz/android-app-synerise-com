package com.synerise.synerise.app.fragment.campaign.listing;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;

import com.synerise.synerise.MainActivity;
import com.synerise.synerise.R;
import com.synerise.synerise.app.ActionBarConfigurableFragment;
import com.synerise.synerise.app.adapter.CampaignRecyclerViewAdapter;
import com.synerise.synerise.app.model.campaign.Campaign;
import com.synerise.synerise.app.model.campaign.SmsCampaign;
import com.synerise.synerise.app.syneutil.EndlessRecyclerViewScrollListener;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.helper.StringUtil;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;


public class SmsCampaignsFragment extends ActionBarConfigurableFragment {
    private int listing_Paginator_offset = 0;
    private int listing_Paginator_limit = 50;
    private int counter = 0;

    private CampaignRecyclerViewAdapter adapter;
    private RecyclerView listView;
    private LinearLayoutManager linearLayoutManager;
    private RecyclerView.OnScrollListener scrollListener;
    private ArrayList<Campaign> campaigns;


    @Override
    public void setUpActionBar() {
        ActionBar actionBar = (ActionBar) ((AppCompatActivity) getActivity()).getSupportActionBar();
        if (actionBar != null) {
            actionBar.show();
            actionBar.setTitle(MainActivity.businessProfileName != null ? MainActivity.businessProfileName : "Synerise");
            actionBar.setSubtitle("SMS campaigns");
            actionBar.setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.colorPrimaryCampaigns)));
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                Window window = getActivity().getWindow();
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                window.setStatusBarColor(getResources().getColor(R.color.colorPrimaryDarkCampaigns));
            }
            actionBar.setElevation(0);
        }
    }

    public SmsCampaignsFragment() {
        // Required empty public constructor
    }

    public static SmsCampaignsFragment newInstance() {
        SmsCampaignsFragment fragment = new SmsCampaignsFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_sms_campaigns, container, false);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }


    private void initList() {
        campaigns = new ArrayList<>();
        adapter = new CampaignRecyclerViewAdapter(campaigns, this);
        View rootView = getView();
        if (rootView == null) {
            return;
        }
        listView = rootView.findViewById(R.id.campaignsRecyclerView);
        linearLayoutManager = new LinearLayoutManager(getActivity());
        listView.setLayoutManager(linearLayoutManager);
        listView.setAdapter(adapter);
        scrollListener = new EndlessRecyclerViewScrollListener(linearLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                new GetCampaigns().execute(listing_Paginator_limit);
            }
        };
        listView.addOnScrollListener(scrollListener);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initList();
        getCampaigns();
    }

    public void getCampaigns() {
        new GetCampaigns().execute(0);
    }

    private class GetCampaigns extends AsyncTask<Integer, Void, String> {
        protected String doInBackground(Integer... params) {
            try {

                listing_Paginator_offset = params[0] == 0 ? 0 : listing_Paginator_offset + params[0];
                counter = 0;

                HttpGet httpget = new HttpGet("https://app.synerise.com/rest/sms-message/" + getListingParams());

                HttpResponse eventList = MainActivity.httpClient.execute(httpget);
                if (eventList == null) {
                    return "not OK";
                }

                HttpEntity entity = eventList.getEntity();

                final String body = EntityUtils.toString(entity);


                JSONObject jsonObject = null;

                try {
                    jsonObject = new JSONObject(body);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if (jsonObject == null) {
                    return null;
                }

                Iterator<?> keys = jsonObject.keys();

                ArrayList<Campaign> newCampaigns = new ArrayList<>();
                while (keys.hasNext()) {
                    String key = (String) keys.next();
                    if (StringUtil.isNumeric(key)) {
                        newCampaigns.add(new SmsCampaign(jsonObject.getJSONObject(key)));
                    }
                }

                if (listing_Paginator_offset == 0) {
                    campaigns.clear();
                }
                campaigns.addAll(newCampaigns);

                counter = jsonObject.has("counter") ? jsonObject.getInt("counter") : 0;

                Activity activity = getActivity();
                if (activity != null) {

                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            adapter.notifyDataSetChanged();
                        }
                    });
                }
            } catch (IOException | JSONException e) {
                e.printStackTrace();
            } catch (NullPointerException | IllegalStateException e) {

            }
            return "ok";
        }
    }

    private String getListingParams() {
        return "?listing[Paginator][limit]=" + listing_Paginator_limit + "&listing[Paginator][offset]=" + listing_Paginator_offset;
    }
}

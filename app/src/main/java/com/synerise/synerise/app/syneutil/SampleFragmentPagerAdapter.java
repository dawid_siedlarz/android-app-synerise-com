package com.synerise.synerise.app.syneutil;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.view.ViewGroup;

import com.synerise.synerise.app.fragment.CommerceBasketsFragment;
import com.synerise.synerise.app.fragment.CommerceOverviewFragment;
import com.synerise.synerise.app.fragment.CommerceTransactionsFragment;
import com.synerise.synerise.app.syneview.CustomPager;


public class SampleFragmentPagerAdapter extends FragmentStatePagerAdapter {
    private String tabTitles[] = new String[]{"Overview", "Transactions", "Baskets"};
    private Fragment[] fragments;
    private int mCurrentPosition = -1;

    public SampleFragmentPagerAdapter(FragmentManager fm) {
        super(fm);
        CommerceOverviewFragment commerceOverview = new CommerceOverviewFragment();
        CommerceTransactionsFragment commerceTransactions = new CommerceTransactionsFragment();
        CommerceBasketsFragment commerceBaskets = new CommerceBasketsFragment();
        fragments = new Fragment[]{commerceOverview, commerceTransactions, commerceBaskets};
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Override
    public Fragment getItem(int position) {
        return fragments[position];
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return tabTitles[position];
    }

    @Override
    public void setPrimaryItem(ViewGroup container, int position, Object object) {
        super.setPrimaryItem(container, position, object);
        if (position != mCurrentPosition) {
            Fragment fragment = (Fragment) object;
            CustomPager pager = (CustomPager) container;
            if (fragment != null && fragment.getView() != null) {
                mCurrentPosition = position;
                pager.measureCurrentView(fragment.getView());
            }
        }
    }
}
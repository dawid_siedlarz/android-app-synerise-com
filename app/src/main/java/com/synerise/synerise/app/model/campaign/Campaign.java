package com.synerise.synerise.app.model.campaign;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Campaign implements Parcelable {
    protected int id;
    protected Date dateSend;
    protected String title;
    protected String name;
    protected String status;
    protected CampaignRecipients recipients;

    public Campaign(JSONObject jsonObject) {
        try {
            if (jsonObject.has("id") && !jsonObject.isNull("id")) {
                id = jsonObject.getInt("id");
            }

            if (jsonObject.has("dateSend") && !jsonObject.isNull("dateSend")) {
                try {
                    dateSend = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(jsonObject.getJSONObject("dateSend").getString("date"));
                } catch (Exception e) {
                    dateSend = new Date(jsonObject.getLong("dateSend") * 1000);
                }
            }
            if (jsonObject.has("recipients") && !jsonObject.isNull("recipients")) {
                recipients = new CampaignRecipients(jsonObject.getJSONObject("recipients"));
            }
            if (jsonObject.has("status")) {
                status = jsonObject.getString("status");
            }
            if (jsonObject.has("title")) {
                title = jsonObject.getString("title");
            }
            if (jsonObject.has("name"))
            name = jsonObject.getString("name");

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    protected Campaign(Parcel in) {
        id = in.readInt();
        title = in.readString();
        name = in.readString();
        status = in.readString();
        recipients = in.readParcelable(CampaignRecipients.class.getClassLoader());
        dateSend = (java.util.Date) in.readSerializable();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(title);
        dest.writeString(name);
        dest.writeString(status);
        dest.writeParcelable(recipients, flags);
        dest.writeSerializable(dateSend);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Campaign> CREATOR = new Creator<Campaign>() {
        @Override
        public Campaign createFromParcel(Parcel in) {
            return new Campaign(in);
        }

        @Override
        public Campaign[] newArray(int size) {
            return new Campaign[size];
        }
    };

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getDateSend() {
        return dateSend;
    }

    public void setDateSend(Date dateSend) {
        this.dateSend = dateSend;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public CampaignRecipients getRecipients() {
        return recipients;
    }

    public void setRecipients(CampaignRecipients recipients) {
        this.recipients = recipients;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRecipientsReadableString() {
        return recipients != null ? recipients.toReadableString() : "Recipients not available";
    }
}
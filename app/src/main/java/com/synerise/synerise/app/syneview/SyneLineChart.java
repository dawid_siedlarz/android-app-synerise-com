package com.synerise.synerise.app.syneview;

import android.content.Context;
import android.util.AttributeSet;

import com.github.mikephil.charting.charts.LineChart;

/**
 * Created by dsiedlarz on 24.02.2017.
 */

public class SyneLineChart extends LineChart {

    public SyneLineChart(Context context, AttributeSet attrs) {
        super(context, attrs);
        SyneComplexLineChart.initLineChart(this);
    }

}

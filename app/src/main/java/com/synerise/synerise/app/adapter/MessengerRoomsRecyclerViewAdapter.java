package com.synerise.synerise.app.adapter;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.synerise.synerise.MainActivity;
import com.synerise.synerise.R;
import com.synerise.synerise.app.controller.MessengerUserController;
import com.synerise.synerise.app.model.FilterAttributes;
import com.synerise.synerise.messenger.fragment.ConversationFragment;
import com.synerise.synerise.messenger.model.MessengerRoom;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;


public class MessengerRoomsRecyclerViewAdapter extends RecyclerView.Adapter<MessengerRoomsRecyclerViewAdapter.RoomViewHolder> {
    private final Context context;
    private final Activity activity;
    private final ArrayList<MessengerRoom> values;
    private MessengerUserController messengerUserController;

    SimpleDateFormat format = new SimpleDateFormat("yy/MM/dd HH:mm");
    private FilterAttributes.FilterAttribute filterAttribute;


    public MessengerRoomsRecyclerViewAdapter(Activity activity, ArrayList<MessengerRoom> values) {
        this.activity = activity;
        this.context = activity.getApplicationContext();
        this.values = values;
        this.messengerUserController = new MessengerUserController(activity);
    }

    @Override
    public RoomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_element_messenger_room, null);
        RoomViewHolder viewHolder = new RoomViewHolder(view);
        return viewHolder;
    }


    @Override
    public void onBindViewHolder(RoomViewHolder holder, final int position) {

        final MessengerRoom roomModel = values.get(position);
        holder.mRoom = roomModel;

        Date date = roomModel.getLastMessageTime();
        if (date != null) {
            holder.mTime.setText(format.format(date));
        }

        holder.mMessageBody.removeAllViewsInLayout();
        ViewGroup parent = (ViewGroup) roomModel.getLastMessage().getMessageBody().getBody().getParent();
        if (parent != null) {
            parent.removeAllViewsInLayout();
        }


        View body = roomModel.getLastMessage().getMessageBody().getBody();
        if(body instanceof TextView) {
            ((TextView)body).setTextSize(TypedValue.COMPLEX_UNIT_DIP, 10);
        }
        holder.mMessageBody.addView(roomModel.getLastMessage().getMessageBody().getBody());

        messengerUserController.setMessengerRoomHolder(holder);
        if (roomModel.getLastMessageAuthorType().compareToIgnoreCase("vendor") == 0) {
            holder.mLastMessageAvatarBox.setVisibility(View.GONE);
        } else {
            holder.mLastMessageAvatarBox.setVisibility(View.VISIBLE);
        }
        holder.mNotificationCountBox.setVisibility(View.GONE);

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putParcelable(ConversationFragment.ARG_ROOM, roomModel);
                Fragment fragment = new ConversationFragment();
                fragment.setArguments(bundle);

                MainActivity.addFragment(fragment);
            }
        });
    }

    @Override
    public int getItemCount() {
        return values.size();
    }

    public class RoomViewHolder extends RecyclerView.ViewHolder {
        private MessengerRoom mRoom;
        private TextView mTime;
        private TextView mName;
        private ViewGroup mMessageBody;
        private ImageView mOwnerAvatar;
        private TextView mOwnerAvatarInitials;
        private RelativeLayout mLastMessageAvatarBox;
        private View mView;
        private RelativeLayout mNotificationCountBox;


        public RoomViewHolder(View view) {
            super(view);
            this.mView = view;

            this.mTime = (TextView) view.findViewById(R.id.roomTime);
            this.mName = (TextView) view.findViewById(R.id.roomName);
            this.mMessageBody = (ViewGroup) view.findViewById(R.id.roomMessageBody);
            this.mOwnerAvatar = (ImageView) view.findViewById(R.id.ownerAvatar);
            this.mOwnerAvatarInitials = (TextView) view.findViewById(R.id.ownerAvatarInitials);
            this.mLastMessageAvatarBox = (RelativeLayout) view.findViewById(R.id.lastMessageAvatarBox);
            this.mNotificationCountBox = (RelativeLayout) view.findViewById(R.id.notificationCountBox);
        }

        public MessengerRoom getmRoom() {
            return mRoom;
        }

        public TextView getmTime() {
            return mTime;
        }

        public TextView getmName() {
            return mName;
        }

        public ImageView getmOwnerAvatar() {
            return mOwnerAvatar;
        }

        public TextView getmOwnerAvatarInitials() {
            return mOwnerAvatarInitials;
        }

        public View getmView() {
            return mView;
        }
    }

    private class getName extends AsyncTask<RoomViewHolder, Void, Void> {
        @Override
        protected Void doInBackground(RoomViewHolder... params) {
            final RoomViewHolder holder = params[0];
            final String name = holder.mRoom.getOwner().getNameFromApi();
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    holder.mName.setText(name);
                }
            });

            return null;
        }
    }

    public static ViewGroup getParent(View view) {
        return (ViewGroup) view.getParent();
    }

    public static void removeView(View view) {
        ViewGroup parent = getParent(view);
        if (parent != null) {
            parent.removeView(view);
        }
    }

    public static void replaceView(View currentView, View newView) {
        ViewGroup parent = getParent(currentView);
        if (parent == null) {
            return;
        }
        final int index = parent.indexOfChild(currentView);
        removeView(currentView);
        removeView(newView);
        parent.addView(newView, index);
    }
}

package com.synerise.synerise.app.fragment;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;

import com.synerise.sdk.tracking.Tracker;
import com.synerise.sdk.tracking.TrackerParams;
import com.synerise.synerise.MainActivity;
import com.synerise.synerise.R;
import com.synerise.synerise.SyneriseOnFragmentInteractionListener;
import com.synerise.synerise.app.ActionBarConfigurableFragment;
import com.synerise.synerise.app.adapter.MyBusinessProfileRecyclerViewAdapter;
import com.synerise.synerise.app.decorator.BusinessProfileItemDecoration;
import com.synerise.synerise.app.model.BusinessProfile;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;

import java.io.IOException;

/**
 * A fragment representing a list of Items.
 * <p/>
 * Activities containing this fragment MUST implement the {@link }
 * interface.
 */
public class BusinessProfileFragment extends ActionBarConfigurableFragment {

    // TODO: Customize parameter argument names
    private static final String ARG_COLUMN_COUNT = "column-count";
    // TODO: Customize parameters
    private int mColumnCount = 1;
    private SyneriseOnFragmentInteractionListener mListener;
private RecyclerView recyclerView;
    private MyBusinessProfileRecyclerViewAdapter adapter;
    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public BusinessProfileFragment() {
    }

    // TODO: Customize parameter initialization
    @SuppressWarnings("unused")
    public static BusinessProfileFragment newInstance(int columnCount) {
        BusinessProfileFragment fragment = new BusinessProfileFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_COLUMN_COUNT, columnCount);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            mColumnCount = getArguments().getInt(ARG_COLUMN_COUNT);
        }
    }

    @Override
    public void setUpActionBar() {
        ActionBar actionBar = (ActionBar) ((AppCompatActivity) getActivity()).getSupportActionBar();
        if (actionBar != null) {
            actionBar.show();
            actionBar.setTitle(MainActivity.businessProfileName != null? MainActivity.businessProfileName : "Synerise");
            actionBar.setSubtitle("Choose your business profile");
            actionBar.setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.colorPrimary)));
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                Window window = getActivity().getWindow();
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                window.setStatusBarColor(getResources().getColor(R.color.colorPrimary));
            }
            getActivity().setTheme(R.style.AppThemeDashboard);
            actionBar.setElevation(0);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
//        container.removeAllViewsInLayout();
        View view = inflater.inflate(R.layout.fragment_businessprofile_list, container, false);

        ActionBar actionBar =((AppCompatActivity)getActivity()).getSupportActionBar();
        if (actionBar!=null){{
            actionBar.setSubtitle("Choose business profile");
        }}

        // Set the adapter
        if (view instanceof RecyclerView) {
            Context context = view.getContext();
            recyclerView = (RecyclerView) view;
            recyclerView.addItemDecoration(new BusinessProfileItemDecoration(context));
       LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context);
            recyclerView.setLayoutManager(linearLayoutManager);

            adapter = new MyBusinessProfileRecyclerViewAdapter(BusinessProfile.ITEMS, mListener, this);
            recyclerView.setAdapter(adapter);


        }

        new GetBusinessProfiles().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
//        new ChooseProfile().execute("https://app.synerise.com/panel/profile/set/16");

        return view;

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Tracker.trackScreen("Choose business profile", new TrackerParams[]{});
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof SyneriseOnFragmentInteractionListener) {
            mListener = (SyneriseOnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnListFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


  private class GetBusinessProfiles extends AsyncTask<String, Void, String> {
        protected String doInBackground(String... params) {
            try {


                HttpGet httpGet = new HttpGet("https://app.synerise.com/rest/panel/get-all");
                httpGet.addHeader("PHPSESSID", MainActivity.httpClient.getPHPSESSID());
                HttpResponse response2 = MainActivity.httpClient.execute(httpGet);
                if (response2 == null ){
                    return "not OK";
                }

                HttpEntity entity = response2.getEntity();

                final String body = EntityUtils.toString(entity);
                JSONArray jsonArray = null;

                try {
                    jsonArray = new JSONArray(body);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                BusinessProfile.clear();

                for (int i = 0; i < jsonArray.length(); i++) {
                    BusinessProfile.add(jsonArray.getJSONObject(i));
                }
                Activity activity = getActivity();
                if (activity != null) {
                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            MainActivity.setDrawerState(false);
                            adapter.notifyDataSetChanged();

                        }
                    });

                }

            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {e.printStackTrace();}
            catch (NullPointerException e) {

            } catch (IllegalStateException e) {

            } catch (JSONException e) {
                e.printStackTrace();
            }
            return "ok";
        }
    }
}

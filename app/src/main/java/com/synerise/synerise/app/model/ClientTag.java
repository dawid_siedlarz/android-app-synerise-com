package com.synerise.synerise.app.model;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by dsiedlarz on 15.12.2016.
 */

public class ClientTag {
    private String id;
    private String name;
    private String color;
    private String type;

    public Boolean getChoosed() {
        return choosed;
    }

    public void setChoosed(Boolean choosed) {
        this.choosed = choosed;
    }

    private Boolean choosed;

    public ClientTag() {
    }


    public ClientTag(JSONObject object) {
        try {
            id = object.getString("id");
            name = object.getString("name");
            color = object.getString("color");
            type = object.getString("type");
            choosed = false;
        } catch (JSONException e) {
            e.printStackTrace();
        }


    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getType() {
        return type;
    }

    @Override
    public String toString() {
        return "ClientTag{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", color='" + color + '\'' +
                ", type='" + type + '\'' +
                ", choosed=" + choosed +
                '}';
    }

    public void setType(String type) {
        this.type = type;
    }
}

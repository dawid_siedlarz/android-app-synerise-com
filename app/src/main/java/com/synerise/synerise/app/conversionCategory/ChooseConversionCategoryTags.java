package com.synerise.synerise.app.conversionCategory;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.synerise.synerise.R;
import com.synerise.synerise.app.adapter.ConversionTagRecyclerAdapter;
import com.synerise.synerise.app.fragment.ConversionFragment;

public class ChooseConversionCategoryTags extends AppCompatActivity {
    private RecyclerView tagsList;
    private ConversionTagRecyclerAdapter adapter;
    private LinearLayoutManager linearLayoutManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_conversion_category_tags);

        tagsList = (RecyclerView)findViewById(R.id.conversion_choose_tag_list);
        adapter = new ConversionTagRecyclerAdapter(this, ConversionFragment.availableTags);
        tagsList.setNestedScrollingEnabled(false);
        linearLayoutManager = new LinearLayoutManager(getApplicationContext());
        tagsList.setLayoutManager(linearLayoutManager);
        tagsList.setAdapter(adapter);

    }
}

package com.synerise.synerise.messenger.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageButton;

import com.synerise.sdk.tracking.Tracker;
import com.synerise.sdk.tracking.TrackerParams;
import com.synerise.synerise.MainActivity;
import com.synerise.synerise.R;
import com.synerise.synerise.app.ActionBarConfigurableFragment;
import com.synerise.synerise.app.adapter.MessengerConversationRecyclerViewAdapter;
import com.synerise.synerise.app.clientPreview.ClientPreview;
import com.synerise.synerise.app.model.Client;
import com.synerise.synerise.messenger.factory.messageBody.MessageFactory;
import com.synerise.synerise.messenger.model.MessengerClient;
import com.synerise.synerise.messenger.model.MessengerMessage;
import com.synerise.synerise.messenger.model.MessengerRoom;
import com.synerise.synerise.messenger.model.MessengerUser;
import com.synerise.synerise.messenger.utils.ClientConverter;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Date;

import io.socket.client.IO;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;

public class ConversationFragment extends ActionBarConfigurableFragment {

    //    private String roomId;
    private MessengerRoom room;
    public static final String ARG_ROOM = "room";

    ArrayList<MessengerMessage> messages = new ArrayList<MessengerMessage>();

    private Socket mSocket;
    private ImageButton sendButton;
    private EditText sendMessageBody;

    String name = "";

    private RecyclerView conversationList;
    private MessengerConversationRecyclerViewAdapter adapter;
    private LinearLayoutManager linearLayoutManager;

    private MessageFactory messageFactory;

    public ConversationFragment() {
        // Required empty public constructor
    }

    public static ConversationFragment newInstance(MessengerRoom param1) {
        ConversationFragment fragment = new ConversationFragment();
        Bundle args = new Bundle();
        args.putParcelable(ARG_ROOM, param1);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            room = (MessengerRoom) getArguments().getParcelable(ARG_ROOM);
        }
        setHasOptionsMenu(true);
    }

    @Override
    public void setUpActionBar() {
        ActionBar actionBar = (ActionBar) ((AppCompatActivity) getActivity()).getSupportActionBar();
        if (actionBar != null) {
            actionBar.show();
            actionBar.setTitle(name);
            actionBar.setSubtitle("");
            actionBar.setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.colorPrimaryMessenger)));
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                Window window = getActivity().getWindow();
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                window.setStatusBarColor(getResources().getColor(R.color.colorPrimaryMessenger));
            }
            getActivity().setTheme(R.style.AppThemeMessenger);
            new GetNameForActionBar().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, room.getOwner());
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_conversation, container, false);
    }

    public void onButtonPressed(Uri uri) {
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initMessengerConversationList();
        Tracker.trackScreen("Messenger converstation - " + room.getOwner().getName(), new TrackerParams[]{});

        messageFactory = new MessageFactory(getContext());

        new GetMessages().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

        try {
            mSocket = IO.socket(MessengerFragment.WEBSOCKET_ENDPOINT);
            mSocket = mSocket.connect();

            mSocket.on(Socket.EVENT_CONNECT, new Emitter.Listener() {
                @Override
                public void call(final Object... args) {


                    JSONObject initData = new JSONObject();
                    try {
                        initData.put("PHPSESSID", MainActivity.httpClient.getPHPSESSID());
                        initData.put("messenger_token", MainActivity.httpClient.getMESSENGER_TOKEN());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    mSocket.emit("initVendorConnection", initData);
                }
            });

            mSocket.on("clientChatMessage", new Emitter.Listener() {
                @Override
                public void call(final Object... args) {
                    Log.v("setUserAvatar", "setBackground");
                    final String message = getArgsMessage(args);

                    Log.v("clientChatMessageMessa", message);

                    Activity activity = getActivity();

                    new GetMessages().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                }
            });

            mSocket.on("vendorChatMessage", new Emitter.Listener() {
                @Override
                public void call(final Object... args) {
                    Log.v("setUserAvatar", "setBackground");
                    final JSONObject obj = (JSONObject) args[0];

                    final String message = getArgsMessage(args);

                    Log.v("vendorChatMessage", message);

                    Activity activity = getActivity();

                    new GetMessages().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                }
            });
            mSocket.on(Socket.EVENT_DISCONNECT, new Emitter.Listener() {
                @Override
                public void call(Object... objects) {
                    JSONObject initData = new JSONObject();
                    try {
                        initData.put("PHPSESSID", MainActivity.httpClient.getPHPSESSID());
                        initData.put("messenger_token", MainActivity.httpClient.getMESSENGER_TOKEN());

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    Log.v("initVendorConnection", initData.toString());

                    mSocket.emit("initVendorConnection", initData);
                }
            });
        } catch (URISyntaxException e) {
            e.printStackTrace();
        } View rootView = getView();
        if (rootView == null) {
            return;
        }
        sendMessageBody = (EditText) rootView.findViewById(R.id.messageSendBody);
        sendButton = (ImageButton) rootView.findViewById(R.id.messageSendButton);
        sendButton.setOnClickListener(sendButtonListener);


        view.setFocusableInTouchMode(true);
        view.requestFocus();
    }

    private void initMessengerConversationList() {
        View rootView = getView();
        if (rootView == null) {
            return;
        }
        conversationList = (RecyclerView) rootView.findViewById(R.id.conversationRecyclerView);
        this.messages.clear();
        adapter = new MessengerConversationRecyclerViewAdapter((MainActivity) getActivity(), this.messages, room);
        conversationList.setNestedScrollingEnabled(false);
        linearLayoutManager = new LinearLayoutManager(getContext());
        conversationList.setLayoutManager(linearLayoutManager);
        conversationList.setAdapter(adapter);
        linearLayoutManager.setStackFromEnd(true);
    }


    private String getArgsMessage(Object[] args) {

        String message = "";
        for (int i = 0; i < args.length; i++) {
            try {
                final JSONObject obj = (JSONObject) args[i];
                message += "arg #" + i + " : " + obj.toString() + "\n";
                continue;
            } catch (ClassCastException e) {
            }
            try {
                final Integer integer = (Integer) args[i];
                message += "arg #" + i + " : " + integer.toString() + "\n";
                continue;
            } catch (ClassCastException e) {
            }
            message += "arg #" + i + " : " + args[i].toString() + "\n";
        }

        return message;
    }

    private View.OnClickListener sendButtonListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            String uuid = String.valueOf(Math.random()).substring(2, 15) + String.valueOf(Math.random()).substring(2, 15);

            String messageText = sendMessageBody.getText().toString();
            JSONObject message = new JSONObject();
            try {
                message.put("uuid", uuid);
                message.put("body", messageText);
                message.put("platformInfo", "Android");
                message.put("time", new Date().getTime());
                message.put("source", "Android");

                JSONObject roomData = new JSONObject();
                roomData.put("roomIndex", "default");
                roomData.put("roomHash", room.getId());
                roomData.put("owner", new JSONObject("{\n" +
                        "                    'id':3400,\n" +
                        "                    'role':'vendor',\n" +
                        "                    'uuid':3400\n" +
                        "                }"));

                message.put("roomData", roomData);
                message.put("roomHash", room.getId());
                message.put("PHPSESSID", MainActivity.httpClient.getPHPSESSID());
                message.put("messenger_token", MainActivity.httpClient.getMESSENGER_TOKEN());

                Log.v("vendorMessage", message.toString());

                Log.v("msocket coneccted", String.valueOf(mSocket.connected()));
                mSocket.emit("vendorChatMessage", message);
                sendMessageBody.setText("");

                Tracker.trackEvent("Send \""+ messageText+"\" to " + room.getOwner().getName(),  new TrackerParams[]{new TrackerParams("message", messageText)});

                new GetMessages().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);


            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    };

    private class GetMessages extends AsyncTask<Void, Void, String> {
        protected String doInBackground(Void... params) {
            HttpGet httpget = new HttpGet(MessengerFragment.API_ENDPOINT + "/conversations?room_id=" + room.getId());

            Log.v("MESSENGER", "Get messages for" + ConversationFragment.this.name);

            HttpResponse eventList = null;
            try {
                eventList = MainActivity.httpClient.execute(httpget);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (eventList == null) {
                return "not OK";
            }

            HttpEntity eventListEntity = eventList.getEntity();
            String body = "";
            try {
                body = EntityUtils.toString(eventListEntity);
            } catch (IOException e) {
                e.printStackTrace();
            }

            JSONArray array = new JSONArray();
            try {
                array = new JSONObject(body).getJSONArray("data");
            } catch (JSONException e) {
                e.printStackTrace();
            }


            ArrayList<MessengerMessage> newMessages = messageFactory.parseMessages(array);


            ConversationFragment.this.messages.clear();
            ConversationFragment.this.messages.addAll(newMessages);

            Activity activity = getActivity();

            if (activity != null) {
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        adapter.notifyDataSetChanged();
                        conversationList.smoothScrollToPosition(conversationList.getAdapter().getItemCount());
                    }
                });
            }
            return "ok";

        }
    }

    @Override
    public void onStop() {
        super.onStop();

        System.out.println("onStop");
        mSocket.close();
    }

    private class GetNameForActionBar extends AsyncTask<MessengerUser, Void, Void> {
        @Override
        protected Void doInBackground(MessengerUser... params) {
            final MessengerUser user = params[0];
            if(ConversationFragment.this.name.isEmpty()) {
                ConversationFragment.this.name = user.getNameFromApi();
            }
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    ActionBar actionBar = (ActionBar) ((AppCompatActivity) getActivity()).getSupportActionBar();
                    if (actionBar!= null) {
                        actionBar.setTitle(name);
                    }
                }
            });

            return null;
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);

        if (menu != null) {
            menu.findItem(R.id.action_go_to_profile).setVisible(true);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_go_to_profile) {
            ClientConverter clientConverter = new ClientConverter();
            Client clientModel = clientConverter.fromMessengerToBasic((MessengerClient) room.getOwner());

            Activity activity =  getActivity();
            if (activity != null) {
                Intent intent = new Intent(activity, ClientPreview.class);
                intent.putExtra("client", clientModel);
                activity.startActivity(intent);
            }

        }
        return super.onOptionsItemSelected(item);
    }


}

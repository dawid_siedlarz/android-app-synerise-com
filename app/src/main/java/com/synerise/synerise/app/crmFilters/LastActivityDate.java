package com.synerise.synerise.app.crmFilters;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.synerise.synerise.R;
import com.synerise.synerise.app.fragment.CRMFragment;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class LastActivityDate extends AppCompatActivity {

    private Calendar absoluteDate;
    private DatePickerDialog absoluteDatePicker;
    private SimpleDateFormat format;

    private RadioButton relativeDateMoreThan;
    private RadioButton relativeDateExactly;
    private RadioButton relativeDateLessThan;
    private RadioButton absoluteDateAfter;
    private RadioButton absoluteDateOn;
    private RadioButton absoluteDateBefore;
    private TextView absoluteDateValue;
    private EditText relativeDateValue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_last_activity_date);
        absoluteDate = Calendar.getInstance();
        format = new SimpleDateFormat("yyyy.MM.dd");
        relativeDateMoreThan = (RadioButton) findViewById(R.id.relativeDateMoreThan);
        relativeDateExactly = (RadioButton) findViewById(R.id.relativeDateExactly);
        relativeDateLessThan = (RadioButton) findViewById(R.id.relativeDateLessThan);
        absoluteDateAfter = (RadioButton) findViewById(R.id.absoluteDateAfter);
        absoluteDateOn = (RadioButton) findViewById(R.id.absoluteDateOn);
        absoluteDateBefore = (RadioButton) findViewById(R.id.absoluteDateBefore);
        absoluteDateValue = (TextView) findViewById(R.id.absoluteDateValue);
        relativeDateValue = (EditText) findViewById(R.id.relativeDateValue);
        if (getActionBar() != null) {
            getActionBar().setTitle("DATE OF LAST TRANSACTION");
        }

        absoluteDatePicker = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                absoluteDate.set(year, monthOfYear, dayOfMonth);
                absoluteDateValue.setText(format.format(absoluteDate.getTime()));
            }

        }, absoluteDate.get(Calendar.YEAR), absoluteDate.get(Calendar.MONTH), absoluteDate.get(Calendar.DAY_OF_MONTH));

        absoluteDateValue.setText(format.format(absoluteDate.getTime()));
        absoluteDateValue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                absoluteDatePicker.show();
            }
        });


    }

    public void relativeDateAction(View v) {
        String relativeValue = relativeDateValue.getText().toString();
        if (relativeValue == null && relativeValue.isEmpty()) {
            Toast.makeText(LastActivityDate.this, "Please input relative date value", Toast.LENGTH_SHORT).show();
            return;
        }
        String dateOption = null;
        if (relativeDateMoreThan.isChecked()) {
            dateOption = "more_than";
        }
        if (relativeDateExactly.isChecked()) {
            dateOption = "exactly";
        }
        if (relativeDateLessThan.isChecked()) {
            dateOption = "less_than";
        }
        if (dateOption == null) {
            Toast.makeText(LastActivityDate.this, "Please input relative date option", Toast.LENGTH_SHORT).show();
            return;
        }
        CRMFragment.listing_activity_option = dateOption;
        CRMFragment.listing_activity_relativeValue = relativeValue;

        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.mainContainer);
        ((CRMFragment) fragment).getClients();

        finish();
    }

    public void absoluteDateAction(View v) {
        String relativeValue = absoluteDateValue.getText().toString();
        if (relativeValue == null && relativeValue.isEmpty()) {
            Toast.makeText(LastActivityDate.this, "Please input relative date value", Toast.LENGTH_SHORT).show();
            return;
        }
        String dateOption = null;
        if (absoluteDateAfter.isChecked()) {
            dateOption = "after";
        }
        if (absoluteDateBefore.isChecked()) {
            dateOption = "before";
        }
        if (absoluteDateOn.isChecked()) {
            dateOption = "on";
        }
        if (dateOption == null) {
            Toast.makeText(LastActivityDate.this, "Please input relative date option", Toast.LENGTH_SHORT).show();
            return;
        }

        CRMFragment.listing_activity_option = dateOption;
        CRMFragment.listing_activity_absoluteValue = relativeValue;

        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.mainContainer);
        ((CRMFragment) fragment).getClients();

        finish();
    }
}

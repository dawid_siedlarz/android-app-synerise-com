package com.synerise.synerise.app.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.synerise.synerise.R;
import com.synerise.synerise.app.model.Segment;
import com.synerise.synerise.app.model.SegmentSelected;

import java.util.ArrayList;

/**
 * Created by dsiedlarz on 15.10.2016.
 */
public class SimpleSelectedSegmentsArrayAdapter extends ArrayAdapter<Segment.SegmentModel> {
    private final Context context;
    private final ArrayList<Segment.SegmentModel> values;
    private TextView mAvatar;
    private Segment.SegmentModel segmentModel;



    public SimpleSelectedSegmentsArrayAdapter(Context context, ArrayList<Segment.SegmentModel> values) {
        super(context, -1, values);
        this.context = context;
        this.values = values;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        segmentModel = SegmentSelected.ITEMS.get(position);
        View rowView = inflater.inflate(R.layout.list_element_tag_selected, parent, false);
        TextView mTagName = (TextView) rowView.findViewById(R.id.tagName);

        mTagName.setText(segmentModel.name);
        return rowView;
    }

}

package com.synerise.synerise.app.model;

import java.util.ArrayList;

public class Segment {

    public static final ArrayList<SegmentModel> ITEMS = new ArrayList<SegmentModel>();

    public static void add(String name, String count) {
        ITEMS.add(new SegmentModel(name, count));
    }

    public static void clear() {
        ITEMS.clear();

    }

    public static class SegmentModel {
        public String name;
        public String count;

        public SegmentModel(String name, String count) {
                this.name = name;
                this.count = count;
        }
    }

}

package com.synerise.synerise.messenger.model.messageBody;

import android.content.Context;

public class TextTemplateBody extends TextBody {

    public TextTemplateBody(Context context, String text, String type) {
        super(context, text, type);
    }
}
